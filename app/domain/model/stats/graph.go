package stats

import (
	"clusctor/app/pkg/alg/diameter"
	"clusctor/app/pkg/alg/dijkstra"
	"clusctor/app/pkg/alg/graph"
	"clusctor/app/pkg/std/slice"
)

type Graph struct {
	N int     `json:"n"`
	M int     `json:"m"`
	S float64 `json:"s"`

	D    int `json:"d"`
	DMin int `json:"dmin"`
	DMax int `json:"dmax"`

	NMin  int     `json:"nmin"`
	NMax  int     `json:"nmax"`
	NMean float64 `json:"nMean"`

	MMin  int     `json:"mmin"`
	MMax  int     `json:"mmax"`
	MMean float64 `json:"mMean"`

	MOut    float64 `json:"mout"`
	MOutMin int     `json:"moutmin"`
	MOutMax int     `json:"moutmax"`

	QMin  float64 `json:"qmin"`
	QMax  float64 `json:"qmax"`
	QMean float64 `json:"qMean"`

	ShortestPathsQuantity               int     `json:"shortestPathsQuantity"`
	LocalShortestPathsQuantity          int     `json:"localShortestPathsQuantity"`
	IncorrectLocalShortestPathsQuantity float64 `json:"incorrectLocalShortestPathsQuantity"`
	GlobalShortestPathsQuantity         int     `json:"globalShortestPathsQuantity"`
	LocalShortestPathsInRercents        float64 `json:"localShortestPathsPercentage"`

	MeanAbsoluteLocalPathError float64 `json:"meanAbsoluteLocalPathError"`
	LocalPathErrorInPercents   float64 `json:"localPathErrorInPercents"`
	MinAbsoluteLocalPathError  float64 `json:"minAbsoluteLocalPathError"`
	MaxAbsoluteLocalPathError  float64 `json:"maxAbsoluteLocalPathError"`

	MeanVertexDegree float64 `json:"meanVertexDegree"`
}

func NewGraph(mainGraph *graph.Graph) Graph {
	g := Graph{}

	subGraphs := mainGraph.SubGraphs

	g.N = len(mainGraph.V)
	g.M = len(mainGraph.E)
	g.S = float64(len(subGraphs))
	g.D = diameter.Find(mainGraph)

	g.DMin, g.DMax = subGraphs.CalculateDMinAndDMax(diameter.Find)

	g.NMin, g.NMax = subGraphs.CalculateNMinAndNMax()
	g.NMean = subGraphs.CalculateNMean()

	g.MMin, g.MMax = subGraphs.CalculateMMinAndMMax()
	g.MMean = subGraphs.CalculateMMean()

	g.MOut = float64(subGraphs.CalculateMOut())
	g.MOutMin, g.MOutMax = subGraphs.CalculateMOutMinAndMOutMax()

	g.QMin, g.QMax = subGraphs.CalculateQMinAndQMax()
	g.QMean = subGraphs.CalculateQMean()

	g.MeanVertexDegree = mainGraph.CalculateMeanVertexDegree()

	var absoluteLocalPathErrors []float64
	g.ShortestPathsQuantity = len(mainGraph.V) * (len(mainGraph.V) - 1)

	if len(subGraphs) == 0 {
		return g
	}

	if len(mainGraph.V) <= 100 {
		for _, subGraph := range subGraphs {
			for v1 := range subGraph.V {
				for v2 := range subGraph.V {
					if v1 != v2 {
						localPath := dijkstra.FindShortestPath(v1, v2, dijkstra.UseOriginalWeight, subGraph)
						globalPath := dijkstra.FindShortestPath(v1, v2, dijkstra.UseOriginalWeight, mainGraph)

						g.LocalShortestPathsQuantity++
						if !localPath.Equal(globalPath) {
							g.IncorrectLocalShortestPathsQuantity++

							localPathLength := graph.CreateEdgeCollection(localPath).SumWeight()
							globalPathLength := graph.CreateEdgeCollection(globalPath).SumWeight()
							absoluteLocalPathError := localPathLength - globalPathLength
							absoluteLocalPathErrors = append(absoluteLocalPathErrors, absoluteLocalPathError)
						}
					}
				}
			}
		}
	}

	if g.LocalShortestPathsQuantity > 0 {
		g.LocalPathErrorInPercents =
			g.IncorrectLocalShortestPathsQuantity / float64(g.LocalShortestPathsQuantity) * 100.0
	}
	g.GlobalShortestPathsQuantity =
		g.ShortestPathsQuantity - g.LocalShortestPathsQuantity
	g.MaxAbsoluteLocalPathError = slice.Max(absoluteLocalPathErrors)
	g.MinAbsoluteLocalPathError = slice.Min(absoluteLocalPathErrors)
	g.MeanAbsoluteLocalPathError = slice.Average(absoluteLocalPathErrors)
	if g.ShortestPathsQuantity > 0 {
		g.LocalShortestPathsInRercents =
			float64(g.LocalShortestPathsQuantity) /
				float64(g.ShortestPathsQuantity) * 100.0
	}

	return g
}
