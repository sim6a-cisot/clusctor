package stats

import (
	"clusctor/app/pkg/alg/graph"
	stdTime "clusctor/app/pkg/std/time"
	"time"
)


type Segmentation struct {
	PredefinedQ float64 `json:"predefinedQ"`
	Graph
	measurable
}

func NewSegmentation(g *graph.Graph, predefinedQ float64, t time.Duration) Segmentation {
	s := Segmentation{
		PredefinedQ: predefinedQ,
		Graph:       NewGraph(g),
	}

	s.Time = float64(stdTime.InMs(t))
	return s
}
