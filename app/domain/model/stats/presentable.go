package stats

type Presentable interface {
	ToJSON() []byte
	ToCSV() []byte
}
