package stats

import (
	"clusctor/app/domain/model/management/slice"
	"clusctor/app/domain/service/management"
	stdTime "clusctor/app/pkg/std/time"
	"time"
)

type Slice struct {
	measurable
	N    int   `json:"n"`
	H    int   `json:"h"`
	M    int   `json:"m"`
	HMin int   `json:"hmin"`
	HMax int   `json:"hmax"`
	NMin int   `json:"nmin"`
	NMax int   `json:"nmax"`
	MMin int   `json:"mmin"`
	MMax int   `json:"mmax"`
}

func NewSlice(slices slice.Collection, devices management.Devices, t time.Duration) Slice {
	entry := Slice{}

	entry.MMin, entry.MMax = slices.CalculateMMinAndMMax()
	entry.HMin, entry.HMax = slices.CalculateHMinAndHMax()
	entry.NMin, entry.NMax = slices.CalculateNMinAndNMax()
	entry.N = len(devices.Switches())
	entry.H = len(devices.Hosts())
	entry.M = len(devices.Links())
	entry.Time = float64(stdTime.InMs(t))

	return entry
}
