package stats

import (
	"clusctor/app/pkg/alg/graph"
	stdTime "clusctor/app/pkg/std/time"
	"time"
)

type GirvanNewman struct {
	Graph
	measurable
}

func NewGirvanNewman(g *graph.Graph, t time.Duration) GirvanNewman {
	gn := GirvanNewman{
		Graph: NewGraph(g),
	}
	gn.Time = float64(stdTime.InMs(t))
	return gn
}
