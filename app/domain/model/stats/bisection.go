package stats

import (
	"clusctor/app/pkg/alg/graph"
	stdTime "clusctor/app/pkg/std/time"
	"time"
)

type Bisection struct {
	PredefinedS int `json:"predefinedS"`
	Graph
	measurable
}

func NewBisection(g *graph.Graph, predefinedS int, t time.Duration) Bisection {
	b := Bisection{
		PredefinedS: predefinedS,
		Graph:       NewGraph(g),
	}
	b.Time = float64(stdTime.InMs(t))
	return b
}
