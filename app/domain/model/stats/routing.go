package stats

import (
	"clusctor/app/pkg/alg/dijkstra"
	"clusctor/app/pkg/alg/graph"
	"clusctor/app/pkg/std/slice"
)

type Routing struct {
	ShortestPathsQuantity               int     `json:"shortestPathsQuantity"`
	LocalShortestPathsQuantity          int     `json:"localShortestPathsQuantity"`
	IncorrectLocalShortestPathsQuantity int     `json:"incorrectLocalShortestPathsQuantity"`
	GlobalShortestPathsQuantity         int     `json:"globalShortestPathsQuantity"`
	LocalShortestPathsPercentage        float64 `json:"localShortestPathsPercentage"`

	MeanAbsoluteLocalPathError float64 `json:"meanAbsoluteLocalPathError"`
	LocalPathErrorInPercents   float64 `json:"localPathErrorInPercents"`
	MinAbsoluteLocalPathError  float64 `json:"minAbsoluteLocalPathError"`
	MaxAbsoluteLocalPathError  float64 `json:"maxAbsoluteLocalPathError"`

	MeanVertexDegree float64 `json:"meanVertexDegree"`
	Segments         int     `json:"segments"`
}

func NewRouting(G *graph.Graph) Routing {
	if len(G.SubGraphs) == 0 {
		return Routing{}
	}

	routingStats := Routing{}

	var absoluteLocalPathErrors []float64
	routingStats.ShortestPathsQuantity = len(G.V) * (len(G.V) - 1)

	for _, subGraph := range G.SubGraphs {
		for v1 := range subGraph.V {
			for v2 := range subGraph.V {
				if v1 != v2 {
					localPath := dijkstra.FindShortestPath(v1, v2, dijkstra.UseOriginalWeight, subGraph)
					globalPath := dijkstra.FindShortestPath(v1, v2, dijkstra.UseOriginalWeight, G)

					routingStats.LocalShortestPathsQuantity++
					if !localPath.Equal(globalPath) {
						routingStats.IncorrectLocalShortestPathsQuantity++

						localPathLength := graph.CreateEdgeCollection(localPath).SumWeight()
						globalPathLength := graph.CreateEdgeCollection(globalPath).SumWeight()
						absoluteLocalPathError := localPathLength - globalPathLength
						absoluteLocalPathErrors = append(absoluteLocalPathErrors, absoluteLocalPathError)
					}
				}
			}
		}
	}
	if routingStats.LocalShortestPathsQuantity > 0 {
		routingStats.LocalPathErrorInPercents =
			float64(routingStats.IncorrectLocalShortestPathsQuantity) /
				float64(routingStats.LocalShortestPathsQuantity) * 100.0
	}
	routingStats.GlobalShortestPathsQuantity =
		routingStats.ShortestPathsQuantity - routingStats.LocalShortestPathsQuantity
	routingStats.MaxAbsoluteLocalPathError = slice.Max(absoluteLocalPathErrors)
	routingStats.MinAbsoluteLocalPathError = slice.Min(absoluteLocalPathErrors)
	routingStats.MeanAbsoluteLocalPathError = slice.Average(absoluteLocalPathErrors)
	if routingStats.ShortestPathsQuantity > 0 {
		routingStats.LocalShortestPathsPercentage =
			float64(routingStats.LocalShortestPathsQuantity) /
				float64(routingStats.ShortestPathsQuantity) * 100.0
	}
	routingStats.MeanVertexDegree = G.CalculateMeanVertexDegree()
	routingStats.Segments = len(G.SubGraphs)

	return routingStats
}
