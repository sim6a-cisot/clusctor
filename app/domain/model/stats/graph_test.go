package stats_test

import (
	"clusctor/app/domain/model/stats"
	"clusctor/app/pkg/alg/graph"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_newGraph(t *testing.T) {
	// see test/alg/statistics/statistics_test_graph.svg
	G := graph.New()

	// main graph
	e12 := G.AddEdge("v1", "v2", 1)
	e13 := G.AddEdge("v1", "v3", 1)
	e23 := G.AddEdge("v2", "v3", 1)

	e24 := G.AddEdge("v2", "v4", 1)
	e35 := G.AddEdge("v3", "v5", 1)

	e45 := G.AddEdge("v4", "v5", 1)

	// s1
	s1 := graph.New()
	s1.E.Add(e12)
	s1.E.Add(e13)
	s1.E.Add(e23)
	s1.EA.Add(e24)
	s1.EA.Add(e35)
	s1.V.Add(e12.V1)
	s1.V.Add(e12.V2)
	s1.V.Add(e13.V2)

	// s2
	s2 := graph.New()
	s2.E.Add(e45)
	s2.EA.Add(e24)
	s2.EA.Add(e35)
	s2.V.Add(e45.V1)
	s2.V.Add(e45.V2)

	G.SubGraphs = []*graph.Graph{s1, s2}
	entry := stats.NewGraph(G)

	expectedEntry := stats.Graph{
		N:       5,
		M:       6,
		S:       2,
		D:       2,
		DMin:    1,
		DMax:    1,
		NMin:    2,
		NMax:    3,
		NMo:     2,
		NMe:     2.5,
		NRange:  1,
		NMean:   2.5,
		NStdDv:  0.5,
		NVarC:   0.2,
		MMin:    1,
		MMax:    3,
		MMo:     1,
		MMe:     2,
		MRange:  2,
		MMean:   2,
		MStdDv:  1,
		MVarC:   0.5,
		MOut:    2,
		MOutMin: 2,
		MOutMax: 2,
		QMin:    0.5,
		QMax:    1.5,
		QMo:     0.5,
		QMe:     1,
		QRange:  1,
		QMean:   1,
		QStdDv:  0.5,
		QVarC:   0.5,
	}

	assert.Equal(t, expectedEntry, entry)
}
