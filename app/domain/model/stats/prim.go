package stats

import (
	"clusctor/app/pkg/alg/diameter"
	"clusctor/app/pkg/alg/graph"
)

type Prim struct {
	N       int     `json:"n"`
	M       int     `json:"m"`
	D       int     `json:"d"`
	MinCost float64 `json:"minCost"`
}

func NewPrim(g *graph.Graph, minCost float64) Prim {
	return Prim{
		N:       len(g.V),
		M:       len(g.E),
		D:       diameter.Find(g),
		MinCost: minCost,
	}
}
