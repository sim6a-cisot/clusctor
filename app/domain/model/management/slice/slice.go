package slice

import (
	"clusctor/app/domain/model/management/mac"
	"clusctor/app/domain/model/management/metric"
	"clusctor/app/domain/model/management/topology"
	"clusctor/app/domain/model/management/traffic"
)

type Description struct {
	TrafficType  traffic.Type
	Hosts        []mac.MAC
	FlowSettings FlowSettings
}

type FlowSettings struct {
	HardTimeout int
	IdleTimeout int
	Priority    int
}

func (d *Description) MetricType() metric.Type {
	return trafficTypeToMetricType[d.TrafficType]
}

var trafficTypeToMetricType = map[traffic.Type]metric.Type{
	traffic.FileExchange: metric.LARACCost,
	traffic.VideoStream:  metric.LARAC,
	traffic.Email:        metric.PacketLossRate,
	traffic.VoIP:         metric.Delay,
	traffic.ECommerce:    metric.Delay,
}

var trafficTypeToSegmentID = map[traffic.Type]int{
	traffic.FileExchange: 1,
	traffic.VideoStream:  2,
	traffic.Email:        3,
	traffic.VoIP:         4,
	traffic.ECommerce:    5,
}

func NewEmptySlice(trafficType traffic.Type) *Slice {
	return &Slice{
		trafficType: trafficType,
		segmentID:   trafficTypeToSegmentID[trafficType],
		hosts:       topology.NewHostSet(),
		switches:    topology.NewSwitchSet(),
		links:       topology.NewLinkSet(),
	}
}

type Slice struct {
	trafficType traffic.Type
	segmentID   int
	hosts       *topology.HostSet
	switches    *topology.SwitchSet
	links       *topology.LinkSet
}

func (s *Slice) SegmentID() int {
	return s.segmentID
}

func (s *Slice) TrafficType() traffic.Type {
	return s.trafficType
}

func (s *Slice) Links() *topology.LinkSet {
	return s.links
}

func (s *Slice) Switches() *topology.SwitchSet {
	return s.switches
}

func (s *Slice) Hosts() *topology.HostSet {
	return s.hosts
}
