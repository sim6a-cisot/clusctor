package slice

import "math"

type Collection []*Slice

func (c Collection) CalculateMMinAndMMax() (mMin, mMax int) {
	mMin = math.MaxInt32

	var m int
	for _, s := range c {
		m = s.Links().Size()
		if m > mMax {
			mMax = m
		}
		if m < mMin {
			mMin = m
		}
	}

	return
}

func (c Collection) CalculateNMinAndNMax() (nMin, nMax int) {
	nMin = math.MaxInt32

	var n int
	for _, s := range c {
		n = s.Switches().Size()
		if n > nMax {
			nMax = n
		}
		if n < nMin {
			nMin = n
		}
	}

	return
}

func (c Collection) CalculateHMinAndHMax() (hMin, hMax int) {
	hMin = math.MaxInt32

	var h int
	for _, s := range c {
		h = s.Hosts().Size()
		if h > hMax {
			hMax = h
		}
		if h < hMin {
			hMin = h
		}
	}

	return
}
