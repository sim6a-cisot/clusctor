package metric

func NewMetrics(bandwidth, delay, packetLossRate, adminMetric float64) Metrics {
	return Metrics{
		bandwidth:      bandwidth,
		delay:          delay,
		packetLossRate: packetLossRate,
		adminMetric:    adminMetric,
	}
}

type Metrics struct {
	bandwidth      float64
	delay          float64
	packetLossRate float64
	adminMetric    float64
}

func (m *Metrics) SetBandwidth(bandwidth float64) {
	m.bandwidth = bandwidth
}

func (m Metrics) AdminMetric() float64 {
	return m.adminMetric
}

func (m Metrics) PacketLossRate() float64 {
	return m.packetLossRate
}

func (m Metrics) Delay() float64 {
	return m.delay
}

func (m Metrics) Bandwidth() float64 {
	return m.bandwidth
}

func (m Metrics) LARAC(lambda float64) float64 {
	return 100/m.bandwidth + lambda*m.delay
}

func (m *Metrics) LARACCost(gamma float64) float64 {
	return (1-gamma)*m.delay + gamma*m.packetLossRate
}

func (m *Metrics) Get(info Info) float64 {
	value := float64(1)

	switch info.currentMetric {
	case Bandwidth:
		value = m.bandwidth
	case Delay:
		value = m.delay
	case PacketLossRate:
		value = m.packetLossRate
	case Admin:
		value = m.adminMetric
	case LARAC:
		value = m.LARAC(info.laracLambda)
	case LARACCost:
		value = m.LARACCost(info.laracCostGamma)
	}

	return value
}
