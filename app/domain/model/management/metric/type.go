package metric

type Type string

func (m Type) String() string {
	return string(m)
}

const (
	Bandwidth      Type = "bandwidth"
	Delay          Type = "delay"
	PacketLossRate Type = "packetLossRate"
	Admin          Type = "adminMetric"
	LARAC          Type = "larac"
	LARACCost      Type = "laracCost"
	QoSParams      Type = "qosParams"
)
