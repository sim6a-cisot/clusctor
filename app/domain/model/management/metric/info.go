package metric

type Info struct {
	currentMetric    Type
	laracLambda      float64
	laracCostGamma   float64
	useCustomMetrics bool
}

func NewInfo() *Info {
	return &Info{
		currentMetric:    Delay,
		laracLambda:      0.5,
		laracCostGamma:   0.5,
		useCustomMetrics: true,
	}
}

func (i Info) LARACCostGamma() float64 {
	return i.laracCostGamma
}

func (i Info) LARACLambda() float64 {
	return i.laracLambda
}

func (i Info) CurrentMetric() Type {
	return i.currentMetric
}

func (i *Info) SetCurrentMetric(currentMetric Type) {
	i.currentMetric = currentMetric
}

func (i *Info) SetUseCustomMetrics(useCustomMetrics bool) {
	i.useCustomMetrics = useCustomMetrics
}

func (i Info) UseCustomMetrics() bool {
	return i.useCustomMetrics
}
