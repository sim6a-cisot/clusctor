package class

type Collection []Class

func NewCollection() Collection {
	return make([]Class, 0)
}

func (c Collection) Strings() []string {
	str := make([]string, 0, len(c))
	for _, cl := range c {
		str = append(str, cl.String())
	}
	return str
}

func (c *Collection) Add(class Class) {
	*c = append(*c, class)
}

func (c *Collection) Clear() {
	*c = make([]Class, 0)
}

