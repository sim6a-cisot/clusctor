package class

type Class string

func (c Class) String() string {
	return string(c)
}

const (
	InTree  Class = "inTree"
	InRoute Class = "inRoute"
	Invisible Class = "invisible"
)
