package topology

import (
	"clusctor/app/domain/model/management/class"
	"clusctor/app/domain/model/management/mac"
)

func NewHost(mac mac.MAC, ipv4, adjDPID string, adjPort int) Host {
	return Host{
		mac:     mac,
		ipv4:    ipv4,
		adjDPID: adjDPID,
		adjPort: adjPort,

		classes: class.NewCollection(),
	}
}

type Host struct {
	id   int
	ipv4 string
	mac  mac.MAC

	adjDPID string
	adjPort int

	classes class.Collection
}

func (h Host) ID() int {
	return h.id
}

func (h *Host) SetID(id int) {
	h.id = id
}

func (h *Host) AddClass(c class.Class) {
	h.classes.Add(c)
}

func (h *Host) Classes() *class.Collection {
	return &h.classes
}

func (h Host) AdjacentPort() int {
	return h.adjPort
}

func (h Host) AdjacentDPID() string {
	return h.adjDPID
}

func (h Host) IPv4() string {
	return h.ipv4
}

func (h Host) MAC() mac.MAC {
	return h.mac
}

// SET

type HostSet struct {
	slice   []*Host
	mapMAC map[mac.MAC]*Host
}

func NewHostSet() *HostSet {
	return &HostSet{
		slice:   make([]*Host, 0, 0),
		mapMAC: make(map[mac.MAC]*Host),
	}
}

func (s *HostSet) Add(host Host) {
	if s.Contains(host) {
		return
	}
	s.slice = append(s.slice, &host)
	s.mapMAC[host.MAC()] = &host
}

func (s HostSet) Contains(host Host) bool {
	_, ok := s.mapMAC[host.MAC()]
	return ok
}

func (s HostSet) Get(mac mac.MAC) *Host {
	h, _ := s.mapMAC[mac]
	return h
}

func (s HostSet) FindMAC(ip string) mac.MAC {
	for _, host := range s.slice {
		if host.IPv4() == ip {
			return host.MAC()
		}
	}
	return ""
}

func (s HostSet) AsSliceCopy() []Host {
	sliceCopy := make([]Host, 0, len(s.slice))
	for _, host := range s.slice {
		sliceCopy = append(sliceCopy, *host)
	}
	return sliceCopy
}

func (s HostSet) AsSlice() []*Host {
	return s.slice
}

func (s HostSet) Size() int {
	return len(s.slice)
}
