package topology_test

import (
	"clusctor/app/domain/model/management/metric"
	"clusctor/app/domain/model/management/topology"
	"github.com/stretchr/testify/assert"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestTopology_AddOrUpdateLink(t *testing.T) {
	Convey("We create two bidirectional links based on the single link.", t, func() {
		sourceDPID := "00:00:00:00:00:00:00:01"
		destinationDPID := "00:00:00:00:00:00:00:02"
		directLink := topology.NewLink(1, 2, sourceDPID, destinationDPID)
		inverseLink := topology.NewLink(2, 1, destinationDPID, sourceDPID)

		Convey("We create a topology and add to it two links.", func() {
			topo := topology.New(metric.NewInfo())
			topo.AddOrUpdateLink(directLink)
			topo.AddOrUpdateLink(inverseLink)

			Convey("In fact the topology must contains only one link.", func() {
				So(len(topo.Links()), ShouldEqual, 1)
			})
		})
	})
}

func TestTopology_Update_AddOneLink(t *testing.T) {
	var switches []topology.Switch
	var hosts []topology.Host
	links := []topology.Link{
		topology.NewLink(0, 0,
			"00:00:00:00:00:00:00:01",
			"00:00:00:00:00:00:00:02"),
		topology.NewLink(0, 0,
			"00:00:00:00:00:00:00:02",
			"00:00:00:00:00:00:00:03"),
	}

	topo := topology.New(metric.NewInfo())
	topo.Update(switches, hosts, links)

	assert.Len(t, topo.Links(), 2)

	// add one link
	links = append(links, topology.NewLink(0, 0,
		"00:00:00:00:00:00:00:01",
		"00:00:00:00:00:00:00:03"))

	topo.Update(switches, hosts, links)

	assert.Len(t, topo.Links(), 3)
}

func TestTopology_Update_LinkModification(t *testing.T) {
	var switches []topology.Switch
	var hosts []topology.Host
	links := []topology.Link{
		topology.NewLink(0, 0,
			"00:00:00:00:00:00:00:01",
			"00:00:00:00:00:00:00:02"),
		topology.NewLink(0, 0,
			"00:00:00:00:00:00:00:02",
			"00:00:00:00:00:00:00:03"),
	}

	topo := topology.New(metric.NewInfo())
	topo.Update(switches, hosts, links)

	assert.Len(t, topo.Links(), 2)

	// modify link
	links[0] = topology.NewLink(0, 0,
		"00:00:00:00:00:00:00:01",
		"00:00:00:00:00:00:00:03")

	topo.Update(switches, hosts, links)

	assert.Len(t, topo.Links(), 2)
}