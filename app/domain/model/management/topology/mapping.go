package topology

import (
	"clusctor/app/domain/model/management/mac"
)

type MapSwitch func(string) Switch

type TranslateIPToMAC func(string) mac.MAC

type ConnectToSwitch func(string) *Switch

type ConnectToHost func(mac.MAC) *Host

type ConnectToLink func (string, string) *Link