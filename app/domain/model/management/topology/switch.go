package topology

import "clusctor/app/domain/model/management/class"

func NewSwitch(dpid string) Switch {
	return Switch{
		dpid: dpid,

		classes: class.NewCollection(),
	}
}

type Switch struct {
	dpid      string
	id        int
	segmentID int

	classes class.Collection
}

func (s *Switch) Classes() *class.Collection {
	return &s.classes
}

func (s *Switch) SetSegmentID(segmentID int) {
	s.segmentID = segmentID
}

func (s Switch) SegmentID() int {
	return s.segmentID
}

func (s *Switch) SetID(id int) {
	s.id = id
}

func (s Switch) ID() int {
	return s.id
}

func (s Switch) DPID() string {
	return s.dpid
}

// SET

type SwitchSet struct {
	slice   []*Switch
	mapDPID map[string]*Switch
}

func NewSwitchSet() *SwitchSet {
	return &SwitchSet{
		slice:   make([]*Switch, 0, 0),
		mapDPID: make(map[string]*Switch),
	}
}

func (s *SwitchSet) Add(sw Switch) {
	if s.Contains(sw) {
		return
	}
	s.slice = append(s.slice, &sw)
	s.mapDPID[sw.DPID()] = &sw
}

func (s SwitchSet) Contains(sw Switch) bool {
	_, ok := s.mapDPID[sw.DPID()]
	return ok
}

func (s SwitchSet) Get(dpid string) *Switch {
	sw, _ := s.mapDPID[dpid]
	return sw
}

func (s SwitchSet) AsSliceCopy() []Switch {
	sliceCopy := make([]Switch, 0, len(s.slice))
	for _, sw := range s.slice {
		sliceCopy = append(sliceCopy, *sw)
	}
	return sliceCopy
}

func (s SwitchSet) AsSlice() []*Switch {
	return s.slice
}

func (s SwitchSet) Size() int {
	return len(s.slice)
}