package topology_test

import (
	"clusctor/app/domain/model/management/topology"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestLinkSet_Contains(t *testing.T) {
	linkSet := topology.NewLinkSet()
	linkSet.Add(topology.NewLink(
		0,
		0,
		"00:00:00:00:00:00:00:01",
		"00:00:00:00:00:00:00:02"),
	)

	straightLink := topology.NewLink(
		0,
		0,
		"00:00:00:00:00:00:00:01",
		"00:00:00:00:00:00:00:02")

	backLink := topology.NewLink(
		0,
		0,
		"00:00:00:00:00:00:00:01",
		"00:00:00:00:00:00:00:02")

	assert.True(t, linkSet.Contains(straightLink))
	assert.True(t, linkSet.Contains(backLink))
}
