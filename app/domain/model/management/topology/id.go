package topology

type IDSequence struct {
	id int
}

func (s *IDSequence) Next() int {
	s.id++
	return s.id
}

func (s *IDSequence) Reset() {
	s.id = 0
}
