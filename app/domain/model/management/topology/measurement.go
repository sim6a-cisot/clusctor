package topology

import "time"

type LinkMeasurementData struct {
	BytesTransferred int
	Date             time.Time
}
