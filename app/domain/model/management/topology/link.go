package topology

import (
	"clusctor/app/domain/model/management/class"
	"clusctor/app/domain/model/management/metric"
	"fmt"
)

func NewLink(sourcePort, destinationPort int, sourceDPID, destinationDPID string) Link {
	return Link{
		sourceDPID:      sourceDPID,
		sourcePort:      sourcePort,
		destinationDPID: destinationDPID,
		destinationPort: destinationPort,
		metrics:         metric.NewMetrics(1, 1, 0, 1),

		classes: class.NewCollection(),
	}
}

type Link struct {
	sourceDPID string
	sourcePort int

	metrics         metric.Metrics
	measurementData LinkMeasurementData

	destinationDPID string
	destinationPort int

	classes class.Collection
}

func (ln Link) String() string {
	return fmt.Sprintf("%s - %s", ln.sourceDPID, ln.destinationDPID)
}

func (ln Link) MeasurementData() LinkMeasurementData {
	return ln.measurementData
}

func (ln *Link) SetMeasurementData(measurementData LinkMeasurementData) {
	ln.measurementData = measurementData
}

func (ln *Link) CalculateBandwidth(data LinkMeasurementData) {
	bytesTransferredDelta := data.BytesTransferred - ln.MeasurementData().BytesTransferred
	measurementPeriod := data.Date.Sub(ln.MeasurementData().Date).Seconds()
	var bandwidth float64
	if measurementPeriod > 0 {
		bandwidth = float64(bytesTransferredDelta) / measurementPeriod
	}
	ln.metrics.SetBandwidth(bandwidth)
	ln.SetMeasurementData(data)
}

func (ln *Link) Classes() *class.Collection {
	return &ln.classes
}

func (ln *Link) OutPort(dpid string) int {
	if ln.SourceDPID() == dpid {
		return ln.sourcePort
	} else if ln.DestinationDPID() == dpid {
		return ln.destinationPort
	}
	return 0
}

func (ln *Link) DestinationDPID() string {
	return ln.destinationDPID
}

func (ln *Link) SourceDPID() string {
	return ln.sourceDPID
}

func (ln *Link) SetMetrics(metrics metric.Metrics) {
	ln.metrics = metrics
}

func (ln Link) Metrics() metric.Metrics {
	return ln.metrics
}

// SET

type LinkSet struct {
	slice         []*Link
	mapDPIDtoDPID map[string]map[string]*Link
}

func NewLinkSet() *LinkSet {
	return &LinkSet{
		slice:         make([]*Link, 0),
		mapDPIDtoDPID: make(map[string]map[string]*Link, 0),
	}
}

func (s LinkSet) Size() int {
	return len(s.slice)
}

func (s *LinkSet) Add(ln Link) {
	if s.Contains(ln) {
		return
	}
	s.slice = append(s.slice, &ln)
	s.addDirectedLink(ln.SourceDPID(), ln.DestinationDPID(), &ln)
	s.addDirectedLink(ln.DestinationDPID(), ln.SourceDPID(), &ln)
}

func (s *LinkSet) addDirectedLink(sourceDPID, destinationDPID string, lnPtr *Link) {
	destinationDPIDMap, ok := s.mapDPIDtoDPID[sourceDPID]
	if !ok {
		destinationDPIDMap = map[string]*Link{
			destinationDPID: lnPtr,
		}
		s.mapDPIDtoDPID[sourceDPID] = destinationDPIDMap
		return
	}
	destinationDPIDMap[destinationDPID] = lnPtr
}

func (s LinkSet) Contains(ln Link) bool {
	destinationDPIDMap, ok := s.mapDPIDtoDPID[ln.SourceDPID()]
	if !ok {
		return false
	}
	_, ok = destinationDPIDMap[ln.DestinationDPID()]
	return ok
}

func (s LinkSet) Get(sourceDPID, destinationDPID string) *Link {
	var ln *Link
	destinationDPIDMap, ok := s.mapDPIDtoDPID[sourceDPID]
	if !ok {
		return ln
	}
	ln, _ = destinationDPIDMap[destinationDPID]
	return ln
}

func (s LinkSet) AsSliceCopy() []Link {
	sliceCopy := make([]Link, 0, len(s.slice))
	for _, ln := range s.slice {
		sliceCopy = append(sliceCopy, *ln)
	}
	return sliceCopy
}

func (s LinkSet) AsSlice() []*Link {
	return s.slice
}
