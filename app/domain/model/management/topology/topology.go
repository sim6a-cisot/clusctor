package topology

import (
	"clusctor/app/domain/model/management/mac"
	"clusctor/app/domain/model/management/metric"
	"clusctor/app/domain/model/management/segmentation"
	"clusctor/app/pkg/alg/graph"
)

func New(metricInfo *metric.Info) *Topology {
	return &Topology{
		switches:   NewSwitchSet(),
		hosts:      NewHostSet(),
		links:      NewLinkSet(),
		metricInfo: metricInfo,
	}
}

type Topology struct {
	switches         *SwitchSet
	switchIDSequence IDSequence

	hosts          *HostSet
	hostIDSequence IDSequence

	links      *LinkSet
	metricInfo *metric.Info

	segments segmentation.Segments
}

const noSegmentID = 0

func (t *Topology) DeleteSegments() {
	for _, sw := range t.Switches() {
		t.PlaceSwitchIntoSegment(sw.DPID(), noSegmentID)
	}
	t.segments = nil
}

func (t *Topology) MetricInfo() *metric.Info {
	return t.metricInfo
}

func (t *Topology) Clear() {
	t.hostIDSequence.Reset()
	t.switchIDSequence.Reset()
	t.switches = NewSwitchSet()
	t.links = NewLinkSet()
	t.hosts = NewHostSet()
	t.segments = nil
}

func (t *Topology) Update(
	switches []Switch,
	hosts []Host,
	links []Link,
) {
	t.UpdateLinks(links)
	t.UpdateHosts(hosts)
	t.UpdateSwitches(switches)
}

func (t *Topology) UpdateSwitches(switches []Switch) {
	for _, sw := range switches {
		t.AddSwitchIfNotExists(sw)
	}
}

func (t *Topology) AddSwitchIfNotExists(sw Switch) {
	if !t.switches.Contains(sw) {
		sw.SetID(t.switchIDSequence.Next())
		t.switches.Add(sw)
	}
}

func (t *Topology) UpdateHosts(hosts []Host) {
	for _, host := range hosts {
		t.AddHostIfNotExists(host)
	}
}

func (t *Topology) AddHostIfNotExists(host Host) {
	if !t.hosts.Contains(host) {
		host.SetID(t.hostIDSequence.Next())
		t.hosts.Add(host)
	}
}

func (t *Topology) areLinksChanged(links []Link) bool {
	for _, ln := range links {
		if !t.links.Contains(ln) {
			return true
		}
	}
	return len(links) != t.links.Size()
}

func (t *Topology) UpdateLinks(links []Link) {
	if t.areLinksChanged(links) {
		t.Clear()
	}
	for _, ln := range links {
		t.AddOrUpdateLink(ln)
	}
}

func (t *Topology) AddOrUpdateLink(ln Link) {
	if !t.links.Contains(ln) {
		t.links.Add(ln)
	}
	if t.metricInfo.UseCustomMetrics() {
		return
	}
	storedLink := t.links.Get(ln.SourceDPID(), ln.DestinationDPID())
	storedLink.CalculateBandwidth(ln.MeasurementData())
}

func (t *Topology) ApplySegments(segmentedGraph *graph.Graph) {
	segmentNumber := 0
	for _, sg := range segmentedGraph.SubGraphs {
		segmentNumber++
		for v := range sg.V {
			t.PlaceSwitchIntoSegment(v.Label, segmentNumber)
		}
	}
	t.segments = segmentation.NewSegments(segmentedGraph.SubGraphs)
}

func (t *Topology) PlaceSwitchIntoSegment(dpid string, segmentID int) {
	if sw := t.switches.Get(dpid); sw != nil {
		sw.SetSegmentID(segmentID)
	}
}

func (t *Topology) ExtractGraph() *graph.Graph {
	G := t.extractGraph()
	if t.segments != nil {
		G.SubGraphs = segmentation.ConvertSegmentsToSubGraphs(t.segments, G)
	}
	return G
}

func (t *Topology) extractGraph() *graph.Graph {
	G := graph.New()

	var metrics metric.Metrics
	var currentMetricValue float64
	for _, ln := range t.Links() {
		metrics = ln.Metrics()
		currentMetricValue = metrics.Get(*t.metricInfo)
		G.AddEdge(
			ln.SourceDPID(),
			ln.DestinationDPID(),
			currentMetricValue,
		)
	}

	return G
}

func (t *Topology) Switches() []Switch {
	return t.switches.AsSliceCopy()
}

func (t *Topology) Hosts() []Host {
	return t.hosts.AsSliceCopy()
}

func (t *Topology) Links() []Link {
	return t.links.AsSliceCopy()
}

func (t *Topology) IsHostConnected(mac mac.MAC) bool {
	return t.hosts.Get(mac) != nil
}

func (t *Topology) SwitchMapper() MapSwitch {
	return func(dpid string) Switch {
		return *t.switches.Get(dpid)
	}
}

func (t *Topology) SwitchConnector() ConnectToSwitch {
	return func(dpid string) *Switch {
		return t.switches.Get(dpid)
	}
}

func (t *Topology) HostConnector() ConnectToHost {
	return func(mac mac.MAC) *Host {
		return t.hosts.Get(mac)
	}
}

func (t *Topology) IPToMACTranslator() TranslateIPToMAC {
	return func(ip string) mac.MAC {
		return t.hosts.FindMAC(ip)
	}
}

func (t *Topology) LinkConnector() ConnectToLink {
	return func(srcDPID, dstDPID string) *Link {
		return t.links.Get(srcDPID, dstDPID)
	}
}

func (t *Topology) ClearLinkClasses() {
	for _, ln := range t.links.AsSlice() {
		ln.Classes().Clear()
	}
	for _, h := range t.hosts.AsSlice() {
		h.Classes().Clear()
	}
}
