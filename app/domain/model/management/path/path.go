package path

import (
	"clusctor/app/domain/model/management/class"
	"clusctor/app/domain/model/management/errors"
	"clusctor/app/domain/model/management/flow"
	"clusctor/app/domain/model/management/mac"
	"clusctor/app/domain/model/management/topology"
	"clusctor/app/domain/service/management"
	"clusctor/app/domain/service/scanning/controller"
)

type path struct {
	connectToHost topology.ConnectToHost
	connectToLink topology.ConnectToLink

	treeService management.TreeService

	flowPusher controller.FlowPusher

	srcHost        mac.MAC
	dstHost        mac.MAC
	switchSequence []string
	priority       int
	idleTimeout    int
	hardTimeout    int
	ethType        string
	ipProto        string
	servicePort    int
}

func (p *path) Visualize() error {
	p.treeService.DeleteTree()
	srcHost := p.connectToHost(p.srcHost)
	srcHost.Classes().Add(class.InRoute)
	dstHost := p.connectToHost(p.dstHost)
	dstHost.Classes().Add(class.InRoute)
	var ln *topology.Link
	for i := 0; i < len(p.switchSequence)-1; i++ {
		ln = p.connectToLink(p.switchSequence[i], p.switchSequence[i+1])
		ln.Classes().Add(class.InRoute)
	}
	return nil
}

func (p *path) Install() error {
	return errors.NewInternalError(
		p.flowPusher.PushFlows(p.convertToFlows()),
	)
}

func (p *path) convertToFlows() []flow.Flow {
	srcHost := p.connectToHost(p.srcHost)
	srcIP := srcHost.IPv4()
	dstHost := p.connectToHost(p.dstHost)
	dstIP := dstHost.IPv4()
	var flows []flow.Flow
	for i := 0; i < len(p.switchSequence)-1; i++ {
		srcDPID, dstDPID := p.switchSequence[i], p.switchSequence[i+1]
		ln := p.connectToLink(srcDPID, dstDPID)
		straightFlow := p.createStraightFlow(
			p.createFlow(srcDPID, srcIP, dstIP, ln.OutPort(srcDPID)),
			p.servicePort,
		)
		backFlow := p.createBackFlow(
			p.createFlow(dstDPID, dstIP, srcIP, ln.OutPort(dstDPID)),
			p.servicePort,
		)
		flows = append(flows, straightFlow, backFlow)
	}
	srcDPID, dstDPID := p.switchSequence[0], p.switchSequence[len(p.switchSequence)-1]
	srcSwitchBackFlow := p.createBackFlow(
		p.createFlow(srcDPID, dstIP, srcIP, srcHost.AdjacentPort()),
		p.servicePort,
	)
	dstSwitchBackFlow := p.createStraightFlow(
		p.createFlow(dstDPID, srcIP, dstIP, dstHost.AdjacentPort()),
		p.servicePort,
	)
	return append(flows, srcSwitchBackFlow, dstSwitchBackFlow)
}

func (p *path) createFlow(dpid, srcIP, dstIP string, outPort int) flow.Flow {
	return flow.Flow{
		DPID:        dpid,
		Priority:    p.priority,
		IdleTimeout: p.idleTimeout,
		HardTimeout: p.hardTimeout,
		Matches: flow.Matches{
			SrcIP:   srcIP,
			DstIP:   dstIP,
			EthType: p.ethType,
			IPProto: p.ipProto,
		},
		OutputAction: flow.Action{Output: outPort},
	}
}

func (path) createStraightFlow(f flow.Flow, servicePort int) flow.Flow {
	f.Matches.TCPDstPort = servicePort
	f.Cookie = StraightCookie
	return f
}

func (path) createBackFlow(f flow.Flow, servicePort int) flow.Flow {
	f.Matches.TCPSrcPort = servicePort
	f.Cookie = BackCookie
	return f
}

func (p path) ServicePort() int {
	return p.servicePort
}

func (p path) IPProto() string {
	return p.ipProto
}

func (p path) EthType() string {
	return p.ethType
}

func (p path) HardTimeout() int {
	return p.hardTimeout
}

func (p path) IdleTimeout() int {
	return p.idleTimeout
}

func (p path) Priority() int {
	return p.priority
}

func (p path) SwitchSequence() []string {
	return p.switchSequence
}

func (p path) DstHost() mac.MAC {
	return p.dstHost
}

func (p path) SrcHost() mac.MAC {
	return p.srcHost
}

func (p *path) SetSwitchSequence(seq []string) {
	p.switchSequence = seq
}

func (p *path) SetMatches(ethType, ipProto string, src, dst mac.MAC, servicePort int) {
	p.srcHost = src
	p.dstHost = dst
	p.ipProto = ipProto
	p.ethType = ethType
	p.servicePort = servicePort
}

func (p *path) SetPriority(priority int) {
	p.priority = priority
}

func (p *path) SetTimeouts(hard, idle int) {
	p.hardTimeout = hard
	p.idleTimeout = idle
}

func (p *path) SrcIP() string {
	if host := p.connectToHost(p.srcHost); host != nil {
		return host.IPv4()
	}
	return defaultIP
}

func (p *path) DstIP() string {
	if host := p.connectToHost(p.dstHost); host != nil {
		return host.IPv4()
	}
	return defaultIP
}
