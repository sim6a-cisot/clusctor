package mock

type Visualizer struct {
	VisualizeFunc func() error
}

func (v *Visualizer) Visualize() error {
	return v.VisualizeFunc()
}

