package mock

import (
	"clusctor/app/domain/model/management/path"
)

type Factory struct {
	InstallerFunc  func() path.Installer
	VisualizerFunc func() path.Visualizer
	CollectionFunc func() path.Collection
}

func (f *Factory) Installer(interface {
	Build(path.Builder)
}) path.Installer {
	return f.InstallerFunc()
}

func (f *Factory) Visualizer(interface {
	Build(path.Builder)
}) path.Visualizer {
	return f.VisualizerFunc()
}

func (f *Factory) Collection() path.Collection {
	return f.CollectionFunc()
}
