package mock

import (
	"clusctor/app/domain/model/management/path"
)

type Collection struct {
	NextFunc func() bool
	ReadFunc func() error
	ValueFunc func() path.View
}

func (c *Collection) Next() bool {
	return c.NextFunc()
}

func (c *Collection) Value() path.View {
	return c.ValueFunc()
}

func (c *Collection) Read() error {
	return c.ReadFunc()
}

