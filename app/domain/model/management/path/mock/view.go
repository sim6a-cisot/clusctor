package mock

import "clusctor/app/domain/model/management/mac"

type View struct {
	ServicePort_    int
	IPProto_        string
	EthType_        string
	HardTimeout_    int
	IdleTimeout_    int
	Priority_       int
	SwitchSequence_ []string
	DstHost_        mac.MAC
	SrcHost_        mac.MAC
	DstIP_          string
	SrcIP_          string
}

func (v View) ServicePort() int {
	return v.ServicePort_
}

func (v View) IPProto() string {
	return v.IPProto_
}

func (v View) EthType() string {
	return v.EthType_
}

func (v View) HardTimeout() int {
	return v.HardTimeout_
}

func (v View) IdleTimeout() int {
	return v.IdleTimeout_
}

func (v View) Priority() int {
	return v.Priority_
}

func (v View) SwitchSequence() []string {
	return v.SwitchSequence_
}

func (v View) DstHost() mac.MAC {
	return v.DstHost_
}

func (v View) SrcHost() mac.MAC {
	return v.SrcHost_
}

func (v View) DstIP() string {
	return v.DstIP_
}

func (v View) SrcIP() string {
	return v.SrcIP_
}

