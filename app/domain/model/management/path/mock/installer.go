package mock

type Installer struct {
	InstallFunc func() error
}

func (i *Installer) Install() error {
	return i.InstallFunc()
}

