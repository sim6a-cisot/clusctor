package path

import (
	"clusctor/app/domain/model/management/errors"
	"clusctor/app/domain/model/management/topology"
)

type validator struct {
	connectToHost   topology.ConnectToHost
	connectToSwitch topology.ConnectToSwitch

	path *path
}

func (v *validator) validate() error {
	if v.path.srcHost == v.path.dstHost {
		return errors.NewValidationError("source host and destination host mustn't be the same")
	}
	if len(v.path.switchSequence) == 0 {
		return errors.NewValidationError("switch sequence must contains at least one openflow switch")
	}
	if srcHost := v.connectToHost(v.path.srcHost); srcHost == nil {
		return errors.NewValidationError("host " + v.path.srcHost.String() + " doesn't exist")
	}
	if dstHost := v.connectToHost(v.path.dstHost); dstHost == nil {
		return errors.NewValidationError("host " + v.path.dstHost.String() + " doesn't exist")
	}
	if errSwitchSequence := v.validateSwitchSequence(); errSwitchSequence != nil {
		return errSwitchSequence
	}
	return nil
}

func (v *validator) Install() error {
	if err := v.validate(); err != nil {
		return err
	}
	return v.path.Install()
}

func (v *validator) Visualize() error {
	if err := v.validate(); err != nil {
		return err
	}
	return v.path.Visualize()
}

func (v *validator) validateSwitchSequence() error {
	for _, psw := range v.path.switchSequence {
		if sw := v.connectToSwitch(psw); sw == nil {
			return errors.NewValidationError("openflow switch " + psw + " doesn't exist")
		}
	}
	return nil
}