package path

import (
	"clusctor/app/domain/model/management/errors"
	"clusctor/app/domain/model/management/topology"
	"clusctor/app/domain/service/scanning/controller"
)

type collection struct {
	flowReader controller.FlowReader
	recognizer *recognizer
	paths      []*path
	current    int
}

func newCollection(
	flowReader controller.FlowReader,
	ipToMAC topology.TranslateIPToMAC,
	connectToHost topology.ConnectToHost,
	connectToLink topology.ConnectToLink,
	connectToSwitch topology.ConnectToSwitch,
) *collection {
	return &collection{
		flowReader: flowReader,
		current:    -1,
		recognizer: newRecognizer(
			ipToMAC,
			connectToHost,
			connectToLink,
			connectToSwitch,
		),
	}
}

func (c *collection) Read() error {
	flows, err := c.flowReader.ReadFlows()
	if err != nil {
		return errors.NewInternalError(err)
	}
	c.paths = c.recognizer.recognize(flows)
	return nil
}

func (c *collection) Next() bool {
	c.current++
	return c.current != len(c.paths)
}

func (c *collection) Value() View {
	return c.paths[c.current]
}
