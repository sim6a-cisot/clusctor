package path_test

import (
	"clusctor/app/domain/model/management/flow"
	"clusctor/app/domain/model/management/mac"
	"clusctor/app/domain/model/management/path"
	"clusctor/app/domain/model/management/topology"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const (
	h1MAC = mac.MAC("00:00:00:00:00:01")
	h2MAC = mac.MAC("00:00:00:00:00:02")

	s1DPID = "00:00:00:00:00:00:00:01"
	s2DPID = "00:00:00:00:00:00:00:02"
)

type externalBuilderStub struct {
	switchSequence []string
}

func newExternalBuilderStub(switchSequence []string) *externalBuilderStub {
	return &externalBuilderStub{switchSequence: switchSequence}
}

func (b *externalBuilderStub) Build(pb path.Builder) {
	pb.SetTimeouts(120, 60)
	pb.SetPriority(10000)
	pb.SetMatches("ipv4", "tcp", h1MAC, h2MAC, 8080)
	pb.SetSwitchSequence(b.switchSequence)
}

type flowPusherMock struct {
	pushedFlows []flow.Flow
}

func (fp *flowPusherMock) PushFlows(flows []flow.Flow) error {
	fp.pushedFlows = flows
	return nil
}

func switchConnectorStub() topology.ConnectToSwitch {
	var sw topology.Switch
	return func(dpid string) *topology.Switch {
		switch dpid {
		case s1DPID, s2DPID:
			sw = topology.NewSwitch(dpid)
		}
		return &sw
	}
}

func linkConnectorStub() topology.ConnectToLink {
	return func(dpid1, dpid2 string) *topology.Link {
		var ln topology.Link
		if dpid1 == s1DPID && dpid2 == s2DPID {
			ln = topology.NewLink(2, 1, s1DPID, s2DPID)
		}
		return &ln
	}
}

func hostConnectorStub() topology.ConnectToHost {
	return func(m mac.MAC) *topology.Host {
		var h topology.Host
		switch m {
		case h1MAC:
			h = topology.NewHost(h1MAC, "10.0.0.1", s1DPID, 1)
		case h2MAC:
			h = topology.NewHost(h2MAC, "10.0.0.2", s1DPID, 2)
		}
		return &h
	}
}

func TestPath_Install_OneOFSwitch(t *testing.T) {
	// see test/alg/path/one_switch_test.svg
	fpm := &flowPusherMock{}
	f := path.NewFactory(
		fpm,
		nil,
		nil,
		hostConnectorStub(),
		linkConnectorStub(),
		switchConnectorStub(),
		nil,
	)
	p := f.Installer(newExternalBuilderStub([]string{s1DPID}))

	require.NoError(t, p.Install())

	actual := fpm.pushedFlows
	expected := []flow.Flow{
		{
			DPID:        "00:00:00:00:00:00:00:01",
			Cookie:      path.BackCookie,
			Priority:    10000,
			IdleTimeout: 60,
			HardTimeout: 120,
			Matches: flow.Matches{
				SrcIP:      "10.0.0.2",
				DstIP:      "10.0.0.1",
				IPProto:    "tcp",
				EthType:    "ipv4",
				TCPSrcPort: 8080,
				TCPDstPort: 0,
			},
			OutputAction: flow.Action{
				Output: 1,
			},
		},
		{
			DPID:        "00:00:00:00:00:00:00:01",
			Cookie:      path.StraightCookie,
			Priority:    10000,
			IdleTimeout: 60,
			HardTimeout: 120,
			Matches: flow.Matches{
				SrcIP:      "10.0.0.1",
				DstIP:      "10.0.0.2",
				IPProto:    "tcp",
				EthType:    "ipv4",
				TCPDstPort: 8080,
				TCPSrcPort: 0,
			},
			OutputAction: flow.Action{
				Output: 2,
			},
		},
	}

	assert.Equal(t, expected, actual)
}

func TestPath_Install_TwoOFSwitches(t *testing.T) {
	// see test/alg/path/two_switches_test.svg
	fpm := &flowPusherMock{}
	f := path.NewFactory(
		fpm,
		nil,
		nil,
		hostConnectorStub(),
		linkConnectorStub(),
		switchConnectorStub(),
		nil,
	)
	p := f.Installer(newExternalBuilderStub([]string{s1DPID, s2DPID}))

	require.NoError(t, p.Install())

	actual := fpm.pushedFlows
	expected := []flow.Flow{
		{
			DPID:        "00:00:00:00:00:00:00:01",
			Cookie:      path.StraightCookie,
			Priority:    10000,
			IdleTimeout: 60,
			HardTimeout: 120,
			Matches: flow.Matches{
				SrcIP:      "10.0.0.1",
				DstIP:      "10.0.0.2",
				IPProto:    "tcp",
				EthType:    "ipv4",
				TCPSrcPort: 0,
				TCPDstPort: 8080,
			},
			OutputAction: flow.Action{
				Output: 2,
			},
		},
		{
			DPID:        "00:00:00:00:00:00:00:02",
			Cookie:      path.BackCookie,
			Priority:    10000,
			IdleTimeout: 60,
			HardTimeout: 120,
			Matches: flow.Matches{
				SrcIP:      "10.0.0.2",
				DstIP:      "10.0.0.1",
				IPProto:    "tcp",
				EthType:    "ipv4",
				TCPSrcPort: 8080,
				TCPDstPort: 0,
			},
			OutputAction: flow.Action{
				Output: 1,
			},
		},
		{
			DPID:        "00:00:00:00:00:00:00:01",
			Cookie:      path.BackCookie,
			Priority:    10000,
			IdleTimeout: 60,
			HardTimeout: 120,
			Matches: flow.Matches{
				SrcIP:      "10.0.0.2",
				DstIP:      "10.0.0.1",
				IPProto:    "tcp",
				EthType:    "ipv4",
				TCPSrcPort: 8080,
				TCPDstPort: 0,
			},
			OutputAction: flow.Action{
				Output: 1,
			},
		},
		{
			DPID:        "00:00:00:00:00:00:00:02",
			Cookie:      path.StraightCookie,
			Priority:    10000,
			IdleTimeout: 60,
			HardTimeout: 120,
			Matches: flow.Matches{
				SrcIP:      "10.0.0.1",
				DstIP:      "10.0.0.2",
				IPProto:    "tcp",
				EthType:    "ipv4",
				TCPSrcPort: 0,
				TCPDstPort: 8080,
			},
			OutputAction: flow.Action{
				Output: 2,
			},
		},
	}
	assert.Equal(t, expected, actual)
}
