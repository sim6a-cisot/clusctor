package path

import (
	"clusctor/app/domain/model/management/flow"
	"clusctor/app/domain/model/management/topology"
)

type recognizer struct {
	ipToMAC         topology.TranslateIPToMAC
	connectToHost   topology.ConnectToHost
	connectToLink   topology.ConnectToLink
	connectToSwitch topology.ConnectToSwitch
	sequencer       *sequencer
}

func newRecognizer(
	ipToMAC topology.TranslateIPToMAC,
	connectToHost topology.ConnectToHost,
	connectToLink topology.ConnectToLink,
	connectToSwitch topology.ConnectToSwitch,
) *recognizer {
	return &recognizer{
		ipToMAC:         ipToMAC,
		connectToHost:   connectToHost,
		connectToLink:   connectToLink,
		connectToSwitch: connectToSwitch,
		sequencer:       newSequencer(connectToLink),
	}
}

func (r *recognizer) recognize(flows flow.Collection) []*path {
	if flows = r.filterStraightFlows(flows); len(flows) == 0 {
		return []*path{}
	}
	var paths []*path
	for len(flows) > 0 {
		flowsWithoutPath, p := r.chopOffPath(flows)
		r.initPath(p)
		paths = append(paths, p)
		flows = flowsWithoutPath
	}
	return paths
}

func (r *recognizer) initPath(p *path) {
	p.connectToLink = r.connectToLink
	p.connectToHost = r.connectToHost
}

func (r *recognizer) filterStraightFlows(flows flow.Collection) flow.Collection {
	var filtered flow.Collection
	for _, f := range flows {
		if f.Cookie == StraightCookie {
			filtered = append(filtered, f)
		}
	}
	return filtered
}

func (r *recognizer) chopOffPath(flows flow.Collection) (flow.Collection, *path) {
	flowsWithoutPath := flows
	index := 0
	referenceFlow := flowsWithoutPath[index]
	flowsWithoutPath = flowsWithoutPath.RemoveAt(index)
	flowsOfPath := flow.Collection{referenceFlow}
	for i := 0; i < len(flowsWithoutPath); i++ {
		f := flowsWithoutPath[i]
		if f.Equals(referenceFlow) {
			flowsOfPath = append(flowsOfPath, f)
			flowsWithoutPath = flowsWithoutPath.RemoveAt(i)
			i--
		}
	}
	return flowsWithoutPath, r.constructPath(flowsOfPath)
}

func (r *recognizer) constructPath(flows flow.Collection) *path {
	index := 0
	srcMAC := r.ipToMAC(flows[index].Matches.SrcIP)
	dstMAC := r.ipToMAC(flows[index].Matches.DstIP)
	priority := flows[index].Priority
	idleTimeout := flows[index].IdleTimeout
	hardTimeout := flows[index].HardTimeout
	ethType := flows[index].Matches.EthType
	ipProto := flows[index].Matches.IPProto
	servicePort := flows[index].Matches.TCPDstPort

	srcHost := r.connectToHost(srcMAC)
	switchSequence := r.sequencer.makeSequence(srcHost.AdjacentDPID(), flows)
	return &path{
		srcHost:        srcMAC,
		dstHost:        dstMAC,
		switchSequence: switchSequence,
		priority:       priority,
		idleTimeout:    idleTimeout,
		hardTimeout:    hardTimeout,
		ethType:        ethType,
		ipProto:        ipProto,
		servicePort:    servicePort,
	}
}
