package path

import "clusctor/app/domain/model/management/mac"

type Factory interface {
	Installer(interface {
		Build(Builder)
	}) Installer
	Visualizer(interface {
		Build(Builder)
	}) Visualizer
	Collection() Collection
}

type Installer interface {
	Install() error
}

type Visualizer interface {
	Visualize() error
}

type Collection interface {
	Iterator
	Read() error
}

type Iterator interface {
	Next() bool
	Value() View
}

type Builder interface {
	SetSwitchSequence([]string)
	SetMatches(ethType, ipProto string, srcHost, dstHost mac.MAC, servicePort int)
	SetPriority(int)
	SetTimeouts(hard, idle int)
}

type View interface {
	ServicePort() int
	IPProto() string
	EthType() string
	HardTimeout() int
	IdleTimeout() int
	Priority() int
	SwitchSequence() []string
	DstHost() mac.MAC
	SrcHost() mac.MAC
	DstIP() string
	SrcIP() string
}
