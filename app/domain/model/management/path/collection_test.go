package path_test

import (
	"clusctor/app/domain/model/management/flow"
	"clusctor/app/domain/model/management/metric"
	"clusctor/app/domain/model/management/path"
	topologyModel "clusctor/app/domain/model/management/topology"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
)

type flowReaderStub struct {
	ReadFlowsFunc func() ([]flow.Flow, error)
}

func (r *flowReaderStub) ReadFlows() (flow.Collection, error) {
	return r.ReadFlowsFunc()
}

func Test_collection_Read_OneSwitch(t *testing.T) {
	const (
		srcMAC     = "00:00:00:00:00:01"
		dstMAC     = "00:00:00:00:00:02"
		srcIP      = "10.0.0.1"
		dstIP      = "10.0.0.2"
		singleDPID = "00:00:00:00:00:00:00:01"
	)

	readerStub := &flowReaderStub{
		ReadFlowsFunc: func() (flows []flow.Flow, err error) {
			return []flow.Flow{
				{
					Cookie:      "0x18181818",
					Priority:    10000,
					IdleTimeout: 60,
					HardTimeout: 0,
					Matches: flow.Matches{
						SrcIP:      "10.0.0.2",
						DstIP:      "10.0.0.1",
						IPProto:    "",
						EthType:    "ipv4",
						TCPSrcPort: 0,
						TCPDstPort: 0,
					},
				},

				{
					Cookie:      path.StraightCookie,
					Priority:    10000,
					IdleTimeout: 60,
					HardTimeout: 0,
					Matches: flow.Matches{
						SrcIP:      srcIP,
						DstIP:      dstIP,
						IPProto:    "tcp",
						EthType:    "ipv4",
						TCPSrcPort: 8080,
						TCPDstPort: 0,
					},
				},
				{
					Cookie:      path.BackCookie,
					Priority:    10000,
					IdleTimeout: 60,
					HardTimeout: 0,
					Matches: flow.Matches{
						SrcIP:      dstIP,
						DstIP:      srcIP,
						IPProto:    "tcp",
						EthType:    "ipv4",
						TCPSrcPort: 0,
						TCPDstPort: 8080,
					},
				},

				{
					Cookie:      path.StraightCookie,
					Priority:    10000,
					IdleTimeout: 60,
					HardTimeout: 0,
					Matches: flow.Matches{
						SrcIP:      srcIP,
						DstIP:      dstIP,
						IPProto:    "",
						EthType:    "ipv4",
						TCPSrcPort: 0,
						TCPDstPort: 0,
					},
				},
				{
					Cookie:      path.BackCookie,
					Priority:    10000,
					IdleTimeout: 60,
					HardTimeout: 0,
					Matches: flow.Matches{
						SrcIP:      dstIP,
						DstIP:      srcIP,
						IPProto:    "",
						EthType:    "ipv4",
						TCPSrcPort: 0,
						TCPDstPort: 0,
					},
				},
			}, nil
		},
	}
	topo := topologyModel.New(metric.NewInfo())
	topo.AddHostIfNotExists(
		topologyModel.NewHost(
			srcMAC,
			srcIP,
			singleDPID,
			1),
	)
	topo.AddHostIfNotExists(
		topologyModel.NewHost(
			dstMAC,
			dstIP,
			singleDPID,
			2),
	)
	topo.AddSwitchIfNotExists(topologyModel.NewSwitch(singleDPID))
	factory := path.NewFactory(
		nil,
		readerStub,
		topo.IPToMACTranslator(),
		topo.HostConnector(),
		topo.LinkConnector(),
		topo.SwitchConnector(),
		nil,
	)
	collection := factory.Collection()
	require.NoError(t, collection.Read())
	var paths []path.View
	for collection.Next() {
		paths = append(paths, collection.Value())
	}
	require.Len(t, paths, 2)
	p1 := paths[0]
	p2 := paths[1]
	assert.Equal(t, []string{singleDPID}, p1.SwitchSequence())
	assert.Equal(t, []string{singleDPID}, p2.SwitchSequence())
}

func Test_collection_Read_TwoSwitches(t *testing.T) {
	const (
		srcMAC           = "00:00:00:00:00:01"
		dstMAC           = "00:00:00:00:00:02"
		srcIP            = "10.0.0.1"
		dstIP            = "10.0.0.2"
		DPID1            = "00:00:00:00:00:00:00:01"
		DPID2            = "00:00:00:00:00:00:00:02"
		PortDPID1ToDPID2 = 1
	)

	readerStub := &flowReaderStub{
		ReadFlowsFunc: func() (flows []flow.Flow, err error) {
			return []flow.Flow{
				{
					DPID:        DPID1,
					Cookie:      path.StraightCookie,
					Priority:    10000,
					IdleTimeout: 60,
					HardTimeout: 0,
					Matches: flow.Matches{
						SrcIP:      srcIP,
						DstIP:      dstIP,
						IPProto:    "tcp",
						EthType:    "ipv4",
						TCPSrcPort: 8080,
						TCPDstPort: 0,
					},
					OutputAction: flow.Action{Output: PortDPID1ToDPID2},
				},
				{
					DPID:        DPID2,
					Cookie:      path.StraightCookie,
					Priority:    10000,
					IdleTimeout: 60,
					HardTimeout: 0,
					Matches: flow.Matches{
						SrcIP:      srcIP,
						DstIP:      dstIP,
						IPProto:    "tcp",
						EthType:    "ipv4",
						TCPSrcPort: 8080,
						TCPDstPort: 0,
					},
					OutputAction: flow.Action{Output: 0},
				},
			}, nil
		},
	}
	topo := topologyModel.New(metric.NewInfo())
	topo.AddHostIfNotExists(
		topologyModel.NewHost(
			srcMAC,
			srcIP,
			DPID1,
			0),
	)
	topo.AddHostIfNotExists(
		topologyModel.NewHost(
			dstMAC,
			dstIP,
			DPID2,
			0),
	)
	topo.AddSwitchIfNotExists(topologyModel.NewSwitch(DPID1))
	topo.AddSwitchIfNotExists(topologyModel.NewSwitch(DPID2))
	topo.AddOrUpdateLink(topologyModel.NewLink(PortDPID1ToDPID2, 0, DPID1, DPID2))
	factory := path.NewFactory(
		nil,
		readerStub,
		topo.IPToMACTranslator(),
		topo.HostConnector(),
		topo.LinkConnector(),
		topo.SwitchConnector(),
		nil,
	)
	collection := factory.Collection()
	require.NoError(t, collection.Read())
	var paths []path.View
	for collection.Next() {
		paths = append(paths, collection.Value())
	}
	require.Len(t, paths, 1)
	p1 := paths[0]
	actualSwitchSequence := p1.SwitchSequence()
	expectedSwitchSequence := []string{DPID1, DPID2}
	assert.Equal(t, expectedSwitchSequence, actualSwitchSequence)
}
