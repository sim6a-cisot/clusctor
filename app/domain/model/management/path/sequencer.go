package path

import (
	"clusctor/app/domain/model/management/flow"
	"clusctor/app/domain/model/management/topology"
)

type sequencer struct {
	connectToLink topology.ConnectToLink
}

func newSequencer(connectToLink topology.ConnectToLink) *sequencer {
	return &sequencer{connectToLink: connectToLink}
}

func (s *sequencer) makeSequence(
	firstDPID string,
	unsortedFlows flow.Collection,
) []string {
	sequence := []string{firstDPID}
	lastSortedFlow := unsortedFlows.GetByDPID(firstDPID)
	unsortedFlows = unsortedFlows.RemoveByDPID(lastSortedFlow.DPID)
	for len(unsortedFlows) > 0 {
		for i := 0; i < len(unsortedFlows); i++ {
			f := unsortedFlows[i]
			if s.isInSequence(lastSortedFlow.DPID, lastSortedFlow.OutputAction.Output, f.DPID) {
				lastSortedFlow = f
				unsortedFlows = unsortedFlows.RemoveByDPID(lastSortedFlow.DPID)
				sequence = append(sequence, lastSortedFlow.DPID)
				i--
			}
		}
	}
	return sequence
}

func (s *sequencer) isInSequence(dpid1 string, outPort int, dpid2 string) bool {
	ln := s.connectToLink(dpid1, dpid2)
	if ln == nil {
		return false
	}
	return ln.OutPort(dpid1) == outPort
}
