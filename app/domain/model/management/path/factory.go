package path

import (
	"clusctor/app/domain/model/management/topology"
	"clusctor/app/domain/service/management"
	"clusctor/app/domain/service/scanning/controller"
)

type factory struct {
	flowPusher      controller.FlowPusher
	flowReader      controller.FlowReader
	ipToMAC         topology.TranslateIPToMAC
	connectToHost   topology.ConnectToHost
	connectToLink   topology.ConnectToLink
	connectToSwitch topology.ConnectToSwitch
	treeService     management.TreeService
}

func NewFactory(
	flowPusher controller.FlowPusher,
	flowReader controller.FlowReader,
	ipToMAC topology.TranslateIPToMAC,
	connectToHost topology.ConnectToHost,
	connectToLink topology.ConnectToLink,
	connectToSwitch topology.ConnectToSwitch,
	treeService management.TreeService,
) Factory {
	return &factory{
		flowPusher:      flowPusher,
		flowReader:      flowReader,
		ipToMAC:         ipToMAC,
		connectToHost:   connectToHost,
		connectToLink:   connectToLink,
		connectToSwitch: connectToSwitch,
		treeService:     treeService,
	}
}

func (f *factory) constructValidatablePath(builder interface {
	Build(Builder)
}) *validator {
	p := &path{
		treeService:   f.treeService,
		flowPusher:    f.flowPusher,
		connectToHost: f.connectToHost,
		connectToLink: f.connectToLink,
	}
	builder.Build(p)
	return &validator{
		path:            p,
		connectToSwitch: f.connectToSwitch,
		connectToHost:   f.connectToHost,
	}
}

func (f *factory) Installer(builder interface {
	Build(Builder)
}) Installer {
	return f.constructValidatablePath(builder)
}

func (f *factory) Collection() Collection {
	return newCollection(
		f.flowReader,
		f.ipToMAC,
		f.connectToHost,
		f.connectToLink,
		f.connectToSwitch,
	)
}

func (f *factory) Visualizer(builder interface {
	Build(Builder)
}) Visualizer {
	return f.constructValidatablePath(builder)
}
