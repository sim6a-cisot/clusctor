package mac

import (
	"errors"
	"regexp"
)

var regularExpression = regexp.MustCompile(`^([0-9a-f]{2}:){5}[0-9a-f]{2}$`)
const validationErrorMessage = "must have next format xx:xx:xx:xx:xx:xx where x is hex"

func Validate(mac MAC) error {
	if !regularExpression.MatchString(mac.String()) {
		return errors.New(validationErrorMessage)
	}
	return nil
}