package mac

type MAC string

func (m MAC) String() string {
	return string(m)
}

func DeleteDuplicates(source []MAC) []MAC {
	var result []MAC
	isDuplicate := map[MAC]bool{}
	for _, mac := range source {
		if !isDuplicate[mac] {
			result = append(result, mac)
			isDuplicate[mac] = true
		}
	}
	return result
}