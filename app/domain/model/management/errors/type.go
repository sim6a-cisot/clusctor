package errors

// validation

type ValidationError struct {
	message string
}

func NewValidationError(message string) *ValidationError {
	return &ValidationError{
		message: message,
	}
}

func (e *ValidationError) Error() string {
	return e.message
}

// internal

type InternalError struct {
	message string
}

func NewInternalError(err error) error {
	if err == nil {
		return nil
	}
	return &InternalError{
		message: err.Error(),
	}
}

func (e *InternalError) Error() string {
	return e.message
}