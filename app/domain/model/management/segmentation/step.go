package segmentation

type Step string

func (s Step) String() string {
	return string(s)
}

const (
	PrimarySegments   Step = "primarySegments"
	SecondarySegments Step = "secondarySegments"
	Merging           Step = "merging"
)