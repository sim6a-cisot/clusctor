package segmentation

import "clusctor/app/pkg/alg/graph"

type Segment struct {
	ID    int
	DPIDs []string
}

func NewSegments(subGraphs graph.Collection) Segments {
	segments := Segments{}

	for _, subGraph := range subGraphs {
		segment := Segment{
			ID:    subGraph.ID,
			DPIDs: []string{},
		}
		for v := range subGraph.V {
			segment.DPIDs = append(segment.DPIDs, v.Label)
		}

		segments = append(segments, segment)
	}

	return segments
}

type Segments []Segment

func ConvertSegmentsToSubGraphs(segments Segments, G *graph.Graph) graph.Collection {
	subGraphs := graph.Collection{}

	for _, segment := range segments {
		subgraph := graph.New()
		subgraph.ID = segment.ID
		for _, dpid := range segment.DPIDs {
			subgraph.Include(G.GetOrAdd(dpid))
		}

		subGraphs = append(subGraphs, subgraph)
	}

	return subGraphs
}
