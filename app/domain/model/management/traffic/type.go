package traffic

type Type string

func (t Type) String() string {
	return string(t)
}

func (t Type) ServicePort() int {
	port := 0
	switch t {
	case FileExchange:
		port = 10100 // TCP/IP FTP
	case VideoStream:
		port = 9009 // TCP/IP HLS
	case Email:
		port = 1025 // TCP/IP SMTP
	case VoIP:
		port = 25000 // UDP/IP RTP
	case ECommerce:
		port = 8080 // TCP/IP HTTP
	}
	return port
}

const (
	FileExchange Type = "fileExchange"
	VideoStream  Type = "videoStream"
	Email        Type = "email"
	VoIP         Type = "voip"
	ECommerce    Type = "eCommerce"
)
