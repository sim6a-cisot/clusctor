package dpid

import "regexp"

var (
	DPID = regexp.MustCompile(`^([0-9a-f]{2}:){7}[0-9a-f]{2}$`)
)
