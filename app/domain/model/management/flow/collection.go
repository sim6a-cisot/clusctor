package flow

type Collection []Flow

// RemoveAt element by index.
// This method changes an order of the collection.
func (c Collection) RemoveAt(i int) Collection {
	if i >= len(c) || i < 0 {
		return c
	}
	lastIndex := len(c)-1
	c[i] = c[lastIndex]
	c[lastIndex] = Flow{}
	return c[:lastIndex]
}

func (c Collection) GetByDPID(dpid string) Flow {
	for _, f := range c {
		if f.DPID == dpid {
			return f
		}
	}
	return Flow{}
}

func (c Collection) RemoveByDPID(dpid string) Collection {
	for i, f := range c {
		if f.DPID == dpid {
			return c.RemoveAt(i)
		}
	}
	return c
}
