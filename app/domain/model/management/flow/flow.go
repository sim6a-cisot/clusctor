package flow

type Matches struct {
	SrcIP      string
	DstIP      string
	IPProto    string
	EthType    string
	TCPSrcPort int
	TCPDstPort int
}

func (m Matches) Equals(other Matches) bool {
	return m.SrcIP == other.SrcIP &&
		m.DstIP == other.DstIP &&
		m.TCPSrcPort == other.TCPSrcPort &&
		m.TCPDstPort == other.TCPDstPort &&
		m.IPProto == other.IPProto &&
		m.EthType == other.EthType
}

type Action struct {
	Output int
}

type Flow struct {
	DPID string

	Cookie       string
	Priority     int
	IdleTimeout  int
	HardTimeout  int
	Matches      Matches
	OutputAction Action
}

func (f Flow) Equals(other Flow) bool {
	return f.Priority == other.Priority &&
		f.IdleTimeout == other.IdleTimeout &&
		f.Cookie == other.Cookie &&
		f.HardTimeout == other.HardTimeout &&
		f.Matches.Equals(other.Matches)
}
