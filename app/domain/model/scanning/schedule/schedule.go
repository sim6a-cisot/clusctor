package schedule

import (
	"clusctor/app/domain/service/scanning"
)

type Config struct {
	NetworkScanningIntervalSec int
}

type Task struct {
	StartImmediately bool
	IntervalSec int
	JobFunc func()
}

type Schedule []Task

func NewSchedule(updater scanning.UpdatableTopology, cfg Config) Schedule {
	return Schedule{
		Task{
			StartImmediately: true,
			IntervalSec:      cfg.NetworkScanningIntervalSec,
			JobFunc:          updater.UpdateTopology,
		},
	}
}