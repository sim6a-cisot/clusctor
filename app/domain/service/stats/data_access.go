package stats

import "clusctor/app/domain/model/stats"

type BisectionStatsWriter interface {
	WriteBisectionStats(stats.Bisection)
}

type SegmentationStatsWriter interface {
	WriteSegmentationStats(stats.Segmentation)
}

type SlicingStatsWriter interface {
	WriteSlicingStats(stats.Slice)
}

type PrimStatsWriter interface {
	WritePrimStats(stats.Prim)
}

type GirvanNewmanStatsWriter interface {
	WriteGirvanNewmanStats(stats.GirvanNewman)
}

type Reader interface {
	ReadBisectionStats() stats.Presentable
	ReadSegmentationStats() stats.Presentable
	ReadSlicingStats() stats.Presentable
	ReadPrimStats() stats.Presentable
	ReadGirvanNewmanStats() stats.Presentable
}

type ReaderCleaner interface {
	Reader
	Clear()
}
