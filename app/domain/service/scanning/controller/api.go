package controller

import (
	"clusctor/app/domain/model/management/flow"
	"clusctor/app/domain/service/management"
)

type Client interface {
	NetworkScanner
	FlowPusher
	FlowReader
}

type NetworkScanner interface {
	ScanNetwork(management.TopologyUpdater) error
}

type FlowPusher interface {
	PushFlows([]flow.Flow) error
}

type FlowReader interface {
	ReadFlows() (flow.Collection, error)
}