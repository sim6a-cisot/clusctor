package controller

type Config struct {
	ControllerName string

	Host string
	Port int
}
