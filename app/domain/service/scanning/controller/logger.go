package controller

import (
	"clusctor/app/application/service"
	"clusctor/app/application/service/log"
	"clusctor/app/domain/model/management/flow"
	"clusctor/app/domain/service/management"
	"clusctor/app/pkg/strutil"
	"time"
)

type networkScannerLogger struct {
	logger  service.Logger
	scanner NetworkScanner
}

func DecorateNetworkScannerWithLogger(scanner NetworkScanner, l service.Logger) NetworkScanner {
	return &networkScannerLogger{
		logger:  l,
		scanner: scanner,
	}
}

func (l *networkScannerLogger) ScanNetwork(topology management.TopologyUpdater) (err error) {
	defer func(start time.Time) {
		if err != nil {
			l.logger.Warn(log.Entry{
				Msg:  "network scanning error: " + err.Error(),
			})
		}
	}(time.Now())
	err = l.scanner.ScanNetwork(topology)
	return err
}

type flowPusherLogger struct {
	logger service.Logger
	pusher FlowPusher
}

func DecorateFlowPusherWithLogger(pusher FlowPusher, l service.Logger) FlowPusher {
	return &flowPusherLogger{
		logger:  l,
		pusher: pusher,
	}
}

func (l *flowPusherLogger) PushFlows(flows []flow.Flow) (err error) {
	defer func(start time.Time) {
		if err != nil {
			l.logger.Warn(log.Entry{
				Msg:  "flow pushing error: " + err.Error(),
				Took: strutil.TimeInMs(time.Since(start)),
			})
		} else {
			l.logger.Info(log.Entry{
				Msg:  "flows pushed",
				Took: strutil.TimeInMs(time.Since(start)),
			})
		}
	}(time.Now())
	err = l.pusher.PushFlows(flows)
	return err
}