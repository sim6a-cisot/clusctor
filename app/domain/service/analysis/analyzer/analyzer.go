package analyzer

import (
	"clusctor/app/domain/model/management/segmentation"
	"clusctor/app/domain/model/management/topology"
	topologyAnalysis "clusctor/app/domain/service/analysis/topology"
	"clusctor/app/domain/service/management"
	"clusctor/app/domain/service/stats"
)

type Analyzer struct {
	segmentationService management.SegmentationService
	bisectionService    management.BisectionService
	girvanNewmanService management.GirvanNewmanService
	topologiesIterator  topologyAnalysis.Iterator
	statsCache          stats.ReaderCleaner
	topo                management.TopologyUpdaterCleaner
}

func NewAnalyzer(
	segmentationService management.SegmentationService,
	bisectionService management.BisectionService,
	girvanNewmanService management.GirvanNewmanService,
	topologiesIterator  topologyAnalysis.Iterator,
	statsCache stats.ReaderCleaner,
	topo *topology.Topology,
) *Analyzer {
	return &Analyzer{
		segmentationService: segmentationService,
		bisectionService:    bisectionService,
		girvanNewmanService: girvanNewmanService,
		topologiesIterator:  topologiesIterator,
		statsCache:          statsCache,
		topo:                topo,
	}
}

func (a *Analyzer) AnalyzeSegmentationAlgorithm(q float64) ([]byte, error) {
	if errReading := a.topologiesIterator.ReadTopologies(); errReading != nil {
		return []byte{}, errReading
	}
	var errScanning error
	for a.topologiesIterator.NextTopology() {
		if errScanning = a.topologiesIterator.ScanNetwork(a.topo); errScanning != nil {
			return []byte{}, errScanning
		}
		a.segmentationService.RunSegmentation(q, segmentation.Merging)
		a.topo.Clear()
	}
	segmentationStats := a.statsCache.ReadSegmentationStats()
	a.statsCache.Clear()
	a.topologiesIterator.Reset()
	return segmentationStats.ToCSV(), nil
}

func (a *Analyzer) AnalyzeBisectionAlgorithm(parts int) ([]byte, error) {
	if errReading := a.topologiesIterator.ReadTopologies(); errReading != nil {
		return []byte{}, errReading
	}
	var errScanning error
	for a.topologiesIterator.NextTopology() {
		if errScanning = a.topologiesIterator.ScanNetwork(a.topo); errScanning != nil {
			return []byte{}, errScanning
		}
		a.bisectionService.RunBisection(parts)
		a.topo.Clear()
	}
	segmentationStats := a.statsCache.ReadBisectionStats()
	a.statsCache.Clear()
	a.topologiesIterator.Reset()
	return segmentationStats.ToCSV(), nil
}

func (a *Analyzer) AnalyzeGirvanNewmanAlgorithm() ([]byte, error) {
	if errReading := a.topologiesIterator.ReadTopologies(); errReading != nil {
		return []byte{}, errReading
	}
	var errScanning error
	for a.topologiesIterator.NextTopology() {
		if errScanning = a.topologiesIterator.ScanNetwork(a.topo); errScanning != nil {
			return []byte{}, errScanning
		}
		a.girvanNewmanService.RunPartitioning()
		a.topo.Clear()
	}
	segmentationStats := a.statsCache.ReadGirvanNewmanStats()
	a.statsCache.Clear()
	a.topologiesIterator.Reset()
	return segmentationStats.ToCSV(), nil
}