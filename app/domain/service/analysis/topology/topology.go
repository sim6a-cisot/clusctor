package topology

import "clusctor/app/domain/service/scanning/controller"

type Generator interface {
	GenerateOne(vertexQuantity, randomEdgesQuantity, hostsQuantity int) error
	GenerateMultiple(vertexQuantity, randomEdgesQuantity, hostsQuantity, topologiesQuantity int) error
}

type Iterator interface {
	controller.NetworkScanner
	ReadTopologies() error
	NextTopology() bool
	Reset()
}