package segmentation

import (
	"clusctor/app/domain/model/management/segmentation"
	"clusctor/app/domain/model/stats"
	"clusctor/app/domain/service/management"
	statsService "clusctor/app/domain/service/stats"
	"clusctor/app/pkg/alg/graph"
	"clusctor/app/pkg/alg/prim"
	segmentationAlgorithm "clusctor/app/pkg/alg/segmentation"
	"time"
)

type Service struct {
	topo           management.TopologyGraph
	cache          statsService.SegmentationStatsWriter
	segmentService management.SegmentService
}

func NewRunner(
	topo management.TopologyGraph,
	cache statsService.SegmentationStatsWriter,
	segmentService management.SegmentService,
) *Service {
	return &Service{
		topo:           topo,
		cache:          cache,
		segmentService: segmentService,
	}
}

func (r *Service) RunSegmentation(q float64, step segmentation.Step) {
	if len(r.topo.Links()) < 2 {
		return
	}

	r.segmentService.DeleteSegments()
	startTime := time.Now()
	segmentedGraph := r.runAlgorithm(q, step)
	r.cache.WriteSegmentationStats(stats.NewSegmentation(segmentedGraph, q, time.Since(startTime)))
	r.topo.ApplySegments(segmentedGraph)
}

func (r *Service) runAlgorithm(q float64, step segmentation.Step) *graph.Graph {
	G := r.topo.ExtractGraph()
	registry := r.runAlgorithmCore(G, q, step)
	segments := registry.Segments()
	subGraphs := make([]*graph.Graph, 0, len(segments))
	for s := range segments {
		subGraphs = append(subGraphs, s.ToSubGraph())
	}
	G.SubGraphs = subGraphs
	return G
}

func (Service) runAlgorithmCore(G *graph.Graph, q float64,
	step segmentation.Step) *segmentationAlgorithm.SegmentRegistry {

	T := prim.CalculateMinTree(G)
	alg := segmentationAlgorithm.New(T)
	alg.FormPrimarySegments(G)

	if step == segmentation.PrimarySegments {
		return alg.SegmentRegistry()
	}

	if !alg.AreAllVerticesSegmented() {
		alg.FormSecondarySegments()
	}
	if step == segmentation.SecondarySegments {
		return alg.SegmentRegistry()
	}

	alg.MergeWeakConnectedSegments(q)
	return alg.SegmentRegistry()
}
