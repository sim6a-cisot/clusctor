package management

import (
	"clusctor/app/domain/model/management/mac"
	"clusctor/app/pkg/alg/graph"
)

type DijkstraService interface {
	FindSwitchSequence(mac.MAC, mac.MAC) graph.VertexCollection
	ShowPath(srcMAC, dstMAC mac.MAC)
}
