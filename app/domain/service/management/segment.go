package management

import (
	"clusctor/app/domain/model/management/segmentation"
)

type SegmentService interface {
	DeleteSegments()
}

type BisectionService interface {
	RunBisection(int)
}

type GirvanNewmanService interface {
	RunPartitioning()
}

type SegmentationService interface {
	RunSegmentation(float64, segmentation.Step)
}