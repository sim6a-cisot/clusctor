package prim

import (
	"clusctor/app/domain/model/management/class"
	"clusctor/app/domain/model/management/topology"
	"clusctor/app/domain/model/stats"
	"clusctor/app/domain/service/management"
	statsService "clusctor/app/domain/service/stats"
	"clusctor/app/pkg/alg/prim"
)

type Runner struct {
	topo          management.TopologyGraph
	connectToLink topology.ConnectToLink
	cache         statsService.PrimStatsWriter
}

func NewRunner(
	topo management.TopologyGraph,
	connectToLink topology.ConnectToLink,
	cache statsService.PrimStatsWriter,
) *Runner {
	return &Runner{
		topo:          topo,
		connectToLink: connectToLink,
		cache:         cache,
	}
}

func (r *Runner) RunPrim() float64 {
	if len(r.topo.Links()) == 0 {
		return 0
	}
	G := r.topo.ExtractGraph()
	T := prim.CalculateMinTree(G)
	var ln *topology.Link
	for e := range T {
		ln = r.connectToLink(e.V1.Label, e.V2.Label)
		ln.Classes().Add(class.InTree)
	}
	r.cache.WritePrimStats(stats.NewPrim(G, prim.CalculateMinCost(T)))
	return prim.CalculateMinCost(T)
}
