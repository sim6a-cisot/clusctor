package management

import (
	"clusctor/app/domain/model/management/slice"
	"clusctor/app/domain/model/management/traffic"
	"time"
)

type SliceRegistry interface {
	Register(slice.Collection)
	Clear()
	GetByTrafficType(traffic.Type) *slice.Slice
}

type SliceConstructor interface {
	ConstructSlices([]slice.Description) error
}

type SliceService interface {
	ShowSlice(traffic.Type)
	DeleteSlices()
}

type SliceStatsWriter interface {
	Write(slice.Collection, Devices, time.Duration)
}