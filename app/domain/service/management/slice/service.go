package slice

import (
	"clusctor/app/domain/model/management/class"
	"clusctor/app/domain/model/management/mac"
	"clusctor/app/domain/model/management/metric"
	"clusctor/app/domain/model/management/path"
	"clusctor/app/domain/model/management/slice"
	"clusctor/app/domain/model/management/topology"
	"clusctor/app/domain/model/management/traffic"
	"clusctor/app/domain/model/stats"
	"clusctor/app/domain/service/management"
	statsService "clusctor/app/domain/service/stats"
	"clusctor/app/pkg/alg/dijkstra"
	"sync"
	"time"
)

type Service struct {
	registry    management.SliceRegistry
	topo        *topology.Topology
	cache       statsService.SlicingStatsWriter
	pathFactory path.Factory
}

func NewService(
	registry management.SliceRegistry,
	topo *topology.Topology,
	cache statsService.SlicingStatsWriter,
	pathFactory path.Factory,
) *Service {
	return &Service{
		registry:    registry,
		topo:        topo,
		cache:       cache,
		pathFactory: pathFactory,
	}
}

func (c *Service) DeleteSlices() {
	c.topo.DeleteSegments()
	for _, host := range c.topo.Hosts() {
		c.topo.HostConnector()(host.MAC()).Classes().Clear()
	}
	for _, sw := range c.topo.Switches() {
		c.topo.SwitchConnector()(sw.DPID()).Classes().Clear()
	}
	for _, ln := range c.topo.Links() {
		c.topo.LinkConnector()(ln.SourceDPID(), ln.DestinationDPID()).Classes().Clear()
	}
}

func (c *Service) ShowSlice(t traffic.Type) {
	c.DeleteSlices()

	sliceByTraffic := c.registry.GetByTrafficType(t)
	for _, host := range c.topo.Hosts() {
		if !sliceByTraffic.Hosts().Contains(host) {
			c.topo.HostConnector()(host.MAC()).Classes().Add(class.Invisible)
		}
	}
	for _, sw := range c.topo.Switches() {
		if !sliceByTraffic.Switches().Contains(sw) {
			c.topo.SwitchConnector()(sw.DPID()).Classes().Add(class.Invisible)
		} else {
			c.topo.PlaceSwitchIntoSegment(sw.DPID(), sliceByTraffic.SegmentID())
		}
	}
	for _, ln := range c.topo.Links() {
		if !sliceByTraffic.Links().Contains(ln) {
			c.topo.LinkConnector()(ln.SourceDPID(), ln.DestinationDPID()).Classes().Add(class.Invisible)
		}
	}
}

func (c *Service) ConstructSlices(descriptions []slice.Description) error {
	if len(c.topo.Links()) == 0 {
		return nil
	}
	c.registry.Clear()
	startTime := time.Now()
	slices, err := c.constructSlicesConsistently(descriptions)
	if err != nil {
		return err
	}
	c.cache.WriteSlicingStats(stats.NewSlice(slices, c.topo, time.Since(startTime)))
	c.registry.Register(slices)
	return nil
}

func (c *Service) constructSlicesConsistently(descriptions []slice.Description) (slice.Collection, error) {
	var slices slice.Collection
	for _, d := range descriptions {
		slc, err := c.constructSlice(d)
		if err != nil {
			return slice.Collection{}, err
		}
		slices = append(slices, slc)
	}
	return slices, nil
}

// TODO: to test on multi-core machine
func (c *Service) constructSlicesParallel(descriptions []slice.Description) (slice.Collection, error) {
	descriptionsLen := len(descriptions)

	errsCh := make(chan error, descriptionsLen)
	slicesCh := make(chan *slice.Slice, descriptionsLen)

	wg := sync.WaitGroup{}
	wg.Add(descriptionsLen)
	for _, d := range descriptions {
		func() {
			slc, err := c.constructSlice(d)
			if err != nil {
				errsCh <- err
			} else {
				slicesCh <- slc
			}
			wg.Done()
		}()
	}
	wg.Wait()
	close(errsCh)
	close(slicesCh)

	var errs []error
	var slices slice.Collection
	for err := range errsCh {
		errs = append(errs, err)
	}
	if len(errs) > 0 {
		return slices, errs[0]
	}
	for slc := range slicesCh {
		slices = append(slices, slc)
	}

	return slices, nil
}

func (c *Service) constructSlice(description slice.Description) (*slice.Slice, error) {
	newSlice := slice.NewEmptySlice(description.TrafficType)
	for _, mac1 := range description.Hosts {
		for _, mac2 := range description.Hosts {
			if mac1 != mac2 {
				pathData := NewPathData(mac1, mac2,
					c.fillInSlice(newSlice, description.MetricType(), mac1, mac2),
					description.TrafficType.ServicePort(),
					description.FlowSettings,
				)
				if err := c.install(pathData); err != nil {
					return nil, err
				}
			}
		}
	}
	return newSlice, nil
}

func (c *Service) fillInSlice(newSlice *slice.Slice, metricType metric.Type, mac1 mac.MAC, mac2 mac.MAC) []string {
	host1 := c.topo.HostConnector()(mac1)
	newSlice.Hosts().Add(*host1)
	host2 := c.topo.HostConnector()(mac2)
	newSlice.Hosts().Add(*host2)
	metricInfo := c.topo.MetricInfo()
	storedCurrentMetric := metricInfo.CurrentMetric()
	metricInfo.SetCurrentMetric(metricType)
	G := c.topo.ExtractGraph()
	metricInfo.SetCurrentMetric(storedCurrentMetric)
	switch1 := G.GetOrAdd(host1.AdjacentDPID())
	switch2 := G.GetOrAdd(host2.AdjacentDPID())
	P := dijkstra.FindLocalShortestPath(switch1, switch2, dijkstra.UseOriginalWeight, G)
	var ln *topology.Link
	var sw *topology.Switch
	var i int
	for ; i < len(P)-1; i++ {
		ln = c.topo.LinkConnector()(P[i].Label, P[i+1].Label)
		newSlice.Links().Add(*ln)
		sw = c.topo.SwitchConnector()(P[i].Label)
		newSlice.Switches().Add(*sw)
	}
	sw = c.topo.SwitchConnector()(P[i].Label)
	newSlice.Switches().Add(*sw)
	return P.CollectLabels()
}

func (c *Service) install(pd PathData) error {
	p := c.pathFactory.Installer(pd)
	return p.Install()
}
