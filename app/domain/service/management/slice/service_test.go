package slice_test

import (
	"clusctor/app/domain/model/management/mac"
	"clusctor/app/domain/model/management/metric"
	"clusctor/app/domain/model/management/path"
	"clusctor/app/domain/model/management/path/mock"
	sliceModel "clusctor/app/domain/model/management/slice"
	topologyModel "clusctor/app/domain/model/management/topology"
	"clusctor/app/domain/model/management/traffic"
	"clusctor/app/domain/model/stats"
	"clusctor/app/domain/service/management/slice"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type cacheStub struct{}

func (c *cacheStub) WriteSlicingStats(stats.Slice) {
	// do nothing
}

func TestService_ConstructSlices(t *testing.T) {
	// see test/alg/slice/qos_slice_test_net.svg
	metricInfo := metric.NewInfo()
	topo := topologyModel.New(metricInfo)
	sliceRegistry := slice.NewRegistry()
	installerStub := &mock.Installer{InstallFunc: func() error {
		return nil
	}}
	factoryStub := &mock.Factory{
		InstallerFunc: func() path.Installer {
			return installerStub
		},
	}
	ctor := slice.NewService(
		sliceRegistry,
		topo,
		&cacheStub{},
		factoryStub,
	)
	mac1 := mac.MAC("00:00:00:00:00:01")
	mac2 := mac.MAC("00:00:00:00:00:02")
	mac3 := mac.MAC("00:00:00:00:00:03")
	dpid1 := "00:00:00:00:00:00:00:01"
	dpid2 := "00:00:00:00:00:00:00:02"
	dpid3 := "00:00:00:00:00:00:00:03"
	ln1 := topologyModel.NewLink(0, 0, dpid1, dpid2)
	ln1.SetMetrics(metric.NewMetrics(10, 1, 0, 0))
	ln2 := topologyModel.NewLink(0, 0, dpid1, dpid3)
	ln2.SetMetrics(metric.NewMetrics(10, 1, 0, 0))
	ln3 := topologyModel.NewLink(0, 0, dpid2, dpid3)
	ln3.SetMetrics(metric.NewMetrics(10, 1, 0, 0))
	topo.AddHostIfNotExists(topologyModel.NewHost(mac1, "", dpid1, 0))
	topo.AddHostIfNotExists(topologyModel.NewHost(mac2, "", dpid2, 0))
	topo.AddHostIfNotExists(topologyModel.NewHost(mac3, "", dpid3, 0))
	topo.AddSwitchIfNotExists(topologyModel.NewSwitch(dpid1))
	topo.AddSwitchIfNotExists(topologyModel.NewSwitch(dpid2))
	topo.AddSwitchIfNotExists(topologyModel.NewSwitch(dpid3))
	topo.AddOrUpdateLink(ln1)
	topo.AddOrUpdateLink(ln2)
	topo.AddOrUpdateLink(ln3)
	descriptions := []sliceModel.Description{
		{
			Hosts:       []mac.MAC{mac1, mac2},
			TrafficType: traffic.FileExchange,
		},
		{
			Hosts:       []mac.MAC{mac2, mac3},
			TrafficType: traffic.VideoStream,
		},
	}

	require.NoError(t, ctor.ConstructSlices(descriptions))

	fileExchangeSlice := sliceRegistry.GetByTrafficType(traffic.FileExchange)
	assert.Equal(t, 2, fileExchangeSlice.Hosts().Size())
	assert.Equal(t, 2, fileExchangeSlice.Switches().Size())
	assert.Equal(t, 1, fileExchangeSlice.Links().Size())
	assert.NotNil(t, fileExchangeSlice.Links().Get(dpid1, dpid2))
	videoStreamSlice := sliceRegistry.GetByTrafficType(traffic.VideoStream)
	assert.Equal(t, 2, videoStreamSlice.Hosts().Size())
	assert.Equal(t, 2, videoStreamSlice.Switches().Size())
	assert.Equal(t, 1, videoStreamSlice.Links().Size())
	assert.NotNil(t, videoStreamSlice.Links().Get(dpid2, dpid3))
}
