package slice

import (
	"clusctor/app/domain/model/management/slice"
	"clusctor/app/domain/model/management/traffic"
)

type Registry struct {
	slices slice.Collection
}

func NewRegistry() *Registry {
	return &Registry{}
}

func (r *Registry) Clear() {
	r.slices = nil
}

func (r *Registry) Register(slices slice.Collection) {
	r.slices = slices
}

func (r *Registry) GetByTrafficType(t traffic.Type) *slice.Slice {
	for _, s := range r.slices {
		if s.TrafficType() == t {
			return s
		}
	}
	return nil
}