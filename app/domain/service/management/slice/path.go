package slice

import (
	"clusctor/app/domain/model/management/mac"
	"clusctor/app/domain/model/management/path"
	"clusctor/app/domain/model/management/slice"
	"clusctor/app/pkg/std/net"
)

type PathData struct {
	SrcHost        mac.MAC
	DstHost        mac.MAC
	SwitchSequence []string
	Priority       int
	IdleTimeout    int
	HardTimeout    int
	EthType        string
	IPProto        string
	ServicePort    int
}

func NewPathData(
	srcHost mac.MAC,
	dstHost mac.MAC,
	switchSequence []string,
	servicePort int,
	flowSettings slice.FlowSettings,
) PathData {
	return PathData{
		SrcHost:        srcHost,
		DstHost:        dstHost,
		SwitchSequence: switchSequence,
		ServicePort:    servicePort,
		Priority:       flowSettings.Priority,
		IdleTimeout:    flowSettings.IdleTimeout,
		HardTimeout:    flowSettings.HardTimeout,
		EthType:        net.IPv4,
		IPProto:        net.TCP,
	}
}

func (d PathData) Build(p path.Builder) {
	p.SetMatches(d.EthType, d.IPProto, d.SrcHost, d.DstHost, d.ServicePort)
	p.SetPriority(d.Priority)
	p.SetSwitchSequence(d.SwitchSequence)
	p.SetTimeouts(d.HardTimeout, d.IdleTimeout)
}