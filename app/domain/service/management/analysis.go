package management

type Analyser interface {
	AnalyzeSegmentationAlgorithm(q float64) ([]byte, error)
	AnalyzeBisectionAlgorithm(parts int) ([]byte, error)
	AnalyzeGirvanNewmanAlgorithm() ([]byte, error)
}
