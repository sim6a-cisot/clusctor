package mock

import "clusctor/app/domain/model/management/topology"

type TopologyUpdater struct {
	Switches []topology.Switch
	Hosts    []topology.Host
	Links    []topology.Link
}

func (u *TopologyUpdater) Update(switches []topology.Switch, hosts []topology.Host,
	links []topology.Link) {
	u.Switches = switches
	u.Hosts = hosts
	u.Links = links
}
