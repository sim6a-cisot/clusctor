package management

import (
	"clusctor/app/domain/model/management/mac"
	"clusctor/app/domain/model/management/metric"
	"clusctor/app/domain/model/management/topology"
	"clusctor/app/pkg/alg/graph"
)

type DeviceConnector interface {
	IsHostConnected(mac mac.MAC) bool
}

type Devices interface {
	Switches() []topology.Switch
	Hosts() []topology.Host
	Links() []topology.Link
}

type TopologyUpdater interface {
	Update(switches []topology.Switch, hosts []topology.Host, links []topology.Link)
}

type TopologyUpdaterCleaner interface {
	TopologyUpdater
	Clear()
	ExtractGraph() *graph.Graph
}

type PresentableTopology interface {
	Switches() []topology.Switch
	SwitchMapper() topology.MapSwitch
	Hosts() []topology.Host
	Links() []topology.Link
	MetricInfo() *metric.Info
}

type TopologyMarshaller interface {
	MarshalJSON() ([]byte, error)
}

type TopologyJSONSerializer interface {
	SaveTopologyAsJSON() error
}

type TopologyGraph interface {
	ApplySegments(segmentedGraph *graph.Graph)
	ExtractGraph() *graph.Graph
	MetricInfo() *metric.Info
	Links() []topology.Link
}

type LinkClassDropper interface {
	ClearLinkClasses()
}

type MetricService interface {
	CurrentMetric() metric.Type
	UseCustomMetrics() bool
	SetCurrentMetric(metric.Type)
	SetUseCustomMetrics(bool)
}
