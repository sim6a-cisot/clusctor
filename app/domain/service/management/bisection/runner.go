package bisection

import (
	"clusctor/app/domain/model/stats"
	"clusctor/app/domain/service/management"
	statsService "clusctor/app/domain/service/stats"
	"clusctor/app/pkg/alg/bisection"
	"clusctor/app/pkg/alg/graph"
	"time"
)

type Runner struct {
	topo  management.TopologyGraph
	cache statsService.BisectionStatsWriter
}

func NewRunner(
	topo management.TopologyGraph,
	cache statsService.BisectionStatsWriter,
) *Runner {
	return &Runner{
		topo:  topo,
		cache: cache,
	}
}

func (r *Runner) RunBisection(parts int) {
	if len(r.topo.Links()) < 2 {
		return
	}
	startTime := time.Now()
	segmentedGraph := r.runAlgorithm(r.topo, parts)
	r.cache.WriteBisectionStats(stats.NewBisection(segmentedGraph, parts, time.Since(startTime)))
	r.topo.ApplySegments(segmentedGraph)
}

func (Runner) runAlgorithm(topo management.TopologyGraph, parts int) *graph.Graph {
	G := topo.ExtractGraph()
	alg := bisection.NewAlgorithm()
	G.SubGraphs = alg.BisectOn(parts, G)
	return G
}
