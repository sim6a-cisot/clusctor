package tree

import (
	"clusctor/app/domain/service/management"
)

type Service struct {
	dropper management.LinkClassDropper
}

func NewService(dropper management.LinkClassDropper) *Service {
	return &Service{dropper: dropper}
}

func (s *Service) DeleteTree() {
	s.dropper.ClearLinkClasses()
}

