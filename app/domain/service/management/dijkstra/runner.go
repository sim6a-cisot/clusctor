package dijkstra

import (
	"clusctor/app/application/service"
	"clusctor/app/domain/model/management/class"
	"clusctor/app/domain/model/management/mac"
	"clusctor/app/domain/model/management/topology"
	"clusctor/app/domain/service/management"
	"clusctor/app/pkg/alg/dijkstra"
	"clusctor/app/pkg/alg/graph"
	stdTime "clusctor/app/pkg/std/time"
	"clusctor/app/pkg/stdconv"
	"time"
)

type Service struct {
	topo          management.TopologyGraph
	connectToHost topology.ConnectToHost
	connectToLink topology.ConnectToLink
	treeService   management.TreeService
}

func NewRunner(
	topo management.TopologyGraph,
	connectToHost topology.ConnectToHost,
	connectToLink topology.ConnectToLink,
	treeService management.TreeService,
) *Service {
	return &Service{
		topo:          topo,
		connectToHost: connectToHost,
		connectToLink: connectToLink,
		treeService:   treeService,
	}
}

func (r *Service) FindSwitchSequence(srcMAC, dstMAC mac.MAC) graph.VertexCollection {
	G := r.topo.ExtractGraph()
	srcHost := r.connectToHost(srcMAC)
	dstHost := r.connectToHost(dstMAC)
	src := G.GetOrAdd(srcHost.AdjacentDPID())
	dst := G.GetOrAdd(dstHost.AdjacentDPID())
	startTime := time.Now()
	sequence := dijkstra.FindLocalShortestPath(src, dst, dijkstra.UseOriginalWeight, G)
	service.LogDebug("Dijkstra:" + stdconv.Int64ToStr(stdTime.InMcs(time.Since(startTime))) + "mcs")
	return sequence
}

func (r *Service) ShowPath(srcMAC, dstMAC mac.MAC) {
	r.treeService.DeleteTree()
	switchSequence := r.FindSwitchSequence(srcMAC, dstMAC)
	srcHost := r.connectToHost(srcMAC)
	srcHost.Classes().Add(class.InRoute)
	dstHost := r.connectToHost(dstMAC)
	dstHost.Classes().Add(class.InRoute)
	var ln *topology.Link
	for i := 0; i < len(switchSequence)-1; i++ {
		ln = r.connectToLink(switchSequence[i].Label, switchSequence[i+1].Label)
		ln.Classes().Add(class.InRoute)
	}
}
