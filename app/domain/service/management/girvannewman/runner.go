package girvannewman

import (
	"clusctor/app/domain/model/stats"
	"clusctor/app/domain/service/management"
	statsService "clusctor/app/domain/service/stats"
	"clusctor/app/pkg/alg/girvannewman"
	"time"
)

type Runner struct {
	topo           management.TopologyGraph
	segmentService management.SegmentService
	cache          statsService.GirvanNewmanStatsWriter
}

func NewRunner(
	topo management.TopologyGraph,
	segmentService management.SegmentService,
	cache statsService.GirvanNewmanStatsWriter,
) *Runner {
	return &Runner{
		topo:           topo,
		segmentService: segmentService,
		cache:          cache,
	}
}

func (r *Runner) RunPartitioning() {
	if len(r.topo.Links()) < 2 {
		return
	}
	r.segmentService.DeleteSegments()
	startTime := time.Now()
	G := r.topo.ExtractGraph()
	GCopy := r.topo.ExtractGraph()
	G.SubGraphs = girvannewman.FindOptimalPartition(GCopy)
	r.cache.WriteGirvanNewmanStats(stats.NewGirvanNewman(G, time.Since(startTime)))
	r.topo.ApplySegments(G)
}
