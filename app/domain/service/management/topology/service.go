package topology

import (
	"clusctor/app/domain/model/management/topology"
	"clusctor/app/domain/service/management/topology/cytoscape"
	"clusctor/app/domain/service/scanning/controller"
	topologyDao "clusctor/app/infrastructure/analysis/topology"
)

type Service struct {
	topology       *topology.Topology
	networkScanner controller.NetworkScanner
	creator        topologyDao.Creator
}

func NewService(
	topology *topology.Topology,
	networkScanner controller.NetworkScanner,
	creator topologyDao.Creator,
) *Service {
	return &Service{topology: topology, networkScanner: networkScanner, creator: creator}
}

func (s *Service) UpdateTopology() {
	_ = s.networkScanner.ScanNetwork(s.topology)
}

func (s *Service) SaveTopologyAsJSON() error {
	return s.creator.Create(s.topology)
}

func (s *Service) MarshalJSON() ([]byte, error) {
	cytoscapeTopology := cytoscape.NewTopology(s.topology)
	return cytoscapeTopology.MarshalJSON()
}