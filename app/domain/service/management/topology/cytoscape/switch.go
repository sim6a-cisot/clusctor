package cytoscape

import (
	"clusctor/app/domain/model/management/topology"
	"strconv"
)

type Switch struct {
	ID        string `json:"id"`
	DPID      string `json:"dpid"`
	IsSwitch  bool   `json:"isSwitch"`
	SegmentID int    `json:"segmentId"`

	classes []string `json:"-"`
}

func (s Switch) Data() Data {
	return Data{
		Data:    s,
		Classes: s.classes,
	}
}

func newSwitches(sdnSwitches []topology.Switch) []Switch {
	switches := make([]Switch, 0, len(sdnSwitches))
	for _, sdnSwitch := range sdnSwitches {
		switches = append(switches, newSwitch(sdnSwitch))
	}
	return switches
}

func newSwitch(sdnSwitch topology.Switch) Switch {
	return Switch{
		ID:        newSwitchID(sdnSwitch.ID()),
		DPID:      sdnSwitch.DPID(),
		IsSwitch:  true,
		SegmentID: sdnSwitch.SegmentID(),
		classes:   sdnSwitch.Classes().Strings(),
	}
}

func newSwitchID(id int) string {
	return "S" + strconv.Itoa(id)
}
