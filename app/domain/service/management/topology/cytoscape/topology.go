package cytoscape

import (
	"clusctor/app/domain/service/management"

	jsoniter "github.com/json-iterator/go"
)

type Topology struct {
	switches    []Switch
	hosts       []Host
	hostLinks   []HostLink
	switchLinks []SwitchLink
}

func NewTopology(sdnTopo management.PresentableTopology) *Topology {
	return &Topology{
		switches:    newSwitches(sdnTopo.Switches()),
		hosts:       newHosts(sdnTopo.Hosts()),
		hostLinks:   newHostLinks(sdnTopo.Hosts(), sdnTopo.SwitchMapper()),
		switchLinks: newSwitchLinks(sdnTopo.Links(), *sdnTopo.MetricInfo(), sdnTopo.SwitchMapper()),
	}
}

func (t *Topology) MarshalJSON() ([]byte, error) {
	elements := make([]Data, 0, len(t.switches))

	for _, sw := range t.switches {
		elements = append(elements, sw.Data())
	}
	for _, host := range t.hosts {
		elements = append(elements, host.Data())
	}
	for _, hostLink := range t.hostLinks {
		elements = append(elements, hostLink.Data())
	}
	for _, switchLink := range t.switchLinks {
		elements = append(elements, switchLink.Data())
	}

	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	return json.Marshal(&elements)
}
