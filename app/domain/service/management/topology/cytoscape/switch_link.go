package cytoscape

import (
	"clusctor/app/domain/model/management/metric"
	"clusctor/app/domain/model/management/topology"
	"clusctor/app/pkg/stdconv"
	"clusctor/app/pkg/strutil"
	"fmt"
)

type SwitchLink struct {
	ID     string `json:"id"`
	Source string `json:"source"`
	Target string `json:"target"`

	IsSwitchLink    bool   `json:"isSwitchLink"`
	Label           string `json:"label"`
	SourceDPID      string `json:"srcdpid"`
	DestinationDPID string `json:"dstdpid"`

	Bandwidth      float64 `json:"bandwidth"`
	Delay          float64 `json:"delay"`
	PacketLossRate float64 `json:"packetLossRate"`
	AdminMetric    float64 `json:"adminMetric"`

	classes []string `json:"-"`
}

func (ln SwitchLink) Data() Data {
	return Data{
		Data:    ln,
		Classes: ln.classes,
	}
}

func newSwitchLinks(
	sdnLinks []topology.Link,
	metricInfo metric.Info,
	sdnSwitchMapper topology.MapSwitch) []SwitchLink {

	switchLinks := make([]SwitchLink, 0, len(sdnLinks))
	for _, sdnLink := range sdnLinks {
		switchLinks = append(switchLinks, newSwitchLink(sdnLink, metricInfo, sdnSwitchMapper))
	}
	return switchLinks
}

func newSwitchLink(
	sdnLink topology.Link,
	metricInfo metric.Info,
	sdnSwitchMapper topology.MapSwitch) SwitchLink {

	sourceSwitch := sdnSwitchMapper(sdnLink.SourceDPID())
	targetSwitch := sdnSwitchMapper(sdnLink.DestinationDPID())
	sourceID := newSwitchID(sourceSwitch.ID())
	targetID := newSwitchID(targetSwitch.ID())
	metrics := sdnLink.Metrics()
	return SwitchLink{
		ID:     sourceID + "-" + targetID,
		Source: sourceID,
		Target: targetID,

		IsSwitchLink:    true,
		Label:           createMetricLabel(metrics, metricInfo),
		SourceDPID:      sdnLink.SourceDPID(),
		DestinationDPID: sdnLink.DestinationDPID(),

		Bandwidth:      stdconv.RoundFloat64(metrics.Bandwidth(), 2),
		Delay:          stdconv.RoundFloat64(metrics.Delay(), 2),
		PacketLossRate: stdconv.RoundFloat64(metrics.PacketLossRate(), 2),
		AdminMetric:    metrics.AdminMetric(),

		classes: sdnLink.Classes().Strings(),
	}
}

func createMetricLabel(m metric.Metrics, info metric.Info) string {
	var strValue string

	switch info.CurrentMetric() {
	case metric.Bandwidth:
		strValue = strutil.RepresentAsBitsPerSecond(m.Bandwidth())
	case metric.Delay:
		strValue = fmt.Sprintf("%.0f ms", m.Delay())
	case metric.PacketLossRate:
		strValue = fmt.Sprintf("%.0f %%", m.PacketLossRate())
	case metric.Admin:
		strValue = fmt.Sprintf("%.0f A", m.AdminMetric())
	case metric.LARAC:
		strValue = fmt.Sprintf("%.1f L", m.LARAC(info.LARACLambda()))
	case metric.LARACCost:
		strValue = fmt.Sprintf("%.1f LC", m.LARACCost(info.LARACCostGamma()))
	case metric.QoSParams:
		strValue = fmt.Sprintf("(%.0fMbit/s)(%.0fms)(%.0f%%)", m.Bandwidth(), m.Delay(), m.PacketLossRate())
	}

	return strValue
}