package cytoscape

type Data struct {
	Data    interface{} `json:"data"`
	Classes []string    `json:"classes,omitempty"`
}