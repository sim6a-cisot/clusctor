package cytoscape_test

import (
	"clusctor/app/domain/service/management/topology/cytoscape"
	"clusctor/app/pkg/strutil"
	jsoniter "github.com/json-iterator/go"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestShowHost(t *testing.T) {
	data := cytoscape.Data{
		Data: cytoscape.Host{
			ID:     "1",
			IPV4:   "10.0.0.1",
			MAC:    "00:00:00:00:00:01",
			IsHost: true,
		},
	}

	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	hostJSON, _ := json.Marshal(&data)

	want := strutil.RemoveWhiteSpaces(`
	{
		"data": {
		"id": "1",
			"ip": "10.0.0.1",
			"mac": "00:00:00:00:00:01",
			"isHost": true
		}
	}
	`)
	assert.Equal(t, want, string(hostJSON))
}
