package cytoscape_test

import (
	"clusctor/app/domain/service/management/topology/cytoscape"
	"clusctor/app/pkg/strutil"
	jsoniter "github.com/json-iterator/go"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestShowSwitchLink(t *testing.T) {
	data := cytoscape.Data{
		Data: cytoscape.SwitchLink{
			ID: "1",
			Source: "1",
			Target: "2",
			SourceDPID: "00:00:00:00:00:00:00:01",
			DestinationDPID: "00:00:00:00:00:00:00:02",
			Label: "100 Mbit/s",
			Bandwidth: 40,
			Delay: 20,
			PacketLossRate: 5,
			AdminMetric: 100,
			IsSwitchLink: true,
		},
	}

	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	switchLinkJSON, _ := json.Marshal(&data)

	want := strutil.RemoveWhiteSpaces(`
	{
		"data": {
			"id": "1",
			"source": "1",
			"target": "2",
			"isSwitchLink": true,
			"label": "100*Mbit/s",
			"srcdpid": "00:00:00:00:00:00:00:01",
			"dstdpid": "00:00:00:00:00:00:00:02",
			"bandwidth": 40,
			"delay": 20,
			"packetLossRate": 5,
			"adminMetric": 100
		}
	}
	`)
	assert.Equal(t, want, string(switchLinkJSON))
}

func TestShowSwitchLinkInRoute(t *testing.T) {
	data := cytoscape.Data{
		Data: cytoscape.SwitchLink{
			ID: "1",
			Source: "1",
			Target: "2",
			SourceDPID: "00:00:00:00:00:00:00:01",
			DestinationDPID: "00:00:00:00:00:00:00:02",
			Label: "100 Mbit/s",
			Bandwidth: 40,
			Delay: 20,
			PacketLossRate: 5,
			AdminMetric: 100,
			IsSwitchLink: true,
		},
		Classes: []string{
			"inRoute",
		},
	}

	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	switchLinkJSON, _ := json.Marshal(&data)

	want := strutil.RemoveWhiteSpaces(`
	{
		"data": {
			"id": "1",
			"source": "1",
			"target": "2",
			"isSwitchLink": true,
			"label": "100*Mbit/s",
			"srcdpid": "00:00:00:00:00:00:00:01",
			"dstdpid": "00:00:00:00:00:00:00:02",
			"bandwidth": 40,
			"delay": 20,
			"packetLossRate": 5,
			"adminMetric": 100
		},
		"classes": [
			"inRoute"
		]
	}
	`)

	assert.Equal(t, want, string(switchLinkJSON))
}