package cytoscape

import (
	"clusctor/app/domain/model/management/class"
	"clusctor/app/domain/model/management/mac"
	"clusctor/app/domain/model/management/topology"
	"strconv"
)

type Host struct {
	ID     string  `json:"id"`
	IPV4   string  `json:"ip"`
	MAC    mac.MAC `json:"mac"`
	IsHost bool    `json:"isHost"`

	classes []string `json:"-"`
}

func (h Host) Data() Data {
	return Data{
		Data:    h,
		Classes: h.classes,
	}
}

func newHosts(sdnHosts []topology.Host) []Host {
	hosts := make([]Host, 0, len(sdnHosts))
	for _, sdnHost := range sdnHosts {
		hosts = append(hosts, newHost(sdnHost))
	}
	return hosts
}

func newHost(sdnHost topology.Host) Host {
	return Host{
		ID:      newHostID(sdnHost.ID()),
		IPV4:    sdnHost.IPv4(),
		MAC:     sdnHost.MAC(),
		IsHost:  true,
		classes: filterHostClasses(sdnHost.Classes()).Strings(),
	}
}

func newHostID(id int) string {
	return "H" + strconv.Itoa(id)
}

func filterHostClasses(collection *class.Collection) class.Collection {
	filtered := class.NewCollection()
	for _, c := range *collection {
		if c == class.InRoute {
			continue
		}
		filtered.Add(c)
	}
	return filtered
}
