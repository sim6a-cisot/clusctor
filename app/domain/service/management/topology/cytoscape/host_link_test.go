package cytoscape_test

import (
	"clusctor/app/domain/service/management/topology/cytoscape"
	"clusctor/app/pkg/strutil"
	jsoniter "github.com/json-iterator/go"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestShowHostLink(t *testing.T) {
	data := cytoscape.Data{
		Data: cytoscape.HostLink{
			ID:     "H1-S1",
			SourceID: "H1",
			TargetID: "S1",
			IsHostLink: true,
		},
	}

	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	hostLinkJSON, _ := json.Marshal(&data)

	want := strutil.RemoveWhiteSpaces(`
	{
		"data": {
			"id": "H1-S1",
			"source": "H1",
			"target": "S1",
			"isHostLink": true
		}
	}
	`)
	assert.Equal(t, want, string(hostLinkJSON))
}

func TestShowHostLinkInRoute(t *testing.T) {
	data := cytoscape.Data{
		Data: cytoscape.HostLink{
			ID:     "H1-S1",
			SourceID: "H1",
			TargetID: "S1",
			IsHostLink: true,
		},
		Classes: []string{
			"inRoute",
		},
	}

	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	hostLinkJSON, _ := json.Marshal(&data)

	want := strutil.RemoveWhiteSpaces(`
	{
		"data": {
			"id": "H1-S1",
			"source": "H1",
			"target": "S1",
			"isHostLink": true
		},
		"classes": [
			"inRoute"
		]
	}
	`)
	assert.Equal(t, want, string(hostLinkJSON))
}