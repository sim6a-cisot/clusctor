package cytoscape_test

import (
	"clusctor/app/domain/model/management/metric"
	"clusctor/app/domain/model/management/topology"
	"clusctor/app/domain/service/management/topology/cytoscape"
	"clusctor/app/pkg/strutil"
	"testing"

	jsoniter "github.com/json-iterator/go"
	"github.com/stretchr/testify/assert"
)

type testPresentableTopology struct {
	switches     []topology.Switch
	hosts        []topology.Host
	switchMapper topology.MapSwitch
	links        []topology.Link
	metricInfo   metric.Info
}

func (t *testPresentableTopology) Switches() []topology.Switch {
	return t.switches
}

func (t *testPresentableTopology) Hosts() []topology.Host {
	return t.hosts
}

func (t *testPresentableTopology) SwitchMapper() topology.MapSwitch {
	return t.switchMapper
}

func (t *testPresentableTopology) Links() []topology.Link {
	return t.links
}

func (t *testPresentableTopology) MetricInfo() *metric.Info {
	return &t.metricInfo
}

func TestTopology_MarshalJSON(t *testing.T) {
	sourceSwitch := topology.NewSwitch("00:00:00:00:00:00:00:01")
	sourceSwitch.SetID(1)
	targetSwitch := topology.NewSwitch("00:00:00:00:00:00:00:02")
	targetSwitch.SetID(2)

	ln := topology.NewLink(1, 1, sourceSwitch.DPID(), targetSwitch.DPID())
	ln.Classes().Add("inTree")

	host := topology.NewHost(
		"00:00:00:00:00:01",
		"10.0.0.1",
		"00:00:00:00:00:00:00:01",
		1,
	)
	host.SetID(1)

	metricInfo := metric.NewInfo()
	metricInfo.SetCurrentMetric(metric.Bandwidth)
	presentableTopology := &testPresentableTopology{
		switches: []topology.Switch{
			sourceSwitch,
			targetSwitch,
		},
		hosts: []topology.Host{
			host,
		},
		switchMapper: func(dpid string) topology.Switch {
			switch dpid {
			case sourceSwitch.DPID():
				return sourceSwitch
			case targetSwitch.DPID():
				return targetSwitch
			}
			return topology.NewSwitch("")
		},
		links: []topology.Link{
			ln,
		},
		metricInfo: *metricInfo,
	}

	topo := cytoscape.NewTopology(presentableTopology)

	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	topoJSON, _ := json.Marshal(topo)
	want := strutil.RemoveWhiteSpaces(`
	[
		{
			"data": {
				"id": "S1",
				"dpid": "00:00:00:00:00:00:00:01",
				"isSwitch": true,
				"segmentId": 0
			}
		},
		{
			"data": {
				"id": "S2",
				"dpid": "00:00:00:00:00:00:00:02",
				"isSwitch": true,
				"segmentId": 0
			}
		},
		{
			"data": {
				"id": "H1",
				"ip": "10.0.0.1",
       			"mac": "00:00:00:00:00:01",
				"isHost": true
			}
		},
		{
			"data": {
				"id": "H1-S1",
				"source": "H1",
				"target": "S1",
				"isHostLink": true
			}
		},
		{
			"data": {
				"id": "S1-S2",
				"source": "S1",
				"target": "S2",
				"isSwitchLink": true,
				"label": "8.00*Bit/s",
				"srcdpid": "00:00:00:00:00:00:00:01",
				"dstdpid": "00:00:00:00:00:00:00:02",
				"bandwidth": 1,
				"delay": 1,
				"packetLossRate": 0,
				"adminMetric": 1
			},
			"classes": [
				"inTree"
			]
		}
	]
	`)
	assert.Equal(t, want, string(topoJSON))
}
