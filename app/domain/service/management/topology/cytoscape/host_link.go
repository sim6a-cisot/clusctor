package cytoscape

import (
	"clusctor/app/domain/model/management/topology"
)

type HostLink struct {
	ID         string `json:"id"`
	SourceID   string `json:"source"`
	TargetID   string `json:"target"`
	IsHostLink bool   `json:"isHostLink"`

	classes []string `json:"-"`
}

func (ln HostLink) Data() Data {
	return Data{
		Data:    ln,
		Classes: ln.classes,
	}
}

func newHostLinks(sdnHosts []topology.Host, sdnSwitchMapper topology.MapSwitch) []HostLink {
	hostLinks := make([]HostLink, 0, len(sdnHosts))
	for _, sdnHost := range sdnHosts {
		hostLinks = append(hostLinks, newHostLink(sdnHost, sdnSwitchMapper))
	}
	return hostLinks
}

func newHostLink(sdnHost topology.Host, sdnSwitchMapper topology.MapSwitch) HostLink {
	sourceID := newHostID(sdnHost.ID())
	targetSwitch := sdnSwitchMapper(sdnHost.AdjacentDPID())
	targetID := newSwitchID(targetSwitch.ID())
	return HostLink{
		ID:         sourceID + "-" + targetID,
		SourceID:   sourceID,
		TargetID:   targetID,
		IsHostLink: true,

		classes: sdnHost.Classes().Strings(),
	}
}
