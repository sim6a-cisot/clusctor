package cytoscape_test

import (
	"clusctor/app/domain/service/management/topology/cytoscape"
	"clusctor/app/pkg/strutil"
	jsoniter "github.com/json-iterator/go"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestShowSwitch(t *testing.T) {
	data := cytoscape.Data{
		Data: cytoscape.Switch{
			ID:        "1",
			DPID:      "00:00:00:00:00:00:00:01",
			IsSwitch:  true,
			SegmentID: 0,
		},
	}

	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	switchJSON, _ := json.Marshal(&data)

	want := strutil.RemoveWhiteSpaces(`
	{
		"data": {
			"id": "1",
			"dpid": "00:00:00:00:00:00:00:01",
			"isSwitch": true,
			"segmentId": 0
		}
	}
	`)
	assert.Equal(t, want, string(switchJSON))

}
