package management

type TreeService interface {
	DeleteTree()
}

type PrimService interface {
	RunPrim() float64
}