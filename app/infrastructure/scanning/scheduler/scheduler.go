package scheduler

import (
	"clusctor/app/domain/model/scanning/schedule"
	"github.com/jasonlvhit/gocron"
)

func Run(schedule schedule.Schedule) {
	for _, task := range schedule {
		if task.StartImmediately {
			task.JobFunc()
		}
		gocron.Every(uint64(task.IntervalSec)).Seconds().Do(task.JobFunc)
	}
	gocron.Start()
}