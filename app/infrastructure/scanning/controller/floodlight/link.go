package floodlight

import (
	"clusctor/app/domain/model/management/topology"
	"clusctor/app/pkg/std"
	"clusctor/app/pkg/std/http"
	jsoniter "github.com/json-iterator/go"
	"io/ioutil"
	nethttp "net/http"
)

type link struct {
	SrcSwitch string `json:"src-of_switch"`
	DstSwitch string `json:"dst-of_switch"`
	SrcPort   int    `json:"src-port"`
	DstPort   int    `json:"dst-port"`
}

func (ln link) toSDNLink() topology.Link {
	return topology.NewLink(ln.SrcPort, ln.DstPort, ln.SrcSwitch, ln.DstSwitch)
}

type links []link

func (ll links) toSDNLinks() []topology.Link {
	var sdnLinks []topology.Link
	for _, link := range ll {
		sdnLinks = append(sdnLinks, link.toSDNLink())
	}
	return sdnLinks
}

func newLinkAPI(client std.HttpClient, host string, port int) *linkAPI {
	return &linkAPI{
		basePath: http.CreateAddress(host, port),
		client:   client,
	}
}

type linkAPI struct {
	basePath string
	client   std.HttpClient
}

func (api *linkAPI) fetchLinks() (lns links, err error) {
	request, _ := nethttp.NewRequest("GET",
		api.basePath+"/wm/topology/lns/json", nil)
	response, errHTTP := api.client.Do(request)
	if errHTTP != nil {
		return make(links, 0), errHTTP
	}
	defer func() {
		if errClosing := response.Body.Close(); errClosing != nil {
			err = errClosing
		}
	}()

	responseBodyBytes, errBodyReading := ioutil.ReadAll(response.Body)
	if errBodyReading != nil {
		return make(links, 0), errBodyReading
	}

	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	return lns, json.Unmarshal(responseBodyBytes, &lns)
}
