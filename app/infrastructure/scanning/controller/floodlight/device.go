package floodlight

import (
	"clusctor/app/domain/model/management/mac"
	"clusctor/app/domain/model/management/topology"
	"clusctor/app/pkg/std"
	"clusctor/app/pkg/std/http"
	jsoniter "github.com/json-iterator/go"
	"io/ioutil"
	nethttp "net/http"
)

type device struct {
	MAC             []string `json:"mac"`
	IPv4            []string `json:"ipv4"`
	AttachmentPoint []struct {
		SwitchDPID string `json:"switchDPID"`
		Port       int    `json:"port"`
	} `json:"attachmentPoint"`
}

func (d device) toSDNHost() topology.Host {
	return topology.NewHost(
		mac.MAC(d.MAC[0]),
		d.IPv4[0],
		d.AttachmentPoint[0].SwitchDPID,
		d.AttachmentPoint[0].Port,
	)
}

type validatableDevice struct {
	device
}

func (d validatableDevice) toSDNHost() topology.Host {
	isIncorrectMAC := len(d.MAC) != 1
	isIncorrectIPv4 := len(d.IPv4) != 1
	isIncorrectAttachmentPoint := len(d.AttachmentPoint) != 1
	incorrectData := "INCORRECT DATA"
	if isIncorrectAttachmentPoint || isIncorrectIPv4 || isIncorrectMAC {
		return topology.NewHost(mac.MAC(incorrectData), incorrectData, incorrectData, 0)
	}
	return d.device.toSDNHost()
}

type devices []validatableDevice

func (dd devices) toSDNHosts() []topology.Host {
	var sdnHosts []topology.Host
	for _, host := range dd {
		sdnHosts = append(sdnHosts, host.toSDNHost())
	}
	return sdnHosts
}

func newDeviceAPI(client std.HttpClient, host string, port int) *deviceAPI {
	return &deviceAPI{
		basePath: http.CreateAddress(host, port),
		client:   client,
	}
}

type deviceAPI struct {
	basePath string
	client   std.HttpClient
}

func (api *deviceAPI) fetchDevices() (devs devices, err error) {
	request, _ := nethttp.NewRequest("GET",
		api.basePath+"/wm/device/", nil)
	response, errHTTP := api.client.Do(request)
	if errHTTP != nil {
		return make(devices, 0), errHTTP
	}
	defer func() {
		if errClosing := response.Body.Close(); errClosing != nil {
			err = errClosing
		}
	}()

	responseBodyBytes, errBodyReading := ioutil.ReadAll(response.Body)
	if errBodyReading != nil {
		return make(devices, 0), errBodyReading
	}

	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	return devs, json.Unmarshal(responseBodyBytes, &devs)
}
