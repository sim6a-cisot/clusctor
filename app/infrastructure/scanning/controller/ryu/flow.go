package ryu

import (
	"bytes"
	"clusctor/app/domain/model/management/flow"
	"clusctor/app/pkg/std"
	"clusctor/app/pkg/std/http"
	"fmt"
	"io/ioutil"
	netHttp "net/http"
	"strconv"
	"strings"

	jsoniter "github.com/json-iterator/go"
)

/* Pushing Flow */

func newPushingFlow(f flow.Flow) PushingFlow {
	return PushingFlow{
		DPID:        dpidBase16StringToBase10Int(f.DPID),
		Cookie:      hexCookieToIntCookie(f.Cookie),
		IdleTimeout: f.IdleTimeout,
		HardTimeout: f.HardTimeout,
		Priority:    f.Priority,
		Match:       newPushingFlowMatch(f.Matches),
		Actions:     newPushingFlowPacketOutActions(f.OutputAction.Output),
	}
}

type PushingFlow struct {
	DPID        int64               `json:"dpid"`
	Cookie      int64               `json:"cookie"`
	IdleTimeout int                 `json:"idle_timeout"`
	HardTimeout int                 `json:"hard_timeout"`
	Priority    int                 `json:"priority"`
	Match       PushingFlowMatch    `json:"match"`
	Actions     []PushingFlowAction `json:"actions"`
}

func newPushingFlowMatch(m flow.Matches) PushingFlowMatch {
	return PushingFlowMatch{
		NwSrc:   m.SrcIP,
		NwDst:   m.DstIP,
		EthType: ethTypeFromString(m.EthType),
		IPProto: ipProtoFromString(m.IPProto),
		TCPSrc:  m.TCPSrcPort,
		TCPDst:  m.TCPDstPort,
	}
}

type PushingFlowMatch struct {
	NwSrc   string `json:"nw_src,omitempty"`
	NwDst   string `json:"nw_dst,omitempty"`
	EthType int    `json:"eth_type,omitempty"`
	IPProto int    `json:"ip_proto,omitempty"`
	TCPSrc  int    `json:"tcp_src,omitempty"`
	TCPDst  int    `json:"tcp_dst,omitempty"`
}

func newPushingFlowPacketOutActions(outPort int) []PushingFlowAction {
	return []PushingFlowAction{
		{
			Type: "OUTPUT",
			Port: outPort,
		},
	}
}

type PushingFlowAction struct {
	Type string `json:"type"`
	Port int    `json:"port"`
}

/* Reading Flow */

type SwitchFlows map[string]ReadingFlowCollection

type ReadingFlow struct {
	HardTimeout int                `json:"hard_timeout"`
	Match       ReadingFlowMatch   `json:"match"`
	IdleTimeout int                `json:"idle_timeout"`
	Priority    int                `json:"priority"`
	Actions     ReadingFlowActions `json:"actions"`
	Cookie      int64              `json:"cookie"`
}

func (f ReadingFlow) toFlow(dpid string) flow.Flow {
	return flow.Flow{
		DPID: dpid,

		Cookie:       intCookieToHexCookie(f.Cookie),
		Priority:     f.Priority,
		IdleTimeout:  f.IdleTimeout,
		HardTimeout:  f.HardTimeout,
		Matches:      f.Match.toMatches(),
		OutputAction: f.Actions.toOutputAction(),
	}
}

type ReadingFlowCollection []ReadingFlow

func (c ReadingFlowCollection) toFlowCollection(dpid string) flow.Collection {
	flows := make(flow.Collection, 0, len(c))
	for _, ryuFlow := range c {
		flows = append(flows, ryuFlow.toFlow(dpid))
	}
	return flows
}

type ReadingFlowActions []string

const output = "OUTPUT:"

func (a ReadingFlowActions) toOutputAction() flow.Action {
	if len(a) == 0 {
		return flow.Action{}
	}
	ryuAction := a[0]
	if !strings.HasPrefix(ryuAction, output) {
		return flow.Action{}
	}
	outputPortString := strings.Replace(ryuAction, output, "", 1)
	outputPortInt, _ := strconv.Atoi(outputPortString)
	return flow.Action{
		Output: outputPortInt,
	}
}

type ReadingFlowMatch struct {
	DlType  int    `json:"dl_type"`
	NwProto int    `json:"nw_proto"`
	TpSrc   int    `json:"tp_src"`
	TpDst   int    `json:"tp_dst"`
	NwDst   string `json:"nw_dst"`
	NwSrc   string `json:"nw_src"`
}

func (m ReadingFlowMatch) toMatches() flow.Matches {
	return flow.Matches{
		SrcIP:      m.NwSrc,
		DstIP:      m.NwDst,
		IPProto:    ipProtoFromInt(m.NwProto),
		TCPSrcPort: m.TpSrc,
		TCPDstPort: m.TpDst,
		EthType:    ethTypeFromInt(m.DlType),
	}
}

/* API */

func newFlowAPI(client std.HttpClient, host string, port int) *flowAPI {
	return &flowAPI{
		basePath: http.CreateAddress(host, port),
		client:   client,
	}
}

type flowAPI struct {
	basePath string
	client   std.HttpClient
}

func (api flowAPI) push(f flow.Flow) (err error) {
	ryuFlow := newPushingFlow(f)
	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	ryuFlowJSON, _ := json.Marshal(&ryuFlow)
	request, _ := netHttp.NewRequest(
		"POST",
		api.basePath+"/stats/flowentry/add",
		bytes.NewBuffer(ryuFlowJSON),
	)
	request.Header.Set(http.ContentType, http.ApplicationJSON)
	response, errHTTP := api.client.Do(request)
	if errHTTP != nil {
		return errHTTP
	}
	defer func() {
		if errClosing := response.Body.Close(); errClosing != nil {
			err = errClosing
		}
	}()
	if response.StatusCode == netHttp.StatusOK {
		return nil
	}
	responseBodyBytes, errBodyReading := ioutil.ReadAll(response.Body)
	if errBodyReading != nil {
		return errBodyReading
	}
	return fmt.Errorf("http status %d; message: %s",
		response.StatusCode, string(responseBodyBytes))
}

func (api flowAPI) readDatapathFlows(dpid string) (flows ReadingFlowCollection, err error) {
	request, _ := netHttp.NewRequest("GET",
		api.basePath+"/stats/flow/"+dpid, nil)
	response, errHTTP := api.client.Do(request)
	if errHTTP != nil {
		return make(ReadingFlowCollection, 0), errHTTP
	}
	defer func() {
		if errClosing := response.Body.Close(); errClosing != nil {
			err = errClosing
		}
	}()

	responseBodyBytes, errBodyReading := ioutil.ReadAll(response.Body)
	if errBodyReading != nil {
		return make(ReadingFlowCollection, 0), errBodyReading
	}

	ryuSwitchFlows := make(SwitchFlows)
	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	if errUnmarshal := json.Unmarshal(responseBodyBytes, &ryuSwitchFlows); errUnmarshal != nil {
		return make(ReadingFlowCollection, 0), errUnmarshal
	}
	ryuFlows := ryuSwitchFlows[dpid]
	return ryuFlows, nil
}
