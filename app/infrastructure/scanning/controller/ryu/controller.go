package ryu

import (
	"clusctor/app/domain/model/management/flow"
	"clusctor/app/domain/service/management"
	"clusctor/app/pkg/std"
	"strconv"
)

func NewController(host string, port int, client std.HttpClient) *Controller {
	return &Controller{
		switchAPI: newSwitchAPI(client, host, port),
		linkAPI:   newLinkAPI(client, host, port),
		hostAPI:   newHostAPI(client, host, port),
		flowAPI:   newFlowAPI(client, host, port),
	}
}

type Controller struct {
	switchAPI *switchAPI
	linkAPI   *linkAPI
	hostAPI   *hostAPI
	flowAPI   *flowAPI
}

func (c *Controller) ScanNetwork(topology management.TopologyUpdater) error {
	switches, errSwitchesFetching := c.switchAPI.fetchSwitches()
	if errSwitchesFetching != nil {
		return errSwitchesFetching
	}

	hosts, errHostFetching := c.hostAPI.fetchHosts()
	if errHostFetching != nil {
		return errHostFetching
	}

	links, errLinksFetching := c.linkAPI.fetchLinks()
	if errLinksFetching != nil {
		return errLinksFetching
	}

	topology.Update(
		switches.toSDNSwitches(),
		hosts.toSDNHosts(),
		links.removeDuplicates().toSDNLinks(),
	)

	return nil
}

func (c *Controller) PushFlows(flows []flow.Flow) error {
	for _, flow := range flows {
		if err := c.flowAPI.push(flow); err != nil {
			return err
		}
	}
	return nil
}

func (c *Controller) ReadFlows() (flow.Collection, error) {
	flows := flow.Collection{}
	switches, errFetchSwitches := c.switchAPI.fetchSwitches()
	if errFetchSwitches != nil {
		return flow.Collection{}, errFetchSwitches
	}
	for _, dpid := range switches.dpids() {
		datapathFlows, errReadDatapathFlows := c.readDatapathFlows(dpid)
		if errReadDatapathFlows != nil {
			return flow.Collection{}, errReadDatapathFlows
		}
		flows = append(flows, datapathFlows...)
	}
	return flows, nil
}

func (c *Controller) readDatapathFlows(dpidBase16String string) (flow.Collection, error) {
	dpidInt64 := dpidBase16StringToBase10Int(dpidBase16String)
	dpidBase10String := strconv.FormatInt(dpidInt64, 10)
	ryuFlows, errReadDatapathFlows := c.flowAPI.readDatapathFlows(dpidBase10String)
	if errReadDatapathFlows != nil {
		return flow.Collection{}, errReadDatapathFlows
	}
	return ryuFlows.toFlowCollection(insertColonsInDPID(dpidBase16String)), nil
}
