package ryu

import (
	"clusctor/app/domain/model/management/topology"
	"clusctor/app/pkg/std"
	"clusctor/app/pkg/std/http"
	"io/ioutil"
	nethttp "net/http"

	jsoniter "github.com/json-iterator/go"
)

type _switch struct {
	DPID string `json:"dpid"`
}

func (sw _switch) toSDNSwitch() topology.Switch {
	return topology.NewSwitch(insertColonsInDPID(sw.DPID))
}

type switches []_switch

func (ss switches) dpids() []string {
	dpids := make([]string, 0, len(ss))
	for _, s := range ss {
		dpids = append(dpids, s.DPID)
	}
	return dpids
}

func (ss switches) toSDNSwitches() []topology.Switch {
	var sdnSwitches []topology.Switch
	for _, sw := range ss {
		sdnSwitches = append(sdnSwitches, sw.toSDNSwitch())
	}
	return sdnSwitches
}

func newSwitchAPI(client std.HttpClient, host string, port int) *switchAPI {
	return &switchAPI{
		basePath: http.CreateAddress(host, port),
		client:   client,
	}
}

type switchAPI struct {
	basePath string
	client   std.HttpClient
}

func (api *switchAPI) fetchSwitches() (sws switches, err error) {
	request, _ := nethttp.NewRequest("GET",
		api.basePath+"/v1.0/topology/switches", nil)
	response, errHTTP := api.client.Do(request)
	if errHTTP != nil {
		return make(switches, 0), errHTTP
	}
	defer func() {
		if errClosing := response.Body.Close(); errClosing != nil {
			err = errClosing
		}
	}()

	responseBodyBytes, errBodyReading := ioutil.ReadAll(response.Body)
	if errBodyReading != nil {
		return make(switches, 0), errBodyReading
	}

	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	return sws, json.Unmarshal(responseBodyBytes, &sws)
}
