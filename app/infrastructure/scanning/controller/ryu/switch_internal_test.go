package ryu

import (
	"bytes"
	"clusctor/app/domain/model/management/topology"
	"clusctor/app/pkg/std/mock"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_switch_toSDNSwitch(t *testing.T) {
	sw := _switch{
		DPID: "0000000000000001",
	}
	wantSw := topology.NewSwitch("00:00:00:00:00:00:00:01")
	assert.Equal(t, wantSw, sw.toSDNSwitch())
}

func Test_switchAPI_fetchSwitches_Success(t *testing.T) {
	bodyContent := `
	[
    	{
    	   "dpid":"0000000000000001",
    	   "ports":[
    	      {
    	         "name":"s1-eth1",
    	         "dpid":"0000000000000001",
    	         "port_no":"00000001",
    	         "hw_addr":"b2:05:77:96:b9:73"
    	      },
    	      {
    	         "name":"s1-eth2",
    	         "dpid":"0000000000000001",
    	         "port_no":"00000002",
    	         "hw_addr":"42:50:27:4e:b0:91"
    	      }
    	   ]
    	},
    	{
    	   "dpid":"0000000000000002",
    	   "ports":[
    	      {
    	         "name":"s2-eth1",
    	         "dpid":"0000000000000002",
    	         "port_no":"00000001",
    	         "hw_addr":"ae:ad:a2:bb:75:6f"
    	      },
    	      {
    	         "name":"s2-eth2",
    	         "dpid":"0000000000000002",
    	         "port_no":"00000002",
    	         "hw_addr":"fa:a3:2b:91:67:ec"
    	      },
    	      {
    	         "name":"s2-eth3",
    	         "dpid":"0000000000000002",
    	         "port_no":"00000003",
    	         "hw_addr":"7e:35:df:61:9b:1f"
    	      }
    	   ]
		}
	]
	`
	bodyReader := ioutil.NopCloser(bytes.NewReader([]byte(bodyContent)))
	response := http.Response{
		StatusCode: 200,
		Body:       bodyReader,
	}
	httpClientMock := mock.HttpClient{
		DoFunc: func(request *http.Request) (*http.Response, error) {
			return &response, nil
		},
	}
	wantSwitches := switches{
		{
			DPID: "0000000000000001",
		},
		{
			DPID: "0000000000000002",
		},
	}
	api := newSwitchAPI(&httpClientMock, "", 0)
	switches, err := api.fetchSwitches()
	assert.NoError(t, err)
	assert.Equal(t, wantSwitches, switches)
}
