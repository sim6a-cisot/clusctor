package ryu

import (
	"clusctor/app/domain/model/management/topology"
	"clusctor/app/pkg/std"
	"clusctor/app/pkg/std/http"
	"io/ioutil"
	nethttp "net/http"
	"strconv"

	jsoniter "github.com/json-iterator/go"
)

type link struct {
	Source      linkNode `json:"src"`
	Destination linkNode `json:"dst"`
}

type linkNode struct {
	DPID   string `json:"dpid"`
	PortNo string `json:"port_no"`
}

func (ln link) toSDNLink() topology.Link {
	srcPort, _ := strconv.Atoi(ln.Source.PortNo)
	dstPort, _ := strconv.Atoi(ln.Destination.PortNo)
	return topology.NewLink(srcPort, dstPort,
		insertColonsInDPID(ln.Source.DPID), insertColonsInDPID(ln.Destination.DPID))
}

func (ln link) straightHash() string {
	return ln.Source.DPID + ln.Destination.DPID
}

func (ln link) backHash() string {
	return ln.Destination.DPID + ln.Source.DPID
}

type links []link

func (ll links) toSDNLinks() []topology.Link {
	var sdnLinks []topology.Link
	for _, link := range ll {
		sdnLinks = append(sdnLinks, link.toSDNLink())
	}
	return sdnLinks
}

func (ll links) removeDuplicates() links {
	filtered := make(links, 0)
	filter := make(map[string]bool)
	for _, ln := range ll {
		if _, ok := filter[ln.straightHash()]; ok {
			continue
		}
		filter[ln.straightHash()] = true
		filter[ln.backHash()] = true
		filtered = append(filtered, ln)
	}
	return filtered
}

func newLinkAPI(client std.HttpClient, host string, port int) *linkAPI {
	return &linkAPI{
		basePath: http.CreateAddress(host, port),
		client:   client,
	}
}

type linkAPI struct {
	basePath string
	client   std.HttpClient
}

func (api *linkAPI) fetchLinks() (lns links, err error) {
	request, _ := nethttp.NewRequest("GET",
		api.basePath+"/v1.0/topology/links", nil)
	response, errHTTP := api.client.Do(request)
	if errHTTP != nil {
		return make(links, 0), errHTTP
	}
	defer func() {
		if errClosing := response.Body.Close(); errClosing != nil {
			err = errClosing
		}
	}()

	responseBodyBytes, errBodyReading := ioutil.ReadAll(response.Body)
	if errBodyReading != nil {
		return make(links, 0), errBodyReading
	}

	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	return lns, json.Unmarshal(responseBodyBytes, &lns)
}
