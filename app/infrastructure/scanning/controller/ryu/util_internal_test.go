package ryu

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_hexCookieToIntCookie(t *testing.T) {
	testCses := []struct {
		hex     string
		wantInt int64
	}{
		{
			hex:     "0x11111111",
			wantInt: int64(286331153),
		},
		{
			hex:     "000000000000000b",
			wantInt: int64(11),
		},
	}

	for _, tt := range testCses {
		t.Run(tt.hex, func(t *testing.T) {
			assert.Equal(t, tt.wantInt, hexCookieToIntCookie(tt.hex))
		})
	}
}

func Test_intCookieToHexCookie(t *testing.T) {
	testCses := []struct {
		name     string
		intValue int64
		wantHex  string
	}{
		{
			name:     "286331153",
			intValue: int64(286331153),
			wantHex:  "0x11111111",
		},
	}

	for _, tt := range testCses {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.wantHex, intCookieToHexCookie(tt.intValue))
		})
	}
}

func Test_dpidBase16StringToBase10Int(t *testing.T) {
	testCases := []struct {
		dpid        string
		wantIntDPID int64
	}{
		{
			dpid:        "00:00:00:00:00:00:00:09",
			wantIntDPID: 9,
		},
		{
			dpid:        "00:00:00:00:00:00:00:0a",
			wantIntDPID: 10,
		},
		{
			dpid:        "00:00:00:00:00:00:00:0b",
			wantIntDPID: 11,
		},
	}
	for _, tt := range testCases {
		t.Run(tt.dpid, func(t *testing.T) {
			assert.Equal(t, tt.wantIntDPID, dpidBase16StringToBase10Int(tt.dpid))
		})
	}
}
