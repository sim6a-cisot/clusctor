package ryu

import (
	"bytes"
	"clusctor/app/domain/model/management/topology"
	"clusctor/app/pkg/std/mock"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_links_toSDNLinks(t *testing.T) {
	ln := link{
		Source: linkNode{
			DPID:   "0000000000000002",
			PortNo: "00000003",
		},
		Destination: linkNode{
			DPID:   "0000000000000001",
			PortNo: "00000001",
		},
	}
	wantLn := topology.NewLink(3, 1, "00:00:00:00:00:00:00:02", "00:00:00:00:00:00:00:01")
	assert.Equal(t, wantLn, ln.toSDNLink())
}

func Test_linkAPI_fetchLinks_Success(t *testing.T) {
	bodyContent := `
	[
    	{
    	   "dst":{
    	      "name":"s1-eth1",
    	      "dpid":"0000000000000001",
    	      "port_no":"00000001",
    	      "hw_addr":"b2:05:77:96:b9:73"
    	   },
    	   "src":{
    	      "name":"s2-eth3",
    	      "dpid":"0000000000000002",
    	      "port_no":"00000003",
    	      "hw_addr":"7e:35:df:61:9b:1f"
    	   }
    	},
    	{
    	   "dst":{
    	      "name":"s2-eth3",
    	      "dpid":"0000000000000002",
    	      "port_no":"00000003",
    	      "hw_addr":"7e:35:df:61:9b:1f"
    	   },
    	   "src":{
    	      "name":"s1-eth1",
    	      "dpid":"0000000000000001",
    	      "port_no":"00000001",
    	      "hw_addr":"b2:05:77:96:b9:73"
    	   }
		}
	]
	`
	bodyReader := ioutil.NopCloser(bytes.NewReader([]byte(bodyContent)))
	response := http.Response{
		StatusCode: 200,
		Body:       bodyReader,
	}
	httpClientMock := mock.HttpClient{
		DoFunc: func(request *http.Request) (*http.Response, error) {
			return &response, nil
		},
	}
	wantLinks := links{
		{
			Source: linkNode{
				DPID:   "0000000000000002",
				PortNo: "00000003",
			},
			Destination: linkNode{
				DPID:   "0000000000000001",
				PortNo: "00000001",
			},
		},
		{
			Source: linkNode{
				DPID:   "0000000000000001",
				PortNo: "00000001",
			},
			Destination: linkNode{
				DPID:   "0000000000000002",
				PortNo: "00000003",
			},
		},
	}
	api := newLinkAPI(&httpClientMock, "", 0)
	lns, err := api.fetchLinks()
	assert.NoError(t, err)
	assert.Equal(t, wantLinks, lns)
}
