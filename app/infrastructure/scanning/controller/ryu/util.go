package ryu

import (
	"fmt"
	"strconv"
	"strings"
)

func insertColonsInDPID(dpid string) string {
	dpidLength := len(dpid)
	sb := strings.Builder{}
	for i, r := range dpid {
		sb.WriteRune(r)
		if i%2 != 0 && i < dpidLength-1 {
			sb.WriteRune(':')
		}
	}
	return sb.String()
}

func dpidBase16StringToBase10Int(dpidBase16String string) int64 {
	dpidWOColons := strings.Replace(dpidBase16String, ":", "", -1)
	number, _ := strconv.ParseInt(dpidWOColons, 16, 64)
	return number
}

func hexCookieToIntCookie(hex string) int64 {
	numberStr := strings.Replace(hex, "0x", "", -1)
	number, _ := strconv.ParseInt(numberStr, 16, 64)
	return number
}

func intCookieToHexCookie(number int64) string {
	return fmt.Sprintf("0x%x", number)
}

func ethTypeFromString(t string) int {
	switch t {
	case "ipv4":
		return 2048
	default:
		return 0
	}
}

func ethTypeFromInt(t int) string {
	switch t {
	case 2048:
		return "ipv4"
	default:
		return ""
	}
}

func ipProtoFromString(proto string) int {
	switch proto {
	case "tcp":
		return 6
	case "udp":
		return 17
	default:
		return 0
	}
}

func ipProtoFromInt(proto int) string {
	switch proto {
	case 6:
		return "tcp"
	case 17:
		return "udp"
	default:
		return ""
	}
}
