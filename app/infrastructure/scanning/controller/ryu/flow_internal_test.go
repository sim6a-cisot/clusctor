package ryu

import (
	"clusctor/app/domain/model/management/flow"
	"clusctor/app/pkg/strutil"
	"testing"

	jsoniter "github.com/json-iterator/go"
	"github.com/stretchr/testify/assert"
)

func Test_newPushingFlowMatch(t *testing.T) {
	actions := flow.Action{Output: 2}
	matches := flow.Matches{
		SrcIP:      "10.0.0.1",
		DstIP:      "10.0.0.2",
		IPProto:    "tcp",
		EthType:    "ipv4",
		TCPSrcPort: 3473,
		TCPDstPort: 8080,
	}
	f := flow.Flow{
		DPID:         "00:00:00:00:00:00:00:0b",
		Cookie:       "0x11111111",
		Priority:     3000,
		IdleTimeout:  30,
		HardTimeout:  40,
		Matches:      matches,
		OutputAction: actions,
	}
	ryuFlow := newPushingFlow(f)

	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	ryuFlowJSON, _ := json.Marshal(&ryuFlow)

	want := strutil.RemoveWhiteSpaces(`
	{
		"dpid": 11,
		"cookie": 286331153,
		"idle_timeout": 30,
		"hard_timeout": 40,
		"priority": 3000,
		"match": {
			"nw_src": "10.0.0.1",
			"nw_dst": "10.0.0.2",
			"eth_type": 2048,
			"ip_proto": 6,
			"tcp_src": 3473,
			"tcp_dst": 8080
		},
		"actions": [
			{
				"type": "OUTPUT",
				"port": 2
			}
		]
	}
	`)
	assert.Equal(t, want, string(ryuFlowJSON))
}

func TestReadingFlowActions_toOutputAction(t *testing.T) {
	testCases := []struct {
		name           string
		ryuActions     ReadingFlowActions
		wantOutputPort int
	}{
		{
			name: "OUTPUT:10",
			ryuActions: ReadingFlowActions{
				"OUTPUT:10",
			},
			wantOutputPort: 10,
		},
		{
			name: "incorrect action",
			ryuActions: ReadingFlowActions{
				"OUTPUT10",
			},
			wantOutputPort: 0,
		},
		{
			name: "OUTPUT:10 and OUTPUT:20",
			ryuActions: ReadingFlowActions{
				"OUTPUT:10",
				"OUTPUT:20",
			},
			wantOutputPort: 10,
		},
		{
			name:           "no actions",
			ryuActions:     ReadingFlowActions{},
			wantOutputPort: 0,
		},
	}

	for _, tt := range testCases {
		t.Run(tt.name, func(t *testing.T) {
			action := tt.ryuActions.toOutputAction()
			assert.Equal(t, tt.wantOutputPort, action.Output)
		})
	}
}
