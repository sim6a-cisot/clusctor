package ryu_test

import (
	"bytes"
	"clusctor/app/domain/model/management/flow"
	"clusctor/app/domain/model/management/mac"
	"clusctor/app/domain/model/management/topology"
	"clusctor/app/domain/service/management/mock"
	"clusctor/app/infrastructure/scanning/controller/ryu"
	stdmock "clusctor/app/pkg/std/mock"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func createSwitchesResponse() http.Response {
	bodyContent := `
	[
    	{
    	   "dpid":"0000000000000001",
    	   "ports":[
    	      {
    	         "name":"s1-eth1",
    	         "dpid":"0000000000000001",
    	         "port_no":"00000001",
    	         "hw_addr":"b2:05:77:96:b9:73"
    	      },
    	      {
    	         "name":"s1-eth2",
    	         "dpid":"0000000000000001",
    	         "port_no":"00000002",
    	         "hw_addr":"42:50:27:4e:b0:91"
    	      }
    	   ]
    	},
    	{
    	   "dpid":"0000000000000002",
    	   "ports":[
    	      {
    	         "name":"s2-eth1",
    	         "dpid":"0000000000000002",
    	         "port_no":"00000001",
    	         "hw_addr":"ae:ad:a2:bb:75:6f"
    	      },
    	      {
    	         "name":"s2-eth2",
    	         "dpid":"0000000000000002",
    	         "port_no":"00000002",
    	         "hw_addr":"fa:a3:2b:91:67:ec"
    	      },
    	      {
    	         "name":"s2-eth3",
    	         "dpid":"0000000000000002",
    	         "port_no":"00000003",
    	         "hw_addr":"7e:35:df:61:9b:1f"
    	      }
    	   ]
		}
	]
	`
	bodyReader := ioutil.NopCloser(bytes.NewReader([]byte(bodyContent)))
	return http.Response{
		StatusCode: 200,
		Body:       bodyReader,
	}
}

func createHostsResponse() http.Response {
	bodyContent := `
	[
    	{
    	   "mac":"00:00:00:00:00:03",
    	   "ipv6":[
    	      "::",
    	      "fe80::200:ff:fe00:3"
    	   ],
    	   "ipv4":[
    	      "10.0.0.3"
    	   ],
    	   "port":{
    	      "name":"s3-eth1",
    	      "dpid":"0000000000000003",
    	      "port_no":"00000001",
    	      "hw_addr":"b2:a9:a8:a8:2f:ed"
    	   }
    	},
    	{
    	   "mac":"00:00:00:00:00:04",
    	   "ipv6":[
    	      "::",
    	      "fe80::200:ff:fe00:4"
    	   ],
    	   "ipv4":[
    	      "10.0.0.4"
    	   ],
    	   "port":{
    	      "name":"s3-eth2",
    	      "dpid":"0000000000000003",
    	      "port_no":"00000002",
    	      "hw_addr":"36:ab:10:c0:49:19"
    	   }
		}
	]
	`
	bodyReader := ioutil.NopCloser(bytes.NewReader([]byte(bodyContent)))
	return http.Response{
		StatusCode: 200,
		Body:       bodyReader,
	}
}

func createLinksResponse() http.Response {
	bodyContent := `
	[
    	{
    	   "dst":{
    	      "name":"s1-eth1",
    	      "dpid":"0000000000000001",
    	      "port_no":"00000001",
    	      "hw_addr":"b2:05:77:96:b9:73"
    	   },
    	   "src":{
    	      "name":"s2-eth3",
    	      "dpid":"0000000000000002",
    	      "port_no":"00000003",
    	      "hw_addr":"7e:35:df:61:9b:1f"
    	   }
    	},
    	{
    	   "dst":{
    	      "name":"s2-eth3",
    	      "dpid":"0000000000000002",
    	      "port_no":"00000003",
    	      "hw_addr":"7e:35:df:61:9b:1f"
    	   },
    	   "src":{
    	      "name":"s1-eth1",
    	      "dpid":"0000000000000001",
    	      "port_no":"00000001",
    	      "hw_addr":"b2:05:77:96:b9:73"
    	   }
		}
	]
	`
	bodyReader := ioutil.NopCloser(bytes.NewReader([]byte(bodyContent)))
	return http.Response{
		StatusCode: 200,
		Body:       bodyReader,
	}
}
func TestController_ScanNetwork(t *testing.T) {
	httpClientMock := stdmock.HttpClient{
		DoFunc: func(request *http.Request) (*http.Response, error) {
			var response http.Response
			switch request.URL.Path {
			case "/v1.0/topology/hosts":
				response = createHostsResponse()
			case "/v1.0/topology/links":
				response = createLinksResponse()
			case "/v1.0/topology/switches":
				response = createSwitchesResponse()
			}
			return &response, nil
		},
	}

	wantHosts := []topology.Host{
		topology.NewHost(mac.MAC("00:00:00:00:00:03"), "10.0.0.3", "00:00:00:00:00:00:00:03", 1),
		topology.NewHost(mac.MAC("00:00:00:00:00:04"), "10.0.0.4", "00:00:00:00:00:00:00:03", 2),
	}

	wantSwitches := []topology.Switch{
		topology.NewSwitch("00:00:00:00:00:00:00:01"),
		topology.NewSwitch("00:00:00:00:00:00:00:02"),
	}

	wantLinks := []topology.Link{
		topology.NewLink(3, 1, "00:00:00:00:00:00:00:02", "00:00:00:00:00:00:00:01"),
	}

	topologyUpdaterMock := mock.TopologyUpdater{}

	ctrl := ryu.NewController("", 0, &httpClientMock)
	err := ctrl.ScanNetwork(&topologyUpdaterMock)
	require.NoError(t, err)

	assert.Equal(t, wantHosts, topologyUpdaterMock.Hosts)
	assert.Equal(t, wantSwitches, topologyUpdaterMock.Switches)
	assert.Equal(t, wantLinks, topologyUpdaterMock.Links)
}

func createDPDI1FlowsResponse() http.Response {
	bodyContent := `
	{
		"1": [
			{
				"hard_timeout": 30,
				"match": {
					"dl_type": 2048,
					"nw_proto": 6,
					"tp_src": 9000,
					"tp_dst": 8080,
					"nw_dst": "10.0.0.2",
					"nw_src": "10.0.0.1"
				},
				"duration_nsec": 249000000,
				"idle_timeout": 30,
				"priority": 111,
				"length": 120,
				"packet_count": 0,
				"table_id": 0,
				"actions": [
					"OUTPUT:10"
				],
				"flags": 0,
				"duration_sec": 21,
				"byte_count": 0,
				"cookie": 286331153
			}
		]
	}
	`
	bodyReader := ioutil.NopCloser(bytes.NewReader([]byte(bodyContent)))
	return http.Response{
		StatusCode: 200,
		Body:       bodyReader,
	}
}

func createDPDI2FlowsResponse() http.Response {
	bodyContent := `
	{
		"2": [
			{
				"hard_timeout": 30,
				"match": {
					"dl_type": 2048,
					"nw_proto": 6,
					"tp_src": 8080,
					"tp_dst": 9000,
					"nw_dst": "10.0.0.1",
					"nw_src": "10.0.0.2"
				},
				"duration_nsec": 249000000,
				"idle_timeout": 30,
				"priority": 111,
				"length": 120,
				"packet_count": 0,
				"table_id": 0,
				"actions": [
					"OUTPUT:5"
				],
				"flags": 0,
				"duration_sec": 21,
				"byte_count": 0,
				"cookie": 286331153
			}
		]
	}
	`
	bodyReader := ioutil.NopCloser(bytes.NewReader([]byte(bodyContent)))
	return http.Response{
		StatusCode: 200,
		Body:       bodyReader,
	}
}
func TestController_ReadFlows(t *testing.T) {
	httpClientMock := stdmock.HttpClient{
		DoFunc: func(request *http.Request) (*http.Response, error) {
			var response http.Response
			switch request.URL.Path {
			case "/v1.0/topology/switches":
				response = createSwitchesResponse()
			case "/stats/flow/1":
				response = createDPDI1FlowsResponse()
			case "/stats/flow/2":
				response = createDPDI2FlowsResponse()
			}
			return &response, nil
		},
	}

	wantFlows := flow.Collection{
		flow.Flow{
			DPID:        "00:00:00:00:00:00:00:01",
			Cookie:      "0x11111111",
			Priority:    111,
			IdleTimeout: 30,
			HardTimeout: 30,
			Matches: flow.Matches{
				SrcIP:      "10.0.0.1",
				DstIP:      "10.0.0.2",
				IPProto:    "tcp",
				EthType:    "ipv4",
				TCPSrcPort: 9000,
				TCPDstPort: 8080,
			},
			OutputAction: flow.Action{Output: 10},
		},
		flow.Flow{
			DPID:        "00:00:00:00:00:00:00:02",
			Cookie:      "0x11111111",
			Priority:    111,
			IdleTimeout: 30,
			HardTimeout: 30,
			Matches: flow.Matches{
				SrcIP:      "10.0.0.2",
				DstIP:      "10.0.0.1",
				IPProto:    "tcp",
				EthType:    "ipv4",
				TCPSrcPort: 8080,
				TCPDstPort: 9000,
			},
			OutputAction: flow.Action{Output: 5},
		},
	}

	ctrl := ryu.NewController("", 0, &httpClientMock)
	flows, err := ctrl.ReadFlows()
	require.NoError(t, err)

	assert.Equal(t, wantFlows, flows)
}
