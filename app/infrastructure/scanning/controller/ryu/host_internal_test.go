package ryu

import (
	"bytes"
	"clusctor/app/domain/model/management/mac"
	"clusctor/app/domain/model/management/topology"
	"clusctor/app/pkg/std/mock"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_host_toSDNHost(t *testing.T) {
	hst := host{
		MAC:  "00:00:00:00:00:03",
		IPv4: []string{"10.0.0.3"},
		Port: hostPort{
			PortNo: "00000001",
			DPID:   "0000000000000003",
		},
	}
	wantHst := topology.NewHost(mac.MAC("00:00:00:00:00:03"), "10.0.0.3", "00:00:00:00:00:00:00:03", 1)

	assert.Equal(t, wantHst, hst.toSDNHost())
}

func Test_hostAPI_fetchHosts_Success(t *testing.T) {
	bodyContent := `
	[
    	{
    	   "mac":"00:00:00:00:00:03",
    	   "ipv6":[
    	      "::",
    	      "fe80::200:ff:fe00:3"
    	   ],
    	   "ipv4":[
    	      "10.0.0.3"
    	   ],
    	   "port":{
    	      "name":"s3-eth1",
    	      "dpid":"0000000000000003",
    	      "port_no":"00000001",
    	      "hw_addr":"b2:a9:a8:a8:2f:ed"
    	   }
    	},
    	{
    	   "mac":"00:00:00:00:00:04",
    	   "ipv6":[
    	      "::",
    	      "fe80::200:ff:fe00:4"
    	   ],
    	   "ipv4":[
    	      "10.0.0.4"
    	   ],
    	   "port":{
    	      "name":"s3-eth2",
    	      "dpid":"0000000000000003",
    	      "port_no":"00000002",
    	      "hw_addr":"36:ab:10:c0:49:19"
    	   }
		}
	]
	`
	bodyReader := ioutil.NopCloser(bytes.NewReader([]byte(bodyContent)))
	response := http.Response{
		StatusCode: 200,
		Body:       bodyReader,
	}
	httpClientMock := mock.HttpClient{
		DoFunc: func(request *http.Request) (*http.Response, error) {
			return &response, nil
		},
	}
	wantHosts := hosts{
		{
			MAC:  "00:00:00:00:00:03",
			IPv4: []string{"10.0.0.3"},
			Port: hostPort{
				PortNo: "00000001",
				DPID:   "0000000000000003",
			},
		},
		{
			MAC:  "00:00:00:00:00:04",
			IPv4: []string{"10.0.0.4"},
			Port: hostPort{
				PortNo: "00000002",
				DPID:   "0000000000000003",
			},
		},
	}
	api := newHostAPI(&httpClientMock, "", 0)
	hsts, err := api.fetchHosts()
	assert.NoError(t, err)
	assert.Equal(t, wantHosts, hsts)
}
