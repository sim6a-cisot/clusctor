package ryu

import (
	"clusctor/app/domain/model/management/mac"
	"clusctor/app/domain/model/management/topology"
	"clusctor/app/pkg/std"
	"clusctor/app/pkg/std/http"
	"io/ioutil"
	nethttp "net/http"
	"strconv"

	jsoniter "github.com/json-iterator/go"
)

type host struct {
	MAC  string   `json:"mac"`
	IPv4 []string `json:"ipv4"`
	Port hostPort `json:"port"`
}

type hostPort struct {
	PortNo string `json:"port_no"`
	DPID   string `json:"dpid"`
}

func (h host) toSDNHost() topology.Host {
	var ipv4 string
	if len(h.IPv4) > 0 {
		ipv4 = h.IPv4[0]
	}
	port, _ := strconv.Atoi(h.Port.PortNo)
	return topology.NewHost(mac.MAC(h.MAC), ipv4, insertColonsInDPID(h.Port.DPID), port)
}

type hosts []host

func (hh hosts) toSDNHosts() []topology.Host {
	var sdnHosts []topology.Host
	for _, host := range hh {
		sdnHosts = append(sdnHosts, host.toSDNHost())
	}
	return sdnHosts
}

func newHostAPI(client std.HttpClient, host string, port int) *hostAPI {
	return &hostAPI{
		basePath: http.CreateAddress(host, port),
		client:   client,
	}
}

type hostAPI struct {
	basePath string
	client   std.HttpClient
}

func (api *hostAPI) fetchHosts() (hsts hosts, err error) {
	request, _ := nethttp.NewRequest("GET",
		api.basePath+"/v1.0/topology/hosts", nil)
	response, errHTTP := api.client.Do(request)
	if errHTTP != nil {
		return make(hosts, 0), errHTTP
	}
	defer func() {
		if errClosing := response.Body.Close(); errClosing != nil {
			err = errClosing
		}
	}()

	responseBodyBytes, errBodyReading := ioutil.ReadAll(response.Body)
	if errBodyReading != nil {
		return make(hosts, 0), errBodyReading
	}

	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	return hsts, json.Unmarshal(responseBodyBytes, &hsts)
}
