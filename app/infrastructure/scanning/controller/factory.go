package controller

import (
	"clusctor/app/application/service"
	"clusctor/app/domain/service/scanning/controller"
	"clusctor/app/infrastructure/analysis/topology"
	"clusctor/app/infrastructure/analysis/virtualctrl"
	"clusctor/app/infrastructure/scanning/controller/aruba"
	"clusctor/app/infrastructure/scanning/controller/floodlight"
	"clusctor/app/infrastructure/scanning/controller/pox"
	"clusctor/app/infrastructure/scanning/controller/ryu"
	"clusctor/app/pkg/std"
)

const (
	virtualCtrl    = "virtual"
	poxCtrl        = "pox"
	arubaCtrl      = "aruba"
	floodlightCtrl = "floodlight"
	ryuCtrl        = "ryu"
)

func New(
	cfg controller.Config, httpClient std.HttpClient, t std.Time,
	topologyStorage topology.Storage, logger service.Logger) (ctrl controller.Client) {
	switch cfg.ControllerName {
	case virtualCtrl:
		ctrl = virtualctrl.New(topologyStorage)
	case poxCtrl:
		ctrl = pox.NewController(cfg.Host, cfg.Port, httpClient)
	case arubaCtrl:
		ctrl = aruba.NewController(cfg.Host, cfg.Port, httpClient, t, logger)
	case floodlightCtrl:
		ctrl = floodlight.NewController(cfg.Host, cfg.Port, httpClient)
	case ryuCtrl:
		ctrl = ryu.NewController(cfg.Host, cfg.Port, httpClient)
	}
	return ctrl
}
