package node

import (
	"clusctor/app/domain/model/management/mac"
	"clusctor/app/domain/model/management/topology"
)

type Node struct {
	DPID string `json:"dpid"`
	IP   string `json:"ip"`
	MAC  string `json:"mac"`
	Port int    `json:"port"`
}

func (n Node) toTopologyHost() topology.Host {
	return topology.NewHost(mac.MAC(n.MAC), n.IP, n.DPID, n.Port)
}

type Collection []Node

func (c Collection) ToTopologyHosts() []topology.Host {
	var topologyHosts []topology.Host
	for _, host := range c {
		topologyHosts = append(topologyHosts, host.toTopologyHost())
	}
	return topologyHosts
}
