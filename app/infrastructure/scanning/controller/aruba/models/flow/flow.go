package flow

import (
	"clusctor/app/domain/model/management/flow"
	"encoding/json"
)

type OF3FlowCollection []OF3Flow

func (c OF3FlowCollection) String() string {
	bytes, _ := json.Marshal(c)
	return string(bytes)
}

type OF3Flow struct {
	Cookie       string          `json:"cookie"`
	Priority     int             `json:"priority"`
	IdleTimeout  int             `json:"idle_timeout"`
	HardTimeout  int             `json:"hard_timeout"`
	Matches      MatchCollection `json:"match"`
	Instructions []Instructions  `json:"instructions"`
}

func (f OF3Flow) String() string {
	flowInBytes, _ := json.Marshal(&f)
	return string(flowInBytes)
}

func ConvertToOF3Flow(f flow.Flow) OF3Flow {
	return OF3Flow{
		Cookie:      f.Cookie,
		Priority:    f.Priority,
		IdleTimeout: f.IdleTimeout,
		HardTimeout: f.HardTimeout,
		Matches:     NewMatches(f.Matches),
		Instructions: []Instructions{
			{
				Actions: []Action{
					NewAction(f.OutputAction),
				},
			},
		},
	}
}

type Match map[string]interface{}

const (
	ipv4Src = "ipv4_src"
	ipv4Dst = "ipv4_dst"
	ethType = "eth_type"
	ipProto = "ip_proto"
	tcpSrc  = "tcp_src"
	tcpDst  = "tcp_dst"
)

func NewMatches(m flow.Matches) MatchCollection {
	var match MatchCollection
	if m.SrcIP != "" {
		match = append(match, Match{ipv4Src: m.SrcIP})
	}
	if m.DstIP != "" {
		match = append(match, Match{ipv4Dst: m.DstIP})
	}
	if m.EthType != "" {
		match = append(match, Match{ethType: m.EthType})
	}
	if m.IPProto != "" {
		match = append(match, Match{ipProto: m.IPProto})
	}
	if m.TCPSrcPort != 0 {
		match = append(match, Match{tcpSrc: m.TCPSrcPort})
	}
	if m.TCPDstPort != 0 {
		match = append(match, Match{tcpDst: m.TCPDstPort})
	}
	return match
}

type MatchCollection []Match

func (c MatchCollection) Get(match string) (interface{}, bool) {
	for _, m := range c {
		if value, ok := m[match]; ok {
			return value, true
		}
	}
	return nil, false
}

type Action map[string]interface{}

const output = "output"

func NewAction(a flow.Action) Action {
	return Action{
		output: a.Output,
	}
}

type Instructions struct {
	Actions []Action `json:"apply_actions"`
}
