package flow

import (
	"clusctor/app/domain/model/management/flow"
)

func ConvertFromOF3Flow(switchDPID string, of3Flow OF3Flow) flow.Flow {
	return flow.Flow{
		DPID:         switchDPID,
		Cookie:       of3Flow.Cookie,
		Priority:     of3Flow.Priority,
		IdleTimeout:  of3Flow.IdleTimeout,
		HardTimeout:  of3Flow.HardTimeout,
		Matches:      convertFromOF3Matches(of3Flow.Matches),
		OutputAction: convertFromOF3Instructions(of3Flow.Instructions),
	}
}

func convertFromOF3Matches(c MatchCollection) flow.Matches {
	flowMatches := flow.Matches{}
	if srcIP, ok := c.Get(ipv4Src); ok {
		flowMatches.SrcIP = srcIP.(string)
	}
	if dstIP, ok := c.Get(ipv4Dst); ok {
		flowMatches.DstIP = dstIP.(string)
	}
	if ipProto, ok := c.Get(ipProto); ok {
		flowMatches.IPProto = ipProto.(string)
	}
	if ethType, ok := c.Get(ethType); ok {
		flowMatches.EthType = ethType.(string)
	}
	if tcpSrcPort, ok := c.Get(tcpSrc); ok {
		flowMatches.TCPSrcPort = int(tcpSrcPort.(float64))
	}
	if tcpDstPort, ok := c.Get(tcpDst); ok {
		flowMatches.TCPDstPort = int(tcpDstPort.(float64))
	}
	return flowMatches
}

func convertFromOF3Instructions(instructions []Instructions) flow.Action {
	if len(instructions) == 0 {
		return flow.Action{}
	}
	instruction := instructions[0]
	if len(instruction.Actions) == 0 {
		return flow.Action{}
	}
	action := instruction.Actions[0]
	flowAction := flow.Action{}
	if value, ok := action[output]; ok {
		if floatValue, ok := value.(float64); ok {
			flowAction.Output = int(floatValue)
		}
	}
	return flowAction
}
