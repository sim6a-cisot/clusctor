package flow_test

import (
	"clusctor/app/domain/model/management/flow"
	flow13 "clusctor/app/infrastructure/scanning/controller/aruba/models/flow"
	"clusctor/app/pkg/strutil"
	jsoniter "github.com/json-iterator/go"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewOF3Flow(t *testing.T) {
	actions := flow.Action{Output: 2}
	matches := flow.Matches{
		SrcIP:   "10.0.0.1",
		DstIP:   "10.0.0.2",
		IPProto: "tcp",
		EthType: "ipv4",
	}
	f := flow.Flow{
		DPID:         "",
		Cookie:       "0x2031987",
		Priority:     30000,
		IdleTimeout:  60,
		HardTimeout:  0,
		Matches:      matches,
		OutputAction: actions,
	}
	arubaFlow := flow13.ConvertToOF3Flow(f)

	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	arubaFlowJSON, _ := json.Marshal(&arubaFlow)

	want := strutil.RemoveWhiteSpaces(`
	{
		"cookie": "0x2031987",
		"priority": 30000,
		"idle_timeout": 60,
		"hard_timeout": 0,
		"match": [
			{"ipv4_src": "10.0.0.1"},
			{"ipv4_dst": "10.0.0.2"},
			{"eth_type": "ipv4"},
			{"ip_proto": "tcp"}
		],
		"instructions": [
			{
				"apply_actions": [
					{"output": 2}
				]
			}
		]
	}
	`)
	assert.Equal(t, want, string(arubaFlowJSON))
}
