package of_switch

import (
	"clusctor/app/domain/model/management/topology"
)

type OFSwitch struct {
	DPID string `json:"dpid"`
}

type Collection []OFSwitch

func (sw OFSwitch) toTopologySwitch() topology.Switch {
	return topology.NewSwitch(sw.DPID)
}

func (c Collection) ToTopologySwitches() []topology.Switch {
	var topologySwitches []topology.Switch
	for _, sw := range c {
		topologySwitches = append(topologySwitches, sw.toTopologySwitch())
	}
	return topologySwitches
}