package port

import "fmt"

type StatsCollection struct {
	Version string `json:"version"`
	Stats []*Stats `json:"port_stats"`
}

type Stats struct {
	PortID int `json:"port_id"`
	ReceivedBytes int `json:"rx_bytes"`
	TransferredBytes int `json:"tx_bytes"`
}

func (s Stats) String() string {
	return fmt.Sprintf("{rx-bytes:%d tx-bytes:%d}", s.ReceivedBytes, s.TransferredBytes)
}

func (s Stats) CountBytes() int {
	return s.ReceivedBytes + s.TransferredBytes
}