package port

type StatsRepository map[string]map[int]*Stats

func NewStatsRepository(capacity int) StatsRepository{
	return make(StatsRepository, capacity)
}

func (r StatsRepository) AddStatsCollection(dpid string, statsCollection StatsCollection) {
	for _, portStats := range statsCollection.Stats {
		r.AddStats(dpid, portStats.PortID, portStats)
	}
}

func (r StatsRepository) AddStats(dpid string, portID int, stats *Stats) {
	if _, ok := r[dpid]; !ok {
		r[dpid] = make(map[int]*Stats)
	}
	r[dpid][portID] = stats
}

func (r StatsRepository) GetStats(dpid string, portID int) *Stats {
	return r[dpid][portID]
}
