package link

import (
	"clusctor/app/domain/model/management/topology"
	"clusctor/app/infrastructure/scanning/controller/aruba/models/stats/port"
	"time"
)

type Link struct {
	SourcePort int `json:"src_port"`
	SourceDPID string `json:"src_dpid"`
	DestinationPort int `json:"dst_port"`
	DestinationDPID string `json:"dst_dpid"`

	BytesTransferred int
	MeasurementDate time.Time
}

func (ln *Link) straightHash() string {
	return ln.SourceDPID + ln.DestinationDPID
}

func (ln *Link) backHash() string {
	return ln.DestinationDPID + ln.SourceDPID
}

func (ln Link) toTopologyLink() topology.Link {
	topologyLink := topology.NewLink(ln.SourcePort, ln.DestinationPort,
		ln.SourceDPID, ln.DestinationDPID)
	topologyLink.SetMeasurementData(topology.LinkMeasurementData{
		BytesTransferred: ln.BytesTransferred,
		Date:             ln.MeasurementDate,
	})
	return topologyLink
}

func (ln *Link) CalculateTransferredBytes(portsStats port.StatsRepository) {
	viaSource :=  portsStats.GetStats(ln.SourceDPID, ln.SourcePort).CountBytes()
	viaDestination := portsStats.GetStats(ln.DestinationDPID, ln.DestinationPort).CountBytes()
	ln.BytesTransferred = (viaSource + viaDestination) / 2
}

type Collection []*Link

func (c Collection) RemoveDuplicates() Collection {
	filtered := make(Collection, 0, 0)
	filter := make(map[string]bool, 0)
	for _, ln := range c {
		if _, ok := filter[ln.straightHash()]; ok {
			continue
		}
		filter[ln.straightHash()] = true
		filter[ln.backHash()] = true
		filtered = append(filtered, ln)
	}
	return filtered
}

func (c Collection) DPIDs() []string {
	uniqueDPIDs := make(map[string]bool)
	for _, ln := range c {
		uniqueDPIDs[ln.SourceDPID] = true
		uniqueDPIDs[ln.DestinationDPID] = true
	}
	dpids := make([]string, 0, len(uniqueDPIDs))
	for dpid := range uniqueDPIDs {
		dpids = append(dpids, dpid)
	}
	return dpids
}

func (c Collection) AddMeasurementData(portsStats port.StatsRepository, date time.Time) {
	for _, ln := range c {
		ln.CalculateTransferredBytes(portsStats)
		ln.MeasurementDate = date
	}
}

func (c Collection) ToTopologyLinks() []topology.Link {
	var topologyLinks []topology.Link
	for _, link := range c {
		topologyLinks = append(topologyLinks, link.toTopologyLink())
	}
	return topologyLinks
}