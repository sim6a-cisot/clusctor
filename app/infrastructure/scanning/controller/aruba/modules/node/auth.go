package node

import (
	"clusctor/app/infrastructure/scanning/controller/aruba/models/node"
	"clusctor/app/infrastructure/scanning/controller/aruba/modules_api"
)

func DecorateWithAuth(
	nodeAPI modules_api.NodeAPI,
	service modules_api.AuthService,
	session modules_api.SessionService,
) modules_api.NodeAPI {
	return &Auth{
		nodeAPI: nodeAPI,
		service: service,
		session: session,
	}
}

type Auth struct {
	nodeAPI modules_api.NodeAPI
	service modules_api.AuthService
	session modules_api.SessionService
}

func (a *Auth) Get() (nodes node.Collection, err error) {
	if a.session.IsExpired() {
		data, errAuth := a.service.GetSessionData()
		if errAuth != nil {
			return make(node.Collection, 0), errAuth
		}
		a.session.New(data)
	}
	return a.nodeAPI.Get()
}
