package flow

import (
	flow13 "clusctor/app/infrastructure/scanning/controller/aruba/models/flow"
	"clusctor/app/infrastructure/scanning/controller/aruba/modules_api"
)

type Auth struct {
	flowPusher modules_api.FlowPusher
	flowReader modules_api.FlowReader
	service    modules_api.AuthService
	session    modules_api.SessionService
}

func DecoratePusherWithAuth(
	flowPusher modules_api.FlowPusher,
	service modules_api.AuthService,
	session modules_api.SessionService,
) modules_api.FlowPusher {
	return &Auth{
		flowPusher: flowPusher,
		service:    service,
		session:    session,
	}
}

func DecorateReaderWithAuth(
	flowReader modules_api.FlowReader,
	service modules_api.AuthService,
	session modules_api.SessionService,
) modules_api.FlowReader {
	return &Auth{
		flowReader: flowReader,
		service:    service,
		session:    session,
	}
}

func (a *Auth) Push(f flow13.OF3Flow, dpid string) error {
	if a.session.IsExpired() {
		data, authErr := a.service.GetSessionData()
		if authErr != nil {
			return authErr
		}
		a.session.New(data)
	}
	return a.flowPusher.Push(f, dpid)
}

func (a *Auth) Read(switchDPID string) (flows flow13.OF3FlowCollection, err error) {
	if a.session.IsExpired() {
		data, authErr := a.service.GetSessionData()
		if authErr != nil {
			return make(flow13.OF3FlowCollection, 0), authErr
		}
		a.session.New(data)
	}
	return a.flowReader.Read(switchDPID)
}
