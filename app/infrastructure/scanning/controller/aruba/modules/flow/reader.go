package flow

import (
	flow13 "clusctor/app/infrastructure/scanning/controller/aruba/models/flow"
	"clusctor/app/infrastructure/scanning/controller/aruba/modules_api"
	"clusctor/app/pkg/std"
	"clusctor/app/pkg/std/http"
	jsoniter "github.com/json-iterator/go"
	"io/ioutil"
	netHttp "net/http"
)

type Reader struct {
	address string
	token   modules_api.SessionToken
	client  std.HttpClient
}

func NewReader(
	client std.HttpClient,
	token modules_api.SessionToken,
	address string,
) *Reader {
	return &Reader{
		address: address,
		token:   token,
		client:  client,
	}
}

type flowsWrapper struct {
	Flows []flow13.OF3Flow `json:"flows"`
}

func (r *Reader) Read(switchDPID string) (flows flow13.OF3FlowCollection, err error) {
	request, _ := netHttp.NewRequest("GET",
		r.address+"/sdn/v2.0/of/datapaths/"+switchDPID+"/flows", nil)
	request.Header.Set(http.XAuthToken, r.token.Token())
	response, errHTTP := r.client.Do(request)
	if errHTTP != nil {
		return make(flow13.OF3FlowCollection, 0), errHTTP
	}
	defer func() {
		if errClosing := response.Body.Close(); errClosing != nil {
			err = errClosing
		}
	}()

	responseBodyBytes, errBodyReading := ioutil.ReadAll(response.Body)
	if errBodyReading != nil {
		return make(flow13.OF3FlowCollection, 0), errBodyReading
	}

	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	flowsWrap := flowsWrapper{}
	return flowsWrap.Flows, json.Unmarshal(responseBodyBytes, &flowsWrap)
}
