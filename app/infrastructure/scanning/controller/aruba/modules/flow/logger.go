package flow

import (
	"clusctor/app/application/service"
	"clusctor/app/application/service/log"
	flow13 "clusctor/app/infrastructure/scanning/controller/aruba/models/flow"
	"clusctor/app/infrastructure/scanning/controller/aruba/modules_api"
)

type Logger struct {
	flowPusher modules_api.FlowPusher
	flowReader modules_api.FlowReader
	logger     service.InfoLogger
}

func DecoratePusherWithLogger(
	flowPusher modules_api.FlowPusher,
	logger service.InfoLogger,
) modules_api.FlowPusher {
	return &Logger{
		flowPusher: flowPusher,
		logger:     logger,
	}
}

func DecorateReaderWithLogger(
	flowReader modules_api.FlowReader,
	logger service.InfoLogger,
) modules_api.FlowReader {
	return &Logger{
		flowReader: flowReader,
		logger:     logger,
	}
}

func (l *Logger) Push(f flow13.OF3Flow, dpid string) error {
	l.logger.Info(log.Entry{
		Msg:     "Aruba flow to dpid = " + dpid,
		Details: f.String(),
	})
	return l.flowPusher.Push(f, dpid)
}

func (l *Logger) Read(switchDPID string) (flows flow13.OF3FlowCollection, err error) {
	if flows, err = l.flowReader.Read(switchDPID); err == nil {
		l.logger.Info(log.Entry{
			Msg:     "Aruba flows of dpid = " + switchDPID,
			Details: flows.String(),
		})
	}
	return
}
