package flow

import (
	"bytes"
	flow13 "clusctor/app/infrastructure/scanning/controller/aruba/models/flow"
	"clusctor/app/infrastructure/scanning/controller/aruba/modules_api"
	"clusctor/app/pkg/std"
	"clusctor/app/pkg/std/http"
	"errors"
	"fmt"
	jsoniter "github.com/json-iterator/go"
	"io/ioutil"
	netHttp "net/http"
)

type Pusher struct {
	address string
	token   modules_api.SessionToken
	client  std.HttpClient
}

func NewPusher(client std.HttpClient, token modules_api.SessionToken, address string) *Pusher {
	return &Pusher{
		address: address,
		client:  client,
		token:   token,
	}
}

type flowWrapper struct {
	Flow flow13.OF3Flow `json:"flow"`
}

func (p *Pusher) Push(f flow13.OF3Flow, dpid string) (err error) {
	of3Flow := flowWrapper{Flow: f}
	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	of3FlowJSON, _ := json.Marshal(&of3Flow)
	request, _ := netHttp.NewRequest(
		"POST",
		p.address+"/sdn/v2.0/of/datapaths/"+dpid+"/flows",
		bytes.NewBuffer(of3FlowJSON),
	)
	request.Header.Set(http.XAuthToken, p.token.Token())
	request.Header.Set(http.ContentType, http.ApplicationJSON)
	response, errHTTP := p.client.Do(request)
	if errHTTP != nil {
		return errHTTP
	}
	defer func() {
		if errClosing := response.Body.Close(); errClosing != nil {
			err = errClosing
		}
	}()
	if response.StatusCode == netHttp.StatusCreated {
		return nil
	}
	responseBodyBytes, errBodyReading := ioutil.ReadAll(response.Body)
	if errBodyReading != nil {
		return errBodyReading
	}
	errorResp := errorResponse{}
	if errUnmarshal := json.Unmarshal(responseBodyBytes, &errorResp); errUnmarshal != nil {
		return errUnmarshal
	}
	return errors.New(fmt.Sprintf("http status %d; message: %s", response.StatusCode, errorResp.Message))
}
