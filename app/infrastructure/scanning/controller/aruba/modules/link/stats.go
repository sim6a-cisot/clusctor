package link

import (
	"clusctor/app/infrastructure/scanning/controller/aruba/models/link"
	"clusctor/app/infrastructure/scanning/controller/aruba/modules_api"
	"clusctor/app/pkg/std"
)

type StatsCollector struct {
	linkAPI            modules_api.LinkAPI
	portStatsCollector modules_api.PortStatsCollector
	stdTime            std.Time
}

func (sc *StatsCollector) Get() (link.Collection, error) {
	links, errLinksFetching := sc.linkAPI.Get()
	if errLinksFetching != nil {
		return link.Collection{}, errLinksFetching
	}
	portsStats, errPortStatsCollecting := sc.portStatsCollector.Collect(links.DPIDs())
	if errPortStatsCollecting != nil {
		return link.Collection{}, errPortStatsCollecting
	}
	links.AddMeasurementData(portsStats, sc.stdTime.Now())
	return links, nil
}

func DecorateWithStatsCollector(
	linkAPI modules_api.LinkAPI,
	portStatsCollector modules_api.PortStatsCollector,
	stdTime std.Time,
) modules_api.LinkAPI {
	return &StatsCollector{
		linkAPI:            linkAPI,
		portStatsCollector: portStatsCollector,
		stdTime:            stdTime,
	}
}
