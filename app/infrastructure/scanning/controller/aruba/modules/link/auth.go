package link

import (
	"clusctor/app/infrastructure/scanning/controller/aruba/models/link"
	"clusctor/app/infrastructure/scanning/controller/aruba/modules_api"
)

func DecorateWithAuth(
	linkAPI modules_api.LinkAPI,
	service modules_api.AuthService,
	session modules_api.SessionService,
) modules_api.LinkAPI {
	return &Auth{
		linkAPI: linkAPI,
		service: service,
		session: session,
	}
}

type Auth struct {
	linkAPI modules_api.LinkAPI
	service modules_api.AuthService
	session modules_api.SessionService
}

func (a *Auth) Get() (links link.Collection, err error) {
	if a.session.IsExpired() {
		data, authErr := a.service.GetSessionData()
		if authErr != nil {
			return link.Collection{}, authErr
		}
		a.session.New(data)
	}
	return a.linkAPI.Get()
}
