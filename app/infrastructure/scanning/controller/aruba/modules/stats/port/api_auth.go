package port

import (
	"clusctor/app/infrastructure/scanning/controller/aruba/models/stats/port"
	"clusctor/app/infrastructure/scanning/controller/aruba/modules_api"
)

type APIAuth struct {
	statsAPI modules_api.PortStatsAPI
	service  modules_api.AuthService
	session  modules_api.SessionService
}

func DecorateWithStatsAuth(
	statsAPI modules_api.PortStatsAPI,
	service modules_api.AuthService,
	session modules_api.SessionService,
) modules_api.PortStatsAPI {
	return &APIAuth{
		statsAPI: statsAPI,
		service: service,
		session: session,
	}
}

func (a *APIAuth) Get(dpid string) (port.StatsCollection, error) {
	if a.session.IsExpired() {
		data, authErr := a.service.GetSessionData()
		if authErr != nil {
			return port.StatsCollection{}, authErr
		}
		a.session.New(data)
	}
	return a.statsAPI.Get(dpid)
}