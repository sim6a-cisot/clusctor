package port

import (
	"clusctor/app/infrastructure/scanning/controller/aruba/models/stats/port"
	"clusctor/app/infrastructure/scanning/controller/aruba/modules_api"
)

type StatsCollector struct {
	api modules_api.PortStatsAPI
}

func NewStatsCollector(api modules_api.PortStatsAPI) *StatsCollector {
	return &StatsCollector{api: api}
}

func (sc *StatsCollector) Collect(dpids []string) (port.StatsRepository, error) {
	r := port.NewStatsRepository(len(dpids))
	for _, dpid := range dpids {
		statsCollection, err := sc.api.Get(dpid)
		if err != nil {
			return port.StatsRepository{}, err
		}
		r.AddStatsCollection(dpid, statsCollection)
	}
	return r, nil
}