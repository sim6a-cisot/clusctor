package port

import (
	"clusctor/app/infrastructure/scanning/controller/aruba/models/stats/port"
	"clusctor/app/infrastructure/scanning/controller/aruba/modules_api"
	"clusctor/app/pkg/std"
	"clusctor/app/pkg/std/http"
	jsoniter "github.com/json-iterator/go"
	"io/ioutil"
	netHttp "net/http"
)

type StatsAPI struct {
	address string
	token   modules_api.SessionToken
	client  std.HttpClient
}

func NewStatsAPI(client std.HttpClient, token modules_api.SessionToken, address string) *StatsAPI {
	return &StatsAPI{
		address: address,
		client:  client,
		token:   token,
	}
}

func (api *StatsAPI) Get(dpid string) (portsStats port.StatsCollection, err error) {
	request, _ := netHttp.NewRequest("GET",
		api.address+"/sdn/v2.0/of/stats/ports?dpid="+dpid, nil)
	request.Header.Set(http.XAuthToken, api.token.Token())
	response, errHTTP := api.client.Do(request)
	if errHTTP != nil {
		return port.StatsCollection{}, errHTTP
	}
	defer func() {
		if errClosing := response.Body.Close(); errClosing != nil {
			err = errClosing
		}
	}()

	responseBodyBytes, errBodyReading := ioutil.ReadAll(response.Body)
	if errBodyReading != nil {
		return port.StatsCollection{}, errBodyReading
	}

	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	statsCollection := port.StatsCollection{}
	return statsCollection, json.Unmarshal(responseBodyBytes, &statsCollection)
}
