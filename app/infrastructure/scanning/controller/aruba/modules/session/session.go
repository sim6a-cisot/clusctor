package session

import (
	"clusctor/app/pkg/std"
)

func New(stdTime std.Time) *Session {
	return &Session{
		stdTime: stdTime,
		data: Data{
			Token:          "",
			ExpirationDate: stdTime.Now(),
		},
	}
}

type Session struct {
	stdTime std.Time
	data    Data
}

func (s *Session) New(data Data) {
	s.data = data
}

func (s *Session) Token() string {
	return s.data.Token
}

func (s *Session) IsExpired() bool {
	isExpired := s.stdTime.Now().After(s.data.ExpirationDate)
	return isExpired
}
