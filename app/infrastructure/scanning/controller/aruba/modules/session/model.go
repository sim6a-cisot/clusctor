package session

import "time"

type Data struct {
	Token          string
	ExpirationDate time.Time
}
