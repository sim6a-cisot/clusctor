package of_switch

import (
	"clusctor/app/infrastructure/scanning/controller/aruba/models/of_switch"
	"clusctor/app/infrastructure/scanning/controller/aruba/modules_api"
)

func DecorateWithAuth(
	ofSwitchAPI modules_api.OFSwitchAPI,
	service modules_api.AuthService,
	session modules_api.SessionService,
) modules_api.OFSwitchAPI {
	return &Auth{
		ofSwitchAPI: ofSwitchAPI,
		service: service,
		session: session,
	}
}

type Auth struct {
	ofSwitchAPI modules_api.OFSwitchAPI
	service     modules_api.AuthService
	session     modules_api.SessionService
}

func (a *Auth) Get() (switches of_switch.Collection, err error) {
	if a.session.IsExpired() {
		data, authErr := a.service.GetSessionData()
		if authErr != nil {
			return make(of_switch.Collection, 0), authErr
		}
		a.session.New(data)
	}
	return a.ofSwitchAPI.Get()
}