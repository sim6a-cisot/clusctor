package of_switch

import (
	"clusctor/app/infrastructure/scanning/controller/aruba/models/of_switch"
	"clusctor/app/infrastructure/scanning/controller/aruba/modules_api"
	"clusctor/app/pkg/std"
	"clusctor/app/pkg/std/http"
	jsoniter "github.com/json-iterator/go"
	"io/ioutil"
	netHttp "net/http"
)

type API struct {
	address string
	token   modules_api.SessionToken
	client  std.HttpClient
}

func NewAPI(client std.HttpClient, token modules_api.SessionToken, address string) *API {
	return &API{
		address: address,
		client:  client,
		token:   token,
	}
}

type ofSwitchesResponse struct {
	DataPaths []of_switch.OFSwitch `json:"datapaths"`
}

func (api *API) Get() (ofSwitches of_switch.Collection, err error) {
	request, _ := netHttp.NewRequest("GET",
		api.address+"/sdn/v2.0/of/datapaths", nil)
	request.Header.Set(http.XAuthToken, api.token.Token())
	response, errHTTP := api.client.Do(request)
	if errHTTP != nil {
		return make(of_switch.Collection, 0), errHTTP
	}
	defer func() {
		if errClosing := response.Body.Close(); errClosing != nil {
			err = errClosing
		}
	}()

	responseBodyBytes, errBodyReading := ioutil.ReadAll(response.Body)
	if errBodyReading != nil {
		return make(of_switch.Collection, 0), errBodyReading
	}

	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	ofSwitchesResp := ofSwitchesResponse{}
	return ofSwitchesResp.DataPaths, json.Unmarshal(responseBodyBytes, &ofSwitchesResp)
}