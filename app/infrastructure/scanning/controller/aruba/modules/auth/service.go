package auth

import (
	session2 "clusctor/app/infrastructure/scanning/controller/aruba/modules/session"
	time2 "clusctor/app/infrastructure/scanning/controller/aruba/time"
	"clusctor/app/pkg/std"
	"clusctor/app/pkg/strutil"
	jsoniter "github.com/json-iterator/go"
	"io/ioutil"
	netHttp "net/http"
	"strings"
)

const login = `
		{
			"login": {
				"user": "sdn",
				"password": "skyline",
				"domain": "sdn"	
			}
		}
	`

type authResponse struct {
	Record struct {
		Token          string `json:"token"`
		ExpirationDate string `json:"expirationDate"`
	} `json:"record"`
}

func NewAPI(client std.HttpClient, address string) *API {
	return &API{
		address: address,
		client:  client,
	}
}

type API struct {
	address string
	client  std.HttpClient
}

func (api *API) GetSessionData() (s session2.Data, err error) {
	request, _ := netHttp.NewRequest("POST",
		api.address+"/sdn/v2.0/auth", strings.NewReader(strutil.RemoveWhiteSpaces(login)))
	response, errHTTP := api.client.Do(request)
	if errHTTP != nil {
		return s, errHTTP
	}
	defer func() {
		if errClosing := response.Body.Close(); errClosing != nil {
			err = errClosing
		}
	}()
	responseBodyBytes, errBodyReading := ioutil.ReadAll(response.Body)
	if errBodyReading != nil {
		return s, errBodyReading
	}
	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	authRes := authResponse{}
	if jsonErr := json.Unmarshal(responseBodyBytes, &authRes); jsonErr != nil {
		return s, jsonErr
	}
	return session2.Data{
		Token:          authRes.Record.Token,
		ExpirationDate: time2.ParseDate(authRes.Record.ExpirationDate),
	}, nil
}
