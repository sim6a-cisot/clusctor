package modules_api

import (
	"clusctor/app/infrastructure/scanning/controller/aruba/modules/session"
)

type AuthService interface {
	GetSessionData() (s session.Data, err error)
}
