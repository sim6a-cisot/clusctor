package modules_api

import (
	"clusctor/app/infrastructure/scanning/controller/aruba/models/link"
)

type LinkAPI interface {
	Get() (links link.Collection, err error)
}
