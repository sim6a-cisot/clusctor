package modules_api

import (
	"clusctor/app/infrastructure/scanning/controller/aruba/models/of_switch"
)

type OFSwitchAPI interface {
	Get() (of_switch.Collection, error)
}
