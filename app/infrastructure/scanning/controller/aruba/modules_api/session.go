package modules_api

import (
	"clusctor/app/infrastructure/scanning/controller/aruba/modules/session"
)

type SessionService interface {
	New(data session.Data)
	IsExpired() bool
}

type SessionToken interface {
	Token() string
}