package modules_api

import (
	"clusctor/app/infrastructure/scanning/controller/aruba/models/flow"
)

type FlowPusher interface {
	Push(flow.OF3Flow, string) error
}

type FlowReader interface {
	Read(string) (flow.OF3FlowCollection, error)
}