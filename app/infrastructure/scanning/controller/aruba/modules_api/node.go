package modules_api

import (
	"clusctor/app/infrastructure/scanning/controller/aruba/models/node"
)

type NodeAPI interface {
	Get() (nodes node.Collection, err error)
}
