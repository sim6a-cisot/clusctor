package modules_api

import (
	"clusctor/app/infrastructure/scanning/controller/aruba/models/stats/port"
)

type PortStatsAPI interface {
	Get(string) (port.StatsCollection, error)
}

type PortStatsCollector interface {
	Collect(dpids []string) (port.StatsRepository, error)
}