package time

import "time"

func ParseDate(date string) time.Time {
	layout := "2006-01-02 15-04-05 -0700"
	t, _ := time.Parse(layout, date)
	return t
}
