package aruba

import (
	"clusctor/app/application/service"
	flowDomainModel "clusctor/app/domain/model/management/flow"
	"clusctor/app/domain/service/management"
	flowModel "clusctor/app/infrastructure/scanning/controller/aruba/models/flow"
	"clusctor/app/infrastructure/scanning/controller/aruba/modules/auth"
	"clusctor/app/infrastructure/scanning/controller/aruba/modules/flow"
	"clusctor/app/infrastructure/scanning/controller/aruba/modules/link"
	"clusctor/app/infrastructure/scanning/controller/aruba/modules/node"
	"clusctor/app/infrastructure/scanning/controller/aruba/modules/of_switch"
	"clusctor/app/infrastructure/scanning/controller/aruba/modules/session"
	portStats "clusctor/app/infrastructure/scanning/controller/aruba/modules/stats/port"
	"clusctor/app/infrastructure/scanning/controller/aruba/modules_api"
	"clusctor/app/pkg/std"
	"clusctor/app/pkg/std/http"
)

func NewController(
	host string, port int,
	client std.HttpClient,
	stdTime std.Time,
	logger service.InfoLogger,
) *Controller {
	s := session.New(stdTime)
	address := http.CreateSecureAddress(host, port)
	authService := auth.NewAPI(client, address)
	nodeAPI := node.NewAPI(client, s, address)
	portStatsAPI := portStats.DecorateWithStatsAuth(
		portStats.NewStatsAPI(client, s, address),
		authService, s,
	)
	portStatsCollector := portStats.NewStatsCollector(portStatsAPI)
	linkAPI := link.DecorateWithStatsCollector(link.NewAPI(client, s, address), portStatsCollector, stdTime)
	ofSwitchAPI := of_switch.NewAPI(client, s, address)
	flowPusher := flow.NewPusher(client, s, address)
	flowReader := flow.NewReader(client, s, address)
	return &Controller{
		session:     s,
		nodeAPI:     node.DecorateWithAuth(nodeAPI, authService, s),
		linkAPI:     link.DecorateWithAuth(linkAPI, authService, s),
		ofSwitchAPI: of_switch.DecorateWithAuth(ofSwitchAPI, authService, s),
		flowPusher:  flow.DecoratePusherWithAuth(flow.DecoratePusherWithLogger(flowPusher, logger), authService, s),
		flowReader:  flow.DecorateReaderWithAuth(flow.DecorateReaderWithLogger(flowReader, logger), authService, s),
	}
}

type Controller struct {
	session     *session.Session
	linkAPI     modules_api.LinkAPI
	nodeAPI     modules_api.NodeAPI
	ofSwitchAPI modules_api.OFSwitchAPI
	flowPusher  modules_api.FlowPusher
	flowReader  modules_api.FlowReader
}

func (c *Controller) ScanNetwork(topology management.TopologyUpdater) error {
	links, errLinksFetching := c.linkAPI.Get()
	if errLinksFetching != nil {
		return errLinksFetching
	}
	links = links.RemoveDuplicates()
	nodes, errNodesFetching := c.nodeAPI.Get()
	if errNodesFetching != nil {
		return errNodesFetching
	}
	switches, errSwitchesFetching := c.ofSwitchAPI.Get()
	if errSwitchesFetching != nil {
		return errSwitchesFetching
	}

	topology.Update(
		switches.ToTopologySwitches(),
		nodes.ToTopologyHosts(),
		links.ToTopologyLinks(),
	)
	return nil
}

func (c *Controller) PushFlows(flows []flowDomainModel.Flow) error {
	var err error
	for _, f := range flows {
		if err = c.flowPusher.Push(flowModel.ConvertToOF3Flow(f), f.DPID); err != nil {
			return err
		}
	}
	return nil
}

func (c *Controller) ReadFlows() (flowDomainModel.Collection, error) {
	switches, errSwitchesFetching := c.ofSwitchAPI.Get()
	if errSwitchesFetching != nil {
		return flowDomainModel.Collection{}, errSwitchesFetching
	}
	var flows []flowDomainModel.Flow
	for _, sw := range switches {
		switchFlows, errFlowsFetching := c.flowReader.Read(sw.DPID)
		if errFlowsFetching != nil {
			return flowDomainModel.Collection{}, errFlowsFetching
		}
		for _, switchFlow := range switchFlows {
			flows = append(flows, flowModel.ConvertFromOF3Flow(sw.DPID, switchFlow))
		}
	}
	return flows, nil
}
