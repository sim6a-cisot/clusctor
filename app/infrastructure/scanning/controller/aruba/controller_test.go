package aruba_test

import (
	"bytes"
	"clusctor/app/application/service/log"
	"clusctor/app/domain/model/management/flow"
	"clusctor/app/infrastructure/scanning/controller/aruba"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"io"
	"net/http"
	"strings"
	"testing"
	"time"
)

type nopCloser struct {
	io.Reader
}

func (nopCloser) Close() error { return nil }

type httpClientStub struct {
	DoFunc func(req *http.Request) (*http.Response, error)
}

func (c *httpClientStub) Do(req *http.Request) (*http.Response, error) {
	return c.DoFunc(req)
}

type infoLoggerStub struct{}

func (infoLoggerStub) Info(log.Entry) {}

type timeStub struct{}

func (timeStub) Now() time.Time {
	return time.Time{}
}

func TestController_ReadFlows(t *testing.T) {
	clientStub := &httpClientStub{
		DoFunc: func(req *http.Request) (*http.Response, error) {
			if strings.Contains(req.URL.String(), "flows") {
				return &http.Response{
					Body: nopCloser{bytes.NewBufferString(
						`
{
  "version": "1.3.0",
  "flows": [
    {
      "table_id": 0,
      "duration_sec": 2,
      "duration_nsec": "845000000",
      "priority": 30000,
      "idle_timeout": 60,
      "hard_timeout": 0,
      "cookie": "0x22222222",
      "packet_count": "0",
      "byte_count": "0",
      "match": [
        {
          "eth_type": "ipv4"
        },
        {
          "ipv4_src": "10.0.0.1"
        },
        {
          "ipv4_dst": "10.0.0.2"
        },
        {
          "ip_proto": "tcp"
        },
        {
          "tcp_dst": 8080
        }
      ],
      "flow_mod_flags": [],
      "instructions": [
        {
          "apply_actions": [
            {
              "output": 2
            }
          ]
        }
      ]
    }
  ]
}`)},
				}, nil
			}
			return &http.Response{
				Body: nopCloser{bytes.NewBufferString(
					`
{
  "datapaths": [
    {
      "dpid": "00:00:00:00:00:00:00:01",
      "negotiated_version": "1.3.0",
      "ready": "2019-11-14T17:57:13.897Z",
      "last_message": "2019-11-14T19:30:56.968Z",
      "num_buffers": 256,
      "num_tables": 254,
      "mfr": "Nicira, Inc.",
      "hw": "Open vSwitch",
      "sw": "2.0.2",
      "serial": "None",
      "desc": "None",
      "device_ip": "192.168.0.105",
      "device_port": 54886,
      "capabilities": [
        "flow_stats",
        "table_stats",
        "port_stats",
        "queue_stats"
      ]
    }
  ]
}`)},
			}, nil
		},
	}
	ctrl := aruba.NewController("", 0, clientStub, &timeStub{}, &infoLoggerStub{})
	flows, err := ctrl.ReadFlows()
	require.NoError(t, err)
	assert.Equal(t, flow.Collection{
		{
			DPID:        "00:00:00:00:00:00:00:01",
			Cookie:      "0x22222222",
			Priority:    30000,
			IdleTimeout: 60,
			HardTimeout: 0,
			Matches: flow.Matches{
				SrcIP:      "10.0.0.1",
				DstIP:      "10.0.0.2",
				IPProto:    "tcp",
				EthType:    "ipv4",
				TCPSrcPort: 0,
				TCPDstPort: 8080,
			},
			OutputAction: flow.Action{
				Output: 2,
			},
		},
	}, flows)
}
