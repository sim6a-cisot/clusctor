package pox

import (
	"clusctor/app/domain/model/management/topology"
	"clusctor/app/pkg/std"
	"clusctor/app/pkg/std/http"
	jsoniter "github.com/json-iterator/go"
	"io/ioutil"
	nethttp "net/http"
)

type link struct {
	PortSource int `json:"portSource"`
	DataLayerSource string `json:"dataLayerSource"`
	PortDestination int `json:"portDestination"`
	DataLayerDestination string `json:"dataLayerDestination"`
}

func (ln link) toSDNLink() topology.Link {
	return topology.NewLink(ln.PortSource, ln.PortDestination, ln.DataLayerSource, ln.DataLayerDestination)
}

type links []link

func (ll links) toSDNLinks() []topology.Link {
	var sdnLinks []topology.Link
	for _, link := range ll {
		sdnLinks = append(sdnLinks, link.toSDNLink())
	}
	return sdnLinks
}

func newLinkAPI(client std.HttpClient, host string, port int) *linkAPI {
	return &linkAPI{
		basePath: http.CreateAddress(host, port),
		client:   client,
	}
}

type linkAPI struct {
	basePath string
	client   std.HttpClient
}

func (api *linkAPI) fetchLinks() (lns links, err error) {
	request, _ := nethttp.NewRequest("GET",
		api.basePath + "/web/jsonrest/discovery/lns", nil)
	response, errHTTP := api.client.Do(request)
	if errHTTP != nil {
		return make(links, 0), errHTTP
	}
	defer func() {
		if errClosing := response.Body.Close(); errClosing != nil {
			err = errClosing
		}
	}()

	responseBodyBytes, errBodyReading := ioutil.ReadAll(response.Body)
	if errBodyReading != nil {
		return make(links, 0), errBodyReading
	}

	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	return lns, json.Unmarshal(responseBodyBytes, &lns)
}
