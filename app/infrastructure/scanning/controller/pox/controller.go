package pox

import (
	"clusctor/app/domain/model/management/flow"
	"clusctor/app/domain/service/management"
	"clusctor/app/pkg/std"
)

func NewController(host string, port int, client std.HttpClient) *Controller {
	return &Controller{
		switchAPI: newSwitchAPI(client, host, port),
		linkAPI:   newLinkAPI(client, host, port),
		deviceAPI: newDeviceAPI(client, host, port),
	}
}

type Controller struct {
	switchAPI *switchAPI
	linkAPI   *linkAPI
	deviceAPI *deviceAPI
}

func (c *Controller) ScanNetwork(topology management.TopologyUpdater) error {
	switches, errSwitchesFetching := c.switchAPI.fetchSwitches()
	if errSwitchesFetching != nil {
		return errSwitchesFetching
	}

	devices, errDevicesFetching := c.deviceAPI.fetchDevices()
	if errDevicesFetching != nil {
		return errDevicesFetching
	}

	links, errLinksFetching := c.linkAPI.fetchLinks()
	if errLinksFetching != nil {
		return errLinksFetching
	}

	topology.Update(
		switches.toSDNSwitches(),
		devices.toSDNHosts(),
		links.toSDNLinks(),
	)

	return nil
}

func (c *Controller) PushFlows([]flow.Flow) error {
	// TODO: implement it
	return nil
}

func (c *Controller) ReadFlows() (flow.Collection, error) {
	// TODO: implement it
	return flow.Collection{}, nil
}
