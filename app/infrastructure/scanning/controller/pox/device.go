package pox

import (
	"clusctor/app/domain/model/management/mac"
	"clusctor/app/domain/model/management/topology"
	"clusctor/app/pkg/std"
	"clusctor/app/pkg/std/http"
	jsoniter "github.com/json-iterator/go"
	"io/ioutil"
	nethttp "net/http"
)

type device struct {
	DataLayerAddress string `json:"dataLayerAddress"`
	SwitchDPID string `json:"switch_dpid"`
	Port int `json:"port"`
}

func (d device) toSDNHost() topology.Host {
	return topology.NewHost(mac.MAC(d.DataLayerAddress), "TODO", d.SwitchDPID, d.Port)
}

type devices []device

func (dd devices) toSDNHosts() []topology.Host {
	var sdnHosts []topology.Host
	for _, host := range dd {
		sdnHosts = append(sdnHosts, host.toSDNHost())
	}
	return sdnHosts
}

func newDeviceAPI(client std.HttpClient, host string, port int) *deviceAPI {
	return &deviceAPI{
		basePath: http.CreateAddress(host, port),
		client:   client,
	}
}

type deviceAPI struct {
	basePath string
	client   std.HttpClient
}

func (api *deviceAPI) fetchDevices() (devs devices, err error) {
	request, _ := nethttp.NewRequest("GET",
		api.basePath + "/web/jsonrest/host_tracker/devs", nil)
	response, errHTTP := api.client.Do(request)
	if errHTTP != nil {
		return make(devices, 0), errHTTP
	}
	defer func() {
		if errClosing := response.Body.Close(); errClosing != nil {
			err = errClosing
		}
	}()

	responseBodyBytes, errBodyReading := ioutil.ReadAll(response.Body)
	if errBodyReading != nil {
		return make(devices, 0), errBodyReading
	}

	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	return devs, json.Unmarshal(responseBodyBytes, &devs)
}
