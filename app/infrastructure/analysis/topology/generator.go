package topology

import (
	"clusctor/app/application/service/file"
	"clusctor/app/pkg/std"
	"strconv"
)

type Generator struct {
	fileNavigator *file.Navigator
	cmdRunner     std.CmdRunner
	writer        std.FileWriter
}

func NewGenerator(
	fileNavigator *file.Navigator,
	cmdRunner std.CmdRunner,
	writer std.FileWriter,
) *Generator {
	return &Generator{
		cmdRunner:     cmdRunner,
		fileNavigator: fileNavigator,
		writer:        writer,
	}
}

const (
	switchesQuantityArgName    = "--switches"
	hostsQuantityArgName       = "--hosts"
	randomLinksQuantityArgName = "--links"
	topologiesQuantityArgName  = "--topologies"
)

func (g *Generator) GenerateOne(switchesQuantity, randomLinksQuantity, hostsQuantity int) error {
	argumentsWithValues := map[string]string{
		switchesQuantityArgName:    strconv.Itoa(switchesQuantity),
		hostsQuantityArgName:       strconv.Itoa(hostsQuantity),
		randomLinksQuantityArgName: strconv.Itoa(randomLinksQuantity),
	}
	return g.generate(argumentsWithValues, g.fileNavigator.TopologyFilePath().String())
}

func (g *Generator) GenerateMultiple(vertexQuantity, randomEdgesQuantity, hostsQuantity, topologiesQuantity int) error {
	argumentsWithValues := map[string]string{
		switchesQuantityArgName:    strconv.Itoa(vertexQuantity),
		hostsQuantityArgName:       strconv.Itoa(hostsQuantity),
		randomLinksQuantityArgName: strconv.Itoa(randomEdgesQuantity),
		topologiesQuantityArgName:  strconv.Itoa(topologiesQuantity),
	}
	return g.generate(argumentsWithValues, g.fileNavigator.TopologiesFilePath().String())
}

func (g *Generator) generate(argumentsWithValues map[string]string, outputFilePath string) error {
	externalGeneratorFilePath := g.fileNavigator.GraphGeneratorFilePath()
	output, errGenerating := g.cmdRunner.Run(externalGeneratorFilePath.String(), argumentsWithValues)
	if errGenerating != nil {
		return errGenerating
	}
	return g.writer.WriteFile(outputFilePath, output)
}
