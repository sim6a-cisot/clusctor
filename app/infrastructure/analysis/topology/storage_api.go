package topology

import (
	"clusctor/app/domain/model/management/topology"
)

type Storage interface {
	Get() (Topology, error)
	GetAll() ([]Topology, error)
}

type Creator interface {
	Create(*topology.Topology) error
}
