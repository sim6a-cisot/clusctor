package topology

import (
	"clusctor/app/domain/model/management/topology"
	jsoniter "github.com/json-iterator/go"
)

type Topology struct {
	Switches Switches `json:"switches"`
	Hosts    Hosts    `json:"hosts"`
	Links    Links    `json:"links"`
}

func (t *Topology) ToJSON() ([]byte, error) {
	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	return json.MarshalIndent(t, "", "  ")
}

func NewTopology(topologyModel *topology.Topology) Topology {
	return Topology{
		Switches: NewSwitches(topologyModel.Switches()),
		Links:    NewLinks(topologyModel.Links()),
		Hosts:    NewHosts(topologyModel.Hosts()),
	}
}









