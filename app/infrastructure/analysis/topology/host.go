package topology

import (
	"clusctor/app/domain/model/management/mac"
	"clusctor/app/domain/model/management/topology"
)

type Host struct {
	MAC          mac.MAC `json:"mac"`
	IPV4         string  `json:"ipv4"`
	AdjacentDPID string  `json:"adjacentDpid"`
	AdjacentPort int     `json:"adjacentPort"`
}

type Hosts []Host

func (hh Hosts) ToSDNHosts() []topology.Host {
	var sdnHosts []topology.Host
	for _, host := range hh {
		sdnHosts = append(sdnHosts, topology.NewHost(
			host.MAC,
			host.IPV4,
			host.AdjacentDPID,
			host.AdjacentPort,
		))
	}
	return sdnHosts
}

func NewHosts(sdnHosts []topology.Host) Hosts {
	hosts := make(Hosts, 0, len(sdnHosts))

	for _, sdnHost := range sdnHosts {
		hosts = append(hosts, NewHost(sdnHost))
	}

	return hosts
}

func NewHost(sdnHost topology.Host) Host {
	return Host{
		MAC:          sdnHost.MAC(),
		IPV4:         sdnHost.IPv4(),
		AdjacentDPID: sdnHost.AdjacentDPID(),
		AdjacentPort: sdnHost.AdjacentPort(),
	}
}
