package topology

import (
	"clusctor/app/domain/model/management/metric"
	"clusctor/app/domain/model/management/topology"
)

type Link struct {
	SourcePort      int     `json:"sourcePort"`
	SourceDPID      string  `json:"sourceDpid"`
	DestinationPort int     `json:"destinationPort"`
	DestinationDPID string  `json:"destinationDpid"`
	Metrics         Metrics `json:"metrics"`
}

type Links []Link

func (ll Links) ToSDNLinks() []topology.Link {
	var sdnLinks []topology.Link
	var sdnLink topology.Link
	for _, ln := range ll {
		sdnLink = topology.NewLink(
			ln.SourcePort,
			ln.DestinationPort,
			ln.SourceDPID,
			ln.DestinationDPID,
		)
		sdnLink.SetMetrics(metric.NewMetrics(
			ln.Metrics.Bandwidth,
			ln.Metrics.Delay,
			ln.Metrics.PacketLossRate,
			ln.Metrics.AdminMetric,
		))
		sdnLinks = append(sdnLinks, sdnLink)
	}
	return sdnLinks
}

type Metrics struct {
	Delay          float64 `json:"delay"`
	Bandwidth      float64 `json:"bandwidth"`
	PacketLossRate float64 `json:"packetLossRate"`
	AdminMetric    float64 `json:"adminMetric"`
}

func NewLinks(sdnLinks []topology.Link) Links {
	links := make(Links, 0, len(sdnLinks))
	for _, sdnLink := range sdnLinks {
		links = append(links, NewLink(sdnLink))
	}
	return links
}

func NewLink(sdnLink topology.Link) Link {
	sdnMetrics := sdnLink.Metrics()
	return Link{
		SourceDPID:      sdnLink.SourceDPID(),
		DestinationDPID: sdnLink.DestinationDPID(),
		Metrics: Metrics{
			Delay:          sdnMetrics.Delay(),
			Bandwidth:      sdnMetrics.Bandwidth(),
			PacketLossRate: sdnMetrics.PacketLossRate(),
			AdminMetric:    sdnMetrics.AdminMetric(),
		},
	}
}
