package topology

import "clusctor/app/domain/model/management/topology"

type Switch struct {
	DPID string `json:"dpid"`
}

type Switches []Switch

func (ss Switches) ToSDNSwitches() []topology.Switch {
	var sdnSwitches []topology.Switch
	for _, sw := range ss {
		sdnSwitches = append(sdnSwitches, topology.NewSwitch(sw.DPID))
	}
	return sdnSwitches
}

func NewSwitches(sdnSwitches []topology.Switch) Switches {
	switches := make(Switches, 0, len(sdnSwitches))
	for _, sdnSwitch := range sdnSwitches {
		switches = append(switches, NewSwitch(sdnSwitch))
	}
	return switches
}

func NewSwitch(sdnSwitch topology.Switch) Switch {
	return Switch{
		DPID: sdnSwitch.DPID(),
	}
}
