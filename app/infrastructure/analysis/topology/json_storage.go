package topology

import (
	"clusctor/app/application/service/file"
	model "clusctor/app/domain/model/management/topology"
	"clusctor/app/pkg/std"
	jsoniter "github.com/json-iterator/go"
)

func New(
	singleTopologyFilePath file.Path,
	topologiesFilePath file.Path,
	reader std.FileReader,
	writer std.FileWriter,
) *JSONReader {
	return &JSONReader{
		singleTopologyFilePath: singleTopologyFilePath,
		topologiesFilePath:     topologiesFilePath,
		reader:                 reader,
		writer:                 writer,
	}
}

type JSONReader struct {
	singleTopologyFilePath file.Path
	topologiesFilePath     file.Path
	reader                 std.FileReader
	writer                 std.FileWriter
}

func (t *JSONReader) Create(topologyModel *model.Topology) error {
	topo := NewTopology(topologyModel)
	topoJSON, _ := topo.ToJSON()
	return t.writer.WriteFile(t.singleTopologyFilePath.String(), topoJSON)
}

func (t *JSONReader) Get() (Topology, error) {
	topoJSONBytes, errReading := t.reader.ReadFile(t.singleTopologyFilePath.String())
	if errReading != nil {
		return Topology{}, errReading
	}
	topo := Topology{}
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	return topo, json.Unmarshal(topoJSONBytes, &topo)
}

func (t *JSONReader) GetAll() ([]Topology, error) {
	topologiesJSONBytes, errReading := t.reader.ReadFile(t.topologiesFilePath.String())
	if errReading != nil {
		return []Topology{}, errReading
	}
	var topologies []Topology
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	return topologies, json.Unmarshal(topologiesJSONBytes, &topologies)
}
