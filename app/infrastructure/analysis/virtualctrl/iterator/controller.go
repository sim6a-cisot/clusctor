package iterator

import (
	"clusctor/app/domain/service/management"
	topologyStorage "clusctor/app/infrastructure/analysis/topology"
)

func New(topologyDAO topologyStorage.Storage) *Controller {
	return &Controller{
		topologyDAO:          topologyDAO,
		currentTopologyIndex: -1,
	}
}

type Controller struct {
	topologyDAO topologyStorage.Storage

	topologiesJSON       []topologyStorage.Topology
	currentTopologyIndex int
}

func (c *Controller) ReadTopologies() error {
	topologiesJSON, errReading := c.topologyDAO.GetAll()
	if errReading != nil {
		return errReading
	}
	c.topologiesJSON = topologiesJSON
	return nil
}

func (c *Controller) NextTopology() bool {
	if c.currentTopologyIndex >= -1 && c.currentTopologyIndex < len(c.topologiesJSON)-1 {
		c.currentTopologyIndex++
		return true
	}
	return false
}

func (c *Controller) Reset() {
	c.currentTopologyIndex = -1
}

func (c *Controller) currentTopology() topologyStorage.Topology {
	if len(c.topologiesJSON) == 0 {
		return topologyStorage.Topology{}
	}
	return c.topologiesJSON[c.currentTopologyIndex]
}

func (c *Controller) ScanNetwork(updater management.TopologyUpdater) error {
	topo := c.currentTopology()
	updater.Update(
		topo.Switches.ToSDNSwitches(),
		topo.Hosts.ToSDNHosts(),
		topo.Links.ToSDNLinks(),
	)
	return nil
}
