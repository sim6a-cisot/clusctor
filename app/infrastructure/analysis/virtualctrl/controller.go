package virtualctrl

import (
	"clusctor/app/domain/model/management/flow"
	"clusctor/app/domain/service/management"
	"clusctor/app/infrastructure/analysis/topology"
)

func New(topologyDAO topology.Storage) *Controller {
	return &Controller{
		topologyDAO: topologyDAO,
	}
}

type Controller struct {
	topologyDAO topology.Storage
}

func (c *Controller) ScanNetwork(updater management.TopologyUpdater) error {
	topo, errReading := c.topologyDAO.Get()
	if errReading != nil {
		return errReading
	}

	updater.Update(
		topo.Switches.ToSDNSwitches(),
		topo.Hosts.ToSDNHosts(),
		topo.Links.ToSDNLinks(),
	)

	return nil
}

func (c *Controller) PushFlows([]flow.Flow) error {
	// Do nothing
	return nil
}

func (c *Controller) ReadFlows() (flow.Collection, error) {
	// Do nothing
	return flow.Collection{}, nil
}
