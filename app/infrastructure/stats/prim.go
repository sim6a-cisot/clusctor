package stats

import (
	"clusctor/app/domain/model/stats"
	"clusctor/app/infrastructure/stats/csv"
	"clusctor/app/pkg/stdconv"
	"encoding/json"
	"strconv"
)

type PrimStats struct {
	entries []stats.Prim
}

func NewPrimStats(entries []stats.Prim) *PrimStats {
	return &PrimStats{entries: entries}
}

func (s *PrimStats) AsCSV() [][]string {
	records := make([][]string, 0, len(s.entries)+1)
	header := []string{
		"№",
		"N",
		"M",
		"D",
		"CostMin",
	}
	records = append(records, header)

	for i, entry := range s.entries {
		records = append(records, []string{
			strconv.Itoa(i + 1),
			strconv.Itoa(entry.N),
			strconv.Itoa(entry.M),
			strconv.Itoa(entry.D),
			stdconv.Float64ToStr(entry.MinCost),
		})
	}

	return records
}

func (s *PrimStats) ToJSON() []byte {
	b, _ := json.Marshal(s.entries)
	return b
}

func (s *PrimStats) ToCSV() []byte {
	return csv.MakeCSVBytes(s.AsCSV())
}
