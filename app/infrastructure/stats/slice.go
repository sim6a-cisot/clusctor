package stats

import (
	"clusctor/app/domain/model/stats"
	"clusctor/app/infrastructure/stats/csv"
	"clusctor/app/pkg/stdconv"
	"encoding/json"
	"strconv"
)

type SliceStats struct {
	entries []stats.Slice
}

func NewSliceStats(entries []stats.Slice) *SliceStats {
	return &SliceStats{entries: entries}
}

func (s *SliceStats) AsCSV() [][]string {
	records := make([][]string, 0, len(s.entries)+1)
	header := []string{
		"№",
		"N",
		"M",
		"H",
		"Nmin",
		"Nmax",
		"Mmin",
		"Mmax",
		"Hmin",
		"Hmax",
		"Time, ms",
	}
	records = append(records, header)

	for i, entry := range s.entries {
		records = append(records, []string{
			strconv.Itoa(i + 1),
			strconv.Itoa(entry.N),
			strconv.Itoa(entry.M),
			strconv.Itoa(entry.H),
			strconv.Itoa(entry.NMin),
			strconv.Itoa(entry.NMax),
			strconv.Itoa(entry.MMin),
			strconv.Itoa(entry.MMax),
			strconv.Itoa(entry.HMin),
			strconv.Itoa(entry.HMax),
			stdconv.Float64ToStr(entry.Time),
		})
	}

	return records
}

func (s *SliceStats) ToJSON() []byte {
	b, _ := json.Marshal(s.entries)
	return b
}

func (s *SliceStats) ToCSV() []byte {
	return csv.MakeCSVBytes(s.AsCSV())
}