package stats

import (
	"clusctor/app/domain/model/stats"
	"clusctor/app/infrastructure/stats/csv"
	"clusctor/app/pkg/stdconv"
	"encoding/json"
	"strconv"
)

type GirvanNewmanStats struct {
	entries []stats.GirvanNewman
}

func NewGirvanNewmanStats(entries []stats.GirvanNewman) *GirvanNewmanStats {
	return &GirvanNewmanStats{entries: entries}
}

func (s *GirvanNewmanStats) AsCSV() [][]string {
	records := make([][]string, 0, len(s.entries)+1)
	header := []string{
		"No",
		"N",
		"M",
		"S",
		"D",
		"Dmin",
		"Dmax",
		"Nmin",
		"Nmax",
		"Nmean",
		"Mmin",
		"Mmax",
		"Mmean",
		"Mout",
		"Moutmin",
		"Moutmax",
		"Qmin",
		"Qmax",
		"Qmean",
		"Time, ms",
	}
	records = append(records, header)

	for i, entry := range s.entries {
		records = append(records, []string{
			strconv.Itoa(i + 1),
			strconv.Itoa(entry.N),
			strconv.Itoa(entry.M),
			stdconv.Float64ToStr(entry.S),
			strconv.Itoa(entry.D),
			strconv.Itoa(entry.DMin),
			strconv.Itoa(entry.DMax),
			strconv.Itoa(entry.NMin),
			strconv.Itoa(entry.NMax),
			stdconv.Float64ToStr(entry.NMean),
			strconv.Itoa(entry.MMin),
			strconv.Itoa(entry.MMax),
			stdconv.Float64ToStr(entry.MMean),
			stdconv.Float64ToStr(entry.MOut),
			strconv.Itoa(entry.MOutMin),
			strconv.Itoa(entry.MOutMax),
			stdconv.Float64ToStr(entry.QMin),
			stdconv.Float64ToStr(entry.QMax),
			stdconv.Float64ToStr(entry.QMean),
			stdconv.Float64ToStr(entry.Time),
		})
	}

	return records
}

func (s *GirvanNewmanStats) ToJSON() []byte {
	b, _ := json.Marshal(s.entries)
	return b
}

func (s *GirvanNewmanStats) ToCSV() []byte {
	return csv.MakeCSVBytes(s.AsCSV())
}
