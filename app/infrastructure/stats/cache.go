package stats

import (
	"clusctor/app/domain/model/stats"
)

func NewCache() *Cache {
	return &Cache{}
}

type Cache struct {
	bisectionStats []stats.Bisection
	girvanNewmanStats []stats.GirvanNewman
	segmentationStats []stats.Segmentation
	slicingStats []stats.Slice
	primStats []stats.Prim
}

func (c *Cache) Clear() {
	c.bisectionStats = []stats.Bisection{}
	c.girvanNewmanStats = []stats.GirvanNewman{}
	c.segmentationStats = []stats.Segmentation{}
	c.slicingStats = []stats.Slice{}
	c.primStats = []stats.Prim{}
}

func (c *Cache) WriteBisectionStats(e stats.Bisection) {
	c.bisectionStats = append(c.bisectionStats, e)
}

func (c *Cache) WriteSegmentationStats(e stats.Segmentation) {
	c.segmentationStats = append(c.segmentationStats, e)
}

func (c *Cache) WriteSlicingStats(e stats.Slice) {
	c.slicingStats = append(c.slicingStats, e)
}

func (c *Cache) WritePrimStats(e stats.Prim) {
	c.primStats = append(c.primStats, e)
}

func (c *Cache) WriteGirvanNewmanStats(e stats.GirvanNewman) {
	c.girvanNewmanStats = append(c.girvanNewmanStats, e)
}

func (c *Cache) ReadBisectionStats() stats.Presentable {
	return NewBisectionStats(c.bisectionStats)
}

func (c *Cache) ReadSegmentationStats() stats.Presentable {
	return NewSegmentationStats(c.segmentationStats)
}

func (c *Cache) ReadGirvanNewmanStats() stats.Presentable {
	return NewGirvanNewmanStats(c.girvanNewmanStats)
}

func (c *Cache) ReadSlicingStats() stats.Presentable {
	return NewSliceStats(c.slicingStats)
}

func (c *Cache) ReadPrimStats() stats.Presentable {
	return NewPrimStats(c.primStats)
}

