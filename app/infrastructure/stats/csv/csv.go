package csv

import (
	"bytes"
	"encoding/csv"
)

func MakeCSVBytes(records [][]string) []byte {
	buffer := &bytes.Buffer{}
	csvWriter := csv.NewWriter(buffer)
	_ = csvWriter.WriteAll(records)
	csvWriter.Flush()
	return buffer.Bytes()
}
