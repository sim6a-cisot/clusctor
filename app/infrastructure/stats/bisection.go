package stats

import (
	"clusctor/app/domain/model/stats"
	"clusctor/app/infrastructure/stats/csv"
	"clusctor/app/pkg/stdconv"
	"encoding/json"
	"strconv"
)

type BisectionStats struct {
	entries []stats.Bisection
}

func NewBisectionStats(entries []stats.Bisection) *BisectionStats {
	s := &BisectionStats{entries: entries}
	s.addFooterWithMeanValues()
	return s
}

func (s *BisectionStats) addFooterWithMeanValues() {
	meanValues := stats.Bisection{}

	for _, entry := range s.entries {
		meanValues.NMean += entry.NMean
		meanValues.MeanVertexDegree += entry.MeanVertexDegree
		meanValues.IncorrectLocalShortestPathsQuantity += entry.IncorrectLocalShortestPathsQuantity
		meanValues.MeanAbsoluteLocalPathError += entry.MeanAbsoluteLocalPathError
		meanValues.LocalShortestPathsInRercents += entry.LocalShortestPathsInRercents
		meanValues.LocalPathErrorInPercents += entry.LocalPathErrorInPercents
		meanValues.S += entry.S
		meanValues.Time += entry.Time
		meanValues.MOut += entry.MOut
	}

	entriesQuantity := float64(len(s.entries))
	meanValues.NMean = meanValues.NMean / entriesQuantity
	meanValues.MeanVertexDegree = meanValues.MeanVertexDegree / entriesQuantity
	meanValues.IncorrectLocalShortestPathsQuantity = meanValues.IncorrectLocalShortestPathsQuantity / entriesQuantity
	meanValues.MeanAbsoluteLocalPathError = meanValues.MeanAbsoluteLocalPathError / entriesQuantity
	meanValues.LocalShortestPathsInRercents = meanValues.LocalShortestPathsInRercents / entriesQuantity
	meanValues.LocalPathErrorInPercents = meanValues.LocalPathErrorInPercents / entriesQuantity
	meanValues.S = meanValues.S / entriesQuantity
	meanValues.Time = meanValues.Time / entriesQuantity
	meanValues.MOut = meanValues.MOut / entriesQuantity

	s.entries = append(s.entries, meanValues)
}

func (s *BisectionStats) AsCSV() [][]string {
	records := make([][]string, 0, len(s.entries)+1)
	header := []string{
		"No",
		"PS",
		"N",
		"M",
		"S",
		"D",
		"Dmin",
		"Dmax",
		"Nmin",
		"Nmax",
		"Nmean",
		"Mmin",
		"Mmax",
		"Mmean",
		"Mout",
		"Moutmin",
		"Moutmax",
		"Qmin",
		"Qmax",
		"Qmean",
		"Time, ms",
		"SPQ",
		"LSPQ",
		"ILSPQ",
		"GSPQ",
		"LSPinP",
		"MeanALPE",
		"LPEinP",
		"MinALPE",
		"MaxALPE",
		"MeanVD",
	}
	records = append(records, header)

	for i, entry := range s.entries {
		records = append(records, []string{
			strconv.Itoa(i + 1),
			strconv.Itoa(entry.PredefinedS),
			strconv.Itoa(entry.N),
			strconv.Itoa(entry.M),
			stdconv.Float64ToStr(entry.S),
			strconv.Itoa(entry.D),
			strconv.Itoa(entry.DMin),
			strconv.Itoa(entry.DMax),
			strconv.Itoa(entry.NMin),
			strconv.Itoa(entry.NMax),
			stdconv.Float64ToStr(entry.NMean),
			strconv.Itoa(entry.MMin),
			strconv.Itoa(entry.MMax),
			stdconv.Float64ToStr(entry.MMean),
			stdconv.Float64ToStr(entry.MOut),
			strconv.Itoa(entry.MOutMin),
			strconv.Itoa(entry.MOutMax),
			stdconv.Float64ToStr(entry.QMin),
			stdconv.Float64ToStr(entry.QMax),
			stdconv.Float64ToStr(entry.QMean),
			stdconv.Float64ToStr(entry.Time),
			strconv.Itoa(entry.ShortestPathsQuantity),
			strconv.Itoa(entry.LocalShortestPathsQuantity),
			stdconv.Float64ToStr(entry.IncorrectLocalShortestPathsQuantity),
			strconv.Itoa(entry.GlobalShortestPathsQuantity),
			stdconv.Float64ToStr(entry.LocalShortestPathsInRercents),
			stdconv.Float64ToStr(entry.MeanAbsoluteLocalPathError),
			stdconv.Float64ToStr(entry.LocalPathErrorInPercents),
			stdconv.Float64ToStr(entry.MinAbsoluteLocalPathError),
			stdconv.Float64ToStr(entry.MaxAbsoluteLocalPathError),
			stdconv.Float64ToStr(entry.MeanVertexDegree),
		})
	}

	return records
}

func (s *BisectionStats) ToJSON() []byte {
	b, _ := json.Marshal(s.entries)
	return b
}

func (s *BisectionStats) ToCSV() []byte {
	return csv.MakeCSVBytes(s.AsCSV())
}
