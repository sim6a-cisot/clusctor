package http

import (
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"time"
)

type ServerConfig struct {
	Port int
}

func RunServer(cfg ServerConfig, router http.Handler) error {
	server := &http.Server{
		Addr:           portStr(cfg.Port),
		Handler:        router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	return <-runServer(server)
}

func portStr(port int) string {
	return ":" + strconv.Itoa(port)
}

func runServer(server *http.Server) chan error {
	errs := make(chan error, 1)

	go func() {
		errs <- server.ListenAndServe()
	}()

	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, os.Interrupt)
		errs <- fmt.Errorf("%s", <-c)
	}()

	return errs
}
