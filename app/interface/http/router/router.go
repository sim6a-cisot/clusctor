package router

import (
	"clusctor/app/application/service"
	"clusctor/app/interface/http/handler/bisection"
	"clusctor/app/interface/http/handler/dijkstra"
	"clusctor/app/interface/http/handler/errors"
	"clusctor/app/interface/http/handler/file"
	"clusctor/app/interface/http/handler/girvannewman"
	"clusctor/app/interface/http/handler/link"
	"clusctor/app/interface/http/handler/metric"
	"clusctor/app/interface/http/handler/path"
	"clusctor/app/interface/http/handler/prim"
	"clusctor/app/interface/http/handler/segment"
	"clusctor/app/interface/http/handler/segmentation"
	"clusctor/app/interface/http/handler/slice"
	"clusctor/app/interface/http/handler/topology"
	"clusctor/app/interface/http/handler/tree"
	ginzap "github.com/gin-contrib/zap"
	"github.com/gin-gonic/gin"
	"time"
)

func NewRouter(
	swagger gin.HandlerFunc,
	errorCatcher *errors.Handler,
	pathHandler *path.Handler,
	fileServer *file.Handler,
	zapProvider service.ZapProvider,
	eSegments *segmentation.Handler,
	segments *segment.Handler,
	bisectionSegments *bisection.Handler,
	girvanNewmanSegments *girvannewman.Handler,
	topology *topology.Handler,
	tree *tree.Handler,
	primTree *prim.Handler,
	link *link.Handler,
	metric *metric.Handler,
	shortestPath *dijkstra.Handler,
	qosSlices *slice.Handler,
) *gin.Engine {
	gin.SetMode(gin.ReleaseMode)
	router := gin.New()
	//useLoggingMiddleware(router, zapProvider)

	v1 := router.Group(APIV1Route)
	{
		v1.POST(PathRoute, pathHandler.CreatePath(errorCatcher)...)
		v1.GET(PathRoute, pathHandler.GetAllPaths(errorCatcher)...)
		v1.POST(PathViewRoute, pathHandler.CreatePathView(errorCatcher)...)
		v1.POST(BisectionSegmentsRoute, bisectionSegments.Create)
		v1.GET(BisectionAnalysisStatsRoute, bisectionSegments.GetAnalysisStats)
		v1.POST(GirvanNewmanSegmentsRoute, girvanNewmanSegments.Create)
		v1.GET(GirvanNewmanSegmentsStatsRoute, girvanNewmanSegments.GetStats)
		v1.GET(GirvanNewmanAnalysisStatsRoute, girvanNewmanSegments.GetAnalysisStats)
		v1.DELETE(SegmentsRoute, segments.Delete)
		v1.GET(TopologyRoute, topology.Get)
		v1.POST(TopologyRoute, topology.Post(errorCatcher)...)
		v1.POST(PrimTreeRoute, primTree.Create)
		v1.GET(PrimTreeStatsRoute, primTree.GetStats)
		v1.DELETE(TreeRoute, tree.Delete)
		v1.POST(ESegmenterSegmentsRoute, eSegments.Create)
		v1.GET(ESegmenterSegmentsStatsRoute, eSegments.GetStats)
		v1.GET(ESegmenterAnalysisStatsRoute, eSegments.GetAnalysisStats)
		v1.GET(BisectionSegmentsStatsRoute, bisectionSegments.GetStats)
		v1.GET(QosSlicesStatsRoute, qosSlices.GetStats)
		v1.PUT(LinkRoute, link.Update)
		v1.PUT(MetricRoute, metric.Update)
		v1.GET(MetricRoute, metric.Get)
		v1.POST(DijkstraPathRoute, shortestPath.CreatePath(errorCatcher)...)
		v1.GET(DijkstraPathRoute, shortestPath.ShowPath)
		v1.POST(QosSlicesRoute, qosSlices.Create)
		v1.PUT(QosSlicesRoute, qosSlices.Put)
		v1.DELETE(QosSlicesRoute, qosSlices.Delete)
	}
	router.GET(PingRoute, func(c *gin.Context) {
		c.String(200, "pong")
	})
	router.GET(SwaggerRoute, swagger)
	router.NoRoute(fileServer.Handle)
	return router
}

func useLoggingMiddleware(router *gin.Engine, zapProvider service.ZapProvider) {
	router.Use(ginzap.Ginzap(zapProvider.ProvideZap(), time.RFC3339, true))
	router.Use(ginzap.RecoveryWithZap(zapProvider.ProvideZap(), true))
}
