package girvannewman

import (
	"clusctor/app/domain/service/management"
	domainStats "clusctor/app/domain/service/stats"
	"clusctor/app/interface/http"
	"github.com/gin-gonic/gin"
	netHttp "net/http"
)

type Handler struct {
	service        management.GirvanNewmanService
	statsReader    domainStats.Reader
	responseWriter http.ResponseWriter
	analyzer       management.Analyser
}

func NewHandler(
	service management.GirvanNewmanService,
	statsReader domainStats.Reader,
	responseWriter http.ResponseWriter,
	analyzer management.Analyser,
) *Handler {
	return &Handler{
		service:        service,
		statsReader:    statsReader,
		responseWriter: responseWriter,
		analyzer:       analyzer,
	}
}

// @Summary Create segments
// @Description Create segments with Girvan Newman algorithm.
// @Tags girvan-newman
// @Accept json
// @Produce json
// @Success 201 {string} string	"Created"
// @Failure 400 {object} http.ErrorResponse
// @Router /girvan-newman/segments [post]
func (h *Handler) Create(c *gin.Context) {
	h.service.RunPartitioning()
	h.responseWriter.WriteSuccess(c, netHttp.StatusCreated)
}

const (
	girvanNewmanStatsCSV = "girvan_newman_stats.csv"
)

// @Summary Get Girvan-Newman algorithm statistics
// @Description Get graph statistics with Girvan-Newman algorithm data.
// @Param export query string false "Export format" Enums(csv)
// @Tags girvan-newman
// @Accept json
// @Produce json
// @Success 200 {string} string	"Exported"
// @Success 200 {array} stats.GirvanNewman
// @Router /girvan-newman/segments/stats [get]
func (h *Handler) GetStats(c *gin.Context) {
	s := h.statsReader.ReadGirvanNewmanStats()
	if c.Query(http.ExportParam) == http.CSVFormat {
		http.AddCSVHeaders(c, girvanNewmanStatsCSV)
		_, _ = c.Writer.Write(s.ToCSV())
		return
	}
	http.AddJSONHeaders(c)
	_, _ = c.Writer.Write(s.ToJSON())
}

// @Summary Get Girvan-Newman algorithm analysis statistics
// @Description Run Girvan-Newman algorithm multiple times and gather statistics.
// @Tags girvan-newman
// @Accept json
// @Produce json
// @Success 200 {string} string	"Exported"
// @Router /girvan-newman/analysis/stats [get]
func (h *Handler) GetAnalysisStats(c *gin.Context) {
	errResponse := http.NewErrResponse()
	if errResponse.IsNotEmpty() {
		h.responseWriter.WriteError(c, netHttp.StatusBadRequest, errResponse)
		return
	}
	csvStats, err := h.analyzer.AnalyzeGirvanNewmanAlgorithm()
	if err != nil {
		errResponse.InternalError = err
		h.responseWriter.WriteInternalError(c, errResponse)
		return
	}
	http.AddCSVHeaders(c, girvanNewmanStatsCSV)
	_, _ = c.Writer.Write(csvStats)
}
