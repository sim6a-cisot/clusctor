package slice

import (
	"clusctor/app/domain/model/management/mac"
	"clusctor/app/domain/model/management/slice"
	"clusctor/app/domain/model/management/traffic"
	"clusctor/app/interface/http"
)

const (
	hosts       = "hosts"
	trafficType = "trafficType"
)

func validateDescriptions(descriptions []slice.Description, errs http.Errors) {
	for _, description := range descriptions {
		validateTrafficType(trafficType, description.TrafficType, errs)
		validateHosts(description, errs)
	}
}

func validateTrafficType(fieldName string, t traffic.Type, errs http.Errors) {
	switch t {
	case traffic.FileExchange, traffic.VideoStream, traffic.Email, traffic.VoIP, traffic.ECommerce:
	// traffic type is correct, do nothing
	default:
		errs[fieldName] = http.CanTakeOneOfFollowingValues(
			traffic.FileExchange.String(), traffic.VideoStream.String(), traffic.Email.String(),
			traffic.VoIP.String(), traffic.ECommerce.String(),
		)
	}
}

func validateHosts(description slice.Description, errs http.Errors) {
	for _, m := range description.Hosts {
		if err := mac.Validate(m); err != nil {
			errs[hosts+"["+m.String()+"]"] = err.Error()
		}
	}
}
