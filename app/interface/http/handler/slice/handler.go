package slice

import (
	"clusctor/app/domain/model/management/slice"
	"clusctor/app/domain/model/management/traffic"
	"clusctor/app/domain/service/management"
	"clusctor/app/domain/service/stats"
	"clusctor/app/interface/http"
	"github.com/gin-gonic/gin"
	netHttp "net/http"
)

type Handler struct {
	responseWriter http.ResponseWriter
	service        management.SliceService
	constructor    management.SliceConstructor
	statsReader    stats.Reader
}

func NewHandler(
	responseWriter http.ResponseWriter,
	service management.SliceService,
	constructor management.SliceConstructor,
	statsReader stats.Reader,
) *Handler {
	return &Handler{
		responseWriter: responseWriter,
		service:        service,
		constructor:    constructor,
		statsReader:    statsReader,
	}
}

const (
	sliceInstallationError = "slice installation error"
)

// @Summary Create QoS slices
// @Description Create QoS slices by traffic type
// @Param SliceDescriptions body model.DescriptionsRequest true "SliceStats descriptions request"
// @Tags qos-slicer
// @Accept json
// @Produce json
// @Success 201 {string} string	"OK"
// @Failure 400 {object} http.ErrorResponse
// @Router /qos-slicer/slices [post]
func (h *Handler) Create(c *gin.Context) {
	errResponse := http.NewErrResponse()
	descriptions := h.getSliceDescriptions(c, errResponse)
	if errResponse.IsNotEmpty() {
		h.responseWriter.WriteError(c, netHttp.StatusBadRequest, errResponse)
		return
	}
	if err := h.constructor.ConstructSlices(descriptions); err != nil {
		errResponse.Errors[sliceInstallationError] = err.Error()
		h.responseWriter.WriteError(c, netHttp.StatusInternalServerError, errResponse)
		return
	}
	h.responseWriter.WriteSuccess(c, netHttp.StatusCreated)
}

func (Handler) getSliceDescriptions(c *gin.Context, errResponse http.ErrorResponse) []slice.Description {
	descriptions := convertRequestToDescriptions(c, errResponse.Errors)
	if errResponse.IsNotEmpty() {
		return descriptions
	}
	validateDescriptions(descriptions, errResponse.Errors)
	return descriptions
}

const trafficTypeParam = "trafficType"

// @Summary Show slice
// @Description Show QoS slice by traffic type
// @Param trafficType query string true "Traffic Type" Enums(fileExchange, videoStream, email)
// @Tags qos-slicer
// @Accept json
// @Produce json
// @Success 201 {string} string	"OK"
// @Failure 400 {object} http.ErrorResponse
// @Router /qos-slicer/slices [put]
func (h *Handler) Put(c *gin.Context) {
	errResponse := http.NewErrResponse()
	trafficType := traffic.Type(c.Query(trafficTypeParam))
	validateTrafficType(trafficTypeParam, trafficType, errResponse.Errors)
	if errResponse.IsNotEmpty() {
		h.responseWriter.WriteError(c, netHttp.StatusBadRequest, errResponse)
		return
	}
	h.service.ShowSlice(trafficType)
	h.responseWriter.WriteSuccess(c, netHttp.StatusOK)
}

// @Summary Delete slices
// @Description Delete QoS slices
// @Tags qos-slicer
// @Accept json
// @Produce json
// @Success 201 {string} string	"OK"
// @Router /qos-slicer/slices [delete]
func (h *Handler) Delete(c *gin.Context) {
	h.service.DeleteSlices()
	h.responseWriter.WriteSuccess(c, netHttp.StatusOK)
}

// @Summary Get slicing statistics
// @Description Get graph statistics with slicing data.
// @Param export query string false "Export format" Enums(csv)
// @Tags qos-slicer
// @Accept json
// @Produce json
// @Success 200 {string} string	"Exported"
// @Success 200 {array} stats.SliceStats
// @Router /qos-slicer/slices/stats [get]
func (h *Handler) GetStats(c *gin.Context) {
	s := h.statsReader.ReadSlicingStats()
	if c.Query(http.ExportParam) == http.CSVFormat {
		http.AddCSVHeaders(c, "slicing_stats.csv")
		_, _ = c.Writer.Write(s.ToCSV())
		return
	}
	http.AddJSONHeaders(c)
	_, _ = c.Writer.Write(s.ToJSON())
}
