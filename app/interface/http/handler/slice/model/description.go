package model

import (
	"clusctor/app/domain/model/management/mac"
	"clusctor/app/domain/model/management/slice"
	"clusctor/app/domain/model/management/traffic"
	"encoding/json"
)

type DescriptionRequest struct {
	TrafficType  string       `json:"trafficType"`
	Hosts        []string     `json:"hosts"`
	FlowSettings FlowSettings `json:"flowSettings"`
}

type FlowSettings struct {
	HardTimeout int `json:"hardTimeout"`
	IdleTimeout int `json:"idleTimeout"`
	Priority    int `json:"priority"`
}

type DescriptionsRequest []DescriptionRequest

func (r DescriptionRequest) ToDescription() slice.Description {
	return slice.Description{
		TrafficType: traffic.Type(r.TrafficType),
		Hosts:       r.stringsToMacs(r.Hosts),
		FlowSettings: slice.FlowSettings{
			HardTimeout: r.FlowSettings.HardTimeout,
			IdleTimeout: r.FlowSettings.IdleTimeout,
			Priority:    r.FlowSettings.Priority,
		},
	}
}

func (r DescriptionRequest) String() string {
	bytes, _ := json.MarshalIndent(&r, "", "  ")
	return string(bytes)
}

func (DescriptionRequest) stringsToMacs(source []string) []mac.MAC {
	var result []mac.MAC
	for _, m := range source {
		result = append(result, mac.MAC(m))
	}
	return result
}
