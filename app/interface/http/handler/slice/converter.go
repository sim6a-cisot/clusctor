package slice

import (
	"clusctor/app/domain/model/management/mac"
	"clusctor/app/domain/model/management/slice"
	"clusctor/app/domain/model/management/traffic"
	"clusctor/app/interface/http"
	"clusctor/app/interface/http/handler/slice/model"
	"github.com/gin-gonic/gin"
)

const (
	sliceRequestJSON = "sliceRequestJson"
)

func convertRequestToDescriptions(c *gin.Context, errs http.Errors) []slice.Description {
	var requests model.DescriptionsRequest
	if errBind := c.BindJSON(&requests); errBind != nil {
		errs[sliceRequestJSON] = errBind.Error()
	}
	descriptions := convertDescriptions(requests)
	return deleteDuplicatesFromRequests(descriptions)
}

func convertDescriptions(source []model.DescriptionRequest) []slice.Description {
	var result []slice.Description
	for _, s := range source {
		result = append(result, s.ToDescription())
	}
	return result
}

func deleteDuplicatesFromRequests(source []slice.Description) []slice.Description {
	var result []slice.Description
	isDuplicate := map[traffic.Type]bool{}
	for _, request := range source {
		if !isDuplicate[request.TrafficType] {
			request.Hosts = mac.DeleteDuplicates(request.Hosts)
			result = append(result, request)
			isDuplicate[request.TrafficType] = true
		}
	}
	return result
}
