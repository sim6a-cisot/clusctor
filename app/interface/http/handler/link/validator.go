package link

import (
	"clusctor/app/domain/model/management/dpid"
	"clusctor/app/interface/http"
)

func validateRequest(req request, errs http.Errors)  {
	validateDelay(req, errs)
	validateBandwidth(req, errs)
	validatePacketLossRate(req, errs)
	validateAdminMetric(req, errs)
	validateSourceDPID(req, errs)
	validateDestinationDPID(req, errs)
}

func validateDelay(req request, errs http.Errors) {
	min := float64(1)
	max := float64(10000)
	if req.delay < min || req.delay > max {
		errs[delayParam] = http.IsNotBetweenExpectedValues(min, max)
	}
}

func validateBandwidth(req request, errs http.Errors) {
	min := float64(0)
	max := float64(10000000)
	if req.bandwidth < min || req.bandwidth > max {
		errs[bandwidthParam] = http.IsNotBetweenExpectedValues(min, max)
	}
}

func validatePacketLossRate(req request, errs http.Errors) {
	min := float64(0)
	max := float64(100)
	if req.packetLossRate < min || req.packetLossRate > max {
		errs[packetLossRateParam] = http.IsNotBetweenExpectedValues(min, max)
	}
}

func validateAdminMetric(req request, errs http.Errors) {
	min := float64(1)
	max := float64(1000)
	if req.adminMetric < min || req.adminMetric > max {
		errs[adminMetricParam] = http.IsNotBetweenExpectedValues(min, max)
	}
}

func validateSourceDPID(req request, errs http.Errors) {
	if !dpid.DPID.MatchString(req.sourceDPID) {
		errs[srcDpidParam] = http.DPIDMustHaveNextFormat
	}
}

func validateDestinationDPID(req request, errs http.Errors) {
	if !dpid.DPID.MatchString(req.destinationDPID) {
		errs[dstDpidParam] = http.DPIDMustHaveNextFormat
	}
}

