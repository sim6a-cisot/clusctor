package link

import (
	"clusctor/app/domain/model/management/metric"
)

type params struct {
	sourceDPID      string
	destinationDPID string

	delay          string
	bandwidth      string
	packetLossRate string
	adminMetric    string
}

type request struct {
	sourceDPID      string
	destinationDPID string

	delay          float64
	bandwidth      float64
	packetLossRate float64
	adminMetric    float64
}

func (r request) metrics() metric.Metrics {
	return metric.NewMetrics(r.bandwidth, r.delay, r.packetLossRate, r.adminMetric)
}
