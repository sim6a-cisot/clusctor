package link

import (
	"clusctor/app/interface/http"
	"clusctor/app/pkg/stdconv"
)

func convertRequest(ln params, errs http.Errors) request {
	req := request{}
	var err error
	req.sourceDPID = ln.sourceDPID
	req.destinationDPID = ln.destinationDPID
	if req.bandwidth, err = stdconv.ParseFloat64(ln.bandwidth); err != nil {
		errs[bandwidthParam] = http.MustBeANumber
		err = nil
	}
	if req.delay, err = stdconv.ParseFloat64(ln.delay); err != nil {
		errs[delayParam] = http.MustBeANumber
		err = nil
	}
	if req.packetLossRate, err = stdconv.ParseFloat64(ln.packetLossRate); err != nil {
		errs[packetLossRateParam] = http.MustBeANumber
		err = nil
	}
	if req.adminMetric, err = stdconv.ParseFloat64(ln.adminMetric); err != nil {
		errs[adminMetricParam] = http.MustBeANumber
		err = nil
	}
	return req
}

