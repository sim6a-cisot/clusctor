package link

import (
	"clusctor/app/domain/model/management/topology"
	"clusctor/app/interface/http"
	netHttp "net/http"

	"github.com/gin-gonic/gin"
)

const (
	srcDpidParam        = "srcdpid"
	dstDpidParam        = "dstdpid"
	bandwidthParam      = "bandwidth"
	delayParam          = "delay"
	packetLossRateParam = "packetLossRate"
	adminMetricParam    = "adminMetric"
)

type Handler struct {
	connectToLink  topology.ConnectToLink
	responseWriter http.ResponseWriter
}

func NewHandler(
	connectToLink topology.ConnectToLink,
	responseWriter http.ResponseWriter,
) *Handler {
	return &Handler{connectToLink: connectToLink, responseWriter: responseWriter}
}

func (h *Handler) getRequest(c *gin.Context, errResponse http.ErrorResponse) request {
	req := convertRequest(params{
		sourceDPID:      c.Query(srcDpidParam),
		destinationDPID: c.Query(dstDpidParam),
		bandwidth:       c.Query(bandwidthParam),
		delay:           c.Query(delayParam),
		packetLossRate:  c.Query(packetLossRateParam),
		adminMetric:     c.Query(adminMetricParam),
	}, errResponse.Errors)
	if errResponse.IsNotEmpty() {
		return req
	}
	validateRequest(req, errResponse.Errors)
	return req
}

// @Summary Update link
// @Description Update link parameters.
// @Param srcdpid query string true "Source DPID"
// @Param dstdpid query string true "Destination DPID"
// @Param bandwidth query number true "Bandwidth"
// @Param delay query number true "Delay"
// @Param packetLossRate query number true "Packet Loss Rate"
// @Param adminMetric query number true "Administrative metric"
// @Tags topology
// @Accept json
// @Produce json
// @Success 200 {string} string	"OK"
// @Failure 400 {object} http.ErrorResponse
// @Router /topology/link [put]
func (h *Handler) Update(c *gin.Context) {
	errResponse := http.NewErrResponse()
	request := h.getRequest(c, errResponse)
	if errResponse.IsNotEmpty() {
		h.responseWriter.WriteError(c, netHttp.StatusBadRequest, errResponse)
		return
	}
	ln := h.connectToLink(request.sourceDPID, request.destinationDPID)
	ln.SetMetrics(request.metrics())
	h.responseWriter.WriteSuccess(c, netHttp.StatusOK)
}
