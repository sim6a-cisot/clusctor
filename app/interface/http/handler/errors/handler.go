package errors

import (
	"clusctor/app/domain/model/management/errors"
	"clusctor/app/interface/http"
	"github.com/gin-gonic/gin"
	netHttp "net/http"
)

type Handler struct {
	responseWriter http.ResponseWriter
}

func NewHandler(responseWriter http.ResponseWriter) *Handler {
	return &Handler{responseWriter: responseWriter}
}

func (ec *Handler) CatchErrors(c *gin.Context) {
	c.Next()
	if len(c.Errors) == 0 {
		return
	}
	errResponse := http.NewErrResponse()
	err := c.Errors[0].Err
	switch err.(type) {
	case *errors.ValidationError:
		errResponse.Errors[http.ValidationError] = err.Error()
		ec.responseWriter.WriteError(c, netHttp.StatusBadRequest, errResponse)
	default:
		errResponse.Errors[http.InternalError] = err.Error()
		ec.responseWriter.WriteError(c, netHttp.StatusInternalServerError, errResponse)
	}
}

