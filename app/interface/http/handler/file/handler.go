package file

import (
	"clusctor/app/application/service"
	"github.com/gin-gonic/gin"
	"path"
	"path/filepath"
)

type Handler struct {
	navigator service.FileServerNavigator
}

func NewHandler(navigator service.FileServerNavigator) *Handler {
	return &Handler{navigator: navigator}
}

func (h *Handler) Handle(c *gin.Context) {
	dir, filename := path.Split(c.Request.RequestURI)
	ext := filepath.Ext(filename)
	if filename == h.navigator.SwaggerFileName().String() {
		// swagger api documentation
		c.File(h.navigator.SwaggerFilePath().String())
	} else if filename == "" || ext == "" {
		// client app page
		c.File(h.navigator.ClientAppIndexFilePath().String())
	} else {
		// client app resources
		c.File(h.navigator.ClientAppResourcesDirPath().AddString(dir).AddString(filename).String())
	}
}
