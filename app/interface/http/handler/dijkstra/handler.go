package dijkstra

import (
	"clusctor/app/domain/model/management/mac"
	"clusctor/app/domain/model/management/path"
	"clusctor/app/domain/service/management"
	"clusctor/app/interface/http"
	"clusctor/app/interface/http/handler/errors"
	"clusctor/app/interface/http/handler/path/model"
	"github.com/gin-gonic/gin"
	netHttp "net/http"
)

type Handler struct {
	service         management.DijkstraService
	responseWriter  http.ResponseWriter
	pathFactory     path.Factory
	deviceConnector management.DeviceConnector
}

func NewHandler(
	service management.DijkstraService,
	responseWriter http.ResponseWriter,
	pathFactory path.Factory,
	deviceConnector management.DeviceConnector,
) *Handler {
	return &Handler{
		service:         service,
		responseWriter:  responseWriter,
		pathFactory:     pathFactory,
		deviceConnector: deviceConnector,
	}
}

// @Summary Create shortest path
// @Description Find shortest path with Dijkstra's algorithm
// @Param PathRequest body model.IncompletePathRequest true "path Request"
// @Tags dijkstra
// @Accept json
// @Produce json
// @Success 201 {string} string	"Created"
// @Failure 400 {object} http.ErrorResponse
// @Router /dijkstra/path [post]
func (h *Handler) CreatePath(catcher *errors.Handler) gin.HandlersChain {
	return gin.HandlersChain{
		catcher.CatchErrors, h.bindPath, h.validatePath, h.createPath,
	}
}

func (h *Handler) createPath(c *gin.Context) {
	req := c.MustGet(gin.BindKey).(*model.PathRequest)
	req.SwitchSequence = h.service.FindSwitchSequence(mac.MAC(req.SrcHost), mac.MAC(req.DstHost)).CollectLabels()
	p := h.pathFactory.Installer(req)
	if err := p.Install(); err != nil {
		_ = c.Error(err)
		return
	}
	h.responseWriter.WriteSuccess(c, netHttp.StatusCreated)
}

func (h *Handler) validatePath(c *gin.Context) {
	req := c.MustGet(gin.BindKey).(*model.PathRequest)
	errResponse := http.NewErrResponse()
	req.Validate(errResponse.Errors)
	if errResponse.IsNotEmpty() {
		h.responseWriter.WriteError(c, netHttp.StatusBadRequest, errResponse)
		c.Abort()
	}
}

func (h *Handler) bindPath(c *gin.Context) {
	req := model.PathRequest{}
	if errBinding := c.BindJSON(&req); errBinding != nil {
		errResponse := http.NewErrResponse()
		errResponse.AddInvalidJSONSchemaError()
		h.responseWriter.WriteError(c, netHttp.StatusBadRequest, errResponse)
		c.Abort()
		return
	}
	c.Set(gin.BindKey, &req)
}

const (
	srcMACParam = "srcmac"
	dstMACParam = "dstmac"
)

// @Summary Show shortest path
// @Description Shows shortest path with Dijkstra's algorithm
// @Tags dijkstra
// @Accept json
// @Produce json
// @Success 201 {string} string	"OK"
// @Router /dijkstra/path [get]
func (h *Handler) ShowPath(c *gin.Context) {
	errResponse := http.NewErrResponse()
	srcMac := h.getSrcMAC(c, errResponse)
	dstMac := h.getDstMAC(c, errResponse)
	if errResponse.IsNotEmpty() {
		h.responseWriter.WriteError(c, netHttp.StatusBadRequest, errResponse)
		return
	}
	h.service.ShowPath(srcMac, dstMac)
}

func (h *Handler) getSrcMAC(c *gin.Context, errResponse http.ErrorResponse) mac.MAC {
	return h.getMAC(c, srcMACParam, errResponse)
}

func (h *Handler) getMAC(c *gin.Context, macParam string, errResponse http.ErrorResponse) mac.MAC {
	macAddress := mac.MAC(c.Query(macParam))
	if errValidation := mac.Validate(macAddress); errValidation != nil {
		errResponse.Errors[macParam] = errValidation.Error()
		return macAddress
	}
	if !h.deviceConnector.IsHostConnected(macAddress) {
		errResponse.Errors[macParam] = http.HostDoesNotExists(macAddress.String())
		return macAddress
	}
	return macAddress
}

func (h *Handler) getDstMAC(c *gin.Context, errResponse http.ErrorResponse) mac.MAC {
	return h.getMAC(c, dstMACParam, errResponse)
}