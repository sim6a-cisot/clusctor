package prim

import (
	"clusctor/app/domain/service/management"
	"clusctor/app/domain/service/stats"
	"clusctor/app/interface/http"
	"github.com/gin-gonic/gin"
	netHttp "net/http"
)

type Handler struct {
	service        management.PrimService
	treeService    management.TreeService
	responseWriter http.ResponseWriter
	reader         stats.Reader
}

func NewHandler(
	service management.PrimService,
	treeService management.TreeService,
	responseWriter http.ResponseWriter,
	reader stats.Reader,
) *Handler {
	return &Handler{
		service: service,
		treeService: treeService,
		responseWriter:responseWriter,
		reader:reader,
	}
}

// @Summary Create minimum spanning tree
// @Description Create minimum spanning tree with PrimStats's algorithm.
// @Tags prim
// @Accept json
// @Produce json
// @Success 201 {string} string	"OK"
// @Router /prim/tree [post]
func (h *Handler) Create(c *gin.Context) {
	h.treeService.DeleteTree()
	h.service.RunPrim()
	h.responseWriter.WriteSuccess(c, netHttp.StatusCreated)
}

// @Summary Get PrimStats's algorithm statistics
// @Description Get PrimStats's algorithm statistics.
// @Param export query string false "Export format" Enums(csv)
// @Tags prim
// @Accept json
// @Produce json
// @Success 200 {string} string	"Exported"
// @Success 200 {array} stats.PrimStats
// @Router /prim/tree/stats [get]
func (h *Handler) GetStats(c *gin.Context) {
	s := h.reader.ReadPrimStats()
	if c.Query(http.ExportParam) == http.CSVFormat {
		http.AddCSVHeaders(c, "prim_stats.csv")
		_, _ = c.Writer.Write(s.ToCSV())
		return
	}
	http.AddJSONHeaders(c)
	_, _ = c.Writer.Write(s.ToJSON())
}