package swagger

import (
	"clusctor/app/interface/http/router"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func NewHandler() gin.HandlerFunc {
	config := &ginSwagger.Config{
		URL: router.SwaggerJSONRoute,
	}
	return ginSwagger.CustomWrapHandler(config, swaggerFiles.Handler)
}
