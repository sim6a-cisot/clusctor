package tree

import (
	"clusctor/app/domain/service/management"
	"clusctor/app/interface/http"
	"github.com/gin-gonic/gin"
	netHttp "net/http"
)

type Handler struct {
	service        management.TreeService
	responseWriter http.ResponseWriter
}

func NewHandler(service management.TreeService, responseWriter http.ResponseWriter) *Handler {
	return &Handler{
		service: service,
		responseWriter:responseWriter,
	}
}

// @Summary Delete tree
// @Description Delete minimum spanning tree or route.
// @Tags topology
// @Accept json
// @Produce json
// @Success 200 {string} string	"OK"
// @Router /topology/tree [delete]
func (h *Handler) Delete(c *gin.Context) {
	h.service.DeleteTree()
	h.responseWriter.WriteSuccess(c, netHttp.StatusOK)
}

