package model

type GeneratedTopologiesParams struct {
	SwitchesQuantity    int `form:"switches"`
	HostsQuantity       int `form:"hosts"`
	RandomLinksQuantity int `form:"randomLinks"`
	TopologiesQuantity  int `form:"topologies"`
}
