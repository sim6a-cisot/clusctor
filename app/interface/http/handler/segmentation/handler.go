package segmentation

import (
	"clusctor/app/domain/model/management/segmentation"
	"clusctor/app/domain/service/management"
	serviceStats "clusctor/app/domain/service/stats"
	"clusctor/app/interface/http"
	"clusctor/app/interface/http/handler/ginutil"
	"github.com/gin-gonic/gin"
	netHttp "net/http"
)

const (
	stepParam = "step"
)

type Handler struct {
	service        management.SegmentationService
	responseWriter http.ResponseWriter
	statsReader    serviceStats.Reader
	analyzer       management.Analyser
}

func NewHandler(
	service management.SegmentationService,
	responseWriter http.ResponseWriter,
	statsReader serviceStats.Reader,
	analyzer management.Analyser,
) *Handler {
	return &Handler{
		service:        service,
		responseWriter: responseWriter,
		statsReader:    statsReader,
		analyzer:       analyzer,
	}
}

func (h *Handler) getStep(c *gin.Context, errResponse http.ErrorResponse) (
	step segmentation.Step,
) {
	step = segmentation.Step(c.Query(stepParam))
	validateStep(
		stepParam,
		step,
		errResponse.Errors,
	)
	return
}

// @Summary Create segments
// @Description Create segments with Segmentation algorithm.
// @Param q query number true "Connectivity value" mininum(0.1) maxinum(1)
// @Param step query string true "Algorithm step" Enums(primarySegments, secondarySegments, merging)
// @Tags e-segmenter
// @Accept json
// @Produce json
// @Success 201 {string} string	"Created"
// @Failure 400 {object} http.ErrorResponse
// @Router /e-segmenter/segments [post]
func (h *Handler) Create(c *gin.Context) {
	errResponse := http.NewErrResponse()
	q := ginutil.GetQ(c, errResponse)
	step := h.getStep(c, errResponse)
	if errResponse.IsNotEmpty() {
		h.responseWriter.WriteError(c, netHttp.StatusBadRequest, errResponse)
		return
	}
	h.service.RunSegmentation(q, step)
	h.responseWriter.WriteSuccess(c, netHttp.StatusCreated)
}

const (
	segmentationStatsCSV = "segmentation_stats.csv"
)

// @Summary Get segmentation statistics
// @Description Get graph statistics with segmentation algorithm data.
// @Param export query string false "Export format" Enums(csv)
// @Tags e-segmenter
// @Accept json
// @Produce json
// @Success 200 {string} string	"Exported"
// @Success 200 {array} stats.Segmentation
// @Router /e-segmenter/segments/stats [get]
func (h *Handler) GetStats(c *gin.Context) {
	s := h.statsReader.ReadSegmentationStats()
	if c.Query(http.ExportParam) == http.CSVFormat {
		http.AddCSVHeaders(c, segmentationStatsCSV)
		_, _ = c.Writer.Write(s.ToCSV())
		return
	}
	http.AddJSONHeaders(c)
	_, _ = c.Writer.Write(s.ToJSON())
}

// @Summary Get segmentation algorithm analysis statistics
// @Description Run segmentation algorithm multiple times and gather statistics.
// @Param q query number true "Connectivity value" mininum(0.1) maxinum(10)
// @Tags e-segmenter
// @Accept json
// @Produce json
// @Success 200 {string} string	"Exported"
// @Router /e-segmenter/analysis/stats [get]
func (h *Handler) GetAnalysisStats(c *gin.Context) {
	errResponse := http.NewErrResponse()
	q := ginutil.GetQ(c, errResponse)
	if errResponse.IsNotEmpty() {
		h.responseWriter.WriteError(c, netHttp.StatusBadRequest, errResponse)
		return
	}
	csvStats, err := h.analyzer.AnalyzeSegmentationAlgorithm(q)
	if err != nil {
		errResponse.InternalError = err
		h.responseWriter.WriteInternalError(c, errResponse)
		return
	}
	http.AddCSVHeaders(c, segmentationStatsCSV)
	_, _ = c.Writer.Write(csvStats)
}