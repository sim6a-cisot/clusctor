package segmentation

import (
	"clusctor/app/domain/model/management/segmentation"
	"clusctor/app/interface/http"
)

func validateStep(
	fieldName string,
	step segmentation.Step,
	errs http.Errors,
) {
	switch step {
	case segmentation.PrimarySegments, segmentation.SecondarySegments, segmentation.Merging:
	// step is correct, do nothing
	default:
		errs[fieldName] = http.CanTakeOneOfFollowingValues(
			segmentation.PrimarySegments.String(), segmentation.SecondarySegments.String(), segmentation.Merging.String(),
		)
	}
}