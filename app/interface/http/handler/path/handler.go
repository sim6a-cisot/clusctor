package path

import (
	"clusctor/app/domain/model/management/path"
	"clusctor/app/interface/http"
	"clusctor/app/interface/http/handler/errors"
	"clusctor/app/interface/http/handler/path/model"
	"github.com/gin-gonic/gin"
	netHttp "net/http"
)

type Handler struct {
	pathFactory    path.Factory
	responseWriter http.ResponseWriter
}

func NewHandler(
	pathFactory path.Factory,
	responseWriter http.ResponseWriter,
) *Handler {
	return &Handler{
		pathFactory:    pathFactory,
		responseWriter: responseWriter,
	}
}

// @Summary Create path
// @Description Create path using custom administrator settings
// @Param PathRequest body model.PathRequest true "path Request"
// @Tags path
// @Accept json
// @Produce json
// @Success 201 {string} string	"Created"
// @Failure 400 {object} http.ErrorResponse
// @Router /path [post]
func (h *Handler) CreatePath(catcher *errors.Handler) gin.HandlersChain {
	return gin.HandlersChain{
		catcher.CatchErrors, h.bindPath, h.validatePath, h.createPath,
	}
}

// @Summary Create path view
// @Description Create path view on topology
// @Param PathRequest body model.PathRequest true "path Request"
// @Tags path
// @Accept json
// @Produce json
// @Success 200 {string} string	"OK"
// @Failure 400 {object} http.ErrorResponse
// @Router /path/view [post]
func (h *Handler) CreatePathView(catcher *errors.Handler) gin.HandlersChain {
	return gin.HandlersChain{
		catcher.CatchErrors, h.bindPath, h.validatePath, h.createPathView,
	}
}

// @Summary Get all paths
// @Description Get all paths in the network
// @Tags path
// @Accept json
// @Produce json
// @Success 200 {object} model.PathResponses
// @Router /path [get]
func (h *Handler) GetAllPaths(catcher *errors.Handler) gin.HandlersChain {
	return gin.HandlersChain{
		catcher.CatchErrors, h.readAllPaths,
	}
}

func (h *Handler) createPath(c *gin.Context) {
	req := c.MustGet(gin.BindKey).(*model.PathRequest)
	p := h.pathFactory.Installer(req)
	if err := p.Install(); err != nil {
		_ = c.Error(err)
		return
	}
	h.responseWriter.WriteSuccess(c, netHttp.StatusCreated)
}

func (h *Handler) createPathView(c *gin.Context) {
	req := c.MustGet(gin.BindKey).(*model.PathRequest)
	p := h.pathFactory.Visualizer(req)
	if err := p.Visualize(); err != nil {
		_ = c.Error(err)
		return
	}
	h.responseWriter.WriteSuccess(c, netHttp.StatusOK)
}

func (h *Handler) readAllPaths(c *gin.Context) {
	col := h.pathFactory.Collection()
	if err := col.Read(); err != nil {
		_ = c.Error(err)
		return
	}
	c.JSON(netHttp.StatusOK, model.NewPathResponseCollection(col))
}

func (h *Handler) validatePath(c *gin.Context) {
	req := c.MustGet(gin.BindKey).(*model.PathRequest)
	errResponse := http.NewErrResponse()
	req.Validate(errResponse.Errors)
	if errResponse.IsNotEmpty() {
		h.responseWriter.WriteError(c, netHttp.StatusBadRequest, errResponse)
		c.Abort()
	}
}

func (h *Handler) bindPath(c *gin.Context) {
	req := model.PathRequest{}
	if errBinding := c.BindJSON(&req); errBinding != nil {
		errResponse := http.NewErrResponse()
		errResponse.AddInvalidJSONSchemaError()
		h.responseWriter.WriteError(c, netHttp.StatusBadRequest, errResponse)
		c.Abort()
		return
	}
	req.Correct()
	c.Set(gin.BindKey, &req)
}