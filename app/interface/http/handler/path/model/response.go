package model

import (
	"clusctor/app/domain/model/management/path"
)

type PathResponse struct {
	SrcIP          string   `json:"srcIp" example:"10.0.0.1"`
	DstIP          string   `json:"dstIp" example:"10.0.0.2"`
	SrcHost        string   `json:"srcHost" example:"00:00:00:00:00:01"`
	DstHost        string   `json:"dstHost" example:"00:00:00:00:00:02"`
	SwitchSequence []string `json:"switchSequence" example:"00:00:00:00:00:00:00:a1,00:00:00:00:00:00:00:a2"`
	Priority       int      `json:"priority"`
	IdleTimeout    int      `json:"idleTimeout"`
	HardTimeout    int      `json:"hardTimeout"`
	EthType        string   `json:"ethType" example:"ipv4"`
	IPProto        string   `json:"ipProto" example:"tcp"`
	ServicePort    int      `json:"servicePort" example:"8080"`
}

type PathResponses []PathResponse

func newPathResponse(p path.View) PathResponse {
	return PathResponse{
		SrcIP:          p.SrcIP(),
		DstIP:          p.DstIP(),
		SrcHost:        p.SrcHost().String(),
		DstHost:        p.DstHost().String(),
		SwitchSequence: p.SwitchSequence(),
		Priority:       p.Priority(),
		IdleTimeout:    p.IdleTimeout(),
		HardTimeout:    p.HardTimeout(),
		EthType:        p.EthType(),
		IPProto:        p.IPProto(),
		ServicePort:    p.ServicePort(),
	}
}

func NewPathResponseCollection(it path.Iterator) PathResponses {
	var responses PathResponses
	for it.Next() {
		responses = append(responses, newPathResponse(it.Value()))
	}
	return responses
}
