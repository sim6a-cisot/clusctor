package model

import (
	"clusctor/app/domain/model/management/dpid"
	"clusctor/app/domain/model/management/mac"
	"clusctor/app/domain/model/management/path"
	"clusctor/app/interface/http"
)

type PathSettings struct {
	IdleTimeout int `json:"idleTimeout" example:"300"`
	HardTimeout int `json:"hardTimeout"`
	Priority    int `json:"priority" example:"30000"`
}

type IncompletePathRequest struct {
	PathSettings
	SrcHost     string `json:"srcHost" example:"00:00:00:00:00:01"`
	DstHost     string `json:"dstHost" example:"00:00:00:00:00:02"`
	EthType     string `json:"ethType" example:"ipv4"`
	IPProto     string `json:"ipProto" example:"tcp"`
	ServicePort int    `json:"servicePort" example:"8080"`
}

type PathRequest struct {
	IncompletePathRequest
	SwitchSequence []string `json:"switchSequence" example:"00:00:00:00:00:00:00:a1,00:00:00:00:00:00:00:a2"`
}

func (req PathRequest) Build(p path.Builder) {
	p.SetMatches(req.EthType, req.IPProto, mac.MAC(req.SrcHost), mac.MAC(req.DstHost), req.ServicePort)
	p.SetPriority(req.Priority)
	p.SetSwitchSequence(req.SwitchSequence)
	p.SetTimeouts(req.HardTimeout, req.IdleTimeout)
}

func (req *PathRequest) Correct() {
	req.SwitchSequence = req.switchSequenceWithoutDuplicates()
}

func (req PathRequest) Validate(errs http.Errors) {
	if errSrcHost := mac.Validate(mac.MAC(req.SrcHost)); errSrcHost != nil {
		errs["srcHost"] = errSrcHost.Error()
	}
	if errDstHost := mac.Validate(mac.MAC(req.DstHost)); errDstHost != nil {
		errs["dstHost"] = errDstHost.Error()
	}
	req.validateSwitchSequence(errs)
	req.validateEthType(errs)
	req.validateIPProto(errs)
	req.validateGreaterThanZero("idleTimeout", req.IdleTimeout, errs)
	req.validateGreaterThanZero("hardTimeout", req.HardTimeout, errs)
	req.validateGreaterThanZero("priority", req.Priority, errs)
}

func (req PathRequest) validateSwitchSequence(errs http.Errors) {
	for _, sw := range req.SwitchSequence {
		if !dpid.DPID.MatchString(sw) {
			errs["switchSequence["+sw+"]"] = http.DPIDMustHaveNextFormat
		}
	}
}

func (req PathRequest) switchSequenceWithoutDuplicates() []string {
	var sequence []string
	exists := map[string]bool{}
	for _, sw := range req.SwitchSequence {
		if !exists[sw] {
			exists[sw] = true
			sequence = append(sequence, sw)
		}
	}
	return sequence
}

func (req PathRequest) validateEthType(errs http.Errors) {
	if req.EthType == "" {
		return
	}
	ipv4 := "ipv4"
	if req.EthType != ipv4 {
		errs["ethType"] = http.CanTakeOneOfFollowingValues(ipv4)
	}
}

func (req PathRequest) validateIPProto(errs http.Errors) {
	if req.IPProto == "" {
		return
	}
	tcp := "tcp"
	udp := "udp"
	switch req.IPProto {
	case tcp, udp:
	// protocol is correct, do nothing
	default:
		errs["ipProto"] = http.CanTakeOneOfFollowingValues(
			tcp, udp,
		)
	}
}

func (PathRequest) validateGreaterThanZero(fieldName string, num int, errs http.Errors) {
	if num < 0 {
		errs[fieldName] = http.MustBeGraterThanInt(0)
	}
}
