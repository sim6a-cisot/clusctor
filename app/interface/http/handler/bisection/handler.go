package bisection

import (
	"clusctor/app/domain/service/management"
	serviceStats "clusctor/app/domain/service/stats"
	"clusctor/app/interface/http"
	"clusctor/app/interface/http/handler/ginutil"
	"github.com/gin-gonic/gin"
	netHttp "net/http"
)

type Handler struct {
	service        management.BisectionService
	statsReader    serviceStats.Reader
	responseWriter http.ResponseWriter
	analyzer       management.Analyser
}

func NewHandler(
	service management.BisectionService,
	responseWriter http.ResponseWriter,
	statsReader serviceStats.Reader,
	analyzer management.Analyser,
) *Handler {
	return &Handler{
		service:        service,
		responseWriter: responseWriter,
		statsReader:    statsReader,
		analyzer:       analyzer,
	}
}

// @Summary Create segments
// @Description Create segments with Recursive Bisection algorithm.
// @Param parts query int true "Result segments quantity." mininum(2)
// @Tags bisection
// @Accept json
// @Produce json
// @Success 201 {string} string	"Created"
// @Failure 400 {object} http.ErrorResponse
// @Router /bisection/segments [post]
func (h *Handler) Create(c *gin.Context) {
	errResponse := http.NewErrResponse()
	parts := ginutil.GetParts(c, errResponse)
	if errResponse.IsNotEmpty() {
		h.responseWriter.WriteError(c, netHttp.StatusBadRequest, errResponse)
		return
	}
	h.service.RunBisection(parts)
	h.responseWriter.WriteSuccess(c, netHttp.StatusCreated)
}

const (
	bisectionStatsCSV = "bisection_stats.csv"
)

// @Summary Get recursive bisection statistics
// @Description Get graph statistics with recursive bisection algorithm data.
// @Param export query string false "Export format" Enums(csv)
// @Tags bisection
// @Accept json
// @Produce json
// @Success 200 {string} string	"Exported"
// @Success 200 {array} stats.Bisection
// @Router /bisection/segments/stats [get]
func (h *Handler) GetStats(c *gin.Context) {
	s := h.statsReader.ReadBisectionStats()
	if c.Query(http.ExportParam) == http.CSVFormat {
		http.AddCSVHeaders(c, bisectionStatsCSV)
		_, _ = c.Writer.Write(s.ToCSV())
		return
	}
	http.AddJSONHeaders(c)
	_, _ = c.Writer.Write(s.ToJSON())
}

// @Summary Get bisection algorithm analysis statistics
// @Description Run bisection algorithm multiple times and gather statistics.
// @Param parts query int true "Result segments quantity." mininum(2)
// @Tags bisection
// @Accept json
// @Produce json
// @Success 200 {string} string	"Exported"
// @Router /bisection/analysis/stats [get]
func (h *Handler) GetAnalysisStats(c *gin.Context) {
	errResponse := http.NewErrResponse()
	parts := ginutil.GetParts(c, errResponse)
	if errResponse.IsNotEmpty() {
		h.responseWriter.WriteError(c, netHttp.StatusBadRequest, errResponse)
		return
	}
	csvStats, err := h.analyzer.AnalyzeBisectionAlgorithm(parts)
	if err != nil {
		errResponse.InternalError = err
		h.responseWriter.WriteInternalError(c, errResponse)
		return
	}
	http.AddCSVHeaders(c, bisectionStatsCSV)
	_, _ = c.Writer.Write(csvStats)
}
