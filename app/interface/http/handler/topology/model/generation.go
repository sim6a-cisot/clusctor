package model

import "clusctor/app/interface/http"

type TopologyGeneratorParams struct {
	SwitchesQuantity    int `form:"switches"`
	HostsQuantity       int `form:"hosts"`
	RandomLinksQuantity int `form:"randomLinks"`
	TopologiesQuantity  int `form:"topologies"`
}

func (p *TopologyGeneratorParams) IsSingleTopology() bool {
	return p.TopologiesQuantity == 1
}

func (p *TopologyGeneratorParams) Validate(errs http.Errors) {
	if p.SwitchesQuantity <= 0 {
		errs["switches"] = http.MustBeGraterThanInt(0)
	}
	if p.HostsQuantity < 0 {
		errs["hosts"] = http.MustBeGraterOrEqualInt(0)
	}
	if p.RandomLinksQuantity < 0 {
		errs["randomLinks"] = http.MustBeGraterOrEqualInt(0)
	}
	if p.TopologiesQuantity < 0 {
		errs["topologies"] = http.MustBeGraterThanInt(0)
	}
}
