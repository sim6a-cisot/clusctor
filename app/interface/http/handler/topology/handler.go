package topology

import (
	"clusctor/app/domain/service/analysis/topology"
	"clusctor/app/domain/service/management"
	"clusctor/app/interface/http"
	"clusctor/app/interface/http/handler/errors"
	"clusctor/app/interface/http/handler/topology/model"
	"github.com/gin-gonic/gin"
	netHttp "net/http"
)

const (
	saveParam  = "save"
	jsonFormat = "json"
)

type Handler struct {
	marshaller     management.TopologyMarshaller
	jsonSerializer management.TopologyJSONSerializer
	generator      topology.Generator
	responseWriter http.ResponseWriter
	topo           management.TopologyGraph
	analyzer       management.Analyser
}

func NewHandler(
	marshaller management.TopologyMarshaller,
	jsonSerializer management.TopologyJSONSerializer,
	responseWriter http.ResponseWriter,
	generator topology.Generator,
	topo management.TopologyGraph,
	analyzer management.Analyser,
) *Handler {
	return &Handler{
		marshaller:     marshaller,
		jsonSerializer: jsonSerializer,
		responseWriter: responseWriter,
		generator:      generator,
		topo:           topo,
		analyzer:       analyzer,
	}
}

// @Summary Get topology
// @Description Get SDN topology as cytoscape.js compatible json or save topology on disc as custom json.
// @Param save query string false "Save SDN topology on disc. Use 'json' or leave empty."
// @Tags topology
// @Accept json
// @Produce json
// @Success 200 {object} cytoscape.Topology
// @Failure 400 {object} http.ErrorResponse
// @Router /topology [get]
func (h *Handler) Get(c *gin.Context) {
	if c.Query(saveParam) == jsonFormat {
		h.createJsonCopyInFileSystem(c)
	} else {
		c.JSON(netHttp.StatusOK, h.marshaller)
	}
}

func (h *Handler) createJsonCopyInFileSystem(c *gin.Context) {
	if err := h.jsonSerializer.SaveTopologyAsJSON(); err != nil {
		errResponse := http.NewErrResponse()
		errResponse.InternalError = err
		h.responseWriter.WriteInternalError(c, errResponse)
	} else {
		h.responseWriter.WriteSuccess(c, netHttp.StatusCreated)
	}
}

// @Summary Generate a new topology
// @Description Generate a new topology for virtual controller.
// @Param switches query int true "Switches quantity."
// @Param hosts query int true "Hosts quantity."
// @Param randomLinks query int false "Random links quantity."
// @Param topologies query int false "Topologies quantity."
// @Tags topology
// @Accept json
// @Produce json
// @Success 201 {string} string	"OK"
// @Failure 400 {object} http.ErrorResponse
// @Router /topology [post]
func (h *Handler) Post(catcher *errors.Handler) gin.HandlersChain {
	return gin.HandlersChain{
		catcher.CatchErrors,
		h.bindTopologyGeneratorParams,
		h.validateTopologyGeneratorParams,
		h.generate,
	}
}

func (h *Handler) generate(c *gin.Context) {
	req := c.MustGet(gin.BindKey).(*model.TopologyGeneratorParams)
	if req.IsSingleTopology() {
		h.generateSingleTopology(c, req)
	} else {
		h.generateMultipleTopologies(c, req)
	}
}

func (h *Handler) generateSingleTopology(c *gin.Context, req *model.TopologyGeneratorParams) {
	errGenerating := h.generator.GenerateOne(req.SwitchesQuantity, req.RandomLinksQuantity, req.HostsQuantity)
	if errGenerating != nil {
		_ = c.Error(errGenerating)
		return
	}
	h.responseWriter.WriteSuccess(c, netHttp.StatusCreated)
}

func (h *Handler) generateMultipleTopologies(c *gin.Context, req *model.TopologyGeneratorParams) {
	errGenerating := h.generator.GenerateMultiple(
		req.SwitchesQuantity,
		req.RandomLinksQuantity,
		req.HostsQuantity,
		req.TopologiesQuantity,
	)
	if errGenerating != nil {
		_ = c.Error(errGenerating)
		return
	}
	h.responseWriter.WriteSuccess(c, netHttp.StatusCreated)
}

func (h *Handler) bindTopologyGeneratorParams(c *gin.Context) {
	req := model.TopologyGeneratorParams{}
	if errBinding := c.BindQuery(&req); errBinding != nil {
		errResponse := http.NewErrResponse()
		errResponse.AddInvalidQueryStringFormatError()
		h.responseWriter.WriteError(c, netHttp.StatusBadRequest, errResponse)
		c.Abort()
		return
	}
	c.Set(gin.BindKey, &req)
}

func (h *Handler) validateTopologyGeneratorParams(c *gin.Context) {
	req := c.MustGet(gin.BindKey).(*model.TopologyGeneratorParams)
	errResponse := http.NewErrResponse()
	req.Validate(errResponse.Errors)
	if errResponse.IsNotEmpty() {
		h.responseWriter.WriteError(c, netHttp.StatusBadRequest, errResponse)
		c.Abort()
	}
}