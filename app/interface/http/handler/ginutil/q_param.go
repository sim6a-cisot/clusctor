package ginutil

import (
	"clusctor/app/interface/http"
	"clusctor/app/pkg/stdconv"
	"github.com/gin-gonic/gin"
)

const (
	qParam    = "q"
)

func GetQ(c *gin.Context, errResponse http.ErrorResponse) (q float64) {
	if q = convertQ(qParam, c.Query(qParam), errResponse.Errors); errResponse.IsNotEmpty() {
		return
	}
	validateQ(qParam, q, errResponse.Errors)
	return
}

func convertQ(fieldName string, qStr string, errs http.Errors) float64 {
	q, errParsing := stdconv.ParseFloat64(qStr)
	if errParsing != nil {
		errs[fieldName] = http.MustBeANumber
	}
	return q
}

func validateQ(fieldName string, q float64,  errs http.Errors) {
	min := 0.1
	max := 10.0
	if q < min || q > max {
		errs[fieldName] = http.IsNotBetweenExpectedValues(min, max)
	}
}

