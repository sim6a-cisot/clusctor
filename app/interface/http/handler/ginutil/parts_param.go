package ginutil

import (
	"clusctor/app/interface/http"
	"clusctor/app/pkg/stdconv"
	"github.com/gin-gonic/gin"
)

const (
	partsParam = "parts"
)


func GetParts(c *gin.Context, errResponse http.ErrorResponse) (parts int) {
	if parts = convertParts(partsParam, c.Query(partsParam), errResponse.Errors); errResponse.IsNotEmpty() {
		return
	}
	validateParts(partsParam, parts, errResponse.Errors)
	return
}

func convertParts(fieldName string, partsStr string, errs http.Errors) int {
	parts, errParsing := stdconv.ParseInt(partsStr)
	if errParsing != nil {
		errs[fieldName] = http.MustBeAnIntegerNumber
	}
	return parts
}

func validateParts(fieldName string, parts int, errs http.Errors) {
	min := 2
	if parts < min{
		errs[fieldName] = http.MustBeGraterThanInt(min)
	}
}
