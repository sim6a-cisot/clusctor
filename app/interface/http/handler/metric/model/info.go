package model

type MetricInfo struct {
	Metric           string `json:"metric"`
	UseCustomMetrics bool   `json:"useCustomMetrics"`
}
