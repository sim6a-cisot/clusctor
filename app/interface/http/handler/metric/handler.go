package metric

import (
	"clusctor/app/domain/model/management/metric"
	"clusctor/app/domain/service/management"
	"clusctor/app/interface/http"
	"clusctor/app/interface/http/handler/metric/model"
	"github.com/gin-gonic/gin"
	netHttp "net/http"
)

const (
	valueParam = "value"
	useCustomMetricsParam = "useCustomMetrics"
)

type Handler struct {
	service        management.MetricService
	responseWriter http.ResponseWriter
}

func NewHandler(
	service management.MetricService,
	responseWriter http.ResponseWriter,
) *Handler {
	return &Handler{service: service, responseWriter: responseWriter}
}

// @Summary Update metric info
// @Description Update current metric type and metric gathering approach.
// @Param value query string true "Metric type" Enums(delay, bandwidth, packetLossRate, adminMetric, larac, laracCost, qosParams)
// @Param useCustomMetrics query bool true "Use user defined metrics"
// @Tags topology
// @Accept json
// @Produce json
// @Success 200 {string} string	"OK"
// @Failure 400 {object} http.ErrorResponse
// @Router /topology/metric [put]
func (h *Handler) Update(c *gin.Context) {
	errResponse := http.NewErrResponse()
	metricType := h.getMetricType(c, errResponse)
	if errResponse.IsNotEmpty() {
		h.responseWriter.WriteError(c, netHttp.StatusBadRequest, errResponse)
		return
	}
	useCustomMetrics := h.useCustomMetrics(c)
	h.service.SetUseCustomMetrics(useCustomMetrics)
	h.service.SetCurrentMetric(metricType)
	h.responseWriter.WriteSuccess(c, netHttp.StatusOK)
}

func (h *Handler) getMetricType(c *gin.Context, errResponse http.ErrorResponse) metric.Type {
	metricType := metric.Type(c.Query(valueParam))
	validateMetric(valueParam, metricType, errResponse.Errors)
	return metricType
}

func (h *Handler) useCustomMetrics(c *gin.Context) bool {
	useCustomMetrics := c.Query(useCustomMetricsParam)
	return useCustomMetrics == "true"
}

// @Summary Get metric info
// @Description Get current metric type and metric gathering approach.
// @Tags topology
// @Accept json
// @Produce json
// @Success 200 {object} model.MetricInfo
// @Failure 400 {object} http.ErrorResponse
// @Router /topology/metric [get]
func (h *Handler) Get(c *gin.Context) {
	c.JSON(netHttp.StatusOK, &model.MetricInfo{
		Metric:           h.service.CurrentMetric().String(),
		UseCustomMetrics: h.service.UseCustomMetrics(),
	})
}