package metric

import (
	"clusctor/app/domain/model/management/metric"
	"clusctor/app/interface/http"
)

func validateMetric(fieldName string, metricType metric.Type, errs http.Errors) {
	switch metricType {
	case metric.Delay,
		metric.Bandwidth,
		metric.PacketLossRate,
		metric.Admin,
		metric.LARAC,
		metric.LARACCost,
		metric.QoSParams:
		// metric is correct, do nothing
	default:
		errs[fieldName] = http.CanTakeOneOfFollowingValues(
			metric.Delay.String(),
			metric.Bandwidth.String(),
			metric.PacketLossRate.String(),
			metric.Admin.String(),
			metric.LARAC.String(),
			metric.LARACCost.String(),
			metric.QoSParams.String(),
		)
	}
}
