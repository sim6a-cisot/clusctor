package segment

import (
	"clusctor/app/domain/service/management"
	"clusctor/app/interface/http"
	"github.com/gin-gonic/gin"
	netHttp "net/http"
)

type Handler struct {
	service        management.SegmentService
	responseWriter http.ResponseWriter
}

func NewHandler(service management.SegmentService, responseWriter http.ResponseWriter) *Handler {
	return &Handler{
		service: service,
		responseWriter:responseWriter,
	}
}

// @Summary Delete segments
// @Description Delete segments and slices.
// @Tags topology
// @Accept json
// @Produce json
// @Success 200 {string} string	"OK"
// @Router /topology/segments [delete]
func (h *Handler) Delete(c *gin.Context) {
	h.service.DeleteSegments()
	h.responseWriter.WriteSuccess(c, netHttp.StatusOK)
}