package http

import (
	"clusctor/app/pkg/std/http"
	"github.com/gin-gonic/gin"
)

func AddCSVHeaders(c *gin.Context, filename string) {
	c.Header(http.ContentType, http.TextCsv)
	c.Header(http.ContentDisposition, http.Attachment(filename))
}

func AddJSONHeaders(c *gin.Context) {
	c.Header(http.ContentType, http.ApplicationJSON)
}