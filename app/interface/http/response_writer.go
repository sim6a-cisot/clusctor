package http

import (
	"clusctor/app/application/service"
	"clusctor/app/application/service/log"
	"github.com/gin-gonic/gin"
	"net/http"
)

func NewResponseWriter(logger service.WarnLogger) *responseWriter {
	return &responseWriter{
		logger: logger,
	}
}

type ResponseWriter interface {
	WriteError(*gin.Context, int, ErrorResponse)
	WriteInternalError(*gin.Context, ErrorResponse)
	WriteSuccess(*gin.Context, int)
}

type responseWriter struct {
	logger service.WarnLogger
}

func (w *responseWriter) WriteInternalError(c *gin.Context, response ErrorResponse) {
	w.logger.Warn(log.Entry{
		Msg: "caught internal error",
		Details: response.InternalError.Error(),
	})
	c.JSON(http.StatusInternalServerError, response.InternalError.Error())
}

func (w *responseWriter) WriteError(c *gin.Context, statusCode int, response ErrorResponse) {
	w.logger.Warn(log.Entry{
		Msg: "caught http error",
		Details: response.Errors.String(),
	})
	c.JSON(statusCode, response)
}

func (w *responseWriter) WriteSuccess(c *gin.Context, statusCode int) {
	c.String(statusCode, "ok")
}

