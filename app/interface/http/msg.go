package http

import (
	"fmt"
	"strings"
)

func IsNotBetweenExpectedValues(min, max float64) string {
	return fmt.Sprintf("is not between expected values %.1f and %.1f", min, max)
}

func MustBeGraterThanInt(min int) string {
	return fmt.Sprintf("must be greater than %d", min)
}

func MustBeGraterOrEqualInt(min int) string {
	return fmt.Sprintf("must be greater than or equal %d", min)
}

func CanTakeOneOfFollowingValues(validValues ...string) string {
	validValuesStr := strings.Join(validValues, ", ")
	return fmt.Sprintf("can take one of the following values: %s", validValuesStr)
}

func HostDoesNotExists(mac string) string {
	return "host " + mac + " doesn't exist"
}

const (
	DPIDMustHaveNextFormat = "must have next format xx:xx:xx:xx:xx:xx:xx:xx where x is hex"
	MustBeANumber          = "must be a number"
	MustBeAnIntegerNumber  = "must be an integer number"
	InternalError          = "internal error"
	ValidationError        = "validation error"
)
