package http

import "strings"

func NewErrResponse() ErrorResponse {
	return ErrorResponse{
		Errors: NewErrors(),
	}
}

type ErrorResponse struct {
	Errors Errors `json:"errors,omitempty"`
	InternalError error
}

const (
	validationError = "validation error"
)

func (r ErrorResponse) AddInvalidJSONSchemaError()  {
	r.Errors[validationError] = "invalid json schema"
}

func (r ErrorResponse) AddInvalidQueryStringFormatError()  {
	r.Errors[validationError] = "invalid query string format"
}

func (r ErrorResponse) IsNotEmpty() bool {
	return len(r.Errors) > 0
}

func (r *ErrorResponse) HasInternalError() bool {
	return r.InternalError != nil
}

type Errors map[string]string

func NewErrors() Errors {
	return make(map[string]string, 0)
}

func (errs Errors) String() string {
	details := make([]string, 0, len(errs))
	for key, value := range errs {
		details = append(details, key + ": " + value)
	}
	return strings.Join(details, ", ")
}
