package stdconv

import "testing"

func TestRoundFloat64(t *testing.T) {
	tests := []struct {
		name          string
		f             float64
		decimalPlaces float64
		want          float64
	}{
		{
			name: "45.200032",
			f: 45.200032,
			decimalPlaces: 2,
			want: 45.20,
		},
		{
			name: "0",
			f: 0,
			decimalPlaces: 2,
			want: 0.00,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RoundFloat64(tt.f, tt.decimalPlaces); got != tt.want {
				t.Errorf("RoundFloat64() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFloat64ToStr(t *testing.T) {
	tests := []struct {
		name string
		f float64
		want string
	}{
		{
			name: "0.00",
			f: 0,
			want: "0.00",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Float64ToStr(tt.f); got != tt.want {
				t.Errorf("Float64ToStr() = %v, want %v", got, tt.want)
			}
		})
	}
}