package stdconv

import (
	"math"
	"strconv"
)

func Float64ToStr(f float64) string {
	return strconv.FormatFloat(f, 'f', 2, 64)
}

func RoundFloat64(f, decimalPlaces float64) float64 {
	factor := math.Pow(10, decimalPlaces)
	return math.Round(f * factor) / factor
}

func ParseFloat64(str string) (float64, error) {
	return strconv.ParseFloat(str, 64)
}

func ParseInt(str string) (int, error) {
	return strconv.Atoi(str)
}

func Int64ToStr(i int64) string {
	return strconv.FormatInt(i, 10)
}