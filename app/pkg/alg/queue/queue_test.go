package queue_test

import (
	"clusctor/app/pkg/alg/queue"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

type item float64

func (t item) Value() float64 {
	return float64(t)
}

func TestNewHeap(t *testing.T) {
	heap := queue.NewHeap(
		item(4),
		item(18),
		item(1),
		item(2),
		item(100),
		item(2),
		item(50),
	)

	sortedSlice := make([]queue.Item, 0, heap.Size())
	for heap.Size() > 0 {
		sortedSlice = append(sortedSlice, heap.TakeMin())
	}

	expectedSlice := []queue.Item{
		item(1),
		item(2),
		item(2),
		item(4),
		item(18),
		item(50),
		item(100),
	}

	assert.Equal(t, expectedSlice, sortedSlice)
}

func TestHeap_Add(t *testing.T) {
	items := []queue.Item{
		item(4),
		item(18),
		item(1),
		item(2),
		item(100),
		item(2),
		item(50),
	}
	heap := queue.NewHeap()
	for _, item := range items {
		heap.Add(item)
	}

	sortedSlice := make([]queue.Item, 0, heap.Size())
	for heap.Size() > 0 {
		sortedSlice = append(sortedSlice, heap.TakeMin())
	}

	expectedSlice := []queue.Item{
		item(1),
		item(2),
		item(2),
		item(4),
		item(18),
		item(50),
		item(100),
	}

	assert.Equal(t, expectedSlice, sortedSlice)
}

func TestHeap_Add_DynamicChanges(t *testing.T) {
	heap := queue.NewHeap()

	heap.Add(item(4))
	heap.Add(item(18))
	heap.Add(item(1))
	assert.Equal(t, item(1), heap.TakeMin())
	assert.Equal(t, item(4), heap.TakeMin())
	heap.Add(item(2))
	heap.Add(item(100))
	assert.Equal(t, item(2), heap.TakeMin())
	heap.Add(item(2))
	heap.Add(item(50))
	assert.Equal(t, item(2), heap.TakeMin())
	assert.Equal(t, item(18), heap.TakeMin())
	assert.Equal(t, item(50), heap.TakeMin())
	assert.Equal(t, item(100), heap.TakeMin())
}

type someType float64

func (t someType) Value() float64 {
	return float64(t)
}

func (t someType) Square() float64 {
	return float64(t) * float64(t)
}

func ExampleItemWrapper() {
	heap := queue.NewHeap()

	object := someType(3)
	heap.Add(object)
	extractedObject := heap.TakeMin()
	castedObject := extractedObject.(someType)

	fmt.Println(castedObject.Square())
	// Output: 9
}

type refItem struct {
	value float64
}

func (i *refItem) Value() float64 {
	return i.value
}

func TestHeap_Rearrange(t *testing.T) {
	it10 := &refItem{10}
	it40 := &refItem{40}

	heap := queue.NewHeap(
		it10,
		&refItem{3},
		it40,
		&refItem{4},
		&refItem{8},
	)

	assert.Equal(t, float64(3), heap.TakeMin().Value())

	it40.value = 1
	it10.value = 2
	heap.Rearrange()

	assert.Equal(t, float64(1), heap.TakeMin().Value())
	assert.Equal(t, float64(2), heap.TakeMin().Value())
}
