package queue

type Item interface {
	Value() float64
}

func NewHeap(slice ...Item) *Heap {
	h := &Heap{
		slice: slice,
	}
	h.Rearrange()
	return h
}

func (h *Heap) Rearrange() {
	for index := h.Size() / 2 - 1; index >= 0; index-- {
		h.heapify(index)
	}
}

type Heap struct {
	slice []Item
}

func (h *Heap) heapify(index int) {
	if h.hasIndex(h.leftChild(index)) && h.valueOf(index).Value() > h.valueOf(h.leftChild(index)).Value() {
		h.swap(index, h.leftChild(index))
		h.heapify(h.leftChild(index))
	}
	if h.hasIndex(h.rightChild(index)) && h.valueOf(index).Value() > h.valueOf(h.rightChild(index)).Value() {
		h.swap(index, h.rightChild(index))
		h.heapify(h.rightChild(index))
	}
}

func (h *Heap) Add(item Item) {
	h.slice = append(h.slice, item)
	index := h.lastIndex()
	parent := h.parent(index)
	for index > 0 && h.valueOf(parent).Value() > h.valueOf(index).Value() {
		h.swap(index, parent)
		index = parent
		parent = h.parent(index)
	}
}

func (h *Heap) parent(index int) int {
	return (index - 1) / 2
}

func (h *Heap) valueOf(index int) Item {
	return h.slice[index]
}

func (h *Heap) rightChild(index int) int {
	return 2*index + 2
}

func (h *Heap) leftChild(index int) int {
	return 2*index + 1
}

func (h *Heap) Size() int {
	return len(h.slice)
}

func (h *Heap) hasIndex(index int) bool {
	return index > 0 && index < h.Size()
}

func (h *Heap) lastIndex() int {
	return h.Size() - 1
}

func (h *Heap) swap(index1, index2 int) {
	h.slice[index1], h.slice[index2] = h.slice[index2], h.slice[index1]
}

func (h *Heap) TakeMin() Item {
	root := h.slice[0]
	h.swap(0, h.lastIndex())
	h.slice = h.slice[0:h.lastIndex()]
	h.heapify(0)
	return root
}