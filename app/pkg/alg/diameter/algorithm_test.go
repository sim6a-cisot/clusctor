package diameter_test

import (
	"clusctor/app/pkg/alg/diameter"
	"clusctor/app/pkg/alg/graph"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFind(t *testing.T) {
	// see test/alg/diameter/graph_diameter_test_graph.svg
	G := graph.New()

	G.AddEdge("v1", "v2", 1)
	G.AddEdge("v2", "v3", 1)
	G.AddEdge("v3", "v4", 1)
	G.AddEdge("v4", "v5", 1)
	G.AddEdge("v5", "v6", 1)
	G.AddEdge("v6", "v7", 1)
	G.AddEdge("v7", "v2", 1)
	G.AddEdge("v6", "v3", 1)

	G.AddEdge("v6", "v8", 1)
	G.AddEdge("v8", "v9", 1)
	G.AddEdge("v9", "v10", 1)
	G.AddEdge("v10", "v11", 1)
	G.AddEdge("v11", "v8", 1)
	G.AddEdge("v10", "v8", 1)
	G.AddEdge("v10", "v12", 1)

	assert.Equal(t, 6, diameter.Find(G))
}
