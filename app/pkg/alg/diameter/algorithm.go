package diameter

import (
	"clusctor/app/pkg/alg/bfs"
	"clusctor/app/pkg/alg/graph"
)

func Find(g *graph.Graph) int {
	extremeVertex, _ := bfs.Run(g.V.Any(), g.V).FindMax()
	_, diameter := bfs.Run(extremeVertex, g.V).FindMax()
	return diameter
}