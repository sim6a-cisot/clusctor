package segmentation

import (
	"clusctor/app/pkg/alg/graph"
	"clusctor/app/pkg/std/number"
)

type segmentFilter struct {
	s          *Segment
	ss         SegmentSet
	candidates SegmentSet
}

func newSegmentFilter(s *Segment, candidates SegmentSet) *segmentFilter {
	return &segmentFilter{
		s:          s,
		ss:         SegmentSet{},
		candidates: candidates,
	}
}

func (f *segmentFilter) findConnectedByMinTreeSegments(minTree graph.EdgeSet) *segmentFilter {
	minWeight := float64(number.Infinity)
	var commonEdgeSet graph.EdgeSet

	for candidate := range f.candidates {
		commonEdgeSet = candidate.EA.Intersect(f.s.EA).Intersect(minTree)
		for e := range commonEdgeSet {
			if e.W < minWeight {
				minWeight = e.W
				f.ss = SegmentSet{}
				f.ss.add(candidate)
			} else if e.W == minWeight {
				f.ss.add(candidate)
			}
		}
	}

	return f
}

func (f *segmentFilter) findMaxConnectedSegment() *Segment {
	if len(f.ss) == 0 {
		return nil
	}

	maxConnectedSegment := f.ss.Any()
	maxConnections := 0
	var commonEdgeSet graph.EdgeSet

	for s := range f.ss {
		commonEdgeSet = f.s.EA.Intersect(s.EA)
		if len(commonEdgeSet) > maxConnections {
			maxConnections = len(commonEdgeSet)
			maxConnectedSegment = s
		}
	}

	return maxConnectedSegment
}