package segmentation

import (
	"clusctor/app/pkg/alg/graph"
)

type Segment struct {
	*graph.Graph
}

func (s *Segment) ToSubGraph() *graph.Graph {
	return s.Graph
}

func newSegment(e *graph.Edge) *Segment {
	segment := &Segment{
		Graph: graph.New(),
	}

	segment.V.Add(e.V1)
	segment.V.Add(e.V2)
	segment.E.Add(e)

	segment.VA = e.V1.VA.Union(e.V2.VA).Difference(segment.V)
	segment.EA = e.V1.EA.Union(e.V2.EA).Difference(segment.E)

	return segment
}

func (s *Segment) summaryInnerWeight() float64 {
	sum := float64(0)

	for e := range s.E {
		sum += e.W
	}

	return sum
}

func (s *Segment) GetAdjacentVertexOfAdjacentEdge(e *graph.Edge) *graph.Vertex {
	if s.V.Contains(e.V1) {
		return e.V2
	} else {
		return e.V1
	}
}

func (s *Segment) q() float64 {
	return float64(len(s.E)) / float64(len(s.EA))
}

func (s *Segment) merge(other *Segment) *Segment {
	newSegment := &Segment{
		Graph: &graph.Graph{
			V: s.V.Union(other.V),
			E: s.E.Union(other.E).Union(s.EA.Intersect(other.EA)),
		},
	}

	newSegment.VA = s.VA.Union(other.VA).Difference(newSegment.V)
	newSegment.EA = s.EA.Union(other.EA).Difference(newSegment.E)

	return newSegment
}

type SegmentSet map[*Segment]bool

func (ss SegmentSet) add(s *Segment) {
	ss[s] = true
}

func (ss SegmentSet) Any() *Segment {
	for v := range ss {
		return v
	}
	return nil
}

func (ss SegmentSet) remove(s *Segment) {
	delete(ss, s)
}
