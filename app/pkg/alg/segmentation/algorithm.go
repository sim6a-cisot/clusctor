package segmentation

import (
	"clusctor/app/pkg/alg/graph"
	"clusctor/app/pkg/std/number"
)

type Algorithm struct {
	registry *SegmentRegistry
	minTree  graph.EdgeSet
}

func New(minTree graph.EdgeSet) *Algorithm {
	return &Algorithm{
		registry: newSegmentRegistry(),
		minTree:  minTree,
	}
}

func (a *Algorithm) SegmentRegistry() *SegmentRegistry {
	return a.registry
}

func (a *Algorithm) FormPrimarySegments(g *graph.Graph) {
	for v := range g.V {
		a.registry.AddFreeVertex(v)
	}

	for e := range a.minTree {
		if a.isLeafEdgeWithoutSegment(e) || a.isBranchingEdge(e) {
			s := newSegment(e)
			a.registry.addSegment(s)
			continue
		}
		if incl := a.getLeafVertexInclusion(e); incl != nil {
			incl.Do(a.registry)
		}
	}
}

func (a *Algorithm) degreeInMinTree(v *graph.Vertex) int {
	return len(v.EA.Intersect(a.minTree))
}

func (a *Algorithm) isLeafEdgeWithoutSegment(e *graph.Edge) bool {
	return (a.degreeInMinTree(e.V1) == 1 && !a.registry.HasSegmentOf(e.V2)) ||
		(a.degreeInMinTree(e.V2) == 1 && !a.registry.HasSegmentOf(e.V1))
}

func (a *Algorithm) isBranchingEdge(e *graph.Edge) bool {
	if a.registry.HasSegmentOf(e.V1) || a.registry.HasSegmentOf(e.V2) {
		return false
	}

	return (a.degreeInMinTree(e.V1) > 2 && e == e.V1.EA.FindWithMinWeight()) ||
		(a.degreeInMinTree(e.V2) > 2 && e == e.V2.EA.FindWithMinWeight())
}

func (a *Algorithm) getLeafVertexInclusion(e *graph.Edge) *inclusion {
	if a.degreeInMinTree(e.V1) == 1 && a.registry.HasSegmentOf(e.V2) {
		return &inclusion{V: e.V1, S: a.registry.SegmentOf(e.V2)}
	}
	if a.degreeInMinTree(e.V2) == 1 && a.registry.HasSegmentOf(e.V1) {
		return &inclusion{V: e.V2, S: a.registry.SegmentOf(e.V1)}
	}
	return nil
}

func (a *Algorithm) FormSecondarySegments() {
	var minInclusion *inclusion
	minInclusionWeight := float64(number.Infinity)
	for {
		for freeVertex := range a.registry.freeVertices {
			adjacentSegments := a.registry.adjacentSegmentsOfVertex(freeVertex)
			if len(adjacentSegments) == 0 {
				continue
			}
			incl := findBestInclusion(freeVertex, adjacentSegments, a.minTree)
			if a.noAdjacentSegments(incl) {
				continue
			}
			if incl.W < minInclusionWeight {
				minInclusionWeight = incl.W
				minInclusion = incl
			}
		}
		if minInclusion != nil {
			minInclusion.Do(a.registry)
		}
		minInclusionWeight = float64(number.Infinity)
		if a.AreAllVerticesSegmented() {
			break
		}
	}
}

func (Algorithm) noAdjacentSegments(incl *inclusion) bool {
	return incl == nil
}

func (a *Algorithm) AreAllVerticesSegmented() bool {
	return len(a.registry.freeVertices) == 0
}

func (a *Algorithm) MergeWeakConnectedSegments(Q float64) {
	for !a.mergeAllWeakConnectedSegments(Q) {
	}
}

func (a *Algorithm) mergeAllWeakConnectedSegments(Q float64) bool {
	for s := range a.registry.Segments() {
		if s.q() < Q {
			bestCandidateToMerge := newSegmentFilter(s, a.registry.adjacentSegmentsOfSegment(s)).
				findConnectedByMinTreeSegments(a.minTree).
				findMaxConnectedSegment()
			if bestCandidateToMerge == nil {
				continue
			}

			a.registry.delete(s)
			a.registry.delete(bestCandidateToMerge)

			newSegment := s.merge(bestCandidateToMerge)
			a.registry.addSegment(newSegment)

			return false
		}
	}
	return true
}
