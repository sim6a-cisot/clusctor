package segmentation

import (
	"clusctor/app/pkg/alg/graph"
	"clusctor/app/pkg/std/number"
)

func findConnectedByMinTreeSegments(
	segmentsToFind SegmentSet,
	v *graph.Vertex,
	minTree graph.EdgeSet,
) SegmentSet {
	connectedByMinTreeSegments := SegmentSet{}
	for s := range segmentsToFind {
		if len(v.EA.Intersect(s.EA).Intersect(minTree)) > 0 {
			connectedByMinTreeSegments.add(s)
		}
	}
	return connectedByMinTreeSegments
}

func findMaxConnectedSegments(
	segmentsToFind SegmentSet,
	v *graph.Vertex,
) SegmentSet {
	maxConnectedSegments := SegmentSet{}
	maxConnections := 0
	var commonEdgeSet graph.EdgeSet

	for s := range segmentsToFind {
		commonEdgeSet = s.EA.Intersect(v.EA)
		if len(commonEdgeSet) > maxConnections {
			maxConnections = len(commonEdgeSet)
			maxConnectedSegments = SegmentSet{}
			maxConnectedSegments.add(s)
		} else if len(commonEdgeSet) == maxConnections {
			maxConnectedSegments.add(s)
		}
	}
	return maxConnectedSegments
}

func findNearestSegment(
	segmentsToFind SegmentSet,
	v *graph.Vertex,
) (*Segment, float64) {
	minWeight := float64(number.Infinity)
	var nearestSegment *Segment

	for s := range segmentsToFind {
		for e := range s.EA.Intersect(v.EA) {
			if e.W < minWeight {
				nearestSegment = s
				minWeight = e.W
			}
		}
	}

	return nearestSegment, minWeight
}

func findBestInclusion(v *graph.Vertex, neighbors SegmentSet, minTree graph.EdgeSet) *inclusion {
	filtered := findConnectedByMinTreeSegments(neighbors, v, minTree)
	filtered = findMaxConnectedSegments(filtered, v)
	s, w := findNearestSegment(filtered, v)

	if s == nil {
		return nil
	}
	return &inclusion{V: v, S: s, W: w}
}
