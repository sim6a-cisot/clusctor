package segmentation

import (
	"clusctor/app/pkg/alg/graph"
)

func newSegmentRegistry() *SegmentRegistry {
	return &SegmentRegistry{
		table:        map[*graph.Vertex]*Segment{},
		freeVertices: graph.VertexSet{},
		segments:     SegmentSet{},
	}
}

type SegmentRegistry struct {
	table        map[*graph.Vertex]*Segment
	freeVertices graph.VertexSet
	segments     SegmentSet
}

func (r *SegmentRegistry) AddFreeVertex(v *graph.Vertex) {
	r.freeVertices.Add(v)
}

func (r *SegmentRegistry) adjacentSegmentsOfSegment(s *Segment) SegmentSet {
	adjacentSegmentSet := SegmentSet{}

	for v := range s.VA {
		adjacentSegmentSet.add(r.SegmentOf(v))
	}

	return adjacentSegmentSet
}

func (r *SegmentRegistry) adjacentSegmentsOfVertex(v *graph.Vertex) SegmentSet {
	adjacentSegments := SegmentSet{}

	for neighbor := range v.VA {
		if s := r.SegmentOf(neighbor); s != nil {
			adjacentSegments.add(s)
		}
	}

	return adjacentSegments
}

func (r *SegmentRegistry) Segments() SegmentSet {
	return r.segments
}

func (r *SegmentRegistry) register(v *graph.Vertex, s *Segment) {
	r.table[v] = s
	r.freeVertices.Remove(v)
	r.segments.add(s)
}

func (r *SegmentRegistry) addSegment(s *Segment) {
	for v := range s.V {
		r.register(v, s)
	}
}

func (r *SegmentRegistry) SegmentOf(v *graph.Vertex) *Segment {
	return r.table[v]
}

func (r *SegmentRegistry) HasSegmentOf(v *graph.Vertex) bool {
	return r.SegmentOf(v) != nil
}

func (r *SegmentRegistry) IsFree(v *graph.Vertex) bool {
	return r.SegmentOf(v) == nil
}

func (r *SegmentRegistry) delete(s *Segment) {
	for v := range s.V {
		delete(r.table, v)
		r.freeVertices.Add(v)
	}
	r.segments.remove(s)
}
