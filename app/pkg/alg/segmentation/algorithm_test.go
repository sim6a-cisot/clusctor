package segmentation_test

import (
	"clusctor/app/pkg/alg/graph"
	"clusctor/app/pkg/alg/prim"
	"clusctor/app/pkg/alg/segmentation"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAlgorithm_FormPrimarySegments(t *testing.T) {
	// see test/alg/segmentation/graph_seg_step_1.svg
	G := graph.New()

	e12 := G.AddEdge("v1", "v2", 4)
	G.AddEdge("v1", "v5", 5)
	G.AddEdge("v2", "v5", 2)
	e53 := G.AddEdge("v5", "v3", 1)
	G.AddEdge("v5", "v4", 7)
	e34 := G.AddEdge("v3", "v4", 9)

	v1 := e12.V1
	v2 := e12.V2
	v3 := e53.V2
	v4 := e34.V2
	v5 := e53.V1

	T := prim.CalculateMinTree(G)

	alg := segmentation.New(T)
	alg.FormPrimarySegments(G)
	registry := alg.SegmentRegistry()
	segments := registry.Segments()

	assert.Equal(t, 2, len(segments))

	s1 := registry.SegmentOf(v1)
	assert.Equal(t, 2, len(s1.V))

	s2 := registry.SegmentOf(v3)
	assert.Equal(t, 3, len(s2.V))

	assert.Equal(t, s1.ID, registry.SegmentOf(v1).ID)
	assert.Equal(t, s1.ID, registry.SegmentOf(v2).ID)

	assert.Equal(t, s2.ID, registry.SegmentOf(v3).ID)
	assert.Equal(t, s2.ID, registry.SegmentOf(v4).ID)
	assert.Equal(t, s2.ID, registry.SegmentOf(v5).ID)
}

func TestAlgorithm_FormSecondarySegments(t *testing.T) {
	// see test/alg/segmentation/graph_seg_step_2.svg
	G := graph.New()

	e12 := G.AddEdge("v1", "v2", 9)
	G.AddEdge("v1", "v3", 10)
	G.AddEdge("v2", "v3", 8)
	G.AddEdge("v4", "v2", 6)
	e43 := G.AddEdge("v4", "v3", 11)

	G.AddEdge("v4", "v5", 7)
	G.AddEdge("v1", "v9", 14)

	e56 := G.AddEdge("v5", "v6", 5)
	G.AddEdge("v6", "v7", 4)
	e78 := G.AddEdge("v7", "v8", 3)
	G.AddEdge("v8", "v9", 2)
	G.AddEdge("v6", "v10", 12)
	e109 := G.AddEdge("v10", "v9", 1)
	G.AddEdge("v10", "v8", 15)

	v1, v2 := e12.V1, e12.V2
	v4, v3 := e43.V1, e43.V2
	v5, v6 := e56.V1, e56.V2
	v7, v8 := e78.V1, e78.V2
	v10, v9 := e109.V1, e109.V2

	T := prim.CalculateMinTree(G)

	alg := segmentation.New(T)
	alg.FormPrimarySegments(G)
	alg.FormSecondarySegments()
	registry := alg.SegmentRegistry()

	s1 := registry.SegmentOf(v1)
	assert.Equal(t, 4, len(s1.V))

	s2 := registry.SegmentOf(v10)
	assert.Equal(t, 6, len(s2.V))

	assert.Equal(t, s1.ID, registry.SegmentOf(v1).ID)
	assert.Equal(t, s1.ID, registry.SegmentOf(v2).ID)
	assert.Equal(t, s1.ID, registry.SegmentOf(v3).ID)
	assert.Equal(t, s1.ID, registry.SegmentOf(v4).ID)

	assert.Equal(t, s2.ID, registry.SegmentOf(v5).ID)
	assert.Equal(t, s2.ID, registry.SegmentOf(v6).ID)
	assert.Equal(t, s2.ID, registry.SegmentOf(v7).ID)
	assert.Equal(t, s2.ID, registry.SegmentOf(v8).ID)
	assert.Equal(t, s2.ID, registry.SegmentOf(v9).ID)
	assert.Equal(t, s2.ID, registry.SegmentOf(v10).ID)
}

func TestAlgorithm_MergeWeakConnectedSegments(t *testing.T) {
	// see test/alg/segmentation/graph_seg_step_3.svg
	G := graph.New()

	e12 := G.AddEdge("v1", "v2", 1)
	G.AddEdge("v2", "v4", 2)
	G.AddEdge("v4", "v8", 3)
	G.AddEdge("v8", "v10", 3)
	G.AddEdge("v10", "v12", 2)
	e1211 := G.AddEdge("v12", "v11", 1)
	G.AddEdge("v11", "v9", 10)
	G.AddEdge("v9", "v5", 10)
	G.AddEdge("v5", "v3", 10)
	G.AddEdge("v3", "v1", 10)

	e43 := G.AddEdge("v4", "v3", 3)
	e87 := G.AddEdge("v8", "v7", 2)
	G.AddEdge("v7", "v6", 2)
	e65 := G.AddEdge("v6", "v5", 2)
	e109 := G.AddEdge("v10", "v9", 3)
	G.AddEdge("v4", "v7", 10)
	G.AddEdge("v10", "v7", 10)
	G.AddEdge("v3", "v6", 10)
	G.AddEdge("v9", "v6", 10)

	v1, v2 := e12.V1, e12.V2
	v4, v3 := e43.V1, e43.V2
	v6, v5 := e65.V1, e65.V2
	v8, v7 := e87.V1, e87.V2
	v10, v9 := e109.V1, e109.V2
	v12, v11 := e1211.V1, e1211.V2

	T := prim.CalculateMinTree(G)

	alg := segmentation.New(T)
	alg.FormPrimarySegments(G)
	alg.FormSecondarySegments()
	alg.MergeWeakConnectedSegments(0.3)
	registry := alg.SegmentRegistry()

	assert.Equal(t, 3, len(registry.Segments()))

	s1 := registry.SegmentOf(v1)
	assert.Equal(t, 4, len(s1.V))
	assert.Equal(t, s1.ID, registry.SegmentOf(v1).ID)
	assert.Equal(t, s1.ID, registry.SegmentOf(v2).ID)
	assert.Equal(t, s1.ID, registry.SegmentOf(v3).ID)
	assert.Equal(t, s1.ID, registry.SegmentOf(v4).ID)

	s2 := registry.SegmentOf(v12)
	assert.Equal(t, 4, len(s2.V))
	assert.Equal(t, s2.ID, registry.SegmentOf(v9).ID)
	assert.Equal(t, s2.ID, registry.SegmentOf(v10).ID)
	assert.Equal(t, s2.ID, registry.SegmentOf(v11).ID)
	assert.Equal(t, s2.ID, registry.SegmentOf(v12).ID)

	s3 := registry.SegmentOf(v8)
	assert.Equal(t, 4, len(s3.V))
	assert.Equal(t, s3.ID, registry.SegmentOf(v5).ID)
	assert.Equal(t, s3.ID, registry.SegmentOf(v6).ID)
	assert.Equal(t, s3.ID, registry.SegmentOf(v7).ID)
	assert.Equal(t, s3.ID, registry.SegmentOf(v8).ID)
}
