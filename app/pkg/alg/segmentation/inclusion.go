package segmentation

import (
	"clusctor/app/pkg/alg/graph"
	"clusctor/app/pkg/alg/queue"
	"fmt"
)

type inclusion struct {
	V *graph.Vertex
	S *Segment
	W float64
}

func (i *inclusion) String() string {
	return fmt.Sprintf("incl{vertex %s, w = %.2f}", i.V.Label, i.W)
}

func (i *inclusion) Do(registry *SegmentRegistry) {
	i.S.Include(i.V)
	registry.register(i.V, i.S)
}

// HEAP

func newInclusionPriorityQueue() *inclusionPriorityQueue {
	return &inclusionPriorityQueue{
		heap: queue.NewHeap(),
	}
}

type heapItem struct {
	incl *inclusion
}

func (i *heapItem) Value() float64 {
	return i.incl.W
}

type inclusionPriorityQueue struct {
	heap *queue.Heap
}

func (q *inclusionPriorityQueue) add(incl *inclusion) {
	q.heap.Add(&heapItem{incl})
}

func (q *inclusionPriorityQueue) takeMin() *inclusion {
	item := q.heap.TakeMin()
	return item.(*heapItem).incl
}

func (q *inclusionPriorityQueue) isEmpty() bool {
	return q.heap.Size() == 0
}