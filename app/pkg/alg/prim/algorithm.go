package prim

import (
	"clusctor/app/pkg/alg/graph"
)

func CalculateMinTree(g *graph.Graph) graph.EdgeSet {
	VT := graph.VertexSet{}
	T := graph.EdgeSet{}

	rootVertex := g.V.Any()
	VT.Add(rootVertex)

	edgeQueue := graph.NewEdgePriorityQueue()
	for e := range rootVertex.EA {
		edgeQueue.Add(e)
	}

	var minEdge *graph.Edge
	currentVertex := rootVertex
	for {
		minEdge = edgeQueue.TakeMin()
		if VT.Contains(minEdge.V1) && VT.Contains(minEdge.V2) {
			continue
		}

		if VT.Contains(minEdge.V1) {
			currentVertex = minEdge.V2
		} else {
			currentVertex = minEdge.V1
		}
		VT.Add(currentVertex)

		for e := range currentVertex.EA {
			if e != minEdge {
				edgeQueue.Add(e)
			}
		}

		T.Add(minEdge)
		if len(VT) == len(g.V) {
			break
		}
	}

	return T
}
