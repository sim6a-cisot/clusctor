package prim_test

import (
	"clusctor/app/pkg/alg/graph"
	"clusctor/app/pkg/alg/prim"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCalculateMinTree(t *testing.T) {
	// see test/alg/prim/prim_test_graph.svg
	G := graph.New()

	G.AddEdge("v1", "v2", 10)
	e23 := G.AddEdge("v2", "v3", 8)
	e34 := G.AddEdge("v3", "v4", 6)
	G.AddEdge("v5", "v4", 5)
	e15 := G.AddEdge("v1", "v5", 1)
	e16 := G.AddEdge("v1", "v6", 2)
	G.AddEdge("v5", "v6", 3)
	e46 := G.AddEdge("v4", "v6", 4)
	G.AddEdge("v3", "v6", 7)
	G.AddEdge("v2", "v6", 9)

	T := prim.CalculateMinTree(G)

	assert.True(t, T.Contains(e23))
	assert.True(t, T.Contains(e34))
	assert.True(t, T.Contains(e15))
	assert.True(t, T.Contains(e16))
	assert.True(t, T.Contains(e46))

	assert.Equal(t, 5, len(T))
}
