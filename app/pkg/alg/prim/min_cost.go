package prim

import (
	"clusctor/app/pkg/alg/graph"
)

func CalculateMinCost(tree graph.EdgeSet) float64 {
	sum := float64(0)
	for e := range tree {
		sum += e.W
	}
	return sum
}
