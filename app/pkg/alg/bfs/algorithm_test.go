package bfs_test

import (
	"clusctor/app/pkg/alg/bfs"
	"clusctor/app/pkg/alg/graph"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRun(t *testing.T) {
	// see test/alg/bfs/bfs_test_graph.svg
	G := graph.New()

	e12 := G.AddEdge("v1", "v2", 1)
	G.AddEdge("v2", "v3", 1)
	e34 := G.AddEdge("v3", "v4", 1)
	G.AddEdge("v4", "v5", 1)
	e56 := G.AddEdge("v5", "v6", 1)
	G.AddEdge("v6", "v7", 1)
	e72 := G.AddEdge("v7", "v2", 1)
	G.AddEdge("v6", "v3", 1)

	G.AddEdge("v6", "v8", 1)
	e89 := G.AddEdge("v8", "v9", 1)
	e910 := G.AddEdge("v9", "v10", 1)
	G.AddEdge("v10", "v11", 1)
	e118 := G.AddEdge("v11", "v8", 1)
	G.AddEdge("v10", "v8", 1)
	e1012 := G.AddEdge("v10", "v12", 1)

	v1 := e12.V1

	distances := bfs.Run(v1, G.V)

	assert.Equal(t, 12, distances.Size())

	assert.Equal(t, 0, distances.DistanceTo(v1))
	assert.Equal(t, 1, distances.DistanceTo(e12.V2))
	assert.Equal(t, 2, distances.DistanceTo(e34.V1))
	assert.Equal(t, 3, distances.DistanceTo(e34.V2))
	assert.Equal(t, 4, distances.DistanceTo(e56.V1))
	assert.Equal(t, 3, distances.DistanceTo(e56.V2))
	assert.Equal(t, 2, distances.DistanceTo(e72.V1))
	assert.Equal(t, 4, distances.DistanceTo(e89.V1))
	assert.Equal(t, 5, distances.DistanceTo(e89.V2))
	assert.Equal(t, 5, distances.DistanceTo(e910.V2))
	assert.Equal(t, 5, distances.DistanceTo(e118.V1))
	assert.Equal(t, 6, distances.DistanceTo(e1012.V2))
}
