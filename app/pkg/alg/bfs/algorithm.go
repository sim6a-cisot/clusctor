package bfs

import (
	"clusctor/app/pkg/alg/graph"
	"clusctor/app/pkg/std/number"
)

func Run(root *graph.Vertex, vertexSet graph.VertexSet) *graph.Distances {
	distances := graph.NewDistances(len(vertexSet))
	for v := range vertexSet {
		distances.SetDistance(v, number.Infinity)
	}
	distances.Add(root, 0)
	q := graph.NewVertexPriorityQueue()
	q.Add(root, distances.DistanceTo(root))

	var cur *graph.Vertex
	for q.Size() > 0 {
		cur = q.TakeMin()
		for v := range cur.VA {
			if distances.DistanceTo(v) == number.Infinity {
				distances.Add(v, distances.DistanceTo(cur) + 1)
				q.Add(v, distances.DistanceTo(v))
			}
		}
	}

	return distances
}