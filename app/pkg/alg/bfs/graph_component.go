package bfs

import (
	"clusctor/app/pkg/alg/graph"
)

func FindComponents(g *graph.Graph) []*graph.Graph {
	var components []*graph.Graph

	unvisitedVertexSet := graph.VertexSet{}
	unvisitedVertexSet.AddSet(g.V)

	for len(unvisitedVertexSet) > 0 {
		distances := Run(unvisitedVertexSet.Any(), unvisitedVertexSet)
		distancesAsGraph := distances.AsGraph()
		components = append(components, distancesAsGraph)
		unvisitedVertexSet = unvisitedVertexSet.Difference(distancesAsGraph.V)
	}

	return components
}
