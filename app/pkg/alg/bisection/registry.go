package bisection

import (
	"clusctor/app/pkg/alg/graph"
)

type segmentRegistry struct {
	queue *graph.Queue
}

func newSegmentRegistry() *segmentRegistry {
	return &segmentRegistry{
		queue: graph.NewQueue(),
	}
}

func (r *segmentRegistry) Register(g *graph.Graph) {
	r.queue.Enqueue(g)
}

func (r *segmentRegistry) Next() *graph.Graph {
	return r.queue.Dequeue()
}

func (r *segmentRegistry) PopToSlice() []*graph.Graph {
	subGraphs := make([]*graph.Graph, 0, r.Size())
	for r.Size() > 0 {
		subGraphs = append(subGraphs, r.Next())
	}
	return subGraphs
}

func (r segmentRegistry) Size() int {
	return r.queue.Size()
}