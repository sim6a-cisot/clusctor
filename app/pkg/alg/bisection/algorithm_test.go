package bisection_test

import (
	"clusctor/app/pkg/alg/bisection"
	"clusctor/app/pkg/alg/graph"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestAlgorithm_BisectOn(t *testing.T) {
	// see  test/bisection/bisection_test_graph.svg
	G := graph.New()

	G.AddEdge("v1", "v2", 1)
	G.AddEdge("v1", "v3", 1)
	G.AddEdge("v2", "v3", 1)

	G.AddEdge("v3", "v4", 1)
	G.AddEdge("v4", "v5", 1)

	G.AddEdge("v3", "v6", 1)

	G.AddEdge("v6", "v8", 1)
	G.AddEdge("v8", "v7", 1)
	G.AddEdge("v6", "v7", 1)

	a := bisection.NewAlgorithm()
	subGraphs := a.BisectOn(len(G.V), G)
	assert.Len(t, subGraphs, len(G.V))
}
