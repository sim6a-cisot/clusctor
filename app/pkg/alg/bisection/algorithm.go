package bisection

import (
	"clusctor/app/pkg/alg/bfs"
	"clusctor/app/pkg/alg/graph"
)

type Algorithm struct {
	registry *segmentRegistry
}

func NewAlgorithm() *Algorithm {
	return &Algorithm{
		registry: newSegmentRegistry(),
	}
}

func (a *Algorithm) BisectOn(parts int, g *graph.Graph) []*graph.Graph {
	if parts > g.V.Size() {
		parts = g.V.Size()
	}
	a.registry.Register(g)

	for a.registry.Size() < parts {
		a.bisect(a.registry.Next())
	}

	return a.registry.PopToSlice()
}

func (a *Algorithm) bisect(g *graph.Graph) {
	if g.V.Size() == 1 {
		a.registry.Register(g)
		return
	}

	extremeVertex, _ := bfs.Run(g.V.Any(), g.V).FindMax()
	distances := bfs.Run(extremeVertex, g.V)

	firstSubGraph := graph.New()
	verticesSortedByAscendingDistances := distances.AsSliceSortedByAscending()
	var i int
	for ; i < len(verticesSortedByAscendingDistances)/2; i++ {
		v := verticesSortedByAscendingDistances[i]
		firstSubGraph.Include(v)
	}
	secondSubGraph := graph.New()
	for ; i < len(verticesSortedByAscendingDistances); i++ {
		v := verticesSortedByAscendingDistances[i]
		secondSubGraph.Include(v)
	}

	a.registry.Register(firstSubGraph)
	if secondSubGraph.IsUnconnected() {
		for _, component := range bfs.FindComponents(secondSubGraph) {
			a.registry.Register(component)
		}
	} else {
		a.registry.Register(secondSubGraph)
	}
}
