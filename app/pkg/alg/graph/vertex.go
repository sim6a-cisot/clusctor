package graph

import (
	"clusctor/app/pkg/alg/queue"
	"strings"
)

type Vertex struct {
	Label string
	VA    VertexSet
	EA    EdgeSet
}

func (v *Vertex) FindEdgeTo(neighbor *Vertex) *Edge {
	for e := range v.EA {
		if e.Opposite(v) == neighbor {
			return e
		}
	}
	return nil
}

type VertexCollection []*Vertex

func (c VertexCollection) CollectLabels() []string {
	var labels []string
	for _, v := range c {
		labels = append(labels, v.Label)
	}
	return labels
}

func (c VertexCollection) Last() *Vertex {
	return c[len(c)-1]
}

func (c VertexCollection) Equal(other VertexCollection) bool {
	if len(c) != len(other) {
		return false
	}

	for i := 0; i < len(c); i++ {
		if c[i] != other[i] {
			return false
		}
	}

	return true
}

func NewVertexPriorityQueue() *VertexPriorityQueue {
	return &VertexPriorityQueue{
		heap: queue.NewHeap(),
	}
}

type vertexHeapItem struct {
	vertex   *Vertex
	distance int
}

func (i *vertexHeapItem) Value() float64 {
	return float64(i.distance)
}

type VertexPriorityQueue struct {
	heap *queue.Heap
}

func (q *VertexPriorityQueue) Size() int {
	return q.heap.Size()
}

func (q *VertexPriorityQueue) Add(v *Vertex, d int) {
	q.heap.Add(&vertexHeapItem{vertex: v, distance: d})
}

func (q *VertexPriorityQueue) TakeMin() *Vertex {
	item := q.heap.TakeMin()
	return item.(*vertexHeapItem).vertex
}

type VertexSet map[*Vertex]bool

func (vs VertexSet) Size() int {
	return len(vs)
}

func (vs VertexSet) IsEmpty() bool {
	return len(vs) == 0
}

func (vs VertexSet) Any() *Vertex {
	for v := range vs {
		return v
	}
	return nil
}

func (vs VertexSet) Add(v *Vertex) {
	vs[v] = true
}

func (vs VertexSet) Remove(v *Vertex) {
	delete(vs, v)
}

func (vs VertexSet) AddSet(other VertexSet) {
	for v := range other {
		vs.Add(v)
	}
}

func (vs VertexSet) Contains(v *Vertex) bool {
	return vs[v]
}

func (vs VertexSet) Intersect(other VertexSet) VertexSet {
	intersection := VertexSet{}

	for v := range vs {
		if other.Contains(v) {
			intersection.Add(v)
		}
	}

	return intersection
}

func (vs VertexSet) Difference(other VertexSet) VertexSet {
	difference := VertexSet{}

	for v := range vs {
		if !other.Contains(v) {
			difference.Add(v)
		}
	}

	return difference
}

func (vs VertexSet) Union(other VertexSet) VertexSet {
	union := VertexSet{}

	for v := range vs {
		union.Add(v)
	}
	for v := range other {
		union.Add(v)
	}

	return union
}

func (vs VertexSet) String() string {
	sb := strings.Builder{}
	for v := range vs {
		sb.WriteString(v.Label)
		sb.WriteString(" ")
	}
	return sb.String()
}
