package graph_test

import (
	"clusctor/app/pkg/alg/graph"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestQueue(t *testing.T) {
	g1 := graph.New()
	g1.V.Add(&graph.Vertex{})

	g2 := graph.New()
	g2.V.Add(&graph.Vertex{})
	g2.V.Add(&graph.Vertex{})

	g3 := graph.New()
	g3.V.Add(&graph.Vertex{})
	g3.V.Add(&graph.Vertex{})
	g3.V.Add(&graph.Vertex{})

	q := graph.NewQueue()
	q.Enqueue(g1)
	q.Enqueue(g2)
	q.Enqueue(g3)

	assert.Equal(t, 1, q.Dequeue().V.Size())
	assert.Equal(t, 2, q.Dequeue().V.Size())
	assert.Equal(t, 3, q.Dequeue().V.Size())

	assert.Equal(t, 0, q.Size())
}