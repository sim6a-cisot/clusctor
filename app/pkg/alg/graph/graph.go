package graph

import (
	"clusctor/app/pkg/std/number"
	"clusctor/app/pkg/std/slice"
	"sort"
)

type Graph struct {
	ID    int
	Label string

	V         VertexSet
	E         EdgeSet
	EA        EdgeSet
	VA        VertexSet
	SubGraphs Collection

	idAndVertex map[string]*Vertex

	isUnconnected bool
}

func New() *Graph {
	return &Graph{
		V:           make(VertexSet),
		E:           make(EdgeSet),
		EA:          make(EdgeSet),
		VA:          make(VertexSet),
		SubGraphs:   make(Collection, 0),
		idAndVertex: make(map[string]*Vertex),
	}
}

func (g *Graph) IsUnconnected() bool {
	return g.isUnconnected
}

func (g *Graph) AddEdge(v1Label, v2Label string, w float64) *Edge {
	v1 := g.GetOrAdd(v1Label)
	v2 := g.GetOrAdd(v2Label)

	v1.VA.Add(v2)
	v2.VA.Add(v1)

	e := &Edge{
		V1: v1,
		V2: v2,
		W:  w,
	}

	v1.EA.Add(e)
	v2.EA.Add(e)

	g.E.Add(e)

	return e
}

func (g *Graph) RemoveEdge(e *Edge) {
	v1 := e.V1
	v2 := e.V2
	v1.VA.Remove(v2)
	v1.EA.Remove(e)
	v2.VA.Remove(v1)
	v2.EA.Remove(e)
	g.E.Remove(e)
}

func (g *Graph) Include(v *Vertex) {
	if g.V.IsEmpty() {
		g.includeFirstVertex(v)
		return
	}
	g.V.Add(v)
	intermediateEdgeSet := v.EA.Intersect(g.EA)
	if intermediateEdgeSet.IsEmpty() {
		g.isUnconnected = true
		return
	}
	g.VA.Remove(v)
	g.E = g.E.Union(intermediateEdgeSet)
	g.EA = g.EA.Difference(intermediateEdgeSet)
	g.VA = g.VA.Union(v.VA.Difference(g.V))
	g.EA = g.EA.Union(v.EA.Difference(intermediateEdgeSet))
}

func (g *Graph) includeFirstVertex(v *Vertex) {
	g.V.Add(v)
	g.EA.AddSet(v.EA)
	g.VA.AddSet(v.VA)
}

func (g *Graph) GetOrAdd(id string) *Vertex {
	if v := g.idAndVertex[id]; v == nil {
		v := &Vertex{
			Label: id,
			VA:    VertexSet{},
			EA:    EdgeSet{},
		}
		g.V.Add(v)
		g.idAndVertex[id] = v
	}
	return g.idAndVertex[id]
}

func (g *Graph) CalculateQ() float64 {
	if len(g.EA) == 0 {
		return 0
	}
	return float64(len(g.E)) / float64(len(g.EA))
}

func (g *Graph) CalculateMeanVertexDegree() float64 {
	if len(g.V) == 0 {
		return 0
	}

	sum := 0
	for v := range g.V {
		sum += len(v.VA)
	}

	return float64(sum) / float64(len(g.V))
}

func (g *Graph) String() string {
	return g.E.String() + g.V.String()
}

type Collection []*Graph

func (c Collection) FindSubGraphOfVertices(vertices ...*Vertex) *Graph {
	for _, g := range c {
		areVerticesInSameSubGraph := true
		for _, v := range vertices {
			areVerticesInSameSubGraph = areVerticesInSameSubGraph && g.V.Contains(v)
		}
		if areVerticesInSameSubGraph {
			return g
		}
	}
	return nil
}

func (c Collection) CalculateDMinAndDMax(findDiameter func(*Graph) int) (dMin, dMax int) {
	dMin = number.Infinity

	var d int
	for _, sg := range c {
		d = findDiameter(sg)
		if d > dMax {
			dMax = d
		}
		if d < dMin {
			dMin = d
		}
	}

	return
}

func (c Collection) CalculateMMinAndMMax() (mMin, mMax int) {
	mMin = number.Infinity

	var m int
	for _, sg := range c {
		m = len(sg.E)
		if m > mMax {
			mMax = m
		}
		if m < mMin {
			mMin = m
		}
	}

	return
}

func (c Collection) CalculateNMinAndNMax() (nMin, nMax int) {
	nMin = number.Infinity

	var n int
	for _, sg := range c {
		n = len(sg.V)
		if n > nMax {
			nMax = n
		}
		if n < nMin {
			nMin = n
		}
	}

	return
}

func (c Collection) CalculateNMo() int {
	valueAndFrequency := make(map[float64]int)

	var n int
	for _, sg := range c {
		n = len(sg.V)
		valueAndFrequency[float64(n)] += 1
	}

	return int(c.calculateMo(valueAndFrequency))
}

func (c Collection) CalculateNMean() float64 {
	nn := c.NOfSegments()
	if len(nn) == 0 {
		return 0
	}
	return slice.Sum(nn) / float64(len(nn))
}

func (c Collection) CalculateMMean() float64 {
	mm := c.MOfSegments()
	if len(mm) == 0 {
		return 0
	}
	return slice.Sum(mm) / float64(len(mm))
}

func (c Collection) CalculateMMo() int {
	valueAndFrequency := make(map[float64]int)

	var m int
	for _, sg := range c {
		m = len(sg.E)
		valueAndFrequency[float64(m)] += 1
	}

	return int(c.calculateMo(valueAndFrequency))
}

func (c Collection) CalculateMMe() float64 {
	return c.calculateMe(c.MOfSegments())
}

func (c Collection) MOfSegments() []float64 {
	mm := make([]float64, 0)

	for _, sg := range c {
		mm = append(mm, float64(len(sg.E)))
	}

	return mm
}

func (c Collection) CalculateNMe() float64 {
	return c.calculateMe(c.NOfSegments())
}

func (c Collection) NOfSegments() []float64 {
	nn := make([]float64, 0)

	for _, sg := range c {
		nn = append(nn, float64(len(sg.V)))
	}

	return nn
}

func (c Collection) calculateMe(values []float64) float64 {
	if len(values) == 0 {
		return 0
	}

	sort.Float64s(values)

	if number.IsOdd(len(values)) {
		return slice.Middle(values)
	}
	left, right := slice.MiddleLeftAndRight(values)
	return (left + right) / 2
}

func (Collection) calculateMo(valueAndFrequency map[float64]int) float64 {
	var modes []float64
	maxFrequency := 0
	for value, frequency := range valueAndFrequency {
		if frequency > maxFrequency {
			modes = make([]float64, 0)
			modes = append(modes, value)
			maxFrequency = frequency
		} else if frequency == maxFrequency {
			modes = append(modes, value)
		}
	}

	return slice.Min(modes)
}

func (c Collection) CalculateMOut() int {
	externalEdges := EdgeSet{}

	for _, sg := range c {
		externalEdges = externalEdges.Union(sg.EA)
	}

	return len(externalEdges)
}

func (c Collection) CalculateMOutMinAndMOutMax() (mOutMin, mOutMax int) {
	mOutMin = number.Infinity

	var mOut int
	for _, sg := range c {
		mOut = len(sg.EA)
		if mOut > mOutMax {
			mOutMax = mOut
		}
		if mOut < mOutMin {
			mOutMin = mOut
		}
	}

	return
}

func (c Collection) CalculateQMinAndQMax() (qMin, qMax float64) {
	qMin = float64(number.Infinity)

	var q float64
	for _, sg := range c {
		q = sg.CalculateQ()
		if q > qMax {
			qMax = q
		}
		if q < qMin {
			qMin = q
		}
	}

	return
}

func (c Collection) CalculateQMo() float64 {
	valueAndFrequency := make(map[float64]int)

	var q float64
	for _, sg := range c {
		q = sg.CalculateQ()
		valueAndFrequency[q] += 1
	}

	return c.calculateMo(valueAndFrequency)
}

func (c Collection) CalculateQMean() float64 {
	qq := c.QOfSegments()
	return slice.Sum(qq) / float64(len(qq))
}

func (c Collection) CalculateQMe() float64 {
	return c.calculateMe(c.QOfSegments())
}

func (c Collection) QOfSegments() []float64 {
	qq := make([]float64, 0)

	for _, sg := range c {
		qq = append(qq, sg.CalculateQ())
	}

	return qq
}
