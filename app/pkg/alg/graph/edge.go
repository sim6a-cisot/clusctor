package graph

import (
	"clusctor/app/pkg/alg/queue"
	"fmt"
	"strings"
)

type Edge struct {
	V1 *Vertex
	V2 *Vertex
	W  float64
}

func (e *Edge) Opposite(v *Vertex) *Vertex {
	var oppositeVertex *Vertex
	if v == e.V1 {
		oppositeVertex = e.V2
	} else if v == e.V2 {
		oppositeVertex = e.V1
	}
	return oppositeVertex
}

func (e *Edge) String() string {
	return fmt.Sprintf("%s-%.2f-%s", e.V1.Label, e.W, e.V2.Label)
}

type EdgeCollection []*Edge

func CreateEdgeCollection(vc VertexCollection) EdgeCollection {
	ec := make(EdgeCollection, 0, len(vc) - 1)
	var v1, v2 *Vertex
	var e *Edge
	for i := 0; i < len(vc) - 1; i++ {
		v1, v2 = vc[i], vc[i+1]
		e = v1.FindEdgeTo(v2)
		ec = append(ec, e)
	}
	return ec
}

func (c EdgeCollection) SumWeight() float64 {
	sumWeight := 0.0

	for _, e := range c {
		sumWeight += e.W
	}

	return sumWeight
}

func NewEdgePriorityQueue() *EdgePriorityQueue {
	return &EdgePriorityQueue{
		heap: queue.NewHeap(),
	}
}

type edgeHeapItem struct {
	edge *Edge
}

func (i *edgeHeapItem) Value() float64 {
	return i.edge.W
}

type EdgePriorityQueue struct {
	heap *queue.Heap
}

func (q *EdgePriorityQueue) Add(e *Edge) {
	q.heap.Add(&edgeHeapItem{e})
}

func (q *EdgePriorityQueue) TakeMin() *Edge {
	item := q.heap.TakeMin()
	return item.(*edgeHeapItem).edge
}

type EdgeSet map[*Edge]bool

func (es EdgeSet) FindWithMinWeight() *Edge {
	if len(es) == 0 {
		return nil
	}

	edgeWithMinWeight := es.Any()
	for e := range es {
		if e.W < edgeWithMinWeight.W {
			edgeWithMinWeight = e
		}
	}

	return edgeWithMinWeight
}

func (es EdgeSet) Any() *Edge {
	for e := range es {
		return e
	}
	return nil
}

func (es EdgeSet) IsEmpty() bool {
	return len(es) == 0
}

func (es EdgeSet) Add(e *Edge) {
	es[e] = true
}

func (es EdgeSet) Remove(e *Edge) {
	delete(es, e)
}

func (es EdgeSet) AddSet(other EdgeSet) {
	for e := range other {
		es.Add(e)
	}
}

func (es EdgeSet) Contains(e *Edge) bool {
	return es[e]
}

func (es EdgeSet) Intersect(other EdgeSet) EdgeSet {
	intersection := EdgeSet{}

	for e := range es {
		if other.Contains(e) {
			intersection.Add(e)
		}
	}

	return intersection
}

func (es EdgeSet) Difference(other EdgeSet) EdgeSet {
	difference := EdgeSet{}

	for e := range es {
		if !other.Contains(e) {
			difference.Add(e)
		}
	}

	return difference
}

func (es EdgeSet) Union(other EdgeSet) EdgeSet {
	union := EdgeSet{}

	for e := range es {
		union.Add(e)
	}
	for e := range other {
		union.Add(e)
	}

	return union
}

func (es EdgeSet) String() string {
	sb := strings.Builder{}
	for e := range es {
		sb.WriteString(e.String())
		sb.WriteString("\n")
	}
	return sb.String()
}