package graph

import (
	"container/list"
)

type Queue struct {
	core *list.List
}

func NewQueue() *Queue {
	return &Queue{
		core: list.New(),
	}
}

func (q *Queue) Enqueue(g *Graph) {
	q.core.PushBack(g)
}

func (q *Queue) Dequeue() *Graph {
	element := q.core.Front()
	q.core.Remove(element)
	return element.Value.(*Graph)
}

func (q *Queue) Size() int {
	return q.core.Len()
}
