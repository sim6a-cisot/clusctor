package graph_test

import (
	"clusctor/app/pkg/alg/graph"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGraph(t *testing.T) {
	G := graph.New()

	e12 := G.AddEdge("v1", "v2", 10)
	e23 := G.AddEdge("v2", "v3", 15)

	assert.Equal(t, e12.W, float64(10))
	assert.Equal(t, e23.W, float64(15))

	v1 := e12.V1
	v2 := e12.V2
	v3 := e23.V2

	assert.Equal(t, "v1", v1.Label)
	assert.Equal(t, "v2", v2.Label)
	assert.Equal(t, "v3", v3.Label)

	assert.Equal(t, 3, len(G.V))
	assert.True(t, G.V.Contains(v1))
	assert.True(t, G.V.Contains(v2))
	assert.True(t, G.V.Contains(v3))

	assert.Equal(t, 2, len(G.E))
	assert.True(t, G.E.Contains(e12))
	assert.True(t, G.E.Contains(e23))
}

func TestGraph_RemoveEdge(t *testing.T) {
	G := graph.New()

	e12 := G.AddEdge("v1", "v2", 10)
	G.AddEdge("v2", "v3", 15)
	e13 := G.AddEdge("v1", "v3", 20)
	v1 := e12.V1
	v3 := e13.V1

	assert.Len(t, v1.VA, 2)
	assert.Len(t, v1.EA, 2)
	assert.Len(t, v3.VA, 2)
	assert.Len(t, v3.EA, 2)
	assert.Len(t, G.E, 3)

	G.RemoveEdge(e13)

	assert.Len(t, v1.VA, 1)
	assert.Len(t, v1.EA, 1)
	assert.Len(t, v3.VA, 1)
	assert.Len(t, v3.EA, 1)
	assert.Len(t, G.E, 2)
}


func TestCollection_CalculateNMo(t *testing.T) {
	sg1 := graph.New()
	sg1.V.Add(&graph.Vertex{})

	sg2 := graph.New()
	sg2.V.Add(&graph.Vertex{})
	sg2.V.Add(&graph.Vertex{})

	sg3 := graph.New()
	sg3.V.Add(&graph.Vertex{})
	sg3.V.Add(&graph.Vertex{})

	sg4 := graph.New()
	sg4.V.Add(&graph.Vertex{})
	sg4.V.Add(&graph.Vertex{})
	sg4.V.Add(&graph.Vertex{})

	sg5 := graph.New()
	sg5.V.Add(&graph.Vertex{})

	subGraphs := graph.Collection{sg1, sg2, sg3, sg4, sg5}

	actual := subGraphs.CalculateNMo()
	expected := 1

	assert.Equal(t, expected, actual)
}

func TestCollection_CalculateMMo(t *testing.T) {
	sg1 := graph.New()
	sg1.E.Add(&graph.Edge{})

	sg2 := graph.New()
	sg2.E.Add(&graph.Edge{})
	sg2.E.Add(&graph.Edge{})

	sg3 := graph.New()
	sg3.E.Add(&graph.Edge{})
	sg3.E.Add(&graph.Edge{})

	sg4 := graph.New()
	sg4.E.Add(&graph.Edge{})
	sg4.E.Add(&graph.Edge{})
	sg4.E.Add(&graph.Edge{})

	sg5 := graph.New()
	sg5.E.Add(&graph.Edge{})

	subGraphs := graph.Collection{sg1, sg2, sg3, sg4, sg5}

	actual := subGraphs.CalculateMMo()
	expected := 1

	assert.Equal(t, expected, actual)
}