package graph

type Distances struct {
	verticesAndDistances              map[*Vertex]int
	verticesSortedByAscendingDistance VertexCollection
	vertexSet                         VertexSet
}

func NewDistances(size int) *Distances {
	return &Distances{
		verticesAndDistances: make(map[*Vertex]int, size),
		vertexSet:            VertexSet{},
	}
}

func (dd *Distances) SetDistance(v *Vertex, distance int) {
	dd.verticesAndDistances[v] = distance
}

func (dd *Distances) Add(v *Vertex, distance int) {
	dd.SetDistance(v, distance)
	dd.vertexSet.Add(v)
	dd.verticesSortedByAscendingDistance = append(dd.verticesSortedByAscendingDistance, v)
}

func (dd *Distances) DistanceTo(v *Vertex) int {
	return dd.verticesAndDistances[v]
}

func (dd *Distances) FindMax() (*Vertex, int) {
	v := dd.verticesSortedByAscendingDistance.Last()
	return v, dd.DistanceTo(v)
}

func (dd *Distances) AsSliceSortedByAscending() VertexCollection {
	return dd.verticesSortedByAscendingDistance
}

func (dd *Distances) AsSet() VertexSet {
	return dd.vertexSet
}

func (dd *Distances) Size() int {
	return len(dd.vertexSet)
}

func (dd *Distances) AsGraph() *Graph {
	g := New()
	for v := range dd.vertexSet {
		g.Include(v)
	}
	return g
}


