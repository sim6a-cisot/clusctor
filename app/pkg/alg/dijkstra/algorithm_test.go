package dijkstra_test

import (
	"clusctor/app/pkg/alg/dijkstra"
	"clusctor/app/pkg/alg/graph"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFindShortestPathInWholeNetwork(t *testing.T) {
	// see test/alg/dijkstra/dijkstra_test_graph.svg
	G := graph.New()

	e13 := G.AddEdge("v1", "v3", 3)
	G.AddEdge("v1", "v2", 6)
	G.AddEdge("v2", "v4", 4)
	e34 := G.AddEdge("v3", "v4", 5)
	G.AddEdge("v3", "v5", 9)
	e46 := G.AddEdge("v4", "v6", 2)
	G.AddEdge("v5", "v6", 3)

	v1 := e13.V1
	v3 := e13.V2
	v4 := e34.V2
	v6 := e46.V2

	shortestPath := dijkstra.FindLocalShortestPath(v1, v6, dijkstra.UseOriginalWeight, G)
	want := graph.VertexCollection{
		v1,
		v3,
		v4,
		v6,
	}

	assert.Equal(t, want, shortestPath)
}

func TestFindShortestPathInSegmentedNetwork(t *testing.T) {
	// see test/alg/dijkstra/dijkstra_test_segmented_graph.svg
	G := graph.New()

	e13 := G.AddEdge("v1", "v3", 3)
	G.AddEdge("v1", "v2", 6)
	G.AddEdge("v2", "v4", 4)
	e34 := G.AddEdge("v3", "v4", 5)
	G.AddEdge("v3", "v5", 9)
	e46 := G.AddEdge("v4", "v6", 2)
	e56 := G.AddEdge("v5", "v6", 3)

	subGraph := graph.New()
	subGraph.Include(e34.V1)
	subGraph.Include(e34.V2)
	subGraph.Include(e56.V1)
	subGraph.Include(e56.V2)
	G.SubGraphs = graph.Collection{subGraph}

	v3 := e13.V2
	v4 := e34.V2
	v6 := e46.V2

	shortestPath := dijkstra.FindLocalShortestPath(v3, v6, dijkstra.UseOriginalWeight, G)
	want := graph.VertexCollection{
		v3,
		v4,
		v6,
	}

	assert.Equal(t, want, shortestPath)
}

func TestFindAllShortestPaths(t *testing.T) {
	G := graph.New()

	e12 := G.AddEdge("v1", "v2", 10)
	G.AddEdge("v2", "v3", 15)
	G.AddEdge("v1", "v3", 20)
	v1 := e12.V1

	paths := dijkstra.FindAllShortestPaths(v1, dijkstra.UseUnitaryWeight, G)

	assert.Len(t, paths, 2)
	p1 := paths[0]
	assert.Len(t, p1, 1)

	p2 := paths[1]
	assert.Len(t, p2, 1)
}