package dijkstra

import (
	"clusctor/app/application/service"
	"clusctor/app/pkg/alg/graph"
	"clusctor/app/pkg/std/number"
)

type TakeWeightFunc func(*graph.Edge) int

var (
	UseOriginalWeight = func(e *graph.Edge) int { return int(e.W) }
	UseUnitaryWeight  = func(*graph.Edge) int { return 1 }
)

func FindAllShortestPaths(src *graph.Vertex, takeWeight TakeWeightFunc, g *graph.Graph) []graph.EdgeCollection {
	paths := make([]graph.EdgeCollection, 0)
	tree := findShortestPathsTree(src, takeWeight, g)
	for v := range g.V {
		if v == src {
			continue
		}
		pathAsVertexSequence := buildPath(tree, v)
		path := graph.CreateEdgeCollection(pathAsVertexSequence)
		paths = append(paths, path)
	}
	return paths
}

func findShortestPathsTree(src *graph.Vertex, takeWeight TakeWeightFunc, g *graph.Graph) map[*graph.Vertex]*graph.Vertex {
	return findShortestPathsTreeToDestination(src, nil, takeWeight, g)
}

func FindLocalShortestPath(src, dst *graph.Vertex, takeWeight TakeWeightFunc, g *graph.Graph) graph.VertexCollection {
	if subGraph := g.SubGraphs.FindSubGraphOfVertices(src, dst); subGraph != nil {
		service.LogDebug("Local path detected!")
		g = subGraph
	}
	paths := findShortestPathsTree(src, takeWeight, g)
	return buildPath(paths, dst)
}

func FindShortestPath(src, dst *graph.Vertex, takeWeight TakeWeightFunc, g *graph.Graph) graph.VertexCollection {
	paths := findShortestPathsTreeToDestination(src, dst, takeWeight, g)
	return buildPath(paths, dst)
}

func findShortestPathsTreeToDestination(
	src, dst *graph.Vertex,
	takeWeight TakeWeightFunc,
	g *graph.Graph,
) map[*graph.Vertex]*graph.Vertex {
	distances := make(map[*graph.Vertex]int, len(g.V))
	shortestPathsTree := make(map[*graph.Vertex]*graph.Vertex, len(g.V))
	for v := range g.V {
		distances[v] = number.Infinity
	}
	distances[src] = 0
	q := graph.NewVertexPriorityQueue()
	q.Add(src, distances[src])

	var cur *graph.Vertex
	var v *graph.Vertex
	for q.Size() > 0 {
		cur = q.TakeMin()
		for e := range cur.EA {
			v = e.Opposite(cur)
			if distances[cur]+takeWeight(e) < distances[v] {
				distances[v] = distances[cur] + takeWeight(e)
				shortestPathsTree[v] = cur
				q.Add(v, distances[v])
			}
		}
		if cur == dst {
			break
		}
	}
	return shortestPathsTree
}

func buildPath(shortestPathsTree map[*graph.Vertex]*graph.Vertex, dst *graph.Vertex) graph.VertexCollection {
	var path graph.VertexCollection
	cur := dst
	path = append(path, cur)
	parent := shortestPathsTree[cur]
	for parent != nil {
		path = append(path, parent)
		cur = parent
		parent = shortestPathsTree[cur]
	}
	revert(path)
	return path
}

func revert(path graph.VertexCollection) {
	for left, right := 0, len(path)-1; left < right; left, right = left+1, right-1 {
		path[left], path[right] = path[right], path[left]
	}
}
