package kerniganlin

import (
	"clusctor/app/pkg/alg/graph"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRun(t *testing.T) {
	// see test/alg/kerniganlin/kerniganlin_test_graph.svg
	G := graph.New()
	G.AddEdge("v1", "v2", 1)
	G.AddEdge("v1", "v5", 1)
	G.AddEdge("v1", "v6", 1)

	G.AddEdge("v2", "v5", 1)
	G.AddEdge("v2", "v6", 1)

	G.AddEdge("v3", "v6", 1)
	G.AddEdge("v3", "v7", 1)
	G.AddEdge("v3", "v8", 1)
	G.AddEdge("v3", "v4", 1)

	G.AddEdge("v4", "v7", 1)
	G.AddEdge("v4", "v8", 1)

	G.AddEdge("v5", "v6", 1)

	G.AddEdge("v7", "v8", 1)

	assert.Len(t, G.V, 8)
	assert.Len(t, G.E, 13)
}

func TestAlgorithm_computeVertexCost(t *testing.T) {
	// see test/alg/kerniganlin/kerniganlin_test_graph.svg
	G := graph.New()
	e12 := G.AddEdge("v1", "v2", 1)
	G.AddEdge("v1", "v5", 1)
	G.AddEdge("v1", "v6", 1)

	G.AddEdge("v2", "v5", 1)
	G.AddEdge("v2", "v6", 1)

	G.AddEdge("v3", "v6", 1)
	G.AddEdge("v3", "v7", 1)
	G.AddEdge("v3", "v8", 1)
	e34 := G.AddEdge("v3", "v4", 1)

	G.AddEdge("v4", "v7", 1)
	G.AddEdge("v4", "v8", 1)

	e56 := G.AddEdge("v5", "v6", 1)

	e78 := G.AddEdge("v7", "v8", 1)

	s1 := graph.New()
	s1.Include(e12.V1)
	s1.Include(e12.V2)
	s1.Include(e34.V1)
	s1.Include(e34.V2)

	s2 := graph.New()
	s2.Include(e56.V1)
	s2.Include(e56.V2)
	s2.Include(e78.V1)
	s2.Include(e78.V2)

	G.SubGraphs = []*graph.Graph{s1, s2}

	alg := NewAlgorithm(G)

	exchanges := alg.findAllOptimalExchanges().findSequenceWithMaxGain()
	for _, ex := range exchanges {
		fmt.Println(ex.vFromSegmentA.Label, ex.vFromSegmentB.Label, ex.gain)
	}
}
