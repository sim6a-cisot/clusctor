package kerniganlin

import "clusctor/app/pkg/alg/graph"

type connectivityMatrix map[*graph.Vertex]map[*graph.Vertex]*graph.Edge

func newConnectivityMatrix(g *graph.Graph) connectivityMatrix {
	m := make(connectivityMatrix)
	for e := range g.E {
		if _, ok := m[e.V1]; !ok {
			m[e.V1] = make(map[*graph.Vertex]*graph.Edge)
		}
		if _, ok := m[e.V2]; !ok {
			m[e.V2] = make(map[*graph.Vertex]*graph.Edge)
		}
		m[e.V1][e.V2] = e
		m[e.V2][e.V1] = e
	}
	return m
}

func (m connectivityMatrix) get(v1, v2 *graph.Vertex) *graph.Edge {
	return m[v1][v2]
}
