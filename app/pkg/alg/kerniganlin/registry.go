package kerniganlin

import "clusctor/app/pkg/alg/graph"

const (
	segmentA = 0
	segmentB = 1
)

type segmentRegistry map[*graph.Vertex]int // vertex | segment id (A or B)

func newSegmentRegistry() segmentRegistry {
	return make(segmentRegistry)
}

func (r segmentRegistry) swap(ex exchange) {
	r[ex.vFromSegmentA], r[ex.vFromSegmentB] = r[ex.vFromSegmentB], r[ex.vFromSegmentA]
}

func (r segmentRegistry) copy() segmentRegistry {
	c := newSegmentRegistry()
	for v, segment := range r {
		c[v] = segment
	}
	return c
}
