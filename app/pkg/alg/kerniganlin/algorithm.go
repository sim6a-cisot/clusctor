package kerniganlin

import (
	"clusctor/app/pkg/alg/graph"
	"math"
)

func NewAlgorithm(g *graph.Graph) *Algorithm {
	segments := g.SubGraphs
	if len(segments) != 2 {
		return nil
	}
	initialRegistry := newSegmentRegistry()
	for v := range segments[0].V {
		initialRegistry[v] = segmentA
	}
	for v := range segments[1].V {
		initialRegistry[v] = segmentB
	}
	notFixedVertices := make(map[*graph.Vertex]bool)
	for v := range g.V {
		notFixedVertices[v] = true
	}
	alg := Algorithm{
		benefits:         make(map[*graph.Vertex]float64),
		segments:         initialRegistry.copy(),
		edges:            newConnectivityMatrix(g),
		notFixedVertices: notFixedVertices,
	}
	return &alg
}

type Algorithm struct {
	benefits         map[*graph.Vertex]float64
	segments         segmentRegistry
	notFixedVertices map[*graph.Vertex]bool
	edges            connectivityMatrix
}

func (a *Algorithm) findAllOptimalExchanges() exchanges {
	exchanges := make(exchanges, 0, len(a.notFixedVertices)/2)

	for len(a.notFixedVertices) > 1 {
		optimalExchange := a.findOptimalExchange()
		a.segments.swap(optimalExchange)
		delete(a.notFixedVertices, optimalExchange.vFromSegmentA)
		delete(a.notFixedVertices, optimalExchange.vFromSegmentB)
		a.updateBenefits(optimalExchange)

		exchanges = append(exchanges, optimalExchange)
	}

	return exchanges
}

func (a *Algorithm) findOptimalExchange() exchange {
	optimal := exchange{
		gain: -1 * float64(math.MaxFloat32),
	}
	potentialExchanges := a.enumeratePotentialExchanges()
	for _, sw := range potentialExchanges {
		gain := a.computeExchangeGain(sw)
		if gain > optimal.gain {
			optimal = sw
			optimal.gain = gain
		}
	}
	return optimal
}

func (a *Algorithm) newExchange(v1, v2 *graph.Vertex) exchange {
	ex := exchange{}
	if a.segments[v1] == segmentA {
		ex.vFromSegmentA = v1
		ex.vFromSegmentB = v2
	} else {
		ex.vFromSegmentA = v2
		ex.vFromSegmentB = v1
	}
	return ex
}

func (a *Algorithm) enumeratePotentialExchanges() []exchange {
	potentialExchanges := []exchange{}
	cache := newExchangeCache()
	for v := range a.segments {
		if a.isFixed(v) {
			continue
		}
		for w := range a.segments {
			ex := a.newExchange(v, w)
			if a.isFixed(w) || a.areInTheSameSegment(v, w) || cache.contains(ex) {
				continue
			}
			cache.add(ex)
			potentialExchanges = append(potentialExchanges, ex)
		}
	}
	return potentialExchanges
}

func (a *Algorithm) areInTheSameSegment(v1, v2 *graph.Vertex) bool {
	return a.segments[v1] == a.segments[v2]
}

func (a *Algorithm) isFixed(v *graph.Vertex) bool {
	return !a.notFixedVertices[v]
}

func (a *Algorithm) computeExchangeGain(ex exchange) float64 {
	var edgeCost float64
	if e := a.edges.get(ex.vFromSegmentA, ex.vFromSegmentB); e != nil {
		edgeCost = e.W
	}
	return a.benefitOfMoving(ex.vFromSegmentA) + a.benefitOfMoving(ex.vFromSegmentB) - 2*edgeCost
}

func (a *Algorithm) computeVertexCosts(v *graph.Vertex) (internal, external float64) {
	vertexSegment := a.segments[v]
	for e := range v.EA {
		neighbor := e.Opposite(v)
		if vertexSegment == a.segments[neighbor] {
			internal += e.W
		} else {
			external += e.W
		}
	}
	return
}

func (a *Algorithm) computeVertexMovingBenefit(v *graph.Vertex) float64 {
	internal, external := a.computeVertexCosts(v)
	return external - internal
}

func (a *Algorithm) benefitOfMoving(v *graph.Vertex) float64 {
	d, ok := a.benefits[v]
	if !ok {
		d = a.computeVertexMovingBenefit(v)
		a.benefits[v] = d
	}
	return d
}

func (a *Algorithm) updateBenefits(ex exchange) {
	var externalEdge, internalEdge *graph.Edge
	var externalEdgeCost, internalEdgeCost float64
	for v := range a.notFixedVertices {
		if a.segments[v] == segmentA {
			externalEdge = a.edges.get(v, ex.vFromSegmentA)
			internalEdge = a.edges.get(v, ex.vFromSegmentB)
		} else {
			externalEdge = a.edges.get(v, ex.vFromSegmentB)
			internalEdge = a.edges.get(v, ex.vFromSegmentA)
		}
		if externalEdge != nil {
			externalEdgeCost = externalEdge.W
		}
		if internalEdge != nil {
			internalEdgeCost = internalEdge.W
		}

		a.updateBenefit(v, internalEdgeCost, externalEdgeCost)
		internalEdgeCost = 0
		externalEdgeCost = 0
	}
}

func (a *Algorithm) updateBenefit(v *graph.Vertex, internalEdgeCost, externalEdgeCost float64) {
	d := a.benefits[v]
	a.benefits[v] = d + 2*externalEdgeCost - 2*internalEdgeCost
}
