package kerniganlin

import "clusctor/app/pkg/alg/graph"

type exchange struct {
	vFromSegmentA *graph.Vertex
	vFromSegmentB *graph.Vertex
	gain          float64
}

func (ex exchange) equals(other exchange) bool {
	return ex.vFromSegmentA.Label == other.vFromSegmentA.Label &&
		ex.vFromSegmentB.Label == other.vFromSegmentB.Label &&
		ex.gain == other.gain
}

type exchanges []exchange

func (exs exchanges) findWithMaxGain() exchange {
	if len(exs) == 0 {
		return exchange{}
	}
	max := exs[0]
	for _, ex := range exs {
		if ex.gain > max.gain {
			max = ex
		}
	}
	return max
}

func (exs exchanges) findSequenceWithMaxGain() exchanges {
	sequence := make(exchanges, 0)
	max := exs.findWithMaxGain()
	for _, ex := range exs {
		sequence = append(sequence, ex)
		if ex.equals(max) {
			break
		}
	}
	return sequence
}

type exchangeCache map[*graph.Vertex]map[*graph.Vertex]bool

func newExchangeCache() exchangeCache {
	return make(exchangeCache)
}

func (c exchangeCache) add(sw exchange) {
	if _, ok := c[sw.vFromSegmentA]; !ok {
		c[sw.vFromSegmentA] = make(map[*graph.Vertex]bool)
	}
	if _, ok := c[sw.vFromSegmentB]; !ok {
		c[sw.vFromSegmentB] = make(map[*graph.Vertex]bool)
	}
	c[sw.vFromSegmentA][sw.vFromSegmentB] = true
	c[sw.vFromSegmentB][sw.vFromSegmentA] = true
}

func (c exchangeCache) contains(sw exchange) bool {
	return c[sw.vFromSegmentA][sw.vFromSegmentB]
}
