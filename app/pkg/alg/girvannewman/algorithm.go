package girvannewman

import (
	"clusctor/app/pkg/alg/bfs"
	"clusctor/app/pkg/alg/girvannewman/adjmtrx"
	"clusctor/app/pkg/alg/girvannewman/betweenness"
	"clusctor/app/pkg/alg/girvannewman/modularity"
	"clusctor/app/pkg/alg/graph"
)

func FindOptimalPartitionWithInterruption(g *graph.Graph) []*graph.Graph {
	componentsQuantity := 1
	maxModularity := float64(0)
	partitionWithMaxModularity := make([]*graph.Graph, 0)
	A, matrixMatcher := adjmtrx.Create(g)
	for len(g.E) > 0 {
		partition := findPartition(g, A, matrixMatcher)
		if len(partition) != componentsQuantity {
			calc := modularity.NewCalculator(A, modularity.CreateCommunities(partition, matrixMatcher))
			m := calc.Calculate()
			if m < maxModularity {
				break
			}
			componentsQuantity = len(partition)
			maxModularity = m
			partitionWithMaxModularity = partition
		}
	}
	return partitionWithMaxModularity
}

func FindOptimalPartition(g *graph.Graph) []*graph.Graph {
	componentsQuantity := 1
	partitions := make(partitionCollection, 0)
	A, matrixMatcher := adjmtrx.Create(g)
	for len(g.E) > 0 {
		partition := findPartition(g, A, matrixMatcher)
		if len(partition) != componentsQuantity {
			calc := modularity.NewCalculator(A, modularity.CreateCommunities(partition, matrixMatcher))
			m := calc.Calculate()
			componentsQuantity = len(partition)
			partitions = append(partitions, partitionCollectionItem{
				partition:  partition,
				modularity: m,
			})
		}
	}
	return partitions.findPartitionWithMaxModularity()
}

func findPartition(g *graph.Graph, A *adjmtrx.Matrix, matrixMatcher adjmtrx.VertexLabelMatcher) []*graph.Graph {
	edgeWithMaxBetweenness := betweenness.FindEdgeWithMaxBetweenness(g)
	g.RemoveEdge(edgeWithMaxBetweenness)
	v1 := edgeWithMaxBetweenness.V1
	v2 := edgeWithMaxBetweenness.V2
	A.RemoveEdge(matrixMatcher[v1.Label], matrixMatcher[v2.Label])
	return bfs.FindComponents(g)
}