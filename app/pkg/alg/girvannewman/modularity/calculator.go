package modularity

import (
	"clusctor/app/pkg/alg/girvannewman/adjmtrx"
)

type Calculator struct {
	matrix      *adjmtrx.Matrix
	communities Communities
}

func NewCalculator(matrix *adjmtrx.Matrix, communities Communities) *Calculator {
	return &Calculator{matrix: matrix, communities: communities}
}

func (c *Calculator) Calculate() float64 {
	edgesQuantity := c.matrix.CountEdges()
	matrixSize := c.matrix.Size()
	modularity := float64(0)
	for i := 0; i < matrixSize; i++ {
		for j := 0; j < matrixSize; j++ {
			modularity += c.q(i, j, edgesQuantity)*c.sigma(i, j)
		}
	}
	return modularity
}

func (c *Calculator) q(i, j, edgesQuantity int) float64 {
	return float64(c.matrix.Item(i, j)) / float64(2 * edgesQuantity) -
		float64(c.matrix.VertexDegree(i) * c.matrix.VertexDegree(j)) / float64(4 * edgesQuantity * edgesQuantity)
}

func (c *Calculator) sigma(i, j int) float64 {
	if c.communities.NumberOfCommunity(i) == c.communities.NumberOfCommunity(j) {
		return 1
	}
	return 0
}
