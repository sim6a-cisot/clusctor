package modularity

import (
	"clusctor/app/pkg/alg/girvannewman/adjmtrx"
	"clusctor/app/pkg/alg/graph"
)

type Communities map[int]int

func NewCommunities() Communities {
	return make(map[int]int)
}

func CreateCommunities(partition []*graph.Graph, vertexMatcher adjmtrx.VertexLabelMatcher) Communities {
	c := NewCommunities()
	for segmentNumber, segment := range partition {
		for v := range segment.V {
			c.Add(vertexMatcher[v.Label], segmentNumber)
		}
	}
	return c
}

func (s Communities) NumberOfCommunity(vertexNumber int) int {
	return s[vertexNumber]
}

func (s Communities) Add(vertexNumber, communityID int) {
	s[vertexNumber] = communityID
}
