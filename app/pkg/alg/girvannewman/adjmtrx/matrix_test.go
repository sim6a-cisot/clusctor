package adjmtrx_test

import (
	"clusctor/app/pkg/alg/girvannewman/adjmtrx"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMatrix_CountEdges(t *testing.T) {
	m := adjmtrx.New()
	m.AddRow([]int{0, 1, 0, 0})
	m.AddRow([]int{1, 0, 1, 0})
	m.AddRow([]int{0, 1, 0, 1})
	m.AddRow([]int{0, 0, 1, 0})

	assert.Equal(t, 3, m.CountEdges())
}

func TestMatrix_VertexDegree(t *testing.T) {
	m := adjmtrx.New()
	m.AddRow([]int{0, 1, 0, 0})
	m.AddRow([]int{1, 0, 1, 0})
	m.AddRow([]int{0, 1, 0, 1})
	m.AddRow([]int{0, 0, 1, 0})

	assert.Equal(t, 1, m.VertexDegree(0))
	assert.Equal(t, 2, m.VertexDegree(1))
	assert.Equal(t, 2, m.VertexDegree(2))
	assert.Equal(t, 1, m.VertexDegree(3))
}

func TestMatrix_RemoveEdge_EdgeExists(t *testing.T) {
	m := adjmtrx.New()
	m.AddRow([]int{0, 1, 0, 0})
	m.AddRow([]int{1, 0, 1, 0})
	m.AddRow([]int{0, 1, 0, 1})
	m.AddRow([]int{0, 0, 1, 0})

	assert.Equal(t, 1, m.Item(0, 1))
	assert.Equal(t, 1, m.Item(1, 0))

	m.RemoveEdge(1, 0)

	assert.Equal(t, 0, m.Item(0, 1))
	assert.Equal(t, 0, m.Item(1, 0))
}

func TestMatrix_RemoveEdge_EdgeDoesNotExist(t *testing.T) {
	m := adjmtrx.New()
	m.AddRow([]int{0, 1, 0, 0})
	m.AddRow([]int{1, 0, 1, 0})
	m.AddRow([]int{0, 1, 0, 1})
	m.AddRow([]int{0, 0, 1, 0})

	assert.Equal(t, 0, m.Item(2, 2))
	assert.Equal(t, 0, m.Item(2, 2))

	m.RemoveEdge(2, 2)

	assert.Equal(t, 0, m.Item(2, 2))
	assert.Equal(t, 0, m.Item(2, 2))
}