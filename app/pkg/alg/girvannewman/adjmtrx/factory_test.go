package adjmtrx_test

import (
	"clusctor/app/pkg/alg/girvannewman/adjmtrx"
	"clusctor/app/pkg/alg/graph"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestCreate(t *testing.T) {
	G := graph.New()

	e12 := G.AddEdge("v1", "v2", 10)
	G.AddEdge("v2", "v3", 15)
	e13 := G.AddEdge("v1", "v3", 20)

	// 0 1 1
	// 1 0 1
	// 1 1 0
	m, vmatcher := adjmtrx.Create(G)

	v1 := vmatcher[e12.V1.Label]
	v2 := vmatcher[e12.V2.Label]
	v3 := vmatcher[e13.V2.Label]
	assert.Equal(t, 1, m.Item(v1, v2))
	assert.Equal(t, 1, m.Item(v2, v1))
	assert.Equal(t, 1, m.Item(v2, v3))
	assert.Equal(t, 1, m.Item(v3, v2))
	assert.Equal(t, 1, m.Item(v1, v3))
	assert.Equal(t, 1, m.Item(v3, v1))
}