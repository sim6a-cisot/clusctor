package adjmtrx

import (
	"clusctor/app/pkg/alg/graph"
)

type VertexLabelMatcher map[string]int

func Create(G *graph.Graph) (*Matrix, VertexLabelMatcher) {
	m := New()
	matcher := make(VertexLabelMatcher)
	i := 0
	for v := range G.V {
		matcher[v.Label] = i
		i++
		m.AddRow(make([]int, len(G.V)))
	}
	for e := range G.E {
		m.AddEdge(matcher[e.V1.Label], matcher[e.V2.Label])
	}
	return m, matcher
}