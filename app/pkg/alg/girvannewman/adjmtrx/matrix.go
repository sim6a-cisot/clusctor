package adjmtrx

type Matrix struct {
	matrix [][]int
}

func New() *Matrix {
	return &Matrix{
		matrix: make([][]int, 0, 0),
	}
}

func (m *Matrix) AddRow(row []int) {
	m.matrix = append(m.matrix, row)
}

func (m *Matrix) CountEdges() int {
	quantity := 0
	for rowNumber, row := range m.matrix {
		for i := 0; i < rowNumber; i++ {
			quantity += row[i]
		}
	}
	return quantity
}

func (m *Matrix) VertexDegree(vertexNumber int) int {
	degree := 0
	for _, rowItem := range m.matrix[vertexNumber] {
		degree += rowItem
	}
	return degree
}

func (m *Matrix) Item(i, j int) int {
	return m.matrix[i][j]
}

func (m *Matrix) AddEdge(i, j int) {
	m.matrix[i][j] = 1
	m.matrix[j][i] = 1
}

func (m *Matrix) RemoveEdge(i, j int) {
	m.matrix[i][j] = 0
	m.matrix[j][i] = 0
}

func (m *Matrix) Size() int {
	return len(m.matrix)
}
