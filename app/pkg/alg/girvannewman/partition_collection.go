package girvannewman

import (
	"clusctor/app/pkg/alg/graph"
)

type partitionCollection []partitionCollectionItem

type partitionCollectionItem struct {
	partition  []*graph.Graph
	modularity float64
}

func (c partitionCollection) findPartitionWithMaxModularity() []*graph.Graph {
	if len(c) == 0 {
		return nil
	}

	itemWithMaxModularity := c[0]

	for _, item := range c {
		if item.modularity > itemWithMaxModularity.modularity {
			itemWithMaxModularity = item
		}
	}

	return itemWithMaxModularity.partition
}
