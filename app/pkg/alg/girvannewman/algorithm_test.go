package girvannewman_test

import (
	"clusctor/app/pkg/alg/girvannewman"
	"clusctor/app/pkg/alg/graph"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFindOptimalPartition(t *testing.T) {
	// see test/alg/girvannewman/girvannewman_test_graph.svg
	G := graph.New()

	G.AddEdge("v1", "v2", 1)
	G.AddEdge("v1", "v3", 1)
	G.AddEdge("v2", "v3", 1)
	G.AddEdge("v3", "v4", 1)
	G.AddEdge("v4", "v5", 1)
	G.AddEdge("v4", "v6", 1)
	G.AddEdge("v5", "v6", 1)

	segments := girvannewman.FindOptimalPartition(G)
	assert.Len(t, segments, 2)
}
