package betweenness

import (
	"clusctor/app/pkg/alg/dijkstra"
	"clusctor/app/pkg/alg/graph"
)

func FindEdgeWithMaxBetweenness(G *graph.Graph) *graph.Edge {
	t := make(table)
	for v := range G.V {
		shortestPaths := dijkstra.FindAllShortestPaths(v, dijkstra.UseUnitaryWeight, G)
		for _, sp := range shortestPaths {
			for _, e := range sp {
				t[e] += 1
			}
		}
	}
	return t.FindEdgeWithMaxBetweenness()
}
