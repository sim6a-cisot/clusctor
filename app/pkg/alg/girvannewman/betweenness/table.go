package betweenness

import (
	"clusctor/app/pkg/alg/graph"
)

type table map[*graph.Edge]int

func (t table) FindEdgeWithMaxBetweenness() *graph.Edge {
	maxBetweenness := 0
	var edgeWithMaxBetweenness *graph.Edge
	for e := range t {
		if t[e] > maxBetweenness {
			maxBetweenness = t[e]
			edgeWithMaxBetweenness = e
		}
	}
	return edgeWithMaxBetweenness
}