package strutil

import (
	"clusctor/app/pkg/stdconv"
	"math"
)

const (
	bitPerSecondUnit     = "Bit/s"
	kiloBitPerSecondUnit = "kBit/s"
	megaBitPerSecondUnit = "MBit/s"
	gigaBitPerSecondUnit = "GBit/s"
)

func RepresentAsBitsPerSecond(bytesPerSecond float64) string {
	if bytesPerSecond == 0 {
		return "0.00 " + bitPerSecondUnit
	}
	bitPerSecond := bytesPerSecond * 8
	decimalPlaces := math.Log10(bitPerSecond)
	result := ""
	if decimalPlaces < 3 {
		result = stdconv.Float64ToStr(bitPerSecond) + " " + bitPerSecondUnit
	} else if decimalPlaces < 6 {
		result = stdconv.Float64ToStr(bitPerSecond/1000) + " " + kiloBitPerSecondUnit
	} else if decimalPlaces < 9 {
		result = stdconv.Float64ToStr(bitPerSecond/1000000) + " " + megaBitPerSecondUnit
	} else if decimalPlaces < 12 {
		result = stdconv.Float64ToStr(bitPerSecond/1000000000) + " " + gigaBitPerSecondUnit
	}
	return result
}