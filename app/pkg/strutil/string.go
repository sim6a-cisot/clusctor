package strutil

import (
	"strings"
	"unicode"
)

func RemoveWhiteSpaces(str string) string {
	var b strings.Builder
	b.Grow(len(str))
	for _, ch := range str {
		if !unicode.IsSpace(ch) {
			if ch == '*' {
				b.WriteRune(' ')
			} else {
				b.WriteRune(ch)
			}
		}
	}
	return b.String()
}