package strutil

import (
	stdTime "clusctor/app/pkg/std/time"
	"strconv"
	"time"
)

func TimeInMs(duration time.Duration) string {
	return strconv.FormatInt(stdTime.InMs(duration), 10) + "ms"
}

func TimeInMcs(duration time.Duration) string {
	return strconv.FormatInt(stdTime.InMcs(duration), 10) + "mcs"
}
