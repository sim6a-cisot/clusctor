package strutil

import "testing"

func TestRepresentAsBitsPerSecond(t *testing.T) {
	tests := []struct {
		name string
		bytesPerSecond float64
		want string
	}{
		{
			name: "26.24 MBit/s",
			bytesPerSecond: 3280000,
			want: "26.24 MBit/s",
		},
		{
			name: "320.00 kBit/s",
			bytesPerSecond: 40000,
			want: "320.00 kBit/s",
		},
		{
			name: "2.56 kBit/s",
			bytesPerSecond: 320,
			want: "2.56 kBit/s",
		},
		{
			name: "256.00 Bit/s",
			bytesPerSecond: 32,
			want: "256.00 Bit/s",
		},
		{
			name: "0.00 Bit/s",
			bytesPerSecond: 0,
			want: "0.00 Bit/s",
		},
		{
			name: "259.46 Bit/s",
			bytesPerSecond: 32.432213,
			want: "259.46 Bit/s",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RepresentAsBitsPerSecond(tt.bytesPerSecond); got != tt.want {
				t.Errorf("RepresentAsBitsPerSecond() = %v, want %v", got, tt.want)
			}
		})
	}
}