package strutil

type StringPair map[string]bool

func NewStringPair() StringPair {
	return make(StringPair)
}

func (sp StringPair) Add(s1, s2 string) {
	sp[s1+s2] = true
	sp[s2+s1] = true
}

func (sp StringPair) Has(s1, s2 string) bool {
	return sp[s1+s2]
}
