package cmd

import (
	"bytes"
	"errors"
	"os/exec"
)

type Runner struct {}

func NewRunner() *Runner {
	return &Runner{}
}

func (r *Runner) Run(relativeFilePath string, argumentsWithValues map[string]string) ([]byte, error) {
	stringArgs := r.convertToStringArguments(argumentsWithValues)
	cmd := exec.Command(relativeFilePath, stringArgs...)
	stdOut := &bytes.Buffer{}
	stdErr := &bytes.Buffer{}
	cmd.Stdout = stdOut
	cmd.Stderr = stdErr
	if err := cmd.Run(); err != nil {
		return []byte{}, err
	}
	if stdErr.Len() > 0 {
		return []byte{}, errors.New(stdErr.String())
	}
	return stdOut.Bytes(), nil
}

func (Runner) convertToStringArguments(argumentsWithValues map[string]string) []string {
	var stringArguments []string
	for arg, value := range argumentsWithValues {
		stringArguments = append(stringArguments, arg, value)
	}
	return stringArguments
}
