package std

type FileWriter interface {
	WriteFile(filePath string, data []byte) error
}

type FileReader interface {
	ReadFile(filePath string) ([]byte, error)
}
