package time

import "time"

type Time struct{}

func New() *Time {
	return &Time{}
}

func (t *Time) Now() time.Time {
	return time.Now()
}

func InMs(duration time.Duration) int64 {
	return int64(duration)/int64(time.Millisecond)
}

func InMcs(duration time.Duration) int64 {
	return int64(duration)/int64(time.Microsecond)
}