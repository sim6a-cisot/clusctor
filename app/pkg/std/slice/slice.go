package slice

import (
	"clusctor/app/pkg/std/number"
)

func Min(slc []float64) float64 {
	if len(slc) == 0 {
		return 0
	}

	min := float64(number.Infinity)

	for _, item := range slc {
		if item < min {
			min = item
		}
	}

	return min
}

func Max(slc []float64) float64 {
	max := 0.0

	for _, item := range slc {
		if item > max {
			max = item
		}
	}

	return max
}

func Average(slc []float64) float64 {
	if len(slc) == 0 {
		return 0
	}

	sum := 0.0

	for _, item := range slc {
		sum += item
	}

	return sum / float64(len(slc))
}

func Sum(slc []float64) float64 {
	sum := float64(0)
	for _, item := range slc {
		sum += item
	}
	return sum
}

func Middle(slc []float64) float64 {
	slcLen := len(slc)
	return slc[slcLen/2]
}

func MiddleLeftAndRight(slc []float64) (float64, float64) {
	if len(slc) == 2 {
		return slc[0], slc[1]
	}
	slcLen := len(slc)
	return slc[slcLen/2], slc[slcLen/2+1]
}
