package env

import "os"

func NewEnvironment() *Environment {
	return &Environment{}
}

type Environment struct {}

func (Environment) UserHomeDir() string {
	return os.Getenv("HOME")
}
