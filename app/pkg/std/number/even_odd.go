package number

func IsEven(i int) bool {
	return i % 2 == 0
}

func IsOdd(i int) bool {
	return !IsEven(i)
}