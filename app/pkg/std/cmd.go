package std

type CmdRunner interface {
	Run(relativeFilePath string, argumentsWithValues map[string]string) ([]byte, error)
}
