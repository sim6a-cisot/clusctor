package std

import "time"

type Time interface {
	Now() time.Time
}

