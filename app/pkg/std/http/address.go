package http

import "strconv"

func CreateAddress(host string, port int) string {
	return createAddress("", host, port)
}

func CreateSecureAddress(host string, port int) string {
	return createAddress("s", host, port)
}

func createAddress(s string, host string, port int) string {
	return "http" + s + "://" + host +  ":" + strconv.Itoa(port)
}
