package http

const (
	ApplicationJSON = "application/json"
	TextCsv = "text/csv"
)

func Attachment(filename string) string {
	return "attachment;filename=" + filename
}
