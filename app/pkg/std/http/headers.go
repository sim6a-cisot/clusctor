package http

const (
	XAuthToken = "X-Auth-Token"
	ContentType = "Content-Type"
	ContentDisposition = "Content-Disposition"
)
