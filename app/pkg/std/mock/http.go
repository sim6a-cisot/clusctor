package mock

import "net/http"

type HttpClient struct {
	DoFunc func(*http.Request) (*http.Response, error)
}

func (c *HttpClient) Do(request *http.Request) (*http.Response, error) {
	return c.DoFunc(request)
}
