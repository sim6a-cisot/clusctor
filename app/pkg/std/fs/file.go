package fs

import (
	"io/ioutil"
)

func NewFileWriter() *FileWriter {
	return &FileWriter{}
}

type FileWriter struct {}

func (FileWriter) WriteFile(filePath string, data []byte) error {
	return ioutil.WriteFile(filePath, data, 0644)
}

func NewFileReader() *FileReader {
	return &FileReader{}
}

type FileReader struct {}

func (FileReader) ReadFile(filePath string) ([]byte, error) {
	return ioutil.ReadFile(filePath)
}