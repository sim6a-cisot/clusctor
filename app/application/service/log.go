package service

import (
	"clusctor/app/application/service/log"
	"fmt"
	"go.uber.org/zap"
)

type InfoLogger interface {
	Info(log.Entry)
}

type WarnLogger interface {
	Warn(log.Entry)
}

type Logger interface {
	InfoLogger
	WarnLogger
}

type ZapProvider interface {
	ProvideZap() *zap.Logger
}

func LogDebug(msg string) {
	fmt.Println("[DEBUG]", msg)
}