package file

import "path"

type Name string

func (fn Name) JSON() Name {
	return fn + ".json"
}

func (fn Name) HTML() Name {
	return fn + ".html"
}

func (fn Name) String() string {
	return string(fn)
}

type Path string

func NewPath() Path {
	return ""
}

func (p Path) Add(file Name) Path {
	return p.AddString(file.String())
}

func (p Path) AddString(s string) Path {
	return Path(path.Join(p.String(), s))
}

func (p Path) ToRelative() Path {
	return "./" + p
}

func (p Path) AddSlash() Path {
	return p + "/"
}

func (p Path) String() string {
	return string(p)
}
