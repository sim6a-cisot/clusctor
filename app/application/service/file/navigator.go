package file

import (
	"clusctor/app/pkg/std"
)

const (
	topologyGeneratorCmd   = Name("Generator.Terminal")
	topologyGeneratorDir = Name("topology-generator")
	topology   = Name("topology")
	topologies = Name("topologies")
	config     = Name("config")
	app        = Name("clusctor")
	swagger    = Name("swagger")
	index      = Name("index")
	clientApp  = app + "-view"
)

func NewNavigator(environment std.Environment) *Navigator {
	return &Navigator{
		environment: environment,
	}
}

type Navigator struct {
	environment std.Environment
}

func (n *Navigator) ConfigDirPath() Path {
	return NewPath().AddString(n.environment.UserHomeDir()).AddString("." + app.String())
}

func (n *Navigator) TopologyFilePath() Path {
	return n.ConfigDirPath().Add(topology.JSON())
}

func (n *Navigator) TopologiesFilePath() Path {
	return n.ConfigDirPath().Add(topologies.JSON())
}

func (n Navigator) ConfigFileName() Name {
	return config
}

func (n *Navigator) ConfigFilePath() Path {
	return n.ConfigDirPath().Add(config)
}

func (n Navigator) SwaggerFileName() Name {
	return swagger.JSON()
}

func (n Navigator) SwaggerFilePath() Path {
	return NewPath().Add(swagger.JSON()).ToRelative()
}

func (n Navigator) ClientAppIndexFilePath() Path {
	return n.ClientAppResourcesDirPath().Add(index.HTML()).ToRelative()
}

func (n Navigator) ClientAppResourcesDirPath() Path {
	return NewPath().Add(clientApp).ToRelative().AddSlash()
}

func (n Navigator) GraphGeneratorFilePath() Path {
	return NewPath().Add(topologyGeneratorDir).Add(topologyGeneratorCmd).ToRelative()
}