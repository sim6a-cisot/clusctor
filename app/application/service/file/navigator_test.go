package file_test

import (
	"clusctor/app/application/service/file"
	"github.com/stretchr/testify/assert"
	"testing"
)

type testEnvironment struct {}

func (testEnvironment) UserHomeDir() string {
	return "/home/user"
}

func TestNavigator_ClientAppIndexFilePath(t *testing.T) {
	navigator := file.NewNavigator(nil)
	want := file.Path("./clusctor-view/index.html")
	assert.Equal(t, want, navigator.ClientAppIndexFilePath())
}

func TestNavigator_ClientAppResourcesDirPath(t *testing.T) {
	navigator := file.NewNavigator(nil)
	want := file.Path("./clusctor-view/")
	assert.Equal(t, want, navigator.ClientAppResourcesDirPath())
}

func TestNavigator_ConfigDirPath(t *testing.T) {
	navigator := file.NewNavigator(&testEnvironment{})
	want := file.Path("/home/user/.clusctor")
	assert.Equal(t, want, navigator.ConfigDirPath())
}

func TestNavigator_ConfigFileName(t *testing.T) {
	navigator := file.NewNavigator(nil)
	want := file.Name("config")
	assert.Equal(t, want, navigator.ConfigFileName())
}

func TestNavigator_ConfigFilePath(t *testing.T) {
	navigator := file.NewNavigator(&testEnvironment{})
	want := file.Path("/home/user/.clusctor/config")
	assert.Equal(t, want, navigator.ConfigFilePath())
}

func TestNavigator_SwaggerFileName(t *testing.T) {
	navigator := file.NewNavigator(nil)
	want := file.Name("swagger.json")
	assert.Equal(t, want, navigator.SwaggerFileName())
}

func TestNavigator_SwaggerFilePath(t *testing.T) {
	navigator := file.NewNavigator(nil)
	want := file.Path("./swagger.json")
	assert.Equal(t, want, navigator.SwaggerFilePath())
}

func TestNavigator_TopologyFilePath(t *testing.T) {
	navigator := file.NewNavigator(&testEnvironment{})
	want := file.Path("/home/user/.clusctor/topology.json")
	assert.Equal(t, want, navigator.TopologyFilePath())
}

func TestNavigator_GraphGeneratorFilePath(t *testing.T) {
	navigator := file.NewNavigator(nil)
	want := file.Path("./topology-generator/Generator.Terminal")
	assert.Equal(t, want, navigator.GraphGeneratorFilePath())
}