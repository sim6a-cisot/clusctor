package service

import "clusctor/app/application/service/file"

type ConfigFileNavigator interface {
	ConfigFileName() file.Name
	ConfigDirPath() file.Path
	ConfigFilePath() file.Path
}

type FileServerNavigator interface {
	SwaggerFileName() file.Name
	SwaggerFilePath() file.Path
	ClientAppIndexFilePath() file.Path
	ClientAppResourcesDirPath() file.Path
}
