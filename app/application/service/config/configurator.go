package config

import (
	"clusctor/app/application/service"
	"clusctor/app/application/service/log"
	"clusctor/app/domain/model/scanning/schedule"
	"clusctor/app/domain/service/scanning/controller"
	"clusctor/app/interface/http"
	"github.com/spf13/viper"
)

const (
	httpServicePort         = "http.service.port"
	controllerName          = "controller.name"
	controllerHost          = "controller.host"
	controllerPort          = "controller.port"
	logLevel                = "log.level"
	logEncoding             = "log.encoding"
	logOutput               = "log.output"
	networkScanningInterval = "schedule.network-scanning-interval-sec"
)

type Configurator struct {
	viper *viper.Viper
}

func Read(fileConfig service.ConfigFileNavigator) (*Configurator, error) {
	configurator := &Configurator{
		viper: viper.New(),
	}
	configurator.setDefaultSettings()
	configurator.prepareViper(fileConfig)

	return configurator, configurator.readConfig(fileConfig)
}

func (c *Configurator) setDefaultSettings() {
	defaultSettings := map[string]interface{}{
		httpServicePort:         8080,
		controllerName:          "stub",
		logLevel:                "info",
		logEncoding:             "json",
		logOutput:               "stdout",
		networkScanningInterval: 5,
	}
	for option, value := range defaultSettings {
		c.viper.SetDefault(option, value)
	}
}

func (c *Configurator) prepareViper(fileConfig service.ConfigFileNavigator) {
	c.viper.SetConfigName(fileConfig.ConfigFileName().String())
	c.viper.AddConfigPath(fileConfig.ConfigDirPath().String())
}

func (c *Configurator) readConfig(fileConfig service.ConfigFileNavigator) error {
	if err := c.viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			if err = c.viper.WriteConfigAs(fileConfig.ConfigFilePath().String()); err != nil {
				return err
			}
		} else {
			return err
		}
	}
	return nil
}

func (c *Configurator) ServerConfig() http.ServerConfig {
	return http.ServerConfig{
		Port: c.viper.GetInt(httpServicePort),
	}
}

func (c *Configurator) ControllerConfig() controller.Config {
	return controller.Config{
		ControllerName: c.viper.GetString(controllerName),
		Host:           c.viper.GetString(controllerHost),
		Port:           c.viper.GetInt(controllerPort),
	}
}

func (c *Configurator) SchedulerConfig() schedule.Config {
	return schedule.Config{
		NetworkScanningIntervalSec: c.viper.GetInt(networkScanningInterval),
	}
}

func (c *Configurator) LoggerConfig() log.Config {
	return log.Config{
		Level:    c.viper.GetString(logLevel),
		Encoding: c.viper.GetString(logEncoding),
		Output:   c.viper.GetString(logOutput),
	}
}
