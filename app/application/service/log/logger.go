package log

import (
	"go.uber.org/zap"
)

const (
	detailsField = "details"
	tookField    = "took"
)

func zapFields(e Entry) []zap.Field {
	fields := make([]zap.Field, 0, 2)
	if e.Details != "" {
		fields = append(fields, zap.Any(detailsField, e.Details))
	}
	if e.Took != "" {
		fields = append(fields, zap.String(tookField, e.Took))
	}
	return fields
}

type Logger struct {
	zapLogger *zap.Logger
}

func (l *Logger) Info(e Entry) {
	l.zapLogger.Info(e.Msg, zapFields(e)...)
}

func (l *Logger) Warn(e Entry) {
	l.zapLogger.Warn(e.Msg, zapFields(e)...)
}

func (l *Logger) ProvideZap() *zap.Logger {
	return l.zapLogger
}
