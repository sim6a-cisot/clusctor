package log

type Entry struct {
	Msg     string
	Details string
	Took    string
}

