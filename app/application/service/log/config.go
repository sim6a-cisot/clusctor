package log

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type Config struct {
	Level    string
	Encoding string
	Output   string
}

func zapLevel(cfg Config) zapcore.Level {
	level := zapcore.InfoLevel
	switch cfg.Level {
	case "debug":
		level = zapcore.DebugLevel
	case "warn":
		level = zapcore.WarnLevel
	case "info":
		level = zapcore.InfoLevel
	}
	return level
}

func zapEncoding(cfg Config) string {
	console := "console"
	json := "json"

	if cfg.Encoding != console && cfg.Encoding != json {
		return console
	}
	return cfg.Encoding
}

func NewLogger(cfg Config) *Logger {
	zapCfg := zap.NewProductionConfig()
	zapCfg.Encoding = zapEncoding(cfg)
	zapCfg.Level = zap.NewAtomicLevelAt(zapLevel(cfg))
	zapCfg.OutputPaths = []string{cfg.Output}

	zapCfg.EncoderConfig.MessageKey = "msg"
	zapCfg.EncoderConfig.LevelKey = "level"
	zapCfg.EncoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder
	zapCfg.EncoderConfig.TimeKey =  "time"
	zapCfg.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder

	zapLogger, _ := zapCfg.Build()
	logger := &Logger{
		zapLogger: zapLogger,
	}
	return logger
}
