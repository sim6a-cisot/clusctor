package api

// @title SDN Cluster Constructor API
// @version 1.0
// @description External SDN application for optimization of a network management.

// @host localhost:8080
// @BasePath /api/v1
