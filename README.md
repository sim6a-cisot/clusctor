# SDN Cluster Constructor
<img align="right" src="https://raw.githubusercontent.com/sim6a/clusctor/master/logo.png">

SDN Cluster Constructor is an external SDN application for such SDN controllers as HP SDN VAN Controller, Floodlight and POX. SDN Cluster Constructor optimises a network management by using clusters (or segments) - groups of the OpenFlow switches.

### Algorithm tests
All algorithm illustrations in the `/test` folder made with yEd Graph Editor (https://www.yworks.com/products/yed).