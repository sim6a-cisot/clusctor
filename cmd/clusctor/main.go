package main

import (
	configMod "clusctor/app/application/service/config"
	fileMod "clusctor/app/application/service/file"
	loggingMod "clusctor/app/application/service/log"
	"clusctor/app/domain/model/management/metric"
	"clusctor/app/domain/model/management/path"
	"clusctor/app/domain/model/management/topology"
	"clusctor/app/domain/model/scanning/schedule"
	analysisSvcMod "clusctor/app/domain/service/analysis/analyzer"
	bisectionSvcMod "clusctor/app/domain/service/management/bisection"
	dijkstraSvcMod "clusctor/app/domain/service/management/dijkstra"
	girvanNewmanSvcMod "clusctor/app/domain/service/management/girvannewman"
	primSvcMod "clusctor/app/domain/service/management/prim"
	segmentationSvcMod "clusctor/app/domain/service/management/segmentation"
	sliceSvcMod "clusctor/app/domain/service/management/slice"
	topologySvcMod "clusctor/app/domain/service/management/topology"
	treeSvcMod "clusctor/app/domain/service/management/tree"
	"clusctor/app/domain/service/scanning/controller"
	topologyJSON "clusctor/app/infrastructure/analysis/topology"
	virtualControllerIteratorMod "clusctor/app/infrastructure/analysis/virtualctrl/iterator"
	conrollerFactory "clusctor/app/infrastructure/scanning/controller"
	schedulerSvcMod "clusctor/app/infrastructure/scanning/scheduler"
	"clusctor/app/infrastructure/stats"
	httpMod "clusctor/app/interface/http"
	bisectionHndlrMod "clusctor/app/interface/http/handler/bisection"
	dijkstraHndlrMod "clusctor/app/interface/http/handler/dijkstra"
	errorsHndlrMod "clusctor/app/interface/http/handler/errors"
	fileHndlrMod "clusctor/app/interface/http/handler/file"
	girvanNewmanHndlrMod "clusctor/app/interface/http/handler/girvannewman"
	linkHndrMod "clusctor/app/interface/http/handler/link"
	metricHndlrMod "clusctor/app/interface/http/handler/metric"
	pathHndlrMod "clusctor/app/interface/http/handler/path"
	primHndlrMod "clusctor/app/interface/http/handler/prim"
	segmentHndlrMod "clusctor/app/interface/http/handler/segment"
	segmentationHndlrMod "clusctor/app/interface/http/handler/segmentation"
	sliceHndlrMod "clusctor/app/interface/http/handler/slice"
	swaggerHndlrMod "clusctor/app/interface/http/handler/swagger"
	topologyHndlrMod "clusctor/app/interface/http/handler/topology"
	treeHndlrMod "clusctor/app/interface/http/handler/tree"
	httpRouter "clusctor/app/interface/http/router"
	stdcmd "clusctor/app/pkg/std/cmd"
	stdenv "clusctor/app/pkg/std/env"
	stdfs "clusctor/app/pkg/std/fs"
	stdhttp "clusctor/app/pkg/std/http"
	stdtime "clusctor/app/pkg/std/time"
	"fmt"
	"time"
)

/*
	import shortnames:
	+ Svc (service)
	+ Mod (module)
	+ Hndlr (http handler)
	+ std (standard library wrappers)
*/

const appName = "SDN Cluster Service"

func main() {
	fmt.Println("*** " + appName + " started at " + time.Now().String() + " ***")
	if err := runApp(); err != nil {
		fmt.Print(err)
	}
	fmt.Println("*** " + appName + " stopped at " + time.Now().String() + " ***")
}

func runApp() error {
	environmentProvider := stdenv.NewEnvironment()
	fileNavigator := fileMod.NewNavigator(environmentProvider)
	standardHttpClient := stdhttp.NewClient()
	timeProvider := stdtime.New()
	cmdRunner := stdcmd.NewRunner()
	fileReader := stdfs.NewFileReader()
	fileWriter := stdfs.NewFileWriter()
	configurator, err := configMod.Read(fileNavigator)
	if err != nil {
		return err
	}
	logger := loggingMod.NewLogger(configurator.LoggerConfig())
	logger.Info(loggingMod.Entry{
		Msg: "[logger module] created",
	})
	logger.Info(loggingMod.Entry{
		Msg: "[std wrapper module] created",
	})
	logger.Info(loggingMod.Entry{
		Msg: "[configurator module] created",
	})
	logger.Info(loggingMod.Entry{
		Msg: "[configurator module] service configuration read",
	})

	metricInfo := metric.NewInfo()
	topologyModel := topology.New(metricInfo)
	logger.Info(loggingMod.Entry{
		Msg: "[topology model] created",
	})

	topologyFileStorage := topologyJSON.New(fileNavigator.TopologyFilePath(),
		fileNavigator.TopologiesFilePath(), fileReader, fileWriter)
	logger.Info(loggingMod.Entry{
		Msg: "[topology dao (as file storage) module] created",
	})

	sdnControllerProvider := conrollerFactory.New(configurator.ControllerConfig(), standardHttpClient, timeProvider, topologyFileStorage, logger)
	networkScanner := controller.DecorateNetworkScannerWithLogger(sdnControllerProvider, logger)
	flowPusher := controller.DecorateFlowPusherWithLogger(sdnControllerProvider, logger)
	logger.Info(loggingMod.Entry{
		Msg: "[sdn controller provider] created",
	})

	statisticsCache := stats.NewCache()
	logger.Info(loggingMod.Entry{
		Msg: "[statistics cache] created",
	})

	analysisService := constructAnalyzer(topologyFileStorage)
	logger.Info(loggingMod.Entry{
		Msg: "[analysis service] created",
	})

	treeService := treeSvcMod.NewService(topologyModel)
	logger.Info(loggingMod.Entry{
		Msg: "[tree service] created",
	})

	httpResponseWriter := httpMod.NewResponseWriter(logger)
	logger.Info(loggingMod.Entry{
		Msg: "[http response writer] created",
	})

	treeHandler := treeHndlrMod.NewHandler(treeService, httpResponseWriter)
	logger.Info(loggingMod.Entry{
		Msg: "[tree http handler] created",
	})

	pathFactory := path.NewFactory(
		flowPusher,
		sdnControllerProvider,
		topologyModel.IPToMACTranslator(),
		topologyModel.HostConnector(),
		topologyModel.LinkConnector(),
		topologyModel.SwitchConnector(),
		treeService,
	)
	logger.Info(loggingMod.Entry{
		Msg: "[path module] created",
	})

	pathHandler := pathHndlrMod.NewHandler(pathFactory, httpResponseWriter)
	logger.Info(loggingMod.Entry{
		Msg: "[path http handler] created",
	})

	segmentHandler := segmentHndlrMod.NewHandler(topologyModel, httpResponseWriter)
	logger.Info(loggingMod.Entry{
		Msg: "[segment http handler] created",
	})

	bisectionService := bisectionSvcMod.NewRunner(topologyModel, statisticsCache)
	logger.Info(loggingMod.Entry{
		Msg: "[bisection service] created",
	})

	bisectionHandler := bisectionHndlrMod.NewHandler(bisectionService, httpResponseWriter, statisticsCache, analysisService)
	logger.Info(loggingMod.Entry{
		Msg: "[bisection http handler] created",
	})

	topologyService := topologySvcMod.NewService(topologyModel, networkScanner, topologyFileStorage)
	logger.Info(loggingMod.Entry{
		Msg: "[topology service] created",
	})

	topologyGenerator := topologyJSON.NewGenerator(fileNavigator, cmdRunner, fileWriter)
	logger.Info(loggingMod.Entry{
		Msg: "[topology generator module] created",
	})

	segmentationService := segmentationSvcMod.NewRunner(topologyModel, statisticsCache, topologyModel)
	logger.Info(loggingMod.Entry{
		Msg: "[segmentation service] created",
	})

	segmentationHandler := segmentationHndlrMod.NewHandler(
		segmentationService,
		httpResponseWriter,
		statisticsCache,
		analysisService,
	)
	logger.Info(loggingMod.Entry{
		Msg: "[segmentation http handler] created",
	})

	topologyHandler := topologyHndlrMod.NewHandler(
		topologyService,
		topologyService,
		httpResponseWriter,
		topologyGenerator,
		topologyModel,
		analysisService,
	)
	logger.Info(loggingMod.Entry{
		Msg: "[topology http handler] created",
	})

	primService := primSvcMod.NewRunner(topologyModel, topologyModel.LinkConnector(), statisticsCache)
	logger.Info(loggingMod.Entry{
		Msg: "[prim service] created",
	})

	primHandler := primHndlrMod.NewHandler(primService, treeService, httpResponseWriter, statisticsCache)
	logger.Info(loggingMod.Entry{
		Msg: "[prim http handler] created",
	})

	linkHandler := linkHndrMod.NewHandler(topologyModel.LinkConnector(), httpResponseWriter)
	logger.Info(loggingMod.Entry{
		Msg: "[link http handler] created",
	})

	metricHandler := metricHndlrMod.NewHandler(metricInfo, httpResponseWriter)
	logger.Info(loggingMod.Entry{
		Msg: "[metric http handler] created",
	})

	dijkstraService := dijkstraSvcMod.NewRunner(
		topologyModel,
		topologyModel.HostConnector(),
		topologyModel.LinkConnector(),
		treeService,
	)
	logger.Info(loggingMod.Entry{
		Msg: "[dijkstra service] created",
	})

	dijkstraHandler := dijkstraHndlrMod.NewHandler(
		dijkstraService,
		httpResponseWriter,
		pathFactory,
		topologyModel,
	)
	logger.Info(loggingMod.Entry{
		Msg: "[dijkstra http handler] created",
	})

	fileHandler := fileHndlrMod.NewHandler(fileNavigator)
	logger.Info(loggingMod.Entry{
		Msg: "[file http handler] created",
	})

	sliceRegistry := sliceSvcMod.NewRegistry()
	logger.Info(loggingMod.Entry{
		Msg: "[slice registry] created",
	})

	sliceService := sliceSvcMod.NewService(
		sliceRegistry,
		topologyModel,
		statisticsCache,
		pathFactory,
	)
	logger.Info(loggingMod.Entry{
		Msg: "[slice service] created",
	})

	sliceHandler := sliceHndlrMod.NewHandler(httpResponseWriter, sliceService, sliceService, statisticsCache)
	logger.Info(loggingMod.Entry{
		Msg: "[slice http handler] created",
	})

	girvanNewmanService := girvanNewmanSvcMod.NewRunner(topologyModel, topologyModel, statisticsCache)
	logger.Info(loggingMod.Entry{
		Msg: "[girvan newman service] created",
	})

	girvanNewmanHandler := girvanNewmanHndlrMod.NewHandler(girvanNewmanService, statisticsCache, httpResponseWriter, analysisService)
	logger.Info(loggingMod.Entry{
		Msg: "[girvan newman http handler] created",
	})

	schedulerSvcMod.Run(schedule.NewSchedule(topologyService, configurator.SchedulerConfig()))
	logger.Info(loggingMod.Entry{
		Msg: "[scheduler module] ran",
	})

	errorCatcher := errorsHndlrMod.NewHandler(httpResponseWriter)
	r := httpRouter.NewRouter(
		swaggerHndlrMod.NewHandler(),
		errorCatcher,
		pathHandler,
		fileHandler,
		logger,
		segmentationHandler,
		segmentHandler,
		bisectionHandler,
		girvanNewmanHandler,
		topologyHandler,
		treeHandler,
		primHandler,
		linkHandler,
		metricHandler,
		dijkstraHandler,
		sliceHandler,
	)
	logger.Info(loggingMod.Entry{
		Msg: "[http router] created",
	})

	logger.Info(loggingMod.Entry{
		Msg: "[http server] ran",
	})
	return httpMod.RunServer(configurator.ServerConfig(), r)
}

func constructAnalyzer(topoDAO *topologyJSON.JSONReader) *analysisSvcMod.Analyzer {
	statisticsCache := stats.NewCache()
	topologyModel := topology.New(metric.NewInfo())
	segmentationService := segmentationSvcMod.NewRunner(topologyModel, statisticsCache, topologyModel)
	bisectionService := bisectionSvcMod.NewRunner(topologyModel, statisticsCache)
	girvanNewmanService := girvanNewmanSvcMod.NewRunner(topologyModel, topologyModel, statisticsCache)
	iteratorController := virtualControllerIteratorMod.New(topoDAO)
	return analysisSvcMod.NewAnalyzer(
		segmentationService,
		bisectionService,
		girvanNewmanService,
		iteratorController,
		statisticsCache,
		topologyModel,
	)
}
