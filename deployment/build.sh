#!/bin/bash

CLUSCTOR_VIEW_DIST_DIR="../clusctor-view/dist"
CLUSCTOR_VIEW_BUILD_DIR="./build/clusctor-view"
TOPO_GENERATOR_DIST_DIR="../TopologyGenerator/Generator.Terminal/bin/Release/netcoreapp3.0"
TOPO_GENERATOR_BUILD_DIR="./build/topology-generator"

function copy_from_dist_to_build() {
  DIST_DIR=$1
  BUILD_DIR=$2
  if [ -d "$DIST_DIR" ]; then
    cp -r "$DIST_DIR" "$BUILD_DIR"
  else
    echo "$DIST_DIR does not exist"
  fi
}

make
copy_from_dist_to_build $CLUSCTOR_VIEW_DIST_DIR $CLUSCTOR_VIEW_BUILD_DIR
copy_from_dist_to_build $TOPO_GENERATOR_DIST_DIR $TOPO_GENERATOR_BUILD_DIR