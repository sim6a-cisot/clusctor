module clusctor

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/gin-contrib/zap v0.0.0-20190528085758-3cc18cd8fce3
	github.com/gin-gonic/gin v1.4.0
	github.com/go-openapi/spec v0.19.9 // indirect
	github.com/go-openapi/swag v0.19.9 // indirect
	github.com/jasonlvhit/gocron v0.0.0-20190528055910-f996a843b48d
	github.com/json-iterator/go v1.1.6
	github.com/mailru/easyjson v0.7.6 // indirect
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337
	github.com/spf13/viper v1.4.0
	github.com/stretchr/testify v1.4.0
	github.com/swaggo/files v0.0.0-20190704085106-630677cd5c14
	github.com/swaggo/gin-swagger v1.2.0
	github.com/swaggo/swag v1.6.7
	go.uber.org/zap v1.10.0
	golang.org/x/net v0.0.0-20200904194848-62affa334b73 // indirect
	golang.org/x/tools v0.0.0-20200909210914-44a2922940c2 // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
)

go 1.13
