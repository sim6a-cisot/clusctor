.PHONY: all
all: clean build gen-docs

.PHONY: build
build:
	GOOS=linux GOARCH=amd64 CGO_ENABLED=0 \
	go build \
	-ldflags "-s -w" \
	-o build/clusctor \
	cmd/clusctor/main.go

.PHONY: run-all-tests
run-all-tests:
	go test ./... -race -v -tags=integration -coverprofile=coverage.out

.PHONY: clean
clean:
	rm -rf build
	rm -rf docs
	rm -f coverage.out

.PHONY: gen-docs
gen-docs:
	swag init -g api/description.go -o docs
	cp docs/swagger.json build/swagger.json